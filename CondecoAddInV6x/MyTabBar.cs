using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using AddinExpress.OL;
  
namespace CondecoAddinV2
{
    public partial class MyTabBar : AddinExpress.OL.ADXOlForm
    {
        ////const int WM_DWMCOMPOSITIONCHANGED = 798;
        ////const int WM_DWMNCRENDERINGCHANGED = 799;
        ////const int WM_DWMCOLORIZATIONCOLORCHANGED = 800;
        ////const int WM_DWMWINDOWMAXIMIZEDCHANGE = 801;

        ////protected override void WndProc(ref Message m)
        ////{
        ////    if (m.Msg == WM_DWMCOLORIZATIONCOLORCHANGED)
        ////    {
        ////        MessageBox.Show("Color Changed");
        ////    }

        ////    base.WndProc(ref m);
        ////}

        public MyTabBar()
        {
            InitializeComponent();
        }


        void MyTabBar_Resize(object sender, System.EventArgs e)
        {
            this.panel1.Location = new Point(0, 0);
            this.panel1.Size = new Size(this.Width, this.Height - webBrowserControl.Top);

            webBrowserControl.Location = panel1.Location;
            webBrowserControl.Size = panel1.Size;
            //MessageBox.Show(webBrowserControl.Version.ToString());

            picLoader.Location = panel1.Location;
            picLoader.Size = panel1.Size;

            ProgressText.Location = panel1.Location;
            ProgressText.Size = panel1.Size;

        }

    }
}
