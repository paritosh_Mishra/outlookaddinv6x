using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Security.Policy;
using Outlook = Microsoft.Office.Interop.Outlook;
using Microsoft.Win32;
using System.Reflection;
using CondecoAddinV2.Repository;
using CondecoAddinV2.App_Resources;
using CondecoAddinV2.Constants;
using System.Collections.Generic;
using System.IO;

namespace CondecoAddinV2
{
    /// <summary>
    /// Summary description for BookingManager.
    /// </summary>
    public class BookingManager : Form //: AddinExpress.OL.ADXOlForm
    {
        public delegate void CollapseHandler(object sender);
        public event CollapseHandler Collapse;
        private  bool IsBookingValidationFail ;
        public bool RoomGridClickStopBrowserEvent { get; set; }

        private System.ComponentModel.IContainer components = null;
      //  private static AppointmentHelper AppointmentHelper = new AppointmentHelper();
       // private static BookingHelper bookingHelper = new BookingHelper();
       // private static SyncManager syncManager = new SyncManager();

        //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
        private static bool IsLoaderHide = false;

  //Added by Ravi Goyal to fix Recurring series new,edit,delete email issues.
      private bool IsSeriesEdited = false;
	    //For V6
        private bool IsRoomBookingClick = false;

        private string currentBookingData = string.Empty;
       // bool minimizedForm = true;
        bool bookingSaved = false;
        bool IsValidationValid = true;
       // bool condition = true;
        
		object OutlookAppObj = null;
        private WebBrowser condecoBrowser;
        object AddinModule = null;
        PictureBox loaderPicture = null;
        private Label LoaderProgress;
		private bool isPartialBooking;
        public BookingManager(WebBrowser browserObject, object OutlookAppObj, object AddinModule, PictureBox loaderPicture ,Label LoaderProgress, bool blnRoomBooking =false,Object CheckGridobj =null)
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            condecoBrowser = browserObject;
            condecoBrowser.Visible = true;
            this.condecoBrowser.Navigated += new WebBrowserNavigatedEventHandler(condecoBrowser_Navigated);
            this.condecoBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(condecoBrowser_DocumentCompleted);
            this.condecoBrowser.ProgressChanged += new WebBrowserProgressChangedEventHandler(condecoBrowser_ProgressChanged);
           // this.condecoBrowser.Navigating += new WebBrowserNavigatingEventHandler(condecoBrowser_Navigating);
            
            this.OutlookAppObj = OutlookAppObj;
            this.AddinModule = AddinModule;
            this.loaderPicture = loaderPicture;
            this.LoaderProgress = LoaderProgress;

            this.IsRoomBookingClick = blnRoomBooking;
            this.IsBookingValidationFail = false;
            if (CheckGridobj != null)
            {
                CheckRoomGrid obj = CheckGridobj as CheckRoomGrid;
                if (obj != null)
                {
                    //WebBrowser browserControl = ((CheckRoomGrid)obj).Controls.Find("gridBrowser", true).Length == 1 ? (WebBrowser)((CheckRoomGrid)obj).Controls.Find("gridBrowser", true)[0] : null;
                    //if (browserControl != null)
                    //{
                        //browserControl.Navigated -= ((CheckRoomGrid)obj).gridBrowser_Navigated;
                        //browserControl.DocumentCompleted -= ((CheckRoomGrid)obj).gridBrowser_DocumentCompleted;
                        //browserControl.ProgressChanged -= ((CheckRoomGrid)obj).gridBrowser_ProgressChanged;
                        //browserControl = null;
                        //obj.DisposeMethod(true);
                        obj.RoomBookingClickStopBrowserEvent = true;
                        if(obj!=null)
                        obj = null;

                   // }
                }
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.condecoBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // condecoBrowser
            // 
            this.condecoBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.condecoBrowser.Location = new System.Drawing.Point(0, 0);
            this.condecoBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.condecoBrowser.Name = "condecoBrowser";
            this.condecoBrowser.Size = new System.Drawing.Size(975, 400);
            this.condecoBrowser.TabIndex = 5;
           
            // 
            // BookingManager
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(975, 400);
            this.Controls.Add(this.condecoBrowser);
            this.Name = "BookingManager";
            this.Text = "Room Booking";
            this.ResumeLayout(false);

        }

        void condecoBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
          
        }


        void condecoBrowser_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            if (RoomGridClickStopBrowserEvent) return;
           
            // !IsLoaderHide  Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
            if (e.CurrentProgress != e.MaximumProgress && e.MaximumProgress >= 0 && e.CurrentProgress >= 0 && !IsLoaderHide)
            {
                UtilityManager.LogMessage("LoaderPicture True --condecoBrowser.Url " + condecoBrowser.Url);
                SetLoader(true);
                //this.loaderPicture.Size = new Size(64, 64);
                //this.loaderPicture.Location = new Point((condecoBrowser.Width / 2) - (loaderPicture.Width / 2), (condecoBrowser.Height / 2) - (loaderPicture.Height / 2));
                //this.loaderPicture.Visible = true;


                //this.LoaderProgress.Size = new Size(64, 64);
                //this.LoaderProgress.Location = new Point((condecoBrowser.Width / 2) - (loaderPicture.Width / 2) - (this.LoaderProgress.Size.Width / 2), (condecoBrowser.Height / 2) + (loaderPicture.Height / 2));
                //this.LoaderProgress.Visible = true;

            }
            else
            {
                //Tp# 21514 fixed by paritosh loader not visible 
               
                if(condecoBrowser.ReadyState ==WebBrowserReadyState.Complete)
                {
                    UtilityManager.LogMessage("LoaderPicture False --condecoBrowser.Url " + condecoBrowser.Url);
                    SetLoader(false);
                 }
               
                //this.condecoBrowser.Visible = true;
            }
        }
        #endregion

        #region BookingManager Form Methods

        //private void BookingManager_ADXBeforeFormShow()
        //{

        //    this.Visible = appHelper.IsCondecoRegionVisible(this.InspectorObj as Outlook.Inspector);

        //}

        private void BookingManager_ADXSelectionChange()
        {
            //this.condecoBrowser.Url = new Uri(this.GetNewBookingPath());
            //if (minimizedForm)
            //{
            //    RegionState = AddinExpress.OL.ADXRegionState.Minimized;


            //}
            //else
            //{
            //    RegionState = AddinExpress.OL.ADXRegionState.Normal;
            //}
            // minimizedForm = !minimizedForm;
        }

        public void BookingManager_Activated(object sender, EventArgs e)
        {
            UtilityManager.LogMessage(" Start BookingManager_Activated SetLoader(true);");
            SetLoader(true);
            if (!UtilityManager.IsCondecoContactable())
            {
                string strHtmlBuffer = global::CondecoAddinV2.App_Resources.CondecoResources.Offline;
               // strHtmlBuffer = strHtmlBuffer.Replace("#INSTALLDIR#", Assembly.GetExecutingAssembly().CodeBase.Substring(0, Assembly.GetExecutingAssembly().CodeBase.LastIndexOf("/")) + "//");
                strHtmlBuffer = strHtmlBuffer.Replace("#INSTALLDIR#", Assembly.GetExecutingAssembly().CodeBase.Substring(0, Assembly.GetExecutingAssembly().CodeBase.LastIndexOf("/")) );
                //Added by Paritosh .Fix issue #16519 of TP ,added multilingual msg 
                strHtmlBuffer = strHtmlBuffer.Replace("#OfflineMsg#" ,global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_Offline_Message);
                // 
                this.condecoBrowser.DocumentText = strHtmlBuffer;
            }
            else
            {
               
                //this.loaderPicture.Size = new Size(64, 64);
                //this.loaderPicture.Location = new Point((condecoBrowser.Width / 2) - (loaderPicture.Width / 2), (condecoBrowser.Height / 2) - (loaderPicture.Height / 2));
                //this.loaderPicture.Visible = true;
                //this.LoaderProgress.Size = new Size(64, 64);

                //this.LoaderProgress.Location = new Point((condecoBrowser.Width / 2) - (loaderPicture.Width / 2) - (this.LoaderProgress.Size.Width/2 ), (condecoBrowser.Height / 2) + (loaderPicture.Height / 2));
                //this.LoaderProgress.Visible = true;
                if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                {
                    if (!UtilityManager.IsSSOTimeOutMsgShow)
                    {

                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    }
                   //UtilityManager.IsSSOTimeOutMsgShow = false;
                    if (!condecoBrowser.IsDisposed)
                    {
                        condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    }
                    return;
                }
                UtilityManager.LogMessage(" Start  this.PostBookingData();");
                string result = this.PostBookingData();
                if (result == "-1") IsBookingValidationFail = true;
                UtilityManager.LogMessage(" End this.PostBookingData();");
            }
            //http://localhost/login/login.asp?thisU=waqask&outlookPostID=1010&outlook=1&edit=1&OutlookEditType=&x=1&OutlookGrid=&BookingID=
        } 

        #endregion

        #region Condeco Methods

        private Uri GetNewBookingPath(string postID, Outlook.AppointmentItem appItem)
        {
            UriBuilder uriBuilder = new UriBuilder();
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();

                //StringBuilder newpath = new StringBuilder();
                string currentUser = UtilityManager.GetCurrentUserName();

                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;
                string port = "";
                string currentHost = UtilityManager.HostName; ;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
               /* newpath.Append("thisU=" + currentUser);
                
                newpath.Append("&outlookPostID=" + postID);
                newpath.Append("&outlook=1&edit=");
                newpath.Append("&OutlookEditType=" + appHelper.GetAppointmentBookingType(appItem));
                newpath.Append("&x=1&OutlookGrid=&BookingID=");
                if (UtilityManager.ConnectionMode() == 1)
                {
                    newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                }
                newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());*/
               
                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, "", -1, -1, "", "", AppointmentHelper.GetAppointmentBookingType(appItem), "", "", "", "");// newpath.ToString();
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Exception recorded in Method GetNewBookingPath()"+ex.Message);
            }

            Uri cURI = uriBuilder.Uri;
            return cURI;
        }

        private Uri GetExistingBookingPath(string postID, Outlook.AppointmentItem appItem,bool isPartialBooking = false)
        {
            // iLoginPath & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "
            //&OutlookEditType="&iOutlookEditType&"&From="&iFrom&"&To="&iTo&"&BookingID="&iBookingID)    
            UriBuilder uriBuilder = new UriBuilder();
            IbookingHelper bookingHelper = new BookingHelper();
            Uri cURI = null;
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();
                DateTime startDate = appItem.Start;
                DateTime endDate = appItem.End;
                //StringBuilder newpath = new StringBuilder();
                // check if Single Occcurrence Got a Condeco Reference or not
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false, isPartialBooking);
                if (currMeeting.BookingID == 0)
                {
                    cURI = GetNewBookingPath(postID, appItem);
                    return cURI;
                }
                //EN-9312
                //if (currMeeting.DeleteBooking)
                //{
                //    cURI = GetNewBookingPath(postID, appItem);
                //    return cURI;
                //}
                //EN-9312
                string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;

                string port = "";
                string currentHost = UtilityManager.HostName; ;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;

                ////long meetingTicks = currMeeting.DateFrom.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                ////startDate = new DateTime(meetingTicks);
                ////meetingTicks = currMeeting.DateTo.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                ////endDate = new DateTime(meetingTicks);
                //march2016
               // uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, "", -1, -1, "", "", AppointmentHelper.GetAppointmentBookingType(appItem), "1", startDate.ToString(dateFormat), endDate.ToString(dateFormat), currMeeting.BookingID.ToString());// newpath.ToString();
                uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, "", -1, -1, "", "", AppointmentHelper.GetAppointmentBookingType(appItem), "1", UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat), UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat), currMeeting.BookingID.ToString());// newpath.ToString();
            }
            catch (System.Exception)
            {
            }
            cURI = uriBuilder.Uri;
            bookingHelper = null;
            return cURI;
        }

        private string PostBookingData()
        {
            string postID = "";
            Outlook.AppointmentItem appItem = null;
            IbookingHelper bookingHelper = new BookingHelper();
            try
            {
               
                UtilityManager.LogMessage("BookingManager -PostBookingData");
                appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);

                if (appItem == null) return string.Empty;

                if (AppointmentHelper.SyncIsWebInitiated(appItem))
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.Sync_WebInitatedBooking);
                    condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    return "-1";
                }

                if (UtilityManager.ConnectionMode() == 1)
                {
                    UtilityManager.LogMessage("--PostBookingData(); --UtilityManager.ConnectionMode() " + UtilityManager.ConnectionMode());
                    //ADDED BY CHANDRA FOR CASE WHERE USER IS NOT ENTERED IN TO CONDECO PART IN fORM aUTHENTICATION
                    if (UtilityManager.UserName.Trim().Equals(""))
                    {
                        if (UtilityManager.GetOutlookVersion(this.OutlookAppObj) == 12)
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2007);
                        }
                        else
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2010_2013);
                        }
                        condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                        return "-1";
                    }
                    //END CHANDRA
                    else if (!UtilityManager.IsCurrentUserValid)
                    {
                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized_RoomBooking);
                        condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                        return "-1";
                    }
                }
                // Bug 6309:Exchange initiated booking: Alert message is not displayed on clicking "Room Grid" and "Room Booking" button from Add-in V6 
                if (UtilityManager.SyncBookingEnabled().Equals(1) && appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                {
                    if (AppointmentHelper.SyncIsMeetingSyncCondeco(appItem.Resources) && !AppointmentHelper.IsSyncAppointment(appItem))
                    {
                        UtilityManager.ShowErrorMessage(CondecoResources.SyncBookingValidation);
                       // UtilityManager.ShowErrorMessage(CondecoResources.Sync_BookingNotSyncToCondeco);
                        condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                        return "-1";
                    }
                }
                //   [SYNC].[usp_IsExchangeResourceMappedWithCondeco] postID

                ////moved here so will set the booking id when clicking on room booking only.
                //bookingHelper.AddCondecoReference(appItem);

                //check for past booking
                if (appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
                {
                    DateTime? origStart = AppointmentDataInfo.GetOriginalStartDate(appItem);
                    DateTime? origEnddate = AppointmentDataInfo.GetOriginalEndDate(appItem);
                    if (origStart != null && origStart < DateTime.Now && !string.IsNullOrEmpty(appItem.Location) && !appItem.Location.ToLower().Contains("cancelled"))
                    {
                        //show message that updated is not allowed
                        if (origStart == DateTime.MinValue)
                        {
                            UtilityManager.ShowErrorMessage(CondecoResources.ObjectIssue_NeedToReopen);
                            condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                            return "-1";
                        }
                        // Below End date  check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                        else if (origEnddate < DateTime.Now)
                        {
                            UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                            condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                            IsValidationValid = false;
                            return "-1";
                        }
                    }
                }
                
                //end

                AppointmentDataInfo.SetSkipWriteCheck(appItem, true);
                //changed by Anand on 27_Feb_2013 for TP11988/TP12075
                bool saveNotRequired = (this.AddinModule as AddinModule).isItemNotInSync;
                postID = AppointmentHelper.GetAppointmentPostID(appItem);
                if (UtilityManager.SyncBookingEnabled().Equals(1) && (string.IsNullOrEmpty(postID) || (AppointmentHelper.IsSyncAppointment(appItem))))
                {
                    postID = AppointmentHelper.SyncGetAppointmentPostID(appItem);
                }
               
                if (!string.IsNullOrEmpty(postID))
                {
                   // int bookingId = bookingHelper.GetBookingID(postID, appItem);
                   int bookingId = 0;
                   bool edited = false;
                   try
                   {
                       bookingId = AppointmentDataInfo.GetBookingId(appItem);
                       if (bookingId == 0 && appItem.Location != CondecoResources.Appointment_Initial_Location)
                       {
                           IbookingHelper bookHelper = new BookingHelper();
                           CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
                           if (!currMeeting.DeleteBooking)
                           {
                               bookingId = currMeeting.BookingID;
                           }
                           if (bookingId == 0)
                           {
                               bookingId = bookingHelper.GetBookingID(postID, appItem);
                           }
                           if (bookingId > 0 )
                           {
                               edited = true;
                           }
                       }
                   }
                   catch { }
                    if (bookingId == 0){ 
                        isPartialBooking = true;
                    }
                    
                   
                    if (bookingId > 0 &&
                        (AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem)))
                    {
                        saveNotRequired = true;
                    }

                    //Added by Ravi Goyal to fix Recurring series new,edit,delete email issues.
                    if (bookingId > 0 && (AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem) || AppointmentDataInfo.GetSubjectChanged(appItem)))
                    {
                        IsSeriesEdited = true;
                    }
                    if (edited && appItem.RecurrenceState==Outlook.OlRecurrenceState.olApptMaster && appItem.IsRecurring)
                    {
                        IsSeriesEdited = true;
                    }
                    //
                }
                UtilityManager.LogMessage("--PostBookingData(); --ValidationManager.ValidateAppointment(appItem, saveNotRequired) " );
                bool rtnVal = ValidationManager.ValidateAppointment(appItem, saveNotRequired);
                //end
                AppointmentDataInfo.SetSkipWriteCheck(appItem, false);
                if (!rtnVal)
                {
                    IsValidationValid = false;
                    condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    return "-1";
                }
                (this.AddinModule as AddinModule).isItemNotInSync = false;
                bool isSeriesBookingPartialpastAwaited = false;

                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && (string.IsNullOrEmpty(postID) ||isPartialBooking))
                {
                    isSeriesBookingPartialpastAwaited = true;
                    DateTime currentDateTime = DateTime.Now;
                    Outlook.RecurrencePattern recPattern = null;
                    recPattern = appItem.GetRecurrencePattern();
                    long meetingTicks = recPattern.PatternStartDate.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                    DateTime currMeetingItemStart = new DateTime(meetingTicks);
                    if (currMeetingItemStart <= currentDateTime)
                    {
                        UtilityManager.ShowWarningMessage(CondecoResources.V62SeriesPastNotchanges);
                        
                    }
                    UtilityManager.FreeCOMObject(recPattern);
                }

                if (AppointmentHelper.IsNewAppointment(appItem) )
                {
                    UtilityManager.LogMessage("--PostBookingData(); --appHelper.IsNewAppointment-- going to be created PostID ");
                    postID = bookingHelper.HandelNewBookingRequest(appItem);
                    UtilityManager.LogMessage("--PostBookingData(); --appHelper.IsNewAppointment--  PostID  created");
                  
                    this.condecoBrowser.Url = this.GetNewBookingPath(postID, appItem);
                    UtilityManager.LogMessage("--PostBookingData(); --this.condecoBrowser.Url --  " + this.condecoBrowser.Url);
                    if (string.IsNullOrEmpty(appItem.Location))
                        appItem.Location = CondecoResources.Appointment_Initial_Location;
                }
                else
                {
                    if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && isSeriesBookingPartialpastAwaited)
                    {
                        postID = bookingHelper.HandelNewBookingRequest(appItem);
                    }
                    else
                    {
                        postID = bookingHelper.HandleExistingBookingRquest(appItem, isPartialBooking);
                    }
                    UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.BookRoomClicked, Convert.ToString("True"));
                    //Added by paritosh on 10/oct 2013 to fix TP issue #18159
                    if (!isPartialBooking && IsCheckBusinessHour(appItem))
                    {
                        this.condecoBrowser.Url = null;
                        bookingSaved = false;
                        IsValidationValid = false;
                        Collapse(this);
                        SetLoader(false);
                        return "-1";
                    }
                    else
                    {
                        this.condecoBrowser.Url = this.GetExistingBookingPath(postID, appItem, isPartialBooking);
                        AppointmentDataInfo.SetAppItemCondecoBookingOpen(appItem, true);
                    }
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Exception recorded in Method BookingManager.PostBookingData()" + ex.Message);
                return "-1";
            }
            finally
            {
                UtilityManager.FreeCOMObject(appItem);
            }
            bookingHelper = null;
            return postID;
        }
       
        private bool IsCheckBusinessHour(Outlook.AppointmentItem appItem)
        {
            Cursor.Current = Cursors.WaitCursor;
            IbookingHelper bookingHelper = new BookingHelper();
            CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
            int result = 1;
            // Below appItem.End check to fix the CRRI2 6550 ,CRI26043,TP-19968 by Paritosh 
            long meetingTicks1 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
            //GetOriginalStartDate
            DateTime meetingStartDateTime1 = new DateTime(meetingTicks1);
            if (appItem.Start.CompareTo(appItem.End) == 0)
            {
                result = -2;
            }
            //TP-21251 by Travendra
            else if (DateTime.Now.CompareTo(appItem.Start) >= 0 && DateTime.Now.CompareTo(appItem.End) > 0 && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                result = -200;
            }
            //24/jan/2004
            else if (meetingStartDateTime1.CompareTo(DateTime.MinValue) != 0 && appItem.Start.CompareTo(meetingStartDateTime1) != 0 && (meetingStartDateTime1.CompareTo(DateTime.Now) < 0) && (appItem.Start.CompareTo(meetingStartDateTime1) < 0) && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                // MessageBox.Show("date change to past");
                result = -4;
            }
            // Below appItem.End check to fix the CRRI 6550 ,TP-19968 by Paritosh 
            else if (DateTime.Now.CompareTo(appItem.Start) >= 0 && DateTime.Now.CompareTo(appItem.End) > 0 && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                result = -3;
            }
            else if (appItem.AllDayEvent)
            {
                result = -400;
            }
            else if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && appItem.Start.CompareTo(DateTime.Now) < 0 && !UtilityManager.IsSeriesSaveAllowed())
            {
                // MessageBox.Show(CondecoResources.Recurrence_Series_PastSave, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                result = -5;
            }
            else
            {
               // UtilityManager.SyncBookingEnabled().Equals(0) && 
               if (!AppointmentHelper.IsSyncAppointment(appItem)  && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
                {
                   
                result = UtilityManager.BookingAdvanceChecking(currMeeting, appItem);
                }
            }
            bool blnSave = true;
            string message = "";
            if (result == -999 || result == -200 || result == -400 || result == -81 || result == -82 || result == -83 || result == -84 || result == -100 || result == -4 || result == -2 || result == -3 || result == -5 || result == -85 || result == -86)
            {
                if (result == -81)
                    message = CondecoResources.Group_SingleDayBookingOnly;
                else if (result == -82)
                    message = CondecoResources.Group_BusinessHourOverLap;
                else if (result == -83)
                    message = CondecoResources.Group_AdvancedBookingPeriod;
                else if (result == -84)
                    message = CondecoResources.Group_NoticeRequired;
                else if (result == -200)
                {
                    message = CondecoResources.Booking_Moved_Past;
                    blnSave = false;
                }
                else if (result == -400)
                    message = CondecoResources.AllDayEvent_NotSupported;
                else if (result == -999) //added by Anand for permission verification
                {
                  //  message = CondecoResources.Check_Booking_Permission;
                }
                else if (result == -2)
                    message = CondecoResources.MeetingDate_Start_End_Same;
                else if (result == -3)
                    message = CondecoResources.Booking_Update_Past;
                else if (result == -4)
                {
                    message = CondecoResources.Booking_Moved_Past;
                }
                else if (result == -85 && appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && appItem.Start.CompareTo(DateTime.Now) < 0 && !UtilityManager.IsSeriesSaveAllowed())
                {
                    message = CondecoResources.BookingIsInProgress;
                }
                else if (result == -86)
                {
                    message = CondecoResources.AppointmentIsAlreadyClosed;
                }
                else if (result == -5)
                {
                   message = CondecoResources.Recurrence_Series_PastSave;
                }
                else if (result == -100)
                {
                }
                UtilityManager.LogMessage("BookingManager IsCheckBusinessHour(Outlook.AppointmentItem appItem)  result ==" + result);
            }
            bookingHelper = null;
            Cursor.Current = Cursors.Default;
            if (!string.IsNullOrEmpty(message))
            {
                UtilityManager.ShowErrorMessage(message);
               // MessageBox.Show(message, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                try
                {
                    //below lines are Added by Paritosh To fix CRI2-5814 on 23/Jan/2014
                    if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                       // SyncManager sync = new SyncManager();
                        Outlook.RecurrencePattern rpItem = appItem.GetRecurrencePattern();
                        long meetingTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                        DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
                        ////DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                        ////                                                                             currMeeting.LocationID,
                        ////                                                                             currMeeting.OriginalTZ);

                        DateTime condecoSeriesStartTimeInLocalTz = currMeeting.BookingDateStartToLocalTZ;
                        if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
                        {
                            rpItem.StartTime = condecoSeriesStartTimeInLocalTz;     
                        }

                        meetingTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                        DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
                        ////DateTime condecoSeriesEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesEnd,
                        ////                                                                             currMeeting.LocationID,
                        ////                                                                             currMeeting.OriginalTZ);
                        DateTime condecoSeriesEndTimeInLocalTz = currMeeting.BookingDateEndToLocalTZ;
                        if (condecoSeriesEndTimeInLocalTz.TimeOfDay != rpItem.EndTime.TimeOfDay)
                        {
                            rpItem.EndTime = condecoSeriesEndTimeInLocalTz;
                            
                        }
                        if (blnSave)
                        {
                            appItem.Subject = currMeeting.MeetingTitle;
                            appItem.Save();
                        }
                        UtilityManager.FreeCOMObject(rpItem);
                        UtilityManager.FreeCOMObject(appItem);
                    }
                    else 
                    {
                        UndoSingleDayBooking( appItem);

////////////                    long meetingTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
////////////                    DateTime meetingStartDateTime = new DateTime(meetingTicks);

/////////////* Bug 7411:Add-in V6.0.6721/Win7/Outlook 2010- Wrong time coming on appointment window after business hours alert for different time zone  */
////////////                    //DateTime meetingStartDateTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime,
////////////                    //                                                                              currMeeting.LocationID,
////////////                    //                                                                              currMeeting.OriginalTZ);

////////////                    DateTime meetingStartDateTimeInLocalTz = currMeeting.BookingDateStartToLocalTZ;


////////////                    ////meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
////////////                    ////DateTime meetingEndDateTime = new DateTime(meetingTicks);

////////////                    ////DateTime meetingEndDateTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime,
////////////                    ////                                                                            currMeeting.LocationID,
////////////                    ////                                                                            currMeeting.OriginalTZ);
////////////                    DateTime meetingEndDateTimeInLocalTz = currMeeting.BookingDateEndToLocalTZ;
////////////                    appItem.Start = meetingStartDateTimeInLocalTz;
////////////                    appItem.End = meetingEndDateTimeInLocalTz;

////////////                    appItem.Subject = currMeeting.MeetingTitle;
////////////                    appItem.Save();
                    }
                    //Added by Paritosh by 23/Jan/2014
                    UtilityManager.FreeCOMObject(appItem);
                }
                catch(Exception ex)
                {
                    UtilityManager.LogMessage(" BookingManager IsCheckBusinessHour(Outlook.AppointmentItem appItem)  catch(Exception ex) ex="+ ex.Message);
                }
               
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        //private void BookingManager_ADXBeforeInspectorSubpaneClose()
        //{

        //}

        //private void BookingManager_ADXLeave(object sender, AddinExpress.OL.ADXLeaveEventArgs e)
        //{

        //}
        private void UndoRecurrenceBooking()
        {
            Outlook.AppointmentItem appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
            IbookingHelper bookingHelper = new BookingHelper();
            CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
            if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
            {
                // SyncManager sync = new SyncManager();
                Outlook.RecurrencePattern rpItem = appItem.GetRecurrencePattern();
                ////////long meetingTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                ////////DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
                ////////DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                ////////                                                                             currMeeting.LocationID,
                ////////                                                                             currMeeting.OriginalTZ);
                DateTime condecoSeriesStartTimeInLocalTz = currMeeting.BookingDateStartToLocalTZ;
                if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
                {
                    rpItem.StartTime = condecoSeriesStartTimeInLocalTz;
                }

                ////meetingTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                ////DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
                ////DateTime condecoSeriesEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesEnd,
                ////                                                                             currMeeting.LocationID,
                ////                                                                             currMeeting.OriginalTZ);
                DateTime condecoSeriesEndTimeInLocalTz = currMeeting.BookingDateEndToLocalTZ;
                if (condecoSeriesEndTimeInLocalTz.TimeOfDay != rpItem.EndTime.TimeOfDay)
                {
                    rpItem.EndTime = condecoSeriesEndTimeInLocalTz;

                }
                ////if (blnSave)
                ////{
                ////    appItem.Subject = currMeeting.MeetingTitle;
                ////    appItem.Save();
                ////}
                UtilityManager.FreeCOMObject(rpItem);
               
            }
            UtilityManager.FreeCOMObject(appItem);
        }

        private void UndoSingleDayBooking(Outlook.AppointmentItem appItem)
        {
            IbookingHelper bookingHelper = new BookingHelper();
            CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
            DateTime meetingStartDateTimeInLocalTz = currMeeting.BookingDateStartToLocalTZ;                                                    
            DateTime meetingEndDateTimeInLocalTz = currMeeting.BookingDateEndToLocalTZ;
            appItem.Start = meetingStartDateTimeInLocalTz;
            appItem.End = meetingEndDateTimeInLocalTz;
            appItem.Subject = currMeeting.MeetingTitle;
            appItem.Save();
            bookingHelper =null;
        }

        ~BookingManager()
        {
          //  Deactivate(); 
        }

        public new void Deactivate()
        {
            UtilityManager.LogMessage("BookingManager -new void Deactivate() before SetAppointmentLocation()");
            SetAppointmentLocation();
           

            if (this.condecoBrowser != null)
            {
                this.condecoBrowser.Navigated -= condecoBrowser_Navigated;
                this.condecoBrowser.DocumentCompleted -= condecoBrowser_DocumentCompleted;
                this.condecoBrowser.ProgressChanged -= condecoBrowser_ProgressChanged;
                this.condecoBrowser = null;
            }
           
           
           // this.condecoBrowser.Navigating -= condecoBrowser_Navigating;
            UtilityManager.LogMessage("BookingManager -new void Deactivate() End");
        }
        private void SetAppointmentLocation()
        {
            if (IsBookingValidationFail) return;
            try
            {
                string newLocSimple;
                IbookingHelper bookingHelper = new BookingHelper();
                UtilityManager.LogMessage("BookingManager -SetAppointmentLocation*****Start******");
                Outlook.AppointmentItem appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                if (appItem == null) return;
               // string newLocSimple = bookingHelper.GetBookingLocation(AppointmentHelper.GetAppointmentPostID(appItem), appItem);
                if (appItem.IsRecurring)
                {
                    newLocSimple = bookingHelper.GetBookingLocation(AppointmentHelper.GetAppointmentPostID(appItem), appItem, false);
                }
                else
                {
                    newLocSimple = bookingHelper.GetBookingLocation(AppointmentHelper.GetAppointmentPostID(appItem), appItem, true);
                }

               // string newLocSimple = bookingHelper.GetBookingLocation("", appItem);
                string newLoc = "";
                if (!AppointmentHelper.SyncIsOwaInitiatedBooking(appItem))
                {

                    newLoc = AppointmentHelper.FormatLocationInCondecoStyle(newLocSimple);
                }
                else
                {
                    newLoc = appItem.Location;
                }
                bool updateLoctaion = true;
                if (newLoc.ToLower().Contains("cancelled"))
                {
                    string NameValueLocSync = "";
                    NameValueLocSync = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                    if (!String.IsNullOrEmpty(NameValueLocSync))
                    {
                        updateLoctaion = false;
                    }
                }
                if (!String.IsNullOrEmpty(newLoc) && updateLoctaion)
                {//anand changed location to " " if new loc is not empty
                    if (appItem.Location == null) appItem.Location = " ";
                    if (!appItem.Location.ToLower().Contains(newLoc.ToLower()))
                    {
                        AppointmentHelper.SetAppointmentLocation(appItem, newLocSimple);
                        //UserProperties uProps = appItem.UserProperties;
                        //UserProperty prop = uProps.Find("AppointmentLocationSynced", true);
                        //if (prop != null)
                        //{
                        //    prop.Delete();
                        //}
                        //UtilityManager.FreeCOMObject(prop);
                        //UtilityManager.FreeCOMObject(uProps);
                        string namedValue = "";
                        namedValue = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                        if (!String.IsNullOrEmpty(namedValue))
                        {
                            UserPropertiesExtension.DeleteNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                        }
                        //////if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !bookingHelper.isCrossDayRecurranceBooking(appItem))
                        //////{
                        //////    //--start-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402-check for outlook 2013 version and this event will be created for outlook 2010 & above versions, not applicable for outlook 2007.
                        //////    //Added by Ravi Goyal to fix Recurring series new,edit,delete email issues.

                        //////    //////if ((AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem) || AppointmentDataInfo.GetSubjectChanged(appItem)))
                        //////    //////{
                        //////    //////    IsSeriesEdited = true;
                        //////    //////}
                        //////   ////////// IsyncManager syncManager = new SyncManager();
                        //////   ////////// //second parameter true means its  Condeco booking and  is  already checked 
                        //////   ////////////check26/10/15 syncManager.SyncIndiviudallyEditedItems(appItem,true);
                        //////   ////////// syncManager.SyncIndiviudallyEditedItems(appItem, true,this.AddinModule as AddinModule, IsSeriesEdited, false);
                        //////   ////////// syncManager = null;
                        //////}
                        UtilityManager.LogMessage("BookingManager.SetAppointmentLocation Appoitment location==  "+ appItem.Location);
                    }
                }
                //Added by Paritosh by 23/Jan/2014
                bookingHelper = null;
                UtilityManager.FreeCOMObject(appItem);
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("BookingManager.SetAppointmentLocation Catch " + ex.Message);
            }
        }

        private void condecoBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            condecoBrowser.Tag = "";
            if (RoomGridClickStopBrowserEvent) return;
            string pagename = UtilityManager.GetPageName(e.Url.ToString().ToLower());
            //Added calender.aspx to hide loader to fix #21918
            //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
            //if (e.Url.ToString().ToLower().Contains("condecogridcache/advancedrecurrenceroomselection.aspx") || e.Url.ToString().ToLower().Contains("ldap/condecouserlookup.asp?") || e.Url.ToString().ToLower().Contains("/calendar.aspx") || e.Url.ToString().ToLower().Contains("booking_resourceitem_addnote.asp") || e.Url.ToString().ToLower().Contains("funclinks/displayresourcedetails.asp"))
            if (pagename=="advancedrecurrenceroomselection.aspx" || pagename=="condecouserlookup.asp" || pagename=="calendar.aspx" || pagename=="booking_resourceitem_addnote.asp" || pagename=="displayresourcedetails.asp" || pagename=="condecouserlookup.aspx" ||  pagename=="booking_resourceitem_addnote.aspx" || pagename=="displayresourcedetails.aspx")
            {
                IsLoaderHide = true;
            }
            else
            {
                IsLoaderHide = false;
            }
            //

            if (bookingSaved || !IsValidationValid)
            {
                // UtilityManager.MinimizeAppointmentWindow(this.CurrentOutlookWindowHandle);
                bookingSaved = false;
                IsValidationValid = true;
                Collapse(this);
                SetLoader(false);

                //UtilityManager.CollapseCondecoRegion(this);
                //UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);


            }
            ////New 26Addins 
            //if (e.Url.ToString().ToLower().Contains("funclinks/bookingform.asp"))
            //{
            //    object obj = (this.AddinModule as AddinModule).FindRibbonControl("adxRibbonButton_28af3e5a-ad46-4552-a566-ebcf422c0185");
            //    AddinExpress.MSO.ADXRibbonButton ribbonGrid = obj as AddinExpress.MSO.ADXRibbonButton;
            //    if (ribbonGrid != null)
            //    {
            //       // ribbonGrid.Enabled = false;
                   
            //    }
            //}

        }

        private void condecoBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            
            //this.condecoBrowser.Visible = true;
            // MessageBox.Show("Navigated to:" + e.Url);
            condecoBrowser.Tag = "";
            if (RoomGridClickStopBrowserEvent) return;
            try
            {
                Uri curreURL = e.Url;
                if (curreURL == null) return;
                if (curreURL.Query == null) return;

                
                if(e.Url.ToString().ToLower().Contains("#defaultbookingperiod=0"))
                {
                    UndoRecurrenceBooking();
                    
                    IsLoaderHide = true;
                    Collapse(this);
                    SetLoader(false);
                    return;
                }
                string pagename = UtilityManager.GetPageName(e.Url.ToString().ToLower());
                //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
                if (pagename=="advancedrecurrenceroomselection.asp" || pagename=="condecouserlookup.asp" || pagename=="displayresourcedetails.asp"|| pagename=="advancedrecurrenceroomselection.aspx" || pagename=="condecouserlookup.aspx" || pagename=="displayresourcedetails.aspx")
                {
                    IsLoaderHide = true;
                }
                else
                {
                    IsLoaderHide = false;
                }
                //Added by Paritosh to fix #17745. 
                //if (e.Url.ToString().ToLower().Contains("/epdq/processmpipayment.asp?") || e.Url.ToString().ToLower().Contains("/epdq/processpayment.asp") || e.Url.ToString().ToLower().Contains("/epdq/processmpipayment.aspx?") || e.Url.ToString().ToLower().Contains("/epdq/processpayment.aspx") )
                if (pagename=="processmpipayment.asp" || pagename=="processpayment.asp" || pagename=="processmpipayment.asp" || pagename=="processpayment.asp" || pagename=="processmpipayment.aspx" || pagename=="processpayment.aspx" || pagename=="processmpipayment.aspx" || pagename=="processpayment.aspx")
                {
                    SetAppointmentLocation();

                    UtilityManager.LogMessage("BookingManager -condecoBrowser_Navigated");
                    Outlook.AppointmentItem appItemPay = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                    //Added by Paritosh to solve isssue TP #17571. 
                    if (appItemPay.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                    {
                        if (appItemPay != null)
                            appItemPay.Save();
                    }
                    //11/dec15 tobecheck
                    if (appItemPay.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                    {
                        //Added by Ravi Goyal to fix CRD-6520- Recurring series and edit an occurence for location only, this will allow appointment to reset on calendar.
                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 1** ");
                        appItemPay.Start = appItemPay.Start.AddMilliseconds(0.0000000001);
                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 1- added 0.0000000001 milliseconds to make it exception** ");
                       
                        if (appItemPay.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                        {
                            SetAppointmentLocation();
                            appItemPay.Save();
                        }

                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 1- SetAppointmentLocation() done** ");
                    }
                    if (appItemPay.IsRecurring)
                    {
                        IbookingHelper bookingHelper = new BookingHelper();

                        List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceBookingHasDST(appItemPay, false, true);
                        IsyncManager syncManager = new SyncManager();
                        if (appItemPay.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-series edited is set to: appItemPay " + IsSeriesEdited);
                            syncManager.SyncIndiviudallyEditedItems(appItemPay, false, this.AddinModule as AddinModule, IsSeriesEdited, false);
                            UtilityManager.LogMessage("**BookingManager.condecoBrowser_Navigated- appItemPaySyncIndiviudallyEditedItems completed**");
                        }
                        syncManager = null;
                    }
                    UtilityManager.FreeCOMObject(appItemPay);
                    
                }
                //

                if (curreURL.Query.ToLower().Contains("booking_saved") && IsValidationValid)
                {
                    UtilityManager.LogMessage("BookingManager - booking_saved  condecoBrowser_Navigated");
                    (this.AddinModule as AddinModule).IsMeetingSaveNCloseOnly = false;
                    Outlook.AppointmentItem appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                    if (appItem != null)
                    {
                        if (curreURL.Query.ToLower().Contains("cancel=yes"))
                        {
                            UndoSingleDayBooking(appItem);
                            IsLoaderHide = true;
                            Collapse(this);
                            SetLoader(false);
                            return;
                        }
                        else
                        {
                            if (appItem.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                            {
                                if (appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptException && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptOccurrence)
                                {
                                    try
                                    {
                                        appItem.Save();
                                    }
                                    catch
                                    {
                                        //Sometime while editing series that exception throw --Outlook Member not found. (Exception from HRESULT: 0x80020003 (DISP_E_MEMBERNOTFOUND))"
                                        // by calling again save method its save successfully .
                                        appItem.Save();
                                    }
                                }
                            }
                            AppointmentDataInfo.SetAppItemCondecoBookingOpen(appItem, true);
                            //below lines added by paritosh to fix chevron issue- some instance of recurrance have Day light saving 
                            if (appItem.IsRecurring)
                            {
                                IbookingHelper bookingHelper = new BookingHelper();
                                if (appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                                {
                                    (this.AddinModule as AddinModule).IsMeetingSaveFromWeb = true;
                                }
                                List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceBookingHasDST(appItem, false, true);
                                IsyncManager syncManager = new SyncManager();
                                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                                {
                                    UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-series edited is set to:" + IsSeriesEdited);
                                    syncManager.SyncIndiviudallyEditedItems(appItem, false, this.AddinModule as AddinModule, IsSeriesEdited, false);

                                    UtilityManager.LogMessage("**BookingManager.condecoBrowser_Navigated-SyncIndiviudallyEditedItems completed**");
                                }
                                syncManager = null;
                            }
                            ////////foreach (CondecoMeeting cMeeting in currMeeting)
                            ////////{
                            ////////    syncManager.SyncSeriesBookingHasDST(appItem, cMeeting);
                            ////////}

                            ////////////if (appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                            ////////////{

                            ////////////    IsyncManager syncManager1 = new SyncManager();
                            ////////////    System.Collections.Generic.List<CondecoMeeting> currMeeting1 = bookingHelper.GetRecurranceMeetingEditedOnly(appItem);
                            ////////////    Outlook.RecurrencePattern recPattern = appItem.GetRecurrencePattern();
                            ////////////    syncManager1.HanldeIndiviudalyEditedItems(appItem, recPattern, currMeeting);
                            ////////////    UtilityManager.FreeCOMObject(recPattern);
                            ////////////    bookingHelper = null;
                            ////////////    syncManager = null;
                            ////////////}

                            //////////////AppointmentDataInfo.SetIsOnlySeriesLocationUpdated(appItem, false);
                            //////////////if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !IsSeriesEdited)
                            //////////////{
                            //////////////    if (appItem.Location.Contains(UtilityManager.LocationStart) || appItem.Location.Contains(UtilityManager.LocationEnd))
                            //////////////    {
                            //////////////        //  UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated - Existing condeco booking identified as locatioin have '" + UtilityManager.LocationEndDelimiter + "' & '" + UtilityManager.GetLocationEndDelimiter() + "'");
                            //////////////        IsSeriesEdited = true;
                            //////////////        CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
                            //////////////        string MeetingLocation = AppointmentHelper.FormatLocationInCondecoStyle(curMeeting.MainLocation);
                            //////////////        //if (appItem.Location != MeetingLocation && appItem.Location != "Awaiting room booking location")
                            //////////////        if (appItem.Location != MeetingLocation && appItem.Location != CondecoAddinV2.App_Resources.CondecoResources.Appointment_Initial_Location)
                            //////////////        {
                            //////////////            AppointmentDataInfo.SetIsOnlySeriesLocationUpdated(appItem, true);
                            //////////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated - Location change identified in recurring series");
                            //////////////        }

                            //////////////    }
                            //////////////    else if (appItem.Location == CondecoResources.Appointment_Initial_Location)
                            //////////////    {
                            //////////////        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated - New Booking Identified because location is " + CondecoResources.Appointment_Initial_Location);
                            //////////////        //new booking, set IsSeriesEdited=false.
                            //////////////        IsSeriesEdited = false;
                            //////////////    }
                            //////////////}
                            //Added by Ravi Goyal to fix Recurring edit issues.
                            ////////if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && (AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem) || AppointmentDataInfo.GetSubjectChanged(appItem) ))
                            ////////{
                            ////////        IsSeriesEdited = true;
                            ////////        if(AppointmentDataInfo.GetIsOnlySeriesLocationUpdated(appItem))
                            ////////        {
                            ////////            AppointmentDataInfo.SetIsOnlySeriesLocationUpdated(appItem, false);
                            ////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-Along with location Following changes are identified in booking");
                            ////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: StartDate:-" + AppointmentDataInfo.GetStartDateChanged(appItem));
                            ////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: EndDate:-" + AppointmentDataInfo.GetEndDateChanged(appItem));
                            ////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: EndDate:-" + AppointmentDataInfo.GetSubjectChanged(appItem));
                            ////////        }
                            ////////        else
                            ////////        {
                            ////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-location is not changed, Following changes are identified in booking");
                            ////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: StartDate:-" + AppointmentDataInfo.GetStartDateChanged(appItem));
                            ////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: EndDate:-" + AppointmentDataInfo.GetEndDateChanged(appItem));
                            ////////            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: EndDate:-" + AppointmentDataInfo.GetSubjectChanged(appItem));
                            ////////        }
                            ////////}
                            //////////Added by Ravi Goyal to fix CRD-6520- Recurring series and edit an occurence for location only, this will allow appointment to reset on calendar.
                            ////////if (!IsSeriesEdited && appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException && (appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting || appItem.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting))
                            ////////{
                            ////////    UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated ---*CRD-6520 Check point 3* ");
                            //////// //   appItem.Start = appItem.Start.AddMilliseconds(0.0000000001);
                            ////////    UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated ---*CRD-6520 Check point 3*- added 0.0000000001 milliseconds to make it exception** ");

                            ////////}
                            ////////else
                            ////////{
                            ////////    if(AppointmentDataInfo.GetIsItemNonMeeting(appItem) && appItem.MeetingStatus==Outlook.OlMeetingStatus.olMeeting)
                            ////////    {
                            ////////        IsSeriesEdited = true;
                            ////////        AppointmentDataInfo.SetIsOnlySeriesLocationUpdated(appItem, true);
                            ////////        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-No Changes in Appointment series, Only switched from NonMeeting to Meeting");
                            ////////    }
                            //////////////////////////  if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                            //////////////////////////    {
                            //////////////////////////        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-series edited is set to:" + IsSeriesEdited);
                            //////////////////////////        syncManager.SyncIndiviudallyEditedItems(appItem, false, this.AddinModule as AddinModule, IsSeriesEdited, false);

                            //////////////////////////        UtilityManager.LogMessage("**BookingManager.condecoBrowser_Navigated-SyncIndiviudallyEditedItems completed**");
                            //////////////////////////    }
                            //////////////////////////    syncManager = null;
                            //////////////////////////}
                            /// bookingHelper = null;

                            // }
                            ////--End-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402



                            //if (appItem.MeetingStatus != Outlook.OlMeetingStatus.olMeeting )
                            //{
                            //    if (appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptException && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptOccurrence)
                            //    {
                            //        appItem.Save();
                            //    }
                            //}


                            //UtilityManager.CollapseCondecoRegion(this);

                            //Added by Paritosh to solve isssue TP #16913

                            //////if (curreURL.Query.ToLower().Contains("booking_saved"))
                            //////{

                            //////    UtilityManager.LogMessage("BookingManager TP #16913 - booking_saved  condecoBrowser_Navigated");
                            //////  ////  Outlook.AppointmentItem appItemTemp = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                            //////    //Added by Paritosh to solve isssue TP #17571. 
                            //////    //if (appItemTemp.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                            //////    //{
                            //////     ////  if (appItemTemp != null)
                            //////    /// {
                            //////        ////    appItemTemp.Save();
                            //////            ////Anurag : below line added to save changes from saveandclose outlook button and also to syncronise booking between web and outlook-addin
                            //////            //(this.AddinModule as AddinModule).IsMeetingSaveFromWeb = false;
                            //////     //// }
                            //////    //////}

                            //////    ////UtilityManager.FreeCOMObject(appItemTemp);
                            //////}
                            //end 
                        }
                    }


                    AppointmentDataInfo.SetAppItemSkipSync(appItem, false);
                    (this.AddinModule as AddinModule).IsUndoMeetingChanges = false;
                    //Added for  PRB0043359 and PRB0043845
                    UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.CondecoBookingID, AppointmentDataInfo.GetBookingId(appItem).ToString());
                    //End for  PRB0043359 and PRB0043845
                    AppointmentDataInfo.ResetProperties(appItem);
                    IsValidationValid = true;
                    AppointmentDataInfo.SetAppItemCondecoBookingOpen(appItem, false);
					Collapse(this);
                    SetLoader(false);
                    UtilityManager.FreeCOMObject(appItem);

                }
            }
            catch (System.Exception)
            {
            }
        }

        private void SetLoader(bool isVisible)
        {
            if (isVisible)
            {
                this.LoaderProgress.Text = CondecoResources.ProgressText;
                this.loaderPicture.Size = new Size(64, 64);
                this.loaderPicture.Location = new Point((condecoBrowser.Width / 2) - (loaderPicture.Width / 2), (condecoBrowser.Height / 2) - (loaderPicture.Height / 2));
                this.loaderPicture.Visible = isVisible;

                this.LoaderProgress.Size = new Size(64, 64);
                this.LoaderProgress.Location = new Point((condecoBrowser.Width / 2) - (loaderPicture.Width / 2) - (this.LoaderProgress.Size.Width / 2), (condecoBrowser.Height / 2) + (loaderPicture.Height / 2));
                this.LoaderProgress.Visible = isVisible;
            }
            else
            {
                this.loaderPicture.Visible = isVisible;
                this.LoaderProgress.Visible = isVisible;
            }
        }



        //private void BookingManager_ADXAfterFormHide(object sender, ADXAfterFormHideEventArgs e)
        //{


        //}

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    UtilityManager.CollapseCondecoRegion(this);
        //   // UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
        //}

        //private void btnRefresh_Click(object sender, EventArgs e)
        //{
        //    String loaderPath = "file://" + (new System.IO.FileInfo(Assembly.GetExecutingAssembly().Location)).Directory.ToString().Replace("\\","/");
        //    loaderPath += "/LoaderPage.html";
        //    //loaderPath = @"file:///D:/Projects/RNMDevelopment/CondecoAddin2013_POC/CondecoAddinV2/bin/Debug/LoaderPage.html";


    }
}

