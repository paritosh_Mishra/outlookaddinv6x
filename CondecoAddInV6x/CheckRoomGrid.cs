using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;
using CondecoAddinV2.App_Resources;
using System.Reflection;
using CondecoAddinV2.Constants;
using CondecoAddinV2.Repository;
using System.Collections.Generic;

namespace CondecoAddinV2
{
    /// <summary>
    /// Summary description for CheckRoomGrid.
    /// </summary>
    public class CheckRoomGrid : Form
    {

        public delegate void CollapseHandler(object sender);
        public event CollapseHandler Collapse;
        public bool RoomBookingClickStopBrowserEvent { get; set; }

        private WebBrowser gridBrowser;
        private System.ComponentModel.IContainer components = null;
        // private static AppointmentHelper appHelper = new AppointmentHelper();
        //private static BookingHelper bookingHelper = new BookingHelper();
        //private static SyncManager syncManager = new SyncManager();
        //private Button btnClose;

        bool IsValidationValid = true;
        bool bookingSaved = false;
        private string currentBookingData = string.Empty;
        object OutlookAppObj = null;
        object AddinModule = null;
        // object InspectorObj = null;
        PictureBox loaderPicture = null;
        Label LoaderProgress;
        //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
        private static bool IsLoaderHide = false;
        private bool isPartialBooking;
        public CheckRoomGrid(WebBrowser browserObject, object OutlookAppObj, object AddinModule, object InspectorObj, PictureBox loaderPicture, Label LoaderProgress, Object RoomBookingObj = null)
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            gridBrowser = browserObject;
            gridBrowser.Visible = true;
            this.gridBrowser.Navigated += new WebBrowserNavigatedEventHandler(gridBrowser_Navigated);
            this.gridBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(gridBrowser_DocumentCompleted);
            this.gridBrowser.ProgressChanged += new WebBrowserProgressChangedEventHandler(gridBrowser_ProgressChanged);

            this.OutlookAppObj = OutlookAppObj;
            this.AddinModule = AddinModule;
            this.loaderPicture = loaderPicture;
            this.LoaderProgress = LoaderProgress;
            //string iconpath = UtilityManager.IconPath + "\\availability.ico";
            //System.Drawing.Icon ico = new System.Drawing.Icon(iconpath);
            //this.Icon = ico;
            try
            {
                // TODO: Add any initialization after the InitializeComponent call
                if (RoomBookingObj != null)
                {
                    BookingManager obj = RoomBookingObj as BookingManager;
                    if (obj != null)
                    {
                        //WebBrowser browserControl = obj.Controls.Find("condecoBrowser", true).Length == 1 ? (WebBrowser)obj.Controls.Find("condecoBrowser", true)[0] : null;
                        //if (browserControl != null)
                        //{
                        //browserControl.Navigated -= ((BookingManager)obj).gridBrowser_Navigated;
                        //browserControl.DocumentCompleted -= ((BookingManager)obj).gridBrowser_DocumentCompleted;
                        //browserControl.ProgressChanged -= ((BookingManager)obj).gridBrowser_ProgressChanged;
                        //browserControl = null;
                        //obj.DisposeMethod(true);
                        obj.RoomGridClickStopBrowserEvent = true;
                        if (obj != null)
                            obj = null;

                        // }
                    }
                }
            }
            catch
            {

            }

        }



        public void gridBrowser_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {

            // !IsLoaderHide  Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
            if (RoomBookingClickStopBrowserEvent) return;
            if (e.CurrentProgress != e.MaximumProgress && e.MaximumProgress >= 0 && e.CurrentProgress >= 0 && !IsLoaderHide)
            {
                UtilityManager.LogMessage("True --gridBrowser.Url " + gridBrowser.Url);
                SetLoader(true);
                //this.loaderPicture.Size = new Size(64, 64);
                //this.loaderPicture.Location = new Point((gridBrowser.Width / 2) - (loaderPicture.Width / 2), (gridBrowser.Height / 2) - (loaderPicture.Height / 2));
                //this.loaderPicture.Visible = true; 
            }
            else
            {
                //Tp# 21514 fixed by paritosh loader not visible 
                if (gridBrowser.ReadyState == WebBrowserReadyState.Complete)
                {
                    UtilityManager.LogMessage("false --gridBrowser.Url " + gridBrowser.Url);
                    // this.loaderPicture.Visible = false;
                    SetLoader(false);
                }
                //this.condecoBrowser.Visible = true;
            }
        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code



        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckRoomGrid));
            this.gridBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // gridBrowser
            // 
            this.gridBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridBrowser.Location = new System.Drawing.Point(0, 0);
            this.gridBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.gridBrowser.Name = "gridBrowser";
            this.gridBrowser.Size = new System.Drawing.Size(975, 400);
            this.gridBrowser.TabIndex = 0;
            this.gridBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.gridBrowser_DocumentCompleted);
            this.gridBrowser.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.gridBrowser_Navigated);
            // 
            // CheckRoomGrid
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(975, 400);
            this.Controls.Add(this.gridBrowser);
            //this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CheckRoomGrid";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = App_Resources.CondecoResources.Check_Room;
            this.ResumeLayout(false);

        }
        #endregion
        public void DisposeMethod(bool disposing)
        {
            if (disposing)
            {
                gridBrowser.Navigated -= gridBrowser_Navigated;
                gridBrowser.DocumentCompleted -= gridBrowser_DocumentCompleted;
                gridBrowser.ProgressChanged -= gridBrowser_ProgressChanged;
                //foreach (var control in this.Controls)
                //{
                // (( WebBrowser)control).Dispose();
                // GC.SuppressFinalize(this);
                //}
                Dispose(disposing);
            }

        }
        public void CheckRoomGrid_Activated(object sender, EventArgs e)
        {
            SetLoader(true);
            Uri currentUri = new Uri(UtilityManager.GetOfflinePath());
            if (!UtilityManager.IsCondecoContactable())
            {
                //Added by Paritosh .Fix issue #16519 of TP ,added multilingual msg 
                string strHtmlBuffer = global::CondecoAddinV2.App_Resources.CondecoResources.Offline;
                //strHtmlBuffer = strHtmlBuffer.Replace("#INSTALLDIR#", Assembly.GetExecutingAssembly().CodeBase.Substring(0, Assembly.GetExecutingAssembly().CodeBase.LastIndexOf("/")) + "//");
                strHtmlBuffer = strHtmlBuffer.Replace("#INSTALLDIR#", Assembly.GetExecutingAssembly().CodeBase.Substring(0, Assembly.GetExecutingAssembly().CodeBase.LastIndexOf("/")));
                strHtmlBuffer = strHtmlBuffer.Replace("#OfflineMsg#", global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_Offline_Message);
                // 
                this.gridBrowser.DocumentText = strHtmlBuffer;
                //Below line is commneted by paritosh for solving #16519 of TP
                // this.gridBrowser.Url = new Uri(UtilityManager.GetOfflinePath());
            }
            else
            {
                //this.loaderPicture.Size = new Size(64, 64);
                //this.loaderPicture.Location = new Point((gridBrowser.Width / 2) - (loaderPicture.Width / 2), (gridBrowser.Height / 2) - (loaderPicture.Height / 2));
                //this.loaderPicture.Visible = true;

                if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                {
                    if (!UtilityManager.IsSSOTimeOutMsgShow)
                    {

                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    }
                    //UtilityManager.IsSSOTimeOutMsgShow = false;
                    if (!gridBrowser.IsDisposed)
                    {
                        this.gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    }
                    return;
                }
                Outlook.AppointmentItem appItem = null;
                appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                if (appItem == null) return;
                if (AppointmentHelper.SyncIsWebInitiated(appItem))
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.Sync_WebInitatedBooking);
                    this.gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    return;
                }
                // Bug 6309:Exchange initiated booking: Alert message is not displayed on clicking "Room Grid" and "Room Booking" button from Add-in V6 
                if (UtilityManager.SyncBookingEnabled().Equals(1) && appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                {
                    if (AppointmentHelper.SyncIsMeetingSyncCondeco(appItem.Resources) && !AppointmentHelper.IsSyncAppointment(appItem))
                    {
                        UtilityManager.ShowErrorMessage(CondecoResources.SyncBookingValidation);
                        // UtilityManager.ShowErrorMessage(CondecoResources.Sync_BookingNotSyncToCondeco);
                        this.gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                        return;
                    }
                }
                //march2016
                int bookingId = AppointmentDataInfo.GetBookingId(appItem);
                if (bookingId == 0)
                {
                    isPartialBooking = true;
                }
                if (UtilityManager.OutlookGridType() == 1)
                {

                    string result = this.PostBookingData();
                }
                else
                {
                    currentUri = this.GetOutlookRoomBookingGridPath();

                    if (currentUri != null)
                        this.gridBrowser.Url = currentUri;
                }
            }


        }

        private Uri GetOutlookRoomBookingGridPath()
        {
            // iLoginPath & "?thisU=" & thisUserName & "&outlookpostID=&outlook=1&edit=&OutlookEditType=&x=1
            //&OutlookGrid=1&MeetingDate=" & iFrom & "&GridBookingID=" & iGridBookingID & "&RoomID=" & iResourceID)    

            UriBuilder uriBuilder = new UriBuilder();
            Uri cURI = null;
            Outlook.AppointmentItem appItem = null;
            try
            {
                string host = UtilityManager.HostName;
                string loginPath = UtilityManager.GetLoginPath();

                // StringBuilder newpath = new StringBuilder();
                // string currentUser = UtilityManager.GetCurrentUserName();
                appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);

                if (appItem == null) return new Uri("");
                IbookingHelper bookingHelper = new BookingHelper();
                //  CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
                bookingHelper = null;
                string port = "";
                string hostCheck = host;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = host.Split(':');
                    host = hostData[0];
                    port = hostData[1];
                }

                string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
                //string timeFormat = "HH:mm";
                uriBuilder.Scheme = UtilityManager.HostScheme;
                uriBuilder.Host = host;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                /*newpath.Append("thisU=" + currentUser);
                if (UtilityManager.ConnectionMode() == 1)
                {
                    newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                }
                newpath.Append("&outlookPostID=" + "");
                newpath.Append("&outlook=1&edit=&x=1&OutlookEditType=&OutlookGrid=1");
                newpath.Append("&gridVersion=" + UtilityManager.OutlookGridVersion().ToString());
                */

                /*
                 * 
                 * Need to populate the booking and Room ID
                 * 
                 */
                string meetingDate = "";
                try
                {
                    if (currMeeting.DateFrom.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);

                        meetingDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToString(dateFormat);
                    }
                    else
                    {
                        if (appItem != null)
                            meetingDate = appItem.Start.ToString(dateFormat);
                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred GetOutlookRoomBookingGridPath MeetingDate:" + ex.Message + " " + ex.StackTrace);
                }
                /* newpath.Append("&MeetingDate=" + meetingDate);
                 newpath.Append("&RoomID=" + currMeeting.ResourceItemID);
                 if (currMeeting.BookingID > 0)
                     newpath.Append("&GridBookingID=" + currMeeting.BookingID);
                 // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID

                 newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());*/

                uriBuilder.Query = UtilityManager.GetQueryStringParameters("", meetingDate, currMeeting.ResourceItemID, currMeeting.BookingID, "1", "", "", "", "", "", "");// newpath.ToString();
                cURI = uriBuilder.Uri;
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred GetOutlookRoomBookingGridPath:" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(appItem);
            }

            return cURI;
        }

        private Uri GetOutlookAdvancedBookingGridPath()
        {
            // iLoginPath & "?thisU=" & thisUserName & "&outlookpostID=&outlook=1&edit=&OutlookEditType=&x=1
            //&OutlookGrid=1&MeetingDate=" & iFrom & "&GridBookingID=" & iGridBookingID & "&RoomID=" & iResourceID)    

            UriBuilder uriBuilder = new UriBuilder();
            Uri cURI = null;
            Outlook.AppointmentItem appItem = null;
            try
            {
                string host = UtilityManager.HostName;
                string loginPath = UtilityManager.GetLoginPath();

                // StringBuilder newpath = new StringBuilder();
                string currentUser = UtilityManager.GetCurrentUserName();
                appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);

                if (appItem == null) return new Uri("");
                IbookingHelper bookingHelper = new BookingHelper();
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
                bookingHelper = null;
                string port = "";
                string hostCheck = host;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = host.Split(':');
                    host = hostData[0];
                    port = hostData[1];
                }

                string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
                // string timeFormat = "HH:mm";
                uriBuilder.Scheme = UtilityManager.HostScheme;
                uriBuilder.Host = host;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;



                /*  newpath.Append("thisU=" + currentUser);
                  if (UtilityManager.ConnectionMode() == 1)
                  {
                      newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                  }
                  newpath.Append("&outlookPostID=" + "");
                  // Set OutlooGrid to 2 so that it is the request for advanced Grid
                  newpath.Append("&outlook=1&edit=&x=1&OutlookEditType=&OutlookGrid=2");
                  newpath.Append("&gridVersion=" + UtilityManager.OutlookGridVersion().ToString());
                  */

                /*
                 * 
                 * Need to populate the booking and Room ID
                 * 
                 */
                string meetingDate = "";
                try
                {
                    if (currMeeting.DateFrom.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);
                        meetingDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    }
                    else
                    {
                        if (appItem != null)
                        {

                            meetingDate = appItem.Start.ToUniversalTime().ToString();

                        }
                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred Advance Grid Meeting Start date:" + ex.Message + " " + ex.StackTrace);
                }
                string meetingEndDate = "";
                try
                {
                    if (currMeeting.DateTo.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateTo.Year, currMeeting.DateTo.Month, currMeeting.DateTo.Day, currMeeting.TimeTo.Hour, currMeeting.TimeTo.Minute, currMeeting.TimeTo.Second);

                        meetingEndDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    }
                    else
                    {
                        meetingEndDate = appItem.End.ToUniversalTime().ToString();

                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred AdvanceGrid Meeting End date:" + ex.Message + " " + ex.StackTrace);
                }

                /* newpath.Append("&MeetingDate=" + meetingDate);
                 newpath.Append("&MeetingEndDate=" + meetingEndDate);

                 newpath.Append("&RoomID=" + currMeeting.ResourceItemID);
                 if (currMeeting.BookingID > 0)
                     newpath.Append("&GridBookingID=" + currMeeting.BookingID);

                 newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());*/
                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = UtilityManager.GetQueryStringParameters("", meetingDate, currMeeting.ResourceItemID, currMeeting.BookingID, "2", meetingEndDate, "", "", "", "", ""); //newpath.ToString();
                cURI = uriBuilder.Uri;
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred Advance Grid Meeting Start date:" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(appItem);
            }

            return cURI;
        }

        public void gridBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (RoomBookingClickStopBrowserEvent) return;
            //  Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
            //Added calender.aspx to hide loader to fix #21918
            //if (e.Url.ToString().ToLower().Contains("condecogridcache/advancedrecurrenceroomselection.aspx") || e.Url.ToString().ToLower().Contains("ldap/condecouserlookup.asp?") || e.Url.ToString().ToLower().Contains("/calendar.aspx") || e.Url.ToString().ToLower().Contains("booking_resourceitem_addnote.asp") || e.Url.ToString().ToLower().Contains("funclinks/displayresourcedetails.asp"))
            //{
            //    IsLoaderHide = true;
            //}
            string pagename = UtilityManager.GetPageName(e.Url.ToString().ToLower());
            if (pagename == "advancedrecurrenceroomselection.aspx" || pagename == "condecouserlookup.asp" || pagename == "condecouserlookup.aspx" || pagename == "calendar.aspx" || pagename == "booking_resourceitem_addnote.asp" || pagename == "booking_resourceitem_addnote.aspx" || pagename == "displayresourcedetails.asp" || pagename == "displayresourcedetails.aspx")
            {
                IsLoaderHide = true;
            }
            else
            {
                IsLoaderHide = false;
            }
            if (bookingSaved || !IsValidationValid)
            {
                // UtilityManager.MinimizeAppointmentWindow(this.CurrentOutlookWindowHandle);
                bookingSaved = false;
                IsValidationValid = true;
                Collapse(this);
                SetLoader(false);
                // this.loaderPicture.Visible = false;
                //UtilityManager.CollapseCondecoRegion(this);
                //UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
            }
        }



        //private void btnClose_Click(object sender, EventArgs e)
        //{
        //    UtilityManager.CollapseCondecoRegion(this);
        //    UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
        //}

        ~CheckRoomGrid()
        {
            //   Deactivate();
        }

        public new void Deactivate()
        {
            SetAppointmentLocation();
            if (this.gridBrowser != null)
            {
                this.gridBrowser.Navigated -= gridBrowser_Navigated;
                this.gridBrowser.DocumentCompleted -= gridBrowser_DocumentCompleted;
                this.gridBrowser.ProgressChanged -= gridBrowser_ProgressChanged;
                // this.gridBrowser.Navigating -= gridBrowser_Navigating;
                this.gridBrowser = null;
            }
        }

        private void SetAppointmentLocation()
        {
            Outlook.AppointmentItem appItem = null;
            try
            {
                IbookingHelper bookingHelper = new BookingHelper();
                appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                if (appItem == null) return;
                //march2016
                string newLocSimple;
                //string newLocSimple = bookingHelper.GetBookingLocation("", appItem);
                if (appItem.IsRecurring)
                {
                    newLocSimple = bookingHelper.GetBookingLocation(AppointmentHelper.GetAppointmentPostID(appItem), appItem, false);
                }
                else
                {
                    newLocSimple = bookingHelper.GetBookingLocation(AppointmentHelper.GetAppointmentPostID(appItem), appItem, true);
                }

                string newLoc = "";
                if (!AppointmentHelper.SyncIsOwaInitiatedBooking(appItem))
                {
                    newLoc = AppointmentHelper.FormatLocationInCondecoStyle(newLocSimple);
                }
                else
                {
                    newLoc = appItem.Location;
                }
                bool updateLoctaion = true;
                if (newLoc.ToLower().Contains("cancelled"))
                {
                    //UserProperties uProps = appItem.UserProperties;
                    //UserProperty prop = uProps.Find("AppointmentLocationSynced", true);
                    //if (prop != null)
                    //{
                    //    updateLoctaion = false;


                    //}
                    //UtilityManager.FreeCOMObject(prop);
                    //UtilityManager.FreeCOMObject(uProps);
                    string namedValue = "";
                    namedValue = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                    if (!String.IsNullOrEmpty(namedValue))
                    {
                        updateLoctaion = false;
                    }
                }

                if (appItem.Location == null)
                    appItem.Location = " ";
                if (!String.IsNullOrEmpty(newLoc) && updateLoctaion)
                {
                    if (!appItem.Location.ToLower().Contains(newLoc.ToLower()))
                    {
                        //appItem.Location = newLoc;
                        AppointmentHelper.SetAppointmentLocation(appItem, newLocSimple);
                        string namedValue = "";
                        namedValue = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                        if (!String.IsNullOrEmpty(namedValue))
                        {
                            UserPropertiesExtension.DeleteNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                        }
                    }
                }
                //--start-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402
                ////////////if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !bookingHelper.isCrossDayRecurranceBooking(appItem))
                ////////////{
                ////////////    UtilityManager.LogMessage("CheckRoomGrid:SetAppointmentLocation- checking for Recurrence booking Exclusion rooms");
                ////////////    SyncManager sync = new SyncManager();
                ////////////    string postID = AppointmentHelper.GetAppointmentPostID(appItem);
                ////////////    bool IsSeriesEdited = false;
                ////////////    int bookingId = bookingHelper.GetBookingID(postID, appItem);
                ////////////    if (bookingId > 0 && (AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem) || AppointmentDataInfo.GetSubjectChanged(appItem)))
                ////////////    {
                ////////////        IsSeriesEdited = true;
                ////////////    }
                ////////////    sync.SyncIndiviudallyEditedItems(appItem,true, this.AddinModule as AddinModule,IsSeriesEdited,false);
                ////////////    sync = null;
                ////////////}
                //End added by Ravi Goyal
                ///////UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in CheckRoomGrid_Deactivate" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(appItem);
            }
        }
        //private void CheckRoomGrid_ADXBeforeFormShow()
        //{
        //    this.Visible = appHelper.IsCondecoRegionVisible(this.InspectorObj as Outlook.Inspector);
        //}

        private string PostBookingData()
        {
            string postID = "";
            int bookingId = 0;
            Outlook.AppointmentItem appItem = null;
            IbookingHelper bookingHelper = new BookingHelper();
            try
            {
                appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);

                if (appItem == null) return string.Empty;
                if (UtilityManager.ConnectionMode() == 1)
                {
                    //ADDED BY CHANDRA FOR CASE WHERE USER IS NOT ENTERED IN TO CONDECO PART IN fORM aUTHENTICATION
                    if (UtilityManager.UserName.Trim().Equals("") && UtilityManager.IsCondecoContactable())
                    {
                        if (UtilityManager.GetOutlookVersion(this.OutlookAppObj) == 12)
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2007);
                        }
                        else
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2010_2013);
                        }
                        gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                        return "-1";
                    }
                    //END cHANDRA
                    else if (!UtilityManager.IsCurrentUserValid)
                    {
                        UtilityManager.ShowErrorMessage(CondecoResources.User_Not_Authorized_RoomBooking);
                        gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());

                        return "-1";
                    }
                }

                //if (UtilityManager.SyncBookingEnabled().Equals(1) && !appHelper.IsSyncAppointment(appItem))
                //{
                if (!ValidationManager.ValidateAppointment(appItem, (this.AddinModule as AddinModule).isItemNotInSync))
                {
                    IsValidationValid = false;
                    this.gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());

                    return "-1";
                }
                // }
                (this.AddinModule as AddinModule).isItemNotInSync = false;
                bookingId = AppointmentDataInfo.GetBookingId(appItem);
                if (bookingId != 0)
                {
                    postID = AppointmentHelper.GetAppointmentPostID(appItem);
                    AppointmentDataInfo.SetAppItemCondecoBookingOpen(appItem, true);
                }

                ////else if (!string.IsNullOrEmpty(postID))
                //// {
                ////     postID = AppointmentHelper.GetAppointmentPostID(appItem);
                ////     bookingId = bookingHelper.GetBookingID(postID, appItem);
                ////     if (bookingId == 0) isPartialBooking = true;
                //// }

                bool isSeriesBookingPartialpastAwaited = false;

                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && (string.IsNullOrEmpty(postID) || isPartialBooking))
                {
                    isSeriesBookingPartialpastAwaited = true;
                    DateTime currentDateTime = DateTime.Now;
                    Outlook.RecurrencePattern recPattern = null;
                    recPattern = appItem.GetRecurrencePattern();
                    long meetingTicks = recPattern.PatternStartDate.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                    DateTime currMeetingItemStart = new DateTime(meetingTicks);
                    if (currMeetingItemStart <= currentDateTime)
                    {
                        UtilityManager.ShowWarningMessage(CondecoResources.V62SeriesPastNotchanges);
                    }
                    UtilityManager.FreeCOMObject(recPattern);
                }
                //if (string.IsNullOrEmpty(currentBookingData))
                //    currentBookingData = bookingHelper.GetCurrentBookingData(appItem);

                //if (UtilityManager.SyncBookingEnabled().Equals(1) && !appHelper.IsSyncAppointment(appItem))
                //{
                if (AppointmentHelper.IsNewAppointment(appItem))
                {
                    postID = bookingHelper.HandelNewBookingRequest(appItem);
                    this.gridBrowser.Url = this.GetNewBookingPath(postID, appItem);

                    if (string.IsNullOrEmpty(appItem.Location))
                        appItem.Location = CondecoResources.Appointment_Initial_Location;

                }
                else
                {
                    postID = AppointmentHelper.GetAppointmentPostID(appItem);
                    int bkId = 0;
                    if (!string.IsNullOrEmpty(postID))
                    {
                        try
                        {
                            bkId = AppointmentDataInfo.GetBookingId(appItem);
                            if (bkId == 0 && appItem.Location != CondecoResources.Appointment_Initial_Location)
                            {
                                IbookingHelper bookHelper = new BookingHelper();
                                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
                                if (!currMeeting.DeleteBooking)
                                {
                                    bkId = currMeeting.BookingID;
                                }
                                if (bkId == 0)
                                {
                                    bkId = bookingHelper.GetBookingID(postID, appItem);
                                }
                            }
                        }
                        catch { }
                    }
                    if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && isSeriesBookingPartialpastAwaited && bkId==0)
                    {
                        postID = bookingHelper.HandelNewBookingRequest(appItem);
                    }
                    else
                    {
                        postID = bookingHelper.HandleExistingBookingRquest(appItem, isPartialBooking);
                    }
                    //string currentURL = string.Empty;
                    //bool pageExist = false;
                    //bool reloadCondecoBooking = true;



                    //if (reloadCondecoBooking)
                    //{
                    this.gridBrowser.Url = this.GetExistingBookingPath(postID, appItem, isPartialBooking);

                    //}

                }
                //}
                //else
                //{
                //    postID = bookingHelper.HandleExistingBookingRquest(appItem);
                //    this.gridBrowser.Url = this.GetExistingBookingPath(postID, appItem);
                //}
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in PostBookingData" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                bookingHelper = null;
                UtilityManager.FreeCOMObject(appItem);
            }
            return postID;
        }

        private Uri GetNewBookingPath(string postID, Outlook.AppointmentItem appItem)
        {
            UriBuilder uriBuilder = new UriBuilder();
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();

                StringBuilder newpath = new StringBuilder();
                //string currentUser = UtilityManager.GetCurrentUserName()
                //IbookingHelper bookingHelper = new BookingHelper();
                // CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
                //  bookingHelper = null;
                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;
                string port = "";
                string currentHost = UtilityManager.HostName; ;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                // newpath.Append("thisU=" + currentUser);
                /*if (UtilityManager.ConnectionMode() == 1)
                {
                    newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                }*/
                string meetingDate = "";
                try
                {
                    //if (currMeeting.DateFrom.Date != DateTime.MinValue.Date)
                    //{
                    //    DateTime newDate = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);
                    //    meetingDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    //}
                    //else
                    //{
                    if (appItem != null)
                    {

                        meetingDate = appItem.Start.ToUniversalTime().ToString();

                    }
                    // }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred GetNewBookingPath Meeting date:" + ex.Message + " " + ex.StackTrace);
                }
                string meetingEndDate = "";
                try
                {
                    //if (currMeeting.DateTo.Date != DateTime.MinValue.Date)
                    //{
                    //    DateTime newDate = new DateTime(currMeeting.DateTo.Year, currMeeting.DateTo.Month, currMeeting.DateTo.Day, currMeeting.TimeTo.Hour, currMeeting.TimeTo.Minute, currMeeting.TimeTo.Second);

                    //    meetingEndDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    //}
                    //else
                    //{
                    meetingEndDate = appItem.End.ToUniversalTime().ToString();

                    //}
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred GetNewBookingPath Meeting End date:" + ex.Message + " " + ex.StackTrace);
                }

                /*newpath.Append("&MeetingDate=" + meetingDate);
                newpath.Append("&MeetingEndDate=" + meetingEndDate);
                newpath.Append("&outlookPostID=" + postID);
                newpath.Append("&outlook=1&edit=");
                newpath.Append("&OutlookEditType=" + appHelper.GetAppointmentBookingType(appItem));
                newpath.Append("&x=1&OutlookGrid=2&BookingID=");*/

                // newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, meetingDate, -1, -1, "2", meetingEndDate, AppointmentHelper.GetAppointmentBookingType(appItem), "", "", "", "");   // newpath.ToString();
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in GetNewBookingPath" + ex.Message + " " + ex.StackTrace);
            }

            Uri cURI = uriBuilder.Uri;
            return cURI;
        }

        private Uri GetExistingBookingPath(string postID, Outlook.AppointmentItem appItem, bool isPartialBooking = false)
        {
            // iLoginPath & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "
            //&OutlookEditType="&iOutlookEditType&"&From="&iFrom&"&To="&iTo&"&BookingID="&iBookingID)    
            UriBuilder uriBuilder = new UriBuilder();
            Uri cURI = null;
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();
                DateTime startDate = appItem.Start;
                DateTime endDate = appItem.End;
                //StringBuilder newpath = new StringBuilder();
                // check if Single Occcurrence Got a Condeco Reference or not
                IbookingHelper bookingHelper = new BookingHelper();
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false, isPartialBooking);
                bookingHelper = null;
                if (currMeeting.BookingID == 0)
                {
                    cURI = GetNewBookingPath(postID, appItem);
                    return cURI;
                }
                string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;

                string port = "";
                string currentHost = UtilityManager.HostName;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                /* newpath.Append("thisU=" + UtilityManager.GetCurrentUserName());
                 if (UtilityManager.ConnectionMode() == 1)
                 {
                     newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                 }*/
                string meetingDate = "";
                //try
                //{
                //    if (currMeeting.DateFrom.Date != DateTime.MinValue.Date)
                //    {
                //        DateTime newDate = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);
                //        meetingDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                //    }
                //    else
                //    {
                //        if (appItem != null)
                //        {

                //            meetingDate = appItem.Start.ToUniversalTime().ToString();

                //        }
                //    }
                //}
                //catch (System.Exception ex)
                //{
                //    UtilityManager.LogMessage("Error Occurred GetExistingBookingPath Meeting date:" + ex.Message + " " + ex.StackTrace);
                //}
                string meetingEndDate = "";
                //try
                //{
                //    if (currMeeting.DateTo.Date != DateTime.MinValue.Date)
                //    {
                //        DateTime newDate = new DateTime(currMeeting.DateTo.Year, currMeeting.DateTo.Month, currMeeting.DateTo.Day, currMeeting.TimeTo.Hour, currMeeting.TimeTo.Minute, currMeeting.TimeTo.Second);

                //        meetingEndDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                //    }
                //    else
                //    {
                //        meetingEndDate = appItem.End.ToUniversalTime().ToString();

                //    }
                //}
                //catch (System.Exception ex)
                //{
                //    UtilityManager.LogMessage("Error Occurred GetExistingBookingPath Meeting End date:" + ex.Message + " " + ex.StackTrace);
                //}
                // string dtFrm=  UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat);
                //  string dtTo =UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat);
                // newpath.Append("&MeetingDate=" + meetingDate);
                //newpath.Append("&MeetingEndDate=" + meetingEndDate);
                //newpath.Append("&outlookPostID=" + postID);
                //newpath.Append("&outlook=1&edit=1&x=1&OutlookGrid=2");
                //newpath.Append("&OutlookEditType=" + appHelper.GetAppointmentBookingType(appItem));
                // newpath.Append("&From=" + UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat));
                //  newpath.Append("&To=" + UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat));
                // newpath.Append("&BookingID=" + currMeeting.BookingID);
                //newpath.Append("&RoomID=" + currMeeting.ResourceItemID);
                //if (currMeeting.BookingID > 0)
                //  newpath.Append("&GridBookingID=" + currMeeting.BookingID);
                //newpath.Append("&gridVersion=" + UtilityManager.OutlookGridVersion.ToString());

                //newpath.Append("&currrentTZ=" + UtilityManager.GetLocalTZName().Replace(" ", "|"));
                //newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                meetingDate = UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat);
                meetingEndDate = UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat);
                uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, meetingDate, currMeeting.ResourceItemID, currMeeting.BookingID, "2", meetingEndDate, AppointmentHelper.GetAppointmentBookingType(appItem), "1", "", "", "");  //newpath.ToString();
                // uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, meetingDate, currMeeting.ResourceItemID, currMeeting.BookingID, "2", meetingEndDate, appHelper.GetAppointmentBookingType(appItem), "1", Convert.ToString(dtFrm), Convert.ToString(dtTo), "");  //newpath.ToString();
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in GetExistingBookingPath" + ex.Message + " " + ex.StackTrace);
            }
            cURI = uriBuilder.Uri;
            return cURI;
        }

        public void gridBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            // MessageBox.Show("Navigated to:" + e.Url);
            try
            {
                if (RoomBookingClickStopBrowserEvent) return;
                Uri curreURL = e.Url;
                if (curreURL == null) return;
                if (curreURL.Query == null) return;
                if (curreURL.Query == "about:blank") return;

                if (e.Url.ToString().ToLower().Contains("#defaultbookingperiod=0"))
                {
                    IsLoaderHide = true;
                    Collapse(this);
                    SetLoader(false);
                    return;
                }
                string pagename = UtilityManager.GetPageName(e.Url.ToString().ToLower());
                //  Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
                //if (e.Url.ToString().ToLower().Contains("condecogridcache/advancedrecurrenceroomselection.aspx") || e.Url.ToString().ToLower().Contains("funclinks/displayresourcedetails.asp"))
                //{
                //    IsLoaderHide = true;
                //}
                if (pagename == "advancedrecurrenceroomselection.aspx" || pagename == "displayresourcedetails.asp" || pagename == "displayresourcedetails.aspx")
                {
                    IsLoaderHide = true;
                }
                else
                {
                    IsLoaderHide = false;
                }
                //Added by Paritosh to fix #17745. 
                //   if (e.Url.ToString().ToLower().Contains("/epdq/processmpipayment.asp?") || e.Url.ToString().ToLower().Contains("/epdq/processpayment.asp"))
                //if (e.Url.ToString().ToLower().Contains("/epdq/processmpipayment.asp?") || e.Url.ToString().ToLower().Contains("/epdq/processpayment.asp") || e.Url.ToString().ToLower().Contains("/epdq/processmpipayment.aspx?") || e.Url.ToString().ToLower().Contains("/epdq/processpayment.aspx"))
                //{
                if (pagename == "processmpipayment.asp" || pagename == "processmpipayment.aspx" || pagename == "processpayment.asp" || pagename == "processmpipayment.aspx?" || pagename == "processpayment.aspx")
                {
                    SetAppointmentLocation();
                    Outlook.AppointmentItem appItemPay = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                    //Added by Paritosh to solve isssue TP #17571. 
                    if (appItemPay.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                    {
                        if (appItemPay != null)
                            appItemPay.Save();
                    }

                    if (appItemPay.IsRecurring)
                    {
                        IbookingHelper bookingHelper = new BookingHelper();
                        //First time After booking Data get from DB from GetRecurranceBookingHasDST method
                        List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceBookingHasDST(appItemPay, false, true);
                        bookingHelper = null;
                        IsyncManager syncManager = new SyncManager();
                        if (appItemPay.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            UtilityManager.LogMessage("CheckRoomGrid.condecoBrowser_Navigated- appItemPay");
                            syncManager.SyncIndiviudallyEditedItems(appItemPay, false, this.AddinModule as AddinModule, false, false);
                            UtilityManager.LogMessage("**CheckRoomGrid.condecoBrowser_Navigated- appItemPay SyncIndiviudallyEditedItems completed**");
                        }
                        bookingHelper = null;
                        syncManager = null;
                    }
                    UtilityManager.FreeCOMObject(appItemPay);

                }
                //

                if (curreURL.Query.ToLower().Contains("booking_saved") && IsValidationValid)
                {

                    (this.AddinModule as AddinModule).IsMeetingSaveNCloseOnly = false;

                    //Added by Paritosh to solve isssue TP #16913
                    if (curreURL.Query.ToLower().Contains("booking_saved"))
                    {
                        Outlook.AppointmentItem appItemTemp = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                        if (appItemTemp != null)
                        {
                            AppointmentDataInfo.SetAppItemSkipSync(appItemTemp, false);
                            (this.AddinModule as AddinModule).IsUndoMeetingChanges = false;
                            AppointmentDataInfo.ResetProperties(appItemTemp);
                            //Added by Paritosh to solve isssue TP #17571. 
                            if (appItemTemp.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                            {
                                if (appItemTemp != null)
                                    appItemTemp.Save();
                            }
                        }
                        UtilityManager.FreeCOMObject(appItemTemp);
                    }
                    //end 
                    Outlook.AppointmentItem appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
                    if (appItem != null)
                    {
                        //added by paritosh 
                        if (appItem.IsRecurring)
                        {
                            IbookingHelper bookingHelper = new BookingHelper();
                            //First time After booking Data get from DB from GetRecurranceBookingHasDST method
                            List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceBookingHasDST(appItem, false, true);
                            bookingHelper = null;
                            IsyncManager syncManager = new SyncManager();
                            //foreach (CondecoMeeting cMeeting in currMeeting)
                            //{
                            //   syncManager.SyncSeriesBookingHasDST(appItem, cMeeting); //, dsOccData, DataFilledInDS
                            //}
                            if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                            {
                                UtilityManager.LogMessage("CheckRoomGrid.condecoBrowser_Navigated-series edited is set to:");
                                syncManager.SyncIndiviudallyEditedItems(appItem, false, this.AddinModule as AddinModule, false, false);

                                UtilityManager.LogMessage("**CheckRoomGrid.condecoBrowser_Navigated-SyncIndiviudallyEditedItems completed**");
                            }
                            bookingHelper = null;
                            syncManager = null;
                        }

                    }
                    //Added  for PRB0043359 and PRB0043845
                    UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.CondecoBookingID, AppointmentDataInfo.GetBookingId(appItem).ToString());
                    //End  for PRB0043359 and PRB0043845
                    AppointmentDataInfo.ResetProperties(appItem);
                    UtilityManager.FreeCOMObject(appItem);
                    IsValidationValid = true;
                    //this.GetType().InvokeMember("HideFloating", System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, null, this, new object[] { });
                    Collapse(this);
                    //this.loaderPicture.Visible = false;
                    SetLoader(false);

                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in gridBrowser_Navigated" + ex.Message + " " + ex.StackTrace);
            }


        }

        private void SetLoader(bool isVisible)
        {
            if (isVisible)
            {
                this.LoaderProgress.Text = CondecoResources.ProgressText;
                this.loaderPicture.Size = new Size(64, 64);
                this.loaderPicture.Location = new Point((gridBrowser.Width / 2) - (loaderPicture.Width / 2), (gridBrowser.Height / 2) - (loaderPicture.Height / 2));
                this.loaderPicture.Visible = isVisible;

                this.LoaderProgress.Size = new Size(64, 64);
                this.LoaderProgress.Location = new Point((gridBrowser.Width / 2) - (loaderPicture.Width / 2) - (this.LoaderProgress.Size.Width / 2), (gridBrowser.Height / 2) + (loaderPicture.Height / 2));
                this.LoaderProgress.Visible = isVisible;
            }
            else
            {
                this.loaderPicture.Visible = isVisible;
                this.LoaderProgress.Visible = isVisible;
            }
        }
    }
}

