using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using AddinExpress.OL;

namespace CondecoAddinV2.DeskBooking
{
    public partial class NavigationPaneDesks : AddinExpress.OL.ADXOlForm
    {
        public NavigationPaneDesks()
        {
            InitializeComponent();
            btnNavigationPaneDesks.Text ="   " + CondecoAddinV2.App_Resources.CondecoResources.Desk_Booking;
        }

        private void btnNevigationPaneDesks_MouseHover(object sender, EventArgs e)
        {
            btnNavigationPaneDesks.BackgroundImage = global::CondecoAddinV2.Properties.Resources.ol_menu_active_bg;
        }

        private void btnNevigationPaneDesks_MouseLeave(object sender, EventArgs e)
        {
            btnNavigationPaneDesks.BackgroundImage = global::CondecoAddinV2.Properties.Resources.ol_menu_bg;
        }

        private void btnNevigationPaneDesks_Click(object sender, EventArgs e)
        {
            //DeskBooking SSO Code
            if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsDeskBookingSSOAuthentication())
            {
                if (!UtilityManager.IsSSOTimeOutMsgShow)
                {

                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                }
                return;
            }
            UtilityManager.LogMessage("NavigationPanelDesks.btnNevigationPaneDesks_Click:*********Started********");
            if (UtilityManager.UserName.Trim().Equals("") && UtilityManager.ConnectionMode() == 1 && UtilityManager.IsCondecoContactable())
            {
                UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2007);
                return;
            }
            DeskBookingHelper deskBookingHelper = new DeskBookingHelper();
            deskBookingHelper.OpenDeskModel(this.OutlookAppObj);
            UtilityManager.LogMessage("NavigationPanelDesks.btnNevigationPaneDesks_Click:*********Closed********");

        }

    }
}
