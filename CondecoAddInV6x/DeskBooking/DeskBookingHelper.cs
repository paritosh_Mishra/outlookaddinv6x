﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CondecoAddinV2.DeskBooking
{
    
    public class DeskBookingHelper
    {
        public Uri GetDeskBookingURL(byte zPageType)
        {
            UtilityManager.LogMessage("DeskBookingHelper.GetDeskBookingURL *********Started********");
            Uri currentURL = new Uri(UtilityManager.GetOfflinePath());
            try
            {
                if (UtilityManager.IsCondecoContactable())
                {
                    if (UtilityManager.ConnectionMode() == 1)
                    {
                        if (!UtilityManager.IsCurrentUserValid)
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized_DeskBooking);
                            currentURL = new Uri(UtilityManager.GetSplashPath());

                            return currentURL;
                        }
                    }

                    bool pageType0 = zPageType.Equals(0);
                    if (pageType0)
                    {
                        switch (DeskBookingUserExistanceCheck())//user Existance check
                        {
                            case "1":  // if user exists with valid desk profile show my desk bookings page
                                zPageType = 2;
                                break;
                            default:  // if does not user exists or does not have valid desk profile show LOGIN PAGE  OR PROFILE PAGE
                                zPageType = 1;
                                break;
                        }
                    }

                    if (zPageType.Equals(1))
                    {
                        currentURL = this.GetNewDeskBookingPath(1);
                    }
                    else if (zPageType.Equals(2))
                    {
                        if (pageType0)
                            currentURL = this.GetNewDeskBookingPath(2);
                        else
                            currentURL = new Uri(UtilityManager.HostURL + "/Core27/DeskBooking/BookedDesks.aspx?outlookdesk=1&username=" + UtilityManager.GetCurrentUserName() + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&ssoUser=" + UtilityManager.SSOUserName);
                            //currentURL = new Uri(UtilityManager.HostURL + "/funcLinks/bookedDesks.asp?outlookdesk=1&username=" + UtilityManager.GetCurrentUserName() + "&thisDomain=" + UtilityManager.GetUserDomainName()+ "&ssoUser=" + UtilityManager.SSOUserName);
                    }
                    else if (zPageType.Equals(3))
                    {
                       // currentURL = new Uri(UtilityManager.HostURL + "/funcLinks/hd_userSearch.asp?outlookdesk=1&username=" + UtilityManager.GetCurrentUserName() + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&ssoUser=" + UtilityManager.SSOUserName);
                        currentURL = new Uri(UtilityManager.HostURL + "/Core27/DeskBooking/hd_usersearch.aspx?outlookdesk=1&username=" + UtilityManager.GetCurrentUserName() + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&ssoUser=" + UtilityManager.SSOUserName);
                    }
                    else if (zPageType.Equals(4))
                    {
                        currentURL = new Uri(UtilityManager.HostURL + "/Core27/User/UserProfile.aspx?outlookdesk=1&username=" + UtilityManager.GetCurrentUserName() + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&buttonClick=1" + "&ssoUser=" + UtilityManager.SSOUserName);
                    }
                    //currentURL = this.GetNewDeskBookingPath(zPageType);
                }
                //Commented by Vineet Yadav 05 Feb 2014 as we change image in offline page and we use offline.htm page here.
                //else
                //{
                //    //currentURL = new Uri(currentURL.ToString().Replace(@"Offline.htm", @"OfflineDeskBooking.htm"));
                //}

                UtilityManager.LogMessage("DeskBookingHelper.GetDeskBookingURL End returning CurrentURL as : " + currentURL);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("DeskBookingHelper.GetDeskBookingURL exception called. Ex.message:" + ex.Message.ToString());
            }
            return currentURL;
        }
        private Uri GetNewDeskBookingPath(int pageNo)
        {
            UtilityManager.LogMessage("DeskBookingHelper.GetNewDeskBookingPath *********Started******** having pageNo as :" + pageNo);
            UriBuilder uriBuilder = new UriBuilder();
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();// "/login/login.asp";

                StringBuilder newpath = new StringBuilder();
                string currentUser = UtilityManager.GetCurrentUserName();

                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;
                string port = "";
                string currentHost = UtilityManager.HostName; ;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                newpath.Append("thisU=" + currentUser);
                newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                //DeskBooking SSO Code
                newpath.Append("&ssoUser=" + UtilityManager.SSOUserName);
                //newpath.Append("&outlookPostID=" + postID);
                //newpath.Append("&outlook=1&outlookdesk=" + zPageType); //
                newpath.Append("&outlook=1&outlookdesk=1");
                //newpath.Append("&OutlookEditType=" + AppointmentHelper.GetAppointmentBookingType(appItem));
                //newpath.Append("&x=1&OutlookGrid=&BookingID=");
                if (UtilityManager.ConnectionMode() == 1)
                {
                    newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                }
                newpath.Append("&Culture=" + UtilityManager.UserLanguage);
                newpath.Append("&PageNo=" + pageNo);

                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = newpath.ToString();
                UtilityManager.LogMessage("DeskBookingHelper.GetNewDeskBookingPath End returning newpath as:" + newpath.ToString());
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("DeskBookingHelper.GetNewDeskBookingPath exception called. Ex.message:" + ex.Message.ToString());
            }
            Uri cURI = uriBuilder.Uri;

            UtilityManager.LogMessage("DeskBookingHelper.GetNewDeskBookingPath*********Finished********");
            return cURI;
        }
        //public string getIconPath(byte zPageType, bool Is0LK2013O)
        //{
        //    string iconpath = "";
        //    iconpath = UtilityManager.IconPath + "\\My_Bookings.ico";
        //    //if (Is0LK2013O)
        //    //{
        //    //    if (zPageType.Equals(1))
        //    //    {
        //    //        // iconpath = UtilityManager.IconPath + "\\Desk_Booking.ico";
        //    //        iconpath = UtilityManager.IconPath + "\\Desk_Search.ico";
        //    //    }
        //    //    else if (zPageType.Equals(2))
        //    //    {
        //    //        // iconpath = UtilityManager.IconPath + "\\My_Desk_Bookings.ico";
        //    //        iconpath = UtilityManager.IconPath + "\\computer-user.ico";

        //    //    }
        //    //    else if (zPageType.Equals(3))
        //    //    {
        //    //        // iconpath = UtilityManager.IconPath + "\\Find_Colleague.ico";
        //    //        iconpath = UtilityManager.IconPath + "\\computer_browse.ico";
        //    //    }
        //    //}
        //    //else
        //    //{
        //        if (zPageType.Equals(1))
        //        {
        //           iconpath = UtilityManager.IconPath + "\\Desk_Booking.ico";
        //        }
        //        else if (zPageType.Equals(2))
        //        {
        //          iconpath = UtilityManager.IconPath + "\\My_Desk_Bookings.ico";
        //        }
        //        else if (zPageType.Equals(3))
        //        {
        //            iconpath = UtilityManager.IconPath + "\\Find_Colleague.ico";
        //        }
        //   // }

        //    return iconpath;
        //}

        //public string getFormText(byte zPageType)
        //{
        //    if (zPageType.Equals(1))
        //    {
        //        return CondecoAddinV2.App_Resources.CondecoResources.DeskBooking_New;// "New Desk Booking";
        //    }
        //    else if (zPageType.Equals(2))
        //    {
        //        return CondecoAddinV2.App_Resources.CondecoResources.DeskBooking_Mine; //"My Desk Bookings";
        //    }
        //    else if (zPageType.Equals(3))
        //    {
        //        return CondecoAddinV2.App_Resources.CondecoResources.DeskBooking_Find;// "Find a Colleague";
        //    }
        //    return CondecoAddinV2.App_Resources.CondecoResources.Desk_Booking; //"Desk Booking";
        //}
       

        public void OpenDeskModel(object OutlookAppObj)
        {
            UtilityManager.LogMessage("DeskBookingHelper.OpenDeskModel *********Started********");
            DeskModel objDeskInstance = DeskModel.Instance(OutlookAppObj);
           // objDeskInstance.Show();
            if (UtilityManager.DeskBookingOpenModal())
            {
                objDeskInstance.ShowDialog();
            }
            else
            {
                objDeskInstance.Show();
            }
            objDeskInstance.WindowState = FormWindowState.Normal;
            objDeskInstance.StartPosition = FormStartPosition.CenterParent;
            objDeskInstance.Focus();
            objDeskInstance.Top = 10;
            objDeskInstance.Height = 680;
            objDeskInstance.Width = 998;
            UtilityManager.LogMessage("DeskBookingHelper.OpenDeskModel *********Finished********");
        }
        public void CloseDeskModel(object OutlookAppObj)
        {
            UtilityManager.LogMessage("DeskBookingHelper.CloseDeskModel *********Started********");
            if (DeskModel.IsInstanceCreated)
            {
                DeskModel.Instance(OutlookAppObj).Close();
              
            }
            UtilityManager.LogMessage("DeskBookingHelper.CloseDeskModel*********Finished********");
        }


        public string DeskBookingUserExistanceCheck()
        {
            //method created by chandra 23Nov13
            string userStatus = "1";
            try
            {
                UtilityManager.LogMessage("DeskBookingHelper.DeskBookingUserExistanceCheck *********Started******** ");

                string dataToPost = "iEmail=" + UtilityManager.GetCurrentUserForPost() + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&iSwitch=13" + "&ssoUser=" + UtilityManager.SSOUserName;
                UtilityManager.LogMessage("DeskBookingHelper.DeskBookingUserExistanceCheck with data: " + dataToPost);
                userStatus= UtilityManager.PostDataToServer(dataToPost);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("DeskBookingHelper.DeskBookingUserExistanceCheck " + ex.Message);
            }
            UtilityManager.LogMessage("DeskBookingHelper.DeskBookingUserExistanceCheck:*********Finished********");
            return userStatus;
        }

        public string BreakTitle(string strInput)
        {
            UtilityManager.LogMessage("DeskBookingHelper.BreakTitle *********Started********");
            //return strInput.Replace(" a ", "~a" + Environment.NewLine).Replace(" ", Environment.NewLine).Replace("~", " ");
            StringBuilder sbOutput=new StringBuilder();
            string[] strArr=strInput.Split(' ');
            if (strArr.Length > 0)
            {
                sbOutput = sbOutput.Append(strArr[0]);
                for (Int16 i = 1; i < strArr.Length; i++)
                {
                    if (strArr[i].Length<=2)
                        sbOutput.Append(" " + strArr[i]);
                    else
                        sbOutput.Append(Environment.NewLine + strArr[i]);
                }
            }
            strArr = null;
            UtilityManager.LogMessage("DeskBookingHelper.BreakTitle:*********Finished********");
            return sbOutput.ToString();
        }
    }
}
