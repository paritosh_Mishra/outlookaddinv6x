﻿namespace CondecoAddinV2.DeskBooking
{
    partial class DeskModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripDeskBooking = new System.Windows.Forms.ToolStrip();
            this.toolFindAColleague = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolBookADesk = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolMyBooking = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolYourProfile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.webBrowserCondeco = new System.Windows.Forms.WebBrowser();
            this.pnlLoader = new System.Windows.Forms.Panel();
            this.picLoader = new System.Windows.Forms.PictureBox();
            this.toolStripDeskBooking.SuspendLayout();
            this.pnlLoader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripDeskBooking
            // 
            this.toolStripDeskBooking.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripDeskBooking.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStripDeskBooking.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolFindAColleague,
            this.toolStripSeparator1,
            this.toolBookADesk,
            this.toolStripSeparator2,
            this.toolMyBooking,
            this.toolStripSeparator3,
            this.toolYourProfile,
            this.toolStripSeparator4});
            this.toolStripDeskBooking.Location = new System.Drawing.Point(0, 0);
            this.toolStripDeskBooking.Name = "toolStripDeskBooking";
            this.toolStripDeskBooking.Size = new System.Drawing.Size(964, 78);
            this.toolStripDeskBooking.Stretch = true;
            this.toolStripDeskBooking.TabIndex = 0;
            this.toolStripDeskBooking.Text = "toolStrip1";
            // 
            // toolFindAColleague
            // 
            this.toolFindAColleague.AutoSize = false;
            this.toolFindAColleague.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolFindAColleague.ImageTransparentColor = System.Drawing.Color.Gainsboro;
            this.toolFindAColleague.Margin = new System.Windows.Forms.Padding(2, 1, 0, 2);
            this.toolFindAColleague.Name = "toolFindAColleague";
            this.toolFindAColleague.Size = new System.Drawing.Size(80, 75);
            this.toolFindAColleague.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolFindAColleague.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolFindAColleague.Click += new System.EventHandler(this.toolFindAColleague_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 78);
            // 
            // toolBookADesk
            // 
            this.toolBookADesk.AutoSize = false;
            this.toolBookADesk.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolBookADesk.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.toolBookADesk.Name = "toolBookADesk";
            this.toolBookADesk.Size = new System.Drawing.Size(75, 75);
            this.toolBookADesk.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolBookADesk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolBookADesk.Click += new System.EventHandler(this.toolBookADesk_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 78);
            // 
            // toolMyBooking
            // 
            this.toolMyBooking.AutoSize = false;
            this.toolMyBooking.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolMyBooking.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.toolMyBooking.Name = "toolMyBooking";
            this.toolMyBooking.Size = new System.Drawing.Size(75, 75);
            this.toolMyBooking.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolMyBooking.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolMyBooking.Click += new System.EventHandler(this.toolMyBooking_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 78);
            // 
            // toolYourProfile
            // 
            this.toolYourProfile.AutoSize = false;
            this.toolYourProfile.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolYourProfile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolYourProfile.Name = "toolYourProfile";
            this.toolYourProfile.Size = new System.Drawing.Size(75, 75);
            this.toolYourProfile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolYourProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolYourProfile.Click += new System.EventHandler(this.toolYourProfile_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 78);
            // 
            // webBrowserCondeco
            // 
            this.webBrowserCondeco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserCondeco.Location = new System.Drawing.Point(0, 78);
            this.webBrowserCondeco.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserCondeco.Name = "webBrowserCondeco";
            this.webBrowserCondeco.ScrollBarsEnabled = false;
            this.webBrowserCondeco.Size = new System.Drawing.Size(964, 564);
            this.webBrowserCondeco.TabIndex = 1;
            this.webBrowserCondeco.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowserCondeco_DocumentCompleted);
            this.webBrowserCondeco.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowserCondeco_Navigating);
            // 
            // pnlLoader
            // 
            this.pnlLoader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlLoader.Controls.Add(this.picLoader);
            this.pnlLoader.Location = new System.Drawing.Point(345, 268);
            this.pnlLoader.Name = "pnlLoader";
            this.pnlLoader.Size = new System.Drawing.Size(175, 100);
            this.pnlLoader.TabIndex = 2;
            this.pnlLoader.UseWaitCursor = true;
            this.pnlLoader.Resize += new System.EventHandler(this.pnlLoader_Resize);
            // 
            // picLoader
            // 
            this.picLoader.BackColor = System.Drawing.Color.Transparent;
            this.picLoader.Image = global::CondecoAddinV2.App_Resources.CondecoResources.preloader64X64;
            this.picLoader.Location = new System.Drawing.Point(37, 25);
            this.picLoader.Name = "picLoader";
            this.picLoader.Size = new System.Drawing.Size(64, 64);
            this.picLoader.TabIndex = 2;
            this.picLoader.TabStop = false;
            this.picLoader.UseWaitCursor = true;
            // 
            // DeskModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 642);
            this.Controls.Add(this.pnlLoader);
            this.Controls.Add(this.webBrowserCondeco);
            this.Controls.Add(this.toolStripDeskBooking);
            this.HelpButton = true;
            this.Location = new System.Drawing.Point(-10, -10);
            this.MinimumSize = new System.Drawing.Size(450, 450);
            this.Name = "DeskModel";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "------Desk Booking-------";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DeskModel_FormClosed);
            this.Resize += new System.EventHandler(this.DeskModel_Resize);
            this.toolStripDeskBooking.ResumeLayout(false);
            this.toolStripDeskBooking.PerformLayout();
            this.pnlLoader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripDeskBooking;
        private System.Windows.Forms.ToolStripButton toolBookADesk;
        private System.Windows.Forms.WebBrowser webBrowserCondeco;
        private System.Windows.Forms.ToolStripButton toolMyBooking;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolFindAColleague;
        private System.Windows.Forms.ToolStripButton toolYourProfile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Panel pnlLoader;
        private System.Windows.Forms.PictureBox picLoader;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}