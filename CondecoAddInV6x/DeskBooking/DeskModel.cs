﻿using System;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Reflection;

namespace CondecoAddinV2.DeskBooking
{
    /// <summary>
    /// Desk Model for Desk Booking Functionality 
    /// </summary>
    public partial class DeskModel : Form
    {
        #region Private Variables

        private DeskBookingHelper deskBookingHelper = new DeskBookingHelper();
        private static DeskModel instance;
        private static int _version = 0;
        private object OutlookAppObj = null;

        #endregion


        /// <summary>
        /// Desk Model Class Constructor
        /// </summary>
        /// <param name="OutlookAppObj"></param>
        private DeskModel(object OutlookAppObj)
        {
            InitializeComponent();
            //to get outlook properties
            this.OutlookAppObj = OutlookAppObj;
            InitialiseDeskModel();
        }

        /// <summary>
        /// Load the Desk form with Links
        /// </summary>
        private void InitialiseDeskModel()
        {
            UtilityManager.LogMessage("DeskModel.InitialiseDeskModel started");
            setFormTitle();
            this.toolFindAColleague.Text = deskBookingHelper.BreakTitle(global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_Find_A_Colleague);
            this.toolBookADesk.Text = deskBookingHelper.BreakTitle(global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_New_Booking);
            this.toolMyBooking.Text = deskBookingHelper.BreakTitle(global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_Your_Booking);
            this.toolYourProfile.Text = deskBookingHelper.BreakTitle(global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_Your_Profile);

            pnlLoader.Dock = DockStyle.Fill;

            if (OutlookVersion == 15)  //icons for Outlook 2013
            {
                toolBookADesk.Image = CondecoAddinV2.Properties.Resources.Desk_Booking2013;
                toolMyBooking.Image = CondecoAddinV2.Properties.Resources.Your_Bookings_2013;
                toolFindAColleague.Image = CondecoAddinV2.Properties.Resources.FindAColleague_2013;
                toolYourProfile.Image = CondecoAddinV2.Properties.Resources.YourProfile_2013;
            }
            else //icons for Outlook 2010,2007 and other older versions
            {
                toolBookADesk.Image = CondecoAddinV2.Properties.Resources.Desk_Booking2010;
                toolMyBooking.Image = CondecoAddinV2.Properties.Resources.Your_Bookings2010;
                toolFindAColleague.Image = CondecoAddinV2.Properties.Resources.FindAColleague_2010;
                toolYourProfile.Image = CondecoAddinV2.Properties.Resources.YourProfile_2010;
            }

            toolStripDeskBooking.Visible = false;
            webBrowserCondeco.Url = deskBookingHelper.GetDeskBookingURL(0);

            //Added by Vineet Yadav 05 Feb 2014 as we change image in offline page and we use offline.htm page here.
            if (webBrowserCondeco.DocumentText.IndexOf("#OfflineMsg#") != -1)
            {
                string strHtmlBuffer = global::CondecoAddinV2.App_Resources.CondecoResources.Offline;
                //strHtmlBuffer = strHtmlBuffer.Replace("#INSTALLDIR#", Assembly.GetExecutingAssembly().CodeBase.Substring(0, Assembly.GetExecutingAssembly().CodeBase.LastIndexOf("/")) + "//");
                strHtmlBuffer = strHtmlBuffer.Replace("#INSTALLDIR#", Assembly.GetExecutingAssembly().CodeBase.Substring(0, Assembly.GetExecutingAssembly().CodeBase.LastIndexOf("/")) + "//");
                strHtmlBuffer = strHtmlBuffer.Replace("#OfflineMsg#", global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_Offline_Message);
                this.webBrowserCondeco.DocumentText = strHtmlBuffer;
            }
            UtilityManager.LogMessage("DeskBookingHelper.InitialiseDeskModel:*********Finished********");
        }


        /// <summary>
        /// Singlton Creation of Desk Model Window
        /// </summary>
        /// <param name="OutlookAppObj"></param>
        /// <returns></returns>
        public static DeskModel Instance(object OutlookAppObj)
        {
            if (instance == null)
            {
                instance = new DeskModel(OutlookAppObj);
            }
            return instance;
        }

        public static bool IsInstanceCreated
        {
            get
            {
                return !(instance == null);
            }
        }

        public int OutlookVersion
        {
            get
            {
                if (_version == 0)
                {
                    Outlook.Application objOLkApp = OutlookAppObj as Outlook.Application;
                    if (objOLkApp != null)
                    {
                        int.TryParse(objOLkApp.Version.ToString().Substring(0, 2), out _version);
                    }
                }
                 UtilityManager.LogMessage("DeskModel.OutlookVersion:*********Finished******** returning version :" + _version );
                return _version;
            }
        }

        private void DeskModel_FormClosed(object sender, FormClosedEventArgs e)
        {
            instance = null;
            UtilityManager.LogMessage("DeskModel.DeskModel_FormClosed:*********Finished******** returning version :" + _version);
        }

        private void toolBookADesk_Click(object sender, EventArgs e)
        {
            webBrowserCondeco.Url = deskBookingHelper.GetDeskBookingURL(1);
        }

        private void toolMyBooking_Click(object sender, EventArgs e)
        {
            webBrowserCondeco.Url = deskBookingHelper.GetDeskBookingURL(2);
        }

        private void toolFindAColleague_Click(object sender, EventArgs e)
        {
            webBrowserCondeco.Url = deskBookingHelper.GetDeskBookingURL(3);
        }

        private void toolYourProfile_Click(object sender, EventArgs e)
        {
            webBrowserCondeco.Url = deskBookingHelper.GetDeskBookingURL(4);
        }


        private void webBrowserCondeco_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            UtilityManager.LogMessage("DeskModel.webBrowserCondeco_DocumentCompleted:*********Started********");
            //hide loader
            string pagename = UtilityManager.GetPageName(e.Url.AbsoluteUri.ToLower());

            //if (e.Url.AbsoluteUri.ToLower().Contains("/funclinks/hd_savebooking.asp") == false && e.Url.AbsoluteUri.ToLower().Contains("/funclinks/hd_deleteaction.asp") == false && e.Url.AbsoluteUri.ToLower().Contains("/funclinks/delete_hotdesk_booking.asp") == false)
            //{
            //    ShowLoader(false);
            //}
            if (pagename== "hd_savebooking.asp" == false && pagename== "hd_deleteaction.asp" == false && pagename== "delete_hotdesk_booking.asp" == false && pagename== "hd_savebooking.aspx" == false && pagename== "hd_deleteaction.aspx" == false && pagename== "delete_hotdesk_booking.aspx" == false)
            {
                ShowLoader(false);
            }
            //show hide toolbar
            //if (e.Url.AbsoluteUri.ToLower().Contains("/login/login.asp") ||
            //    e.Url.AbsoluteUri.ToLower().Contains("/login/splash.asp") ||
            //   // e.Url.AbsoluteUri.ToLower().Contains("/login/register.asp") ||
            //   e.Url.AbsoluteUri.ToLower().Contains("/login/Register.aspx") ||
            //   (e.Url.AbsoluteUri.ToLower().Contains("/Core27/User/UserProfile.aspx") && !e.Url.AbsoluteUri.ToLower().Contains("buttonclick")) ||
            //    //(e.Url.AbsoluteUri.ToLower().Contains("/funclinks/userprofile.asp") && !e.Url.AbsoluteUri.ToLower().Contains("buttonclick")) ||
            //    e.Url.AbsoluteUri.ToLower().Contains("/funclinks/sessionrefresh.asp") ||
            //    e.Url.AbsoluteUri.ToLower().Contains("/funclinks/master.asp") ||
            //    //Commented by Vineet Yadav 05 Feb 2014 as we change image in offline page and we use offline.htm page here.
            //    //e.Url.AbsoluteUri.ToLower().Contains("/offlinedeskbooking.htm") ||
            //     e.Url.AbsoluteUri.ToLower().Contains("/login/customemessage.asp") ||
            //     e.Url.AbsoluteUri.ToLower().Contains("/offline.htm") ||
            //    e.Url.AbsoluteUri.ToLower().Contains("/sessionkill.ashx") ||
            //    e.Url.AbsoluteUri.ToLower().Contains("/login/savepassword.asp") ||
            //     e.Url.AbsoluteUri.ToLower().Contains("/Login/SSO/login_saml.aspx") ||
            //    //e.Url.AbsoluteUri.ToLower().Contains("/saml/login_saml.asp")||
            //    e.Url.AbsoluteUri.ToLower().Contains("about:blank"))
            if (pagename== "login.asp" || pagename== "login.aspx" || pagename== "splash.asp" || pagename== "splash.aspx" || pagename== "Register.aspx" ||
               pagename== "UserProfile.aspx" && !e.Url.AbsoluteUri.ToLower().Contains("buttonclick") ||
                pagename == "sessionrefresh.asp" || pagename == "sessionrefresh.aspx" || pagename == "master.asp" || pagename == "master.aspx" ||
                pagename == "customemessage.asp" || pagename == "customemessage.aspx" || e.Url.AbsoluteUri.ToLower().Contains("/offline.htm") ||
                e.Url.AbsoluteUri.ToLower().Contains("/sessionkill.ashx") ||
                pagename == "savepassword.asp" || pagename == "savepassword.aspx" || pagename == "login_saml.aspx" || e.Url.AbsoluteUri.ToLower().Contains("about:blank"))
            {
                //toolStripDeskBooking.Visible = false; ///not uncomment this without proper investigation
            }
            else
            {
                toolStripDeskBooking.Visible = true;
            }

           

            UtilityManager.LogMessage("DeskModel.webBrowserCondeco_DocumentCompleted:*********Finished********");
        }

        private void pnlLoader_Resize(object sender, EventArgs e)
        {
            picLoader.Left = (pnlLoader.Width / 2) - (picLoader.Width / 2);
            picLoader.Top = (pnlLoader.Height / 2) - (picLoader.Height / 2);
            //MessageBox.Show(picLoader.Height.ToString());
        }
        private void ShowLoader(bool flag)
        {
            UtilityManager.LogMessage("DeskModel.ShowLoader:*********Started******** having flag as :" + flag);
            pnlLoader.Visible = flag;
            toolStripDeskBooking.Enabled = !flag;
            //toolStripDeskBooking.Select();
            UtilityManager.LogMessage("DeskModel.ShowLoader:*********Finished********");
        }

        private void webBrowserCondeco_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            UtilityManager.LogMessage("DeskModel.webBrowserCondeco_Navigating:*********Started********");
            if (!e.Url.AbsoluteUri.ToLower().Contains("/condecogridcache/calendar.aspx") &&
                !e.Url.AbsoluteUri.ToLower().Contains("about:blank") &&
                !e.Url.AbsoluteUri.ToLower().Contains("javascript:"))
            {
                ShowLoader(true);
            }
            UtilityManager.LogMessage("DeskModel.webBrowserCondeco_Navigating:*********Finished********");
        }

        private void DeskModel_Resize(object sender, EventArgs e)
        {
            setFormTitle();
        }
        private void setFormTitle()
        {
            UtilityManager.LogMessage("DeskModel.setFormTitle:*********Started********");
            //this.Text = new string(' ', Convert.ToInt32(this.Width / 7.setFormTitle)) + CondecoAddinV2.App_Resources.CondecoResources.Desk_Booking;
            this.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Desk_Booking;
            UtilityManager.LogMessage("DeskModel.setFormTitle:*********Finished********");
        }
    }
}
