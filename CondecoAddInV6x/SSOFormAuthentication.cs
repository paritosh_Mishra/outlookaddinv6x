﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CondecoAddinV2
{
    public partial class SSOFormAuthentication : Form, IDisposable
    {
        private System.ComponentModel.IContainer components = null;
        private static SSOFormAuthentication instance;
       
        private static bool _disposed;
      //  private WebBrowser condecoBrowser;
        private String condecoBrowserUrl;
        private Panel pnlLoader;
        private PictureBox picLoader;
        public System.Windows.Forms.WebBrowser webBrowserSSO;
        public  System.Timers.Timer aTimer;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SSOFormAuthentication));
            this.webBrowserSSO = new System.Windows.Forms.WebBrowser();
            this.pnlLoader = new System.Windows.Forms.Panel();
            this.picLoader = new System.Windows.Forms.PictureBox();
            this.pnlLoader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).BeginInit();
            this.SuspendLayout();
            // 
            // webBrowserSSO
            // 
            this.webBrowserSSO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserSSO.Location = new System.Drawing.Point(0, 0);
            this.webBrowserSSO.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserSSO.Name = "webBrowserSSO";
            this.webBrowserSSO.Size = new System.Drawing.Size(201, 61);
            this.webBrowserSSO.TabIndex = 0;
            this.webBrowserSSO.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowserSSO_DocumentCompleted);
            this.webBrowserSSO.FileDownload += new System.EventHandler(this.webBrowserSSO_FileDownload);
            this.webBrowserSSO.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.webBrowserSSO_Navigated);
            this.webBrowserSSO.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowserSSO_Navigating);
            this.webBrowserSSO.ProgressChanged += new System.Windows.Forms.WebBrowserProgressChangedEventHandler(this.webBrowserSSO_ProgressChanged);
            this.webBrowserSSO.LocationChanged += new System.EventHandler(this.webBrowserSSO_LocationChanged);
            // 
            // pnlLoader
            // 
            this.pnlLoader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlLoader.Controls.Add(this.picLoader);
            this.pnlLoader.Location = new System.Drawing.Point(13, -20);
            this.pnlLoader.Name = "pnlLoader";
            this.pnlLoader.Size = new System.Drawing.Size(175, 100);
            this.pnlLoader.TabIndex = 3;
            this.pnlLoader.UseWaitCursor = true;
            this.pnlLoader.Resize += new System.EventHandler(this.pnlLoader_Resize);
            // 
            // picLoader
            // 
            this.picLoader.BackColor = System.Drawing.Color.Transparent;
            this.picLoader.Image = global::CondecoAddinV2.App_Resources.CondecoResources.preloader64X64;
            this.picLoader.Location = new System.Drawing.Point(37, 25);
            this.picLoader.Name = "picLoader";
            this.picLoader.Size = new System.Drawing.Size(64, 64);
            this.picLoader.TabIndex = 2;
            this.picLoader.TabStop = false;
            this.picLoader.UseWaitCursor = true;
            // 
            // SSOFormAuthentication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(201, 61);
            this.Controls.Add(this.pnlLoader);
            this.Controls.Add(this.webBrowserSSO);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SSOFormAuthentication";
            this.Text = "Condeco Add-in V6";
            this.pnlLoader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        private SSOFormAuthentication(string url)
        {
            InitializeComponent();
            if (UtilityManager.SsoWebBasedAuthentication())
            {
                pnlLoader.Dock = DockStyle.Fill;
            }
           
            condecoBrowserUrl = url;
            webBrowserSSO.Url = new Uri(url);
            _disposed = false;
            int timeInterval = UtilityManager.SSOTimeOut();
            aTimer = new System.Timers.Timer(1000 * timeInterval);
        }
        public static SSOFormAuthentication Instance(string url)
        {
            if (instance == null || _disposed)
            {
                instance = new SSOFormAuthentication(url);
               
            }
            return instance;
        }

        public static bool IsInstanceCreated
        {
            get
            {
                return !(instance == null);
            }
        }
        
        private void webBrowserSSO_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            UtilityManager.LogMessage("SSOFormAuthentication Form webBrowserSSO_DocumentCompleted Method *********start*****");
            if (UtilityManager.SsoWebBasedAuthentication() && !UtilityManager.IsWebSSOAuthenticated)
            {
                ShowLoader(false);

                string getuserFromServer = string.Empty;

                UtilityManager.LogMessage("SSOFormAuthentication Form webBrowserSSO_DocumentCompleted Method  UtilityManager.SsoWebBasedAuthentication()=1::UtilityManager.IsWebSSOAuthenticated=false ::webBrowserSSO.DocumentText.ToString()= " + webBrowserSSO.DocumentText.ToString()+"--");

                while (webBrowserSSO.DocumentText.ToString().ToLower().Contains("condeco#~"))
                {
                    
                    getuserFromServer = webBrowserSSO.DocumentText.Trim();
                    UtilityManager.LogMessage("SSOFormAuthentication Form webBrowserSSO_DocumentCompleted Method  getuserFromServer =" + getuserFromServer);
                    getuserFromServer = webBrowserSSO.DocumentText.Replace("condeco#~", "").Trim();


                    if (getuserFromServer == "-99")
                    {
                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                       
                    }
                    else if (getuserFromServer == "0")
                    {
                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_SSO_Inactive);
                        
                    }
                    else if (getuserFromServer == "-1")
                    {
                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_SSO_Suspended);
                       
                    }
                    else
                    {
                        UtilityManager.SSOUserName = getuserFromServer;
                        UtilityManager.IsWebSSOAuthenticated = true;
                    }
                    
                    break;
                }
                UtilityManager.LogMessage("SSOFormAuthentication Form webBrowserSSO_DocumentCompleted Method  ::UtilityManager.SSOUserName="+ UtilityManager.SSOUserName+ "--");
                if (!string.IsNullOrEmpty(UtilityManager.SSOUserName))
                {
                    webBrowserSSO.Dispose();
                    if (webBrowserSSO.IsDisposed)
                    {
                        this.Close();
                    }

                }
            }
            UtilityManager.LogMessage("SSOFormAuthentication Form webBrowserSSO_DocumentCompleted Method *********End*****");
        }
        private void webBrowserSSO_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {

        }

        private void webBrowserSSO_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (UtilityManager.SsoWebBasedAuthentication())
            {
                ShowLoader(true);
            }
        }

        private void webBrowserSSO_LocationChanged(object sender, EventArgs e)
        {

        }

        private void webBrowserSSO_FileDownload(object sender, EventArgs e)
        {

        }

        private void webBrowserSSO_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            //if (e.CurrentProgress == e.MaximumProgress)
            //{
            //    MessageBox.Show(((WebBrowser)sender).Url.ToString());
            //}
        }
        public new void Dispose()
        {
            _disposed = true;
            try
            {
                webBrowserSSO.Dispose();
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }
        public static bool DisposeSSO(bool str)
        {
            _disposed = true;
            try
            {
                if (instance != null)
                {
                    instance = null;
                }
                return _disposed;
            }
            finally
            {
               
            }
        }

        private void pnlLoader_Resize(object sender, EventArgs e)
        {
            picLoader.Left = (pnlLoader.Width / 2) - (picLoader.Width / 2);
            picLoader.Top = (pnlLoader.Height / 2) - (picLoader.Height / 2);
        }
        private void ShowLoader(bool flag)
        {
            pnlLoader.Visible = flag;
        }
    }
}
