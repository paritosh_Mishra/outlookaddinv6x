﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Text;
using CondecoAddinV2.App_Resources;
using System.Collections.Generic;
using System.Resources;
using System.Globalization;
 
namespace CondecoAddinV2
{
    /// <summary>
    /// Add-in Express Outlook Option Page
    /// </summary>
    [GuidAttribute("BB63FAD7-7CB8-4FE3-A4BC-CE696F7C9A27"), ProgId("CondecoAddinV2.PropertyPage1")]
    public class CondecoPropertyPage : AddinExpress.MSO.ADXOlPropertyPage
    {
        public CondecoPropertyPage()
        {
            // This call is required by the Component Designer
            InitializeComponent();
            InitLanguages();
            
        }

        private void InitLanguages()
        {
            var dict = new SortedDictionary<string, string>();
            ResourceManager rm = new ResourceManager(typeof(CondecoAddinV2.App_Resources.CondecoResources));
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
            foreach (CultureInfo culture in cultures)
            {
                ResourceSet rs = rm.GetResourceSet(culture, true, false);
                if (rs != null && (!string.IsNullOrEmpty(culture.Name)))
                {
                    dict.Add(culture.EnglishName, culture.Name);
                }
            }


            /*dict.Add("Dutch", "nl");
            dict.Add("English(UK)", "en-GB");
            dict.Add("English(US)", "en-US");
            
            dict.Add("French", "fr");
            dict.Add("German", "de");
            dict.Add("Hungary", "hu");
            
            dict.Add("Italian", "it");
            dict.Add("Indonesian", "id");
            dict.Add("Norwegian", "no");
            
            dict.Add("Portuguese", "pt");
            dict.Add("Portuguese (Brazil)", "pt-BR");
            dict.Add("Spanish", "es");
            dict.Add("Welsh", "cy-GB");
            dict.Add("Swedish", "sv");*/
            cmbLanguage.DataSource = new BindingSource(dict, null);
            cmbLanguage.DisplayMember = "Key";
            cmbLanguage.ValueMember = "Value";

        }

        private Label lblCondeco;
        private Button btnTestConnection;
        private Button btnSave;
        private TextBox txtHost;
        private Label lblHostName;
        private GroupBox groupInfo;
        private GroupBox groupHost;
        private Label lblCopyright;
        private GroupBox groupUserName;
        private TextBox txtUserName;
        private Label label1;
        private Button cmdSaveUserNamePassword;
        private TextBox txtPassword;
        private Label label2;
        private LinkLabel linkRegister;
        private Label lblLanguage;
        private ComboBox cmbLanguage;
        private GroupBox gbLanguage;
        private Button btnLanguageSave;
        private Label lbPinData;
        private Label label3;
        private Label lblHostData;
        private Label lblVersionData;
        private Label lblVersion;
        private Label lblHost;
 
        #region Component Designer generated code
        /// <summary>
        /// Required by designer
        /// </summary>
        private System.ComponentModel.Container components = null;
 
        /// <summary>
        /// Required by designer - do not modify
        /// the following method
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCondeco = new System.Windows.Forms.Label();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbLanguage = new System.Windows.Forms.ComboBox();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.txtHost = new System.Windows.Forms.TextBox();
            this.lblHostName = new System.Windows.Forms.Label();
            this.groupInfo = new System.Windows.Forms.GroupBox();
            this.lbPinData = new System.Windows.Forms.Label();
            this.lblVersionData = new System.Windows.Forms.Label();
            this.lblHostData = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblHost = new System.Windows.Forms.Label();
            this.groupHost = new System.Windows.Forms.GroupBox();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.groupUserName = new System.Windows.Forms.GroupBox();
            this.linkRegister = new System.Windows.Forms.LinkLabel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdSaveUserNamePassword = new System.Windows.Forms.Button();
            this.gbLanguage = new System.Windows.Forms.GroupBox();
            this.btnLanguageSave = new System.Windows.Forms.Button();
            this.groupInfo.SuspendLayout();
            this.groupHost.SuspendLayout();
            this.groupUserName.SuspendLayout();
            this.gbLanguage.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCondeco
            // 
            this.lblCondeco.AutoSize = true;
            this.lblCondeco.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblCondeco.Location = new System.Drawing.Point(149, 23);
            this.lblCondeco.Name = "lblCondeco";
            this.lblCondeco.Size = new System.Drawing.Size(123, 13);
            this.lblCondeco.TabIndex = 0;
            this.lblCondeco.Text = "Condeco Outlook Add-in";
            this.lblCondeco.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestConnection.AutoSize = true;
            this.btnTestConnection.Location = new System.Drawing.Point(284, 386);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(106, 25);
            this.btnTestConnection.TabIndex = 1;
            this.btnTestConnection.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Test_Connection;
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(278, 44);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Save;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbLanguage
            // 
            this.cmbLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLanguage.FormattingEnabled = true;
            this.cmbLanguage.Location = new System.Drawing.Point(106, 19);
            this.cmbLanguage.Name = "cmbLanguage";
            this.cmbLanguage.Size = new System.Drawing.Size(245, 21);
            this.cmbLanguage.TabIndex = 10;
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Location = new System.Drawing.Point(15, 22);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(55, 13);
            this.lblLanguage.TabIndex = 9;
            this.lblLanguage.Text = "Language";
            this.lblLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHost
            // 
            this.txtHost.Location = new System.Drawing.Point(104, 16);
            this.txtHost.Name = "txtHost";
            this.txtHost.Size = new System.Drawing.Size(247, 20);
            this.txtHost.TabIndex = 6;
            // 
            // lblHostName
            // 
            this.lblHostName.AutoSize = true;
            this.lblHostName.Location = new System.Drawing.Point(15, 19);
            this.lblHostName.Name = "lblHostName";
            this.lblHostName.Size = new System.Drawing.Size(29, 13);
            this.lblHostName.TabIndex = 7;
            this.lblHostName.Text = "Host";
            this.lblHostName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupInfo
            // 
            this.groupInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupInfo.Controls.Add(this.lbPinData);
            this.groupInfo.Controls.Add(this.lblVersionData);
            this.groupInfo.Controls.Add(this.lblHostData);
            this.groupInfo.Controls.Add(this.label3);
            this.groupInfo.Controls.Add(this.lblVersion);
            this.groupInfo.Controls.Add(this.lblHost);
            this.groupInfo.Location = new System.Drawing.Point(25, 16);
            this.groupInfo.Name = "groupInfo";
            this.groupInfo.Size = new System.Drawing.Size(365, 80);
            this.groupInfo.TabIndex = 8;
            this.groupInfo.TabStop = false;
            this.groupInfo.Text = "Basic Info";
            // 
            // lbPinData
            // 
            this.lbPinData.AutoSize = true;
            this.lbPinData.Location = new System.Drawing.Point(106, 54);
            this.lbPinData.Name = "lbPinData";
            this.lbPinData.Size = new System.Drawing.Size(22, 13);
            this.lbPinData.TabIndex = 8;
            this.lbPinData.Text = "Pin";
            this.lbPinData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVersionData
            // 
            this.lblVersionData.AutoSize = true;
            this.lblVersionData.Location = new System.Drawing.Point(106, 16);
            this.lblVersionData.Name = "lblVersionData";
            this.lblVersionData.Size = new System.Drawing.Size(42, 13);
            this.lblVersionData.TabIndex = 5;
            this.lblVersionData.Text = "Version";
            this.lblVersionData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHostData
            // 
            this.lblHostData.AutoSize = true;
            this.lblHostData.Location = new System.Drawing.Point(106, 35);
            this.lblHostData.Name = "lblHostData";
            this.lblHostData.Size = new System.Drawing.Size(29, 13);
            this.lblHostData.TabIndex = 6;
            this.lblHostData.Text = "Host";
            this.lblHostData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Pin";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVersion
            // 
            this.lblVersion.AllowDrop = true;
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(15, 16);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(42, 13);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "Version";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHost
            // 
            this.lblHost.AutoSize = true;
            this.lblHost.Location = new System.Drawing.Point(15, 35);
            this.lblHost.Name = "lblHost";
            this.lblHost.Size = new System.Drawing.Size(29, 13);
            this.lblHost.TabIndex = 4;
            this.lblHost.Text = "Host";
            this.lblHost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblHost.Click += new System.EventHandler(this.lblHost_Click);
            // 
            // groupHost
            // 
            this.groupHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupHost.Controls.Add(this.txtHost);
            this.groupHost.Controls.Add(this.lblHostName);
            this.groupHost.Controls.Add(this.btnSave);
            this.groupHost.Location = new System.Drawing.Point(25, 185);
            this.groupHost.Name = "groupHost";
            this.groupHost.Size = new System.Drawing.Size(365, 72);
            this.groupHost.TabIndex = 9;
            this.groupHost.TabStop = false;
            this.groupHost.Text = "Host Settings";
            // 
            // lblCopyright
            // 
            this.lblCopyright.Location = new System.Drawing.Point(24, 418);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(368, 13);
            this.lblCopyright.TabIndex = 10;
            this.lblCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupUserName
            // 
            this.groupUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupUserName.Controls.Add(this.linkRegister);
            this.groupUserName.Controls.Add(this.txtPassword);
            this.groupUserName.Controls.Add(this.label2);
            this.groupUserName.Controls.Add(this.txtUserName);
            this.groupUserName.Controls.Add(this.label1);
            this.groupUserName.Controls.Add(this.cmdSaveUserNamePassword);
            this.groupUserName.Location = new System.Drawing.Point(23, 259);
            this.groupUserName.Name = "groupUserName";
            this.groupUserName.Size = new System.Drawing.Size(367, 119);
            this.groupUserName.TabIndex = 10;
            this.groupUserName.TabStop = false;
            this.groupUserName.Text = "Login Settings";
            this.groupUserName.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // linkRegister
            // 
            this.linkRegister.AutoSize = true;
            this.linkRegister.Location = new System.Drawing.Point(204, 95);
            this.linkRegister.Name = "linkRegister";
            this.linkRegister.Size = new System.Drawing.Size(46, 13);
            this.linkRegister.TabIndex = 10;
            this.linkRegister.TabStop = true;
            this.linkRegister.Text = "Register";
            this.linkRegister.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(106, 55);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(245, 20);
            this.txtPassword.TabIndex = 8;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Password";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(106, 23);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(245, 20);
            this.txtUserName.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "UserName";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cmdSaveUserNamePassword
            // 
            this.cmdSaveUserNamePassword.Location = new System.Drawing.Point(278, 89);
            this.cmdSaveUserNamePassword.Name = "cmdSaveUserNamePassword";
            this.cmdSaveUserNamePassword.Size = new System.Drawing.Size(75, 25);
            this.cmdSaveUserNamePassword.TabIndex = 2;
            this.cmdSaveUserNamePassword.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Save;
            this.cmdSaveUserNamePassword.UseVisualStyleBackColor = true;
            this.cmdSaveUserNamePassword.Click += new System.EventHandler(this.cmdSaveUserNamePassword_Click);
            // 
            // gbLanguage
            // 
            this.gbLanguage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbLanguage.Controls.Add(this.cmbLanguage);
            this.gbLanguage.Controls.Add(this.lblLanguage);
            this.gbLanguage.Controls.Add(this.btnLanguageSave);
            this.gbLanguage.Location = new System.Drawing.Point(25, 102);
            this.gbLanguage.Name = "gbLanguage";
            this.gbLanguage.Size = new System.Drawing.Size(365, 78);
            this.gbLanguage.TabIndex = 11;
            this.gbLanguage.TabStop = false;
            this.gbLanguage.Text = "Language Settings";
            // 
            // btnLanguageSave
            // 
            this.btnLanguageSave.Location = new System.Drawing.Point(278, 50);
            this.btnLanguageSave.Name = "btnLanguageSave";
            this.btnLanguageSave.Size = new System.Drawing.Size(75, 23);
            this.btnLanguageSave.TabIndex = 2;
            this.btnLanguageSave.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Save;
            this.btnLanguageSave.UseVisualStyleBackColor = true;
            this.btnLanguageSave.Click += new System.EventHandler(this.btnLanguageSave_Click);
            // 
            // CondecoPropertyPage
            // 
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.gbLanguage);
            this.Controls.Add(this.groupUserName);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.groupHost);
            this.Controls.Add(this.groupInfo);
            this.Controls.Add(this.btnTestConnection);
            this.Controls.Add(this.lblCondeco);
            this.Name = "CondecoPropertyPage";
            this.Size = new System.Drawing.Size(407, 448);
            this.Load += new System.EventHandler(this.CondecoPropertyPage_Load);
            this.groupInfo.ResumeLayout(false);
            this.groupInfo.PerformLayout();
            this.groupHost.ResumeLayout(false);
            this.groupHost.PerformLayout();
            this.groupUserName.ResumeLayout(false);
            this.groupUserName.PerformLayout();
            this.gbLanguage.ResumeLayout(false);
            this.gbLanguage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void LoadControlText()
        {
            this.btnLanguageSave.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Save;
            this.gbLanguage.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Language_Settings;
            this.label1.Text = global::CondecoAddinV2.App_Resources.CondecoResources.UserName;
            this.label2.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Password;
            this.linkRegister.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Register;
            this.groupUserName.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Login_Settings;
            this.groupHost.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Host_Settings;
            this.lblHost.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Host;
            this.lblVersion.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Version;
            this.label3.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Pin;
            this.lblHostData.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Host;
            this.lblVersionData.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Version;
            this.lblCondeco.Text = global::CondecoAddinV2.App_Resources.CondecoResources.AddIn_Name;
            this.btnTestConnection.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Test_Connection;
            this.btnSave.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Save;
            this.lblLanguage.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Language;
            this.lblHostName.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Host;
            this.groupInfo.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Basic_Info;
            this.lbPinData.Text = global::CondecoAddinV2.App_Resources.CondecoResources.Pin;
            
        }

        /// <summary>
        /// Clean up any resources being used
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if(components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            try
            {
                bool isContactable = UtilityManager.IsCondecoContactable();
                string message = CondecoResources.Condeco_Contactable;
                if (!isContactable)
                    message = CondecoResources.Condeco_NotContactable;
                UtilityManager.ShowWarningMessage(message);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in btnTestConnection_Click :" + ex.Message + " " + ex.StackTrace);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string currentHost = txtHost.Text;
                if (string.IsNullOrEmpty(currentHost))
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.Host_Not_Valid);
                    return;
                }
                if (!UtilityManager.IsHostContactable(currentHost))
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.Condeco_NotContactable);
                    return;
                }
                bool hostSaved = UtilityManager.SaveCondecoHostName(currentHost);

                string message = "";
                if (hostSaved)
                {
                    lblHostData.Text = UtilityManager.HostURL;
                    message = CondecoResources.Host_Saved;
                    txtHost.Text = string.Empty;

                }
                else
                {
                    message = CondecoResources.Host_Not_Saved;
                }
                if (!string.IsNullOrEmpty(message))
                    UtilityManager.ShowWarningMessage(message);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in btnSave_Click :" + ex.Message + " " + ex.StackTrace);
            }
        }

        private void CondecoPropertyPage_Load(object sender, EventArgs e)
        {
            UtilityManager.GetCondecoUserLanguage();
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(UtilityManager.UserLanguage);    //twoLetterCurrentCulture);
            LoadControlText();
            gbLanguage.Visible = true;
            lbPinData.Text = UtilityManager.GetUserPin();
            string appHostvalue = ConfigurationManager.AppSettings["CondecoHost"].ToString();
            lblVersionData.Text = CondecoResources.Addin_Version;
            string currentHost = UtilityManager.HostURL;
            bool showHostBox = false;
            if (string.IsNullOrEmpty(currentHost))
            {
                currentHost = CondecoResources.Host_Not_Found;
                showHostBox = true;
            }
            if (showHostBox || string.IsNullOrEmpty(appHostvalue))
            {
                groupHost.Visible = true;
            }
            else
            {
                groupHost.Visible = false;
            }
            lblHostData.Text = currentHost;
            lblCopyright.Text = CondecoResources.Condeco_Copyright;
            bool condecohostresponding = UtilityManager.IsCondecoContactable();
            if (UtilityManager.ConnectionMode() == 0 || UtilityManager.ConnectionMode() == 2)
            {
                groupUserName.Visible = false;
                linkRegister.Visible = false;
            }
            else
            {
                groupUserName.Visible = true;
                if (condecohostresponding)
                {
                    txtPassword.Text = UtilityManager.Password;
                    txtUserName.Text = UtilityManager.UserName;
                }
                if (string.IsNullOrEmpty(UtilityManager.UserName))
                {
                    linkRegister.Visible = true;
                }
                else
                {
                    linkRegister.Visible = false;
                }
                //Added by Ravi Goyal for PRB0040396 - Option to register new user in Outlook add-in not contextual
                if (condecohostresponding)
                {
                    UtilityManager.LogMessage("CondecoPropertypage.CondecoPropertyPage_Load:Condeco host is reachable, setting user pin");
                    lbPinData.Text = UtilityManager.GetUserPin();
                    if (UtilityManager.ApplicationSettings("SelfRegisteration") == "0")
                    {
                        linkRegister.Visible = false;
                        UtilityManager.LogMessage("CondecoPropertyPage.CondecoPropertyPage_Load: self registration is disabled in global setup");
                    }
                }
                else
                {
                    UtilityManager.LogMessage("CondecoPropertypage.CondecoPropertyPage_Load:Condeco host is not reachable, user pin & self registration is disabled");
                    linkRegister.Visible = false;
                }

            }
            int selectedIndex = GetIndexOfLanguage(UtilityManager.UserLanguage);
            if (selectedIndex == -1)
            {
                string[] broken = UtilityManager.UserLanguage.Split('-');
                if (broken.Length > 0)
                    selectedIndex = GetIndexOfLanguage(broken[0]);
                // Below lines  Added by Paritosh Mishra .Fixed the TP issue 16505 
                // After selecting any language other than the supportive or non supportive languages(14), a blank drop down gets created in Condeco options with null value.
                if (selectedIndex == -1)
                {
                    selectedIndex = GetIndexOfLanguage("en");
                }
                //End updated  
            }
            cmbLanguage.SelectedIndex = selectedIndex;
        }

        private int GetIndexOfLanguage(string Language)
        {
            for(int i = 0; i < this.cmbLanguage.Items.Count; i++)
             {
                KeyValuePair<string, string> value = ((KeyValuePair<string, string>)(this.cmbLanguage.Items[i]));
                if(value.Value.Equals(Language))
                    return i;
            }

            return -1;
        }


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cmdSaveUserNamePassword_Click(object sender, EventArgs e)
        {
            StringBuilder message = new StringBuilder();
            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                message.Append(CondecoResources.UserLogin_UserName_Missing);
                txtUserName.Focus();
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                if(!string.IsNullOrEmpty(message.ToString()))
                    message.AppendLine();
                message.Append(CondecoResources.UserLogin_password_Missing);
                txtPassword.Focus();
            }
            if (string.IsNullOrEmpty(message.ToString()))
            {
                if (!UtilityManager.IsValidCondecoUser(txtUserName.Text, txtPassword.Text))
                {
                    message.Append(CondecoResources.UserLogin_NotValild);
                    txtUserName.Focus();
                }   
            }
            
            if (!string.IsNullOrEmpty(message.ToString()))
            {
                
                UtilityManager.ShowWarningMessage(message.ToString());
            }
            
            else
            {
                UtilityManager.SaveCondecoUserNamePassword(txtUserName.Text, txtPassword.Text);
                lbPinData.Text = UtilityManager.GetUserPin();
                UtilityManager.ShowWarningMessage(CondecoResources.UserLogin_Saved_Sucessfully);
            }
            
        }

        private void lblHost_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (string.IsNullOrEmpty(UtilityManager.HostURL))
            {
                MessageBox.Show("Host information is missing");
                return;
            }
          //  System.Diagnostics.Process.Start(UtilityManager.HostURL + "/login/register.asp");
            System.Diagnostics.Process.Start(UtilityManager.HostURL + "/login/Register.aspx");
        }

        private void btnLanguageSave_Click(object sender, EventArgs e)
        {
            try
            {
                string currentLanguage = cmbLanguage.SelectedValue.ToString();
                bool lngSaved = UtilityManager.SaveCondecoUserLanguage(currentLanguage);

                string message = "";
                if (lngSaved)
                {
                    cmbLanguage.SelectedValue = currentLanguage;
                    message = CondecoResources.Language_Saved;
                    UtilityManager.UserLanguage = currentLanguage;
                }
                else
                {
                    message = CondecoResources.Language_Not_Saved;
                }
                if (!string.IsNullOrEmpty(message))
                    UtilityManager.ShowWarningMessage(message);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in btnLanguageSave_Click :" + ex.Message + " " + ex.StackTrace);
            }
        }
 
    }
}

