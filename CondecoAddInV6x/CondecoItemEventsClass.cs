using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;

using System.Data;

using Exception = System.Exception;
using CondecoAddinV2.Repository;
using CondecoAddinV2.App_Resources;
using Outlook = Microsoft.Office.Interop.Outlook;
using CondecoAddinV2.Constants;

namespace CondecoAddinV2
{
    /// <summary>
    /// Add-in Express Outlook Item Events Class
    /// </summary>
    public class CondecoItemEventsClass : AddinExpress.MSO.ADXOutlookItemEvents
    {
        private const int WM_MYToolBar = WM_USER + 100;
        private const int WM_USER = 0x0400;
        private const int WM_MYMyAppDeleted = WM_USER + 150;
        private const int WM_MYMESSAGE = WM_USER + 1000;
        private AddinModule CurrentModule = null;
        private bool isSelectedChanged = false;
       // private static BookingHelper bookingHelper = new BookingHelper();
        //private static AppointmentHelper appHelper = new AppointmentHelper();
        private DateTime origStart;
        private bool isAppointmentMoved;
        private DateTime origSeriesStart;
        private DateTime origSeriesEnd;

        private bool IsDatePastMsgShow = false;

        private bool V6SeriesUpdateUndoWhileCancel = false;

        //CRD 6754 Addded flag  isOLK2007RecCancelExp only for OLK 2007 by paritosh
        private bool isOLK2007RecCancelExp = false;

        //PRB0041438-Added by Ravi Goyal to capture Yes, No cancel for user option
        private bool IsOccurenceChangesDiscard = false;

        //Anand : Added this property to identify that process open completed
        public bool IsEnableWriteEventCheck { get; set; }

        //CRD 6754 Addded flag  isOLK2007RecCancelExp only for OLK 2007 by paritosh
        public bool IsOLK2007RecCancelExp
        {
            get { return isOLK2007RecCancelExp; }
            set { isOLK2007RecCancelExp = value; }
        }
        // end 
        public DateTime OrigStart
        {
            get { return origStart; }
            set { origStart = value; }
        }
        private DateTime origEnd;

        public DateTime OrigEnd
        {
            get { return origEnd; }
            set { origEnd = value; }
        }
        private DateTime deletionDate;

        public DateTime DeletionDate
        {
            get { return deletionDate; }
            set { deletionDate = value; }
        }
        private string origSubject;

        public string OrigSubject
        {
            get { return origSubject; }
            set { origSubject = value; }
        }
        private bool isNormalBooking;
        public bool IsNormalBooking
        {
            get { return isNormalBooking; }
            set { isNormalBooking = value; }
        }
        public bool IsAppointmentMoved
        {
            get { return isAppointmentMoved; }
            set { isAppointmentMoved = value; }
        }

        private bool isSubjectChanged;
        private bool isLocationChanged;
        private bool isStartDateChanged;
        private bool isEndDateChanged;
        private bool isAlertDisplayed;
        private bool isCondecoBookingSaved;
        private bool isSeriesDeleted;
        private bool isRecurrenceRemoved;
        public bool IsSubjectChanged
        {
            get { return isSubjectChanged; }
            set { isSubjectChanged = value; }
        }
        public bool IsLocationChanged
        {
            get { return isLocationChanged; }
            set { isLocationChanged = value; }
        }
        public bool IsStartDateChanged
        {
            get { return isStartDateChanged; }

            set { isStartDateChanged = value; }
        }
        public bool IsEndDateChanged
        {
            get { return isEndDateChanged; }
            set { isEndDateChanged = value; }
        }
        public bool IsAlertDisplayed
        {
            get { return isAlertDisplayed; }
            set { isAlertDisplayed = value; }
        }
        public bool IsCondecoBookingSaved
        {
            get { return isCondecoBookingSaved; }
            set { isCondecoBookingSaved = value; }
        }
        public bool IsSeriesDeleted
        {
            get { return isSeriesDeleted; }
            set { isSeriesDeleted = value; }
        }
        public bool IsRecurrenceRemoved
        {
            get { return isRecurrenceRemoved; }
            set { isRecurrenceRemoved = value; }
        }
        public CondecoItemEventsClass(AddinExpress.MSO.ADXAddinModule module, bool isSelectedChanged)
            : base(module)
        {
            this.isSelectedChanged = isSelectedChanged;
            if (CurrentModule == null)
                CurrentModule = module as AddinModule;
        }



        public override void ProcessAttachmentAdd(object attachment)
        {
            // TODO: Add some code
        }

        public override void ProcessAttachmentRead(object attachment)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAttachmentSave(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeCheckNames(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessClose(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessClose:*********Started********");
            try
            {
                Outlook.AppointmentItem currApp = this.ItemObj as Outlook.AppointmentItem;
                if (currApp == null)
                {
                    e.Cancel = false;
                    if (!isSelectedChanged)
                    {
                       // CurrentModule.itemEvents.Remove(this);
                        this.Dispose();
                    }
                    return;
                }
                try
                {
                    if (currApp != null && (currApp is Outlook.AppointmentItem || currApp is Outlook.MeetingItem))
                    {
                        if (!UtilityManager.IsCondecoContactable())
                        {
                            UtilityManager.LogMessage("CondecoItemEventsClass: ProcessClose():-Timeout detected-Condeco host is not responding, booking will not be sync with Condeco----");
                            UtilityManager.LogMessage("CondecoItemEventsClass: ProcessClose():-Appointment closed without sync with Condeco----");
                            return;
                        }
                    }
                    if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                    {
                        if (!isSelectedChanged)
                        {
                            //CurrentModule.itemEvents.Remove(this);
                            this.Dispose();
                        }
                        return;
                    }

                }
                catch
                {
                    e.Cancel = false;
                    return;
                }
                if (currApp.IsRecurring && currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                {
                    AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currApp, true);
                }
              
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessClose:Check Booking is In Sync With Condeco");
                // TODO: Add some code
                bool isPartialBooking = false;
                if (!V6SeriesUpdateUndoWhileCancel && !CheckBookingIsInSyncWithCondeco(out isPartialBooking)) 
                { 
                    IsAlertDisplayed = false;
                    e.Cancel = true;
                    //if (!isSelectedChanged)
                    //{
                    //    CurrentModule.itemEvents.Remove(this);
                    //    this.Dispose();
                    //}
                    return;
                }
                V6SeriesUpdateUndoWhileCancel = false;

                //Added by Anand on 27-Feb-2013 for TP 11197
                if (currApp.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster && !isPartialBooking)
                {
                    IbookingHelper bookingHelper = new BookingHelper();
                    bool isCondecoBooking = bookingHelper.IsCondecoBooking(currApp);
                    bookingHelper = null;
                    // Below origEnd check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                    // if (isCondecoBooking && origStart < DateTime.Now && !currApp.Location.ToLower().Contains("cancelled"))
                    if (isCondecoBooking && origEnd < DateTime.Now && !currApp.Location.ToLower().Contains("cancelled"))
                    {
                        AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currApp, false);
                        //proceed without event removal
                        e.Cancel = false;
                        return;
                    }
                }

                 if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !isPartialBooking )
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessClose:Series booking so checking for individually edited item sync");
                    //CRD 6754 Addded flag  isOLK2007RecCancelExp only for OLK 2007 by paritosh
                    if (!IsOLK2007RecCancelExp)
                    {
                        ////NeedtobecheckParitosh 3Feb2016
                        //Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402 (Only for >outlook 2007, not for outlook 2007 or lesser versions.'
                        bool IsSeriesEdited = false;
                        if ((AppointmentDataInfo.GetStartDateChanged(currApp) || AppointmentDataInfo.GetEndDateChanged(currApp) || AppointmentDataInfo.GetSubjectChanged(currApp)))
                        {
                            IsSeriesEdited = true;
                        }
                        if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                        {
                            IsyncManager syncManager = new SyncManager();
                            //NeedtobecheckParitosh Feb2015
                            syncManager.SyncIndiviudallyEditedItems(currApp, false, CurrentModule as AddinModule, IsSeriesEdited, false);
                            // syncManager.SyncIndiviudallyEditedItems(currApp);
                            syncManager = null;
                        }
                        IsOLK2007RecCancelExp = false;
                    }

                }
                if (!isSelectedChanged)
                {  
				 
                   // CurrentModule.itemEvents.Remove(this); // check12/10/15
                    this.Dispose();    // check12/10/15
					return;
                }
            }
            catch
            { }
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessClose:*********Finished********");
        }

        public override void ProcessCustomAction(object action, object response, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessCustomPropertyChange(string name)
        {
            // TODO: Add some code
        }

        public override void ProcessForward(object forward, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessOpen(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessOpen:*********Started********");
            try
            {
                Outlook.AppointmentItem currApp = this.ItemObj as Outlook.AppointmentItem;
                if (currApp == null) return;
                int bookingid = AppointmentDataInfo.GetBookingId(currApp);
                if (bookingid == 0)
                {
                    if (currApp != null && (currApp is Outlook.AppointmentItem || currApp is Outlook.MeetingItem))
                    {
                        if (!UtilityManager.IsCondecoContactable())
                        {
                            UtilityManager.LogMessage("CondecoItemEventsClass: ProcessOpen():-Timeout detected-Condeco host is not responding, booking will not be sync with Condeco----");
                            UtilityManager.LogMessage("CondecoItemEventsClass: ProcessOpen():-Appointment open without sync with Condeco----");
                            return;
                        }
                    }

                    IsEnableWriteEventCheck = false;
                    if (currApp.MeetingStatus != Outlook.OlMeetingStatus.olNonMeeting)
                    {
                        CurrentModule.IsMeetingSaveNCloseOnly = true;
                    }

                    bool isTimechanged = (Module as AddinModule).isNewTimeProposed;


                    //By Anand on 8-Feb-2013 for TP issue 10614, 10611
                    bool havingPermission = true;
                    string postId = string.Empty;
                    if (UtilityManager.SyncBookingEnabled().Equals(1) && AppointmentHelper.IsSyncAppointment(currApp))
                    {
                        postId = AppointmentHelper.SyncGetAppointmentPostID(currApp);
                    }
                    else
                    {
                        postId = AppointmentHelper.GetAppointmentPostID(currApp);

                    }
                    if (!string.IsNullOrEmpty(postId))
                    {
                        if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                        {
                            if (!UtilityManager.IsSSOTimeOutMsgShow)
                            {

                                UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                            }
                            //UtilityManager.IsSSOTimeOutMsgShow = false;
                            return;
                        }
                        havingPermission = UtilityManager.PermissionCheck(postId, UtilityManager.GetCurrentUserForPost());
                    }

                    IbookingHelper bookingHelper = new BookingHelper();
                    int bookingId = bookingHelper.GetBookingID(postId, currApp);
                    bookingHelper = null;

                    AppointmentDataInfo.AddAppointmentInCollection(currApp, havingPermission, bookingId);
                    AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currApp, false);
                    //end
                    //Sync need to verify
                    ////////if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled ||
                    ////////    currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceived)
                    ////////{

                    ////////    return;
                    ////////}
                    if (currApp.IsRecurring)
                    {
                        IsNormalBooking = false;
                        if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                        {
                            AppointmentDataInfo.SetIsItemNonMeeting(currApp, true);
                        }
                    }
                    else
                    {
                        IsNormalBooking = true;
                    }
                    ////Removing setting condeco reference from here and moving to click of room booking
                    //bookingHelper.AddCondecoReference(currApp);
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessOpen:Going to Sync the appointment");

                    //if (!appHelper.IsSyncAppointment(currApp))
                    //{
                    //if (UtilityManager.SyncBookingEnabled().Equals(1) && !isTimechanged)
                    //    {
                    //        syncManager.SyncAppointmentWithCondeco(currApp);
                    //    }
                    //}
                    bool isappsync = AppointmentDataInfo.GetAppItemSkipSync(currApp);

                    if (!isTimechanged && !isappsync && bookingId > 0)
                    {

                        // Added by Ravi Goyal - if item is olApptException, save its start, end and Subject info to tuple list in addin module.
                        if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                        {
                            (Module as AddinModule).exclusionoccurencesinfo = new Tuple<DateTime, DateTime, string>(currApp.Start, currApp.End, currApp.Subject);
                        }
                        if (String.IsNullOrEmpty(currApp.Location) && currApp.Start < DateTime.Now && (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException))
                        {
                            UtilityManager.LogMessage("Process open past  ");
                            //UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                        }
                        else
                        {
                            IsyncManager syncManager = new SyncManager();
                            syncManager.SyncAppointmentWithCondeco(currApp);
                            syncManager = null;
                        }

                        if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                        {
                            (Module as AddinModule).IsMeetingSaveFromWeb = false;
                        }
                    }
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessOpen:Sync completed");
                    OrigStart = currApp.Start;
                    OrigEnd = currApp.End;
                    OrigSubject = currApp.Subject;
                    //added by anand for TP 9497 issue below variables are newly added variables
                    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        Outlook.RecurrencePattern pattern = currApp.GetRecurrencePattern();
                        origSeriesStart = new DateTime(pattern.PatternStartDate.Date.Ticks + pattern.StartTime.TimeOfDay.Ticks);
                        origSeriesEnd = new DateTime(pattern.PatternEndDate.Date.Ticks + pattern.EndTime.TimeOfDay.Ticks);
                        UtilityManager.FreeCOMObject(pattern);
                    }
                    IsStartDateChanged = false;
                    IsEndDateChanged = false;
                    IsSubjectChanged = false;
                    IsAlertDisplayed = false;
                    IsRecurrenceRemoved = false;
                    IsEnableWriteEventCheck = true;
                    //changed by Anand on 13-Feb-2012. Moved here as need to set the properties as well
                    if (isTimechanged)
                    {
                        IsCondecoBookingSaved = false;
                        IsStartDateChanged = true;
                        IsEndDateChanged = true;
                        (Module as AddinModule).isNewTimeProposed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in Process Open" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                (Module as AddinModule).IsSendPressed = false;
            }
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessOpen:*********Finished********");
        }

        public override void ProcessPropertyChange(string name)
        {
            UtilityManager.LogMessage("CondecoItemEventsClass: *******Started**************** For Name : " + name);
            // if (this.ItemObj is Outlook.AppointmentItem)
            // {
            try
            {
                if (name.Equals("Subject"))
                {
                    Outlook.AppointmentItem cItem = this.ItemObj as Outlook.AppointmentItem;
                    IsSubjectChanged = true;
                    IsCondecoBookingSaved = false;
                    AppointmentDataInfo.SetSubjectChanged(cItem, true);
                    
                    AppointmentHelper.UpdateBookingClickedSettings(cItem, false);
                }
                else if (name.Equals("Start"))
                {
                    Outlook.AppointmentItem cItem = this.ItemObj as Outlook.AppointmentItem;
                    IsStartDateChanged = true;
                    IsCondecoBookingSaved = false;
                    AppointmentDataInfo.SetStartDateChanged(cItem, true);
                    AppointmentHelper.UpdateBookingClickedSettings(cItem, false);
                }
                else if (name.Equals("End"))
                {
                    Outlook.AppointmentItem cItem = this.ItemObj as Outlook.AppointmentItem;
                    IsEndDateChanged = true;
                    IsCondecoBookingSaved = false;
                    AppointmentDataInfo.SetEndDateChanged(cItem, true);
                    AppointmentHelper.UpdateBookingClickedSettings(cItem, false);
                }
                else if (name.Equals("IsRecurring"))
                {
                    Outlook.AppointmentItem cItem = this.ItemObj as Outlook.AppointmentItem;
                    if (!isNormalBooking && !cItem.IsRecurring)
                    {
                        IsRecurrenceRemoved = true;
                    }
                    IsCondecoBookingSaved = false;
                    AppointmentHelper.UpdateBookingClickedSettings(cItem, false);
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in ProcessPropertyChange for prop:" + name + " " + ex.Message + " " + ex.StackTrace);
            }
            //}
            UtilityManager.LogMessage("CondecoItemEventsClass: *******Finished**************** ");

        }

        public override void ProcessRead()
        {
            // TODO: Add some code
        }

        public override void ProcessReply(object response, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessReplyAll(object response, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessSend(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:*********Started********");
            IsEnableWriteEventCheck = false;
            try
            {
                IbookingHelper bookingHelper = new BookingHelper();
                Outlook.AppointmentItem currApp = null;
                try
                {
                 currApp = this.ItemObj as Outlook.AppointmentItem;
                }
                catch
                {
                    // Added by Ravi Goyal - if item is olApptException, com object was released,recreate it from inspector object and match the changes with tuple list.
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:CurrApp Location is fixed in catch() using active inspector");
                    Outlook.Inspector ins = CurrentModule.OutlookApp.ActiveInspector();
                    if (ins != null)
                    {
                        object item = ins.CurrentItem;
                        if (item != null)
                        {
                            if (item is Outlook.AppointmentItem)
                            {
                                currApp = item as Outlook.AppointmentItem;
                                try
                                {
                                    if(currApp.RecurrenceState==Outlook.OlRecurrenceState.olApptException)
                                    if ((Module as AddinModule).exclusionoccurencesinfo != null)
                                    if (currApp.Start != (Module as AddinModule).exclusionoccurencesinfo.Item1)
                                        IsStartDateChanged = true;
                                    if (currApp.End != (Module as AddinModule).exclusionoccurencesinfo.Item2)
                                        IsEndDateChanged = true;
                                    if (currApp.Subject != (Module as AddinModule).exclusionoccurencesinfo.Item3)
                                        IsSubjectChanged = true;
                                }
                                catch (Exception ex)
                                {
                                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:CurrApp Location try to fixed in catch() , error="+ex.Message);
                                }
                            }
                        }
                    }
                }

                if ((currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting || currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled) && currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                {
                    CurrentModule.IsMeetingSaveNCloseOnly = false;
                }

               


                if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                {
                    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence)
                    {
                        // bookingHelper.DeleteSingleOccurrenceFromCondeco();
                        if (!isSelectedChanged)
                        {
                            //CurrentModule.itemEvents.Remove(this);
                            this.Dispose();
                        }
                        return;
                    }
                    else if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        CurrentModule.IsMeetingSaveNCloseOnly = false;
                        string MasterLocation = string.Empty;
                        if (CurrentModule.AppMasterLoc != null)
                        {
                            if (CurrentModule.AppMasterLoc.Count > 0)
                            {
                                for (int i = 0; i < 1; i++)
                                {
                                    MasterLocation = CurrentModule.AppMasterLoc[0].ToString();
                                }
                            }
                            else
                            {
                                MasterLocation = currApp.Location;
                            }
                        }
                        else
                        {
                            MasterLocation = currApp.Location;
                        }
                        AppointmentHelper.SetAppointmentLocation(currApp, MasterLocation);
                       // CurrentModule.itemEvents.Remove(this);
                        this.Dispose();
                        return;
                    }
                    else
                    {
                       // bookingHelper.DeleteCondecoBooking(currApp);
                        //if single appointment meeting  is canceled  by calender then 2 msg (deleted booking )comes , first call procesbefore delete then processsend event . 
                        if (currApp.MeetingStatus != Outlook.OlMeetingStatus.olMeetingCanceled)
                           bookingHelper.DeleteCondecoBooking(currApp, CurrentModule as AddinModule);
                    }
                  
                    CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(currApp, true);
                    //bookingHelper.GetBookingLocation(postID,delAppItem)
                    //currApp.Location = curMeeting.MainLocation;
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:Uupdating the location");
                    AppointmentHelper.SetAppointmentLocation(currApp, curMeeting.MainLocation);
                    e.Cancel = false;
                    if (!isSelectedChanged)
                    {
                        //CurrentModule.itemEvents.Remove(this);
                        this.Dispose();
                    }
                    bookingHelper = null;
                    return;

                }//Added by Ravi Goyal Check if Meeting was deleted from Delete booking button, if true then set the meeting cancel here
                else if (CurrentModule.AppMasterLoc != null)
                {
                    string MasterLocation = string.Empty;
                    if (CurrentModule.AppMasterLoc.Count > 0)
                    {
                            for (int i = 0; i < 1; i++)
                            {
                                MasterLocation = CurrentModule.AppMasterLoc[0].ToString();
                            }
                            currApp.MeetingStatus = Outlook.OlMeetingStatus.olMeetingCanceled;
                            AppointmentHelper.SetAppointmentLocation(currApp, MasterLocation);
                           // CurrentModule.itemEvents.Remove(this);
                            this.Dispose();
                            return;
                    }
                    
                }

                bool ispartialBooking = false;
                if (!IsAppointmentMoved)
                {
                      //UserStory 4401 :: 7a step :Non recurrance  mutliple room  meeting updated
                            //if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && !currApp.IsRecurring)
                            //{
                            //    AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currApp, true);
                            //}
                    //UserStory 4407/UserStory 4401 :: 7a :
                    if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptNotRecurring || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence))
                            {
                                AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currApp, true);
                            }
                    // end 
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:checking Appointment is in sync with Condeco");
                    if (!CheckBookingIsInSyncWithCondeco(out ispartialBooking))
                    {
                        IsAlertDisplayed = false;
                        e.Cancel = true;
                        //Commented by Anand on 21_feb_13 for TP issue 10829
                        //if (!isSelectedChanged)
                        //{
                        //    CurrentModule.itemEvents.Remove(this);
                        //    this.Dispose();
                        //}
                        return;
                    }
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:Syncing Finished");
                }
                else
                {
                    IsAppointmentMoved = false;
                }
                bool isappsync = AppointmentDataInfo.GetAppItemSkipSync(currApp);
                if (isappsync && currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                {
                    bool ret = this.HandleAppointmentMove(currApp);
                    if (ret == false)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                ////if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                ////{

                ////    IsyncManager syncManager = new SyncManager();
                ////    System.Collections.Generic.List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceMeetingEditedOnly(currApp);
                ////    Outlook.RecurrencePattern recPattern = currApp.GetRecurrencePattern();
                ////    syncManager.HanldeIndiviudalyEditedItems(currApp, recPattern, currMeeting);
                ////    UtilityManager.FreeCOMObject(recPattern);
                ////    bookingHelper = null;
                ////    syncManager = null;
                ////}

               
                //currApp.MeetingStatus != Outlook.OlMeetingStatus.olMeeting  &&
                ////////////if ( currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !ispartialBooking && !currApp.Location.ToLower().Contains(CondecoAddinV2.App_Resources.CondecoResources.Appointment_Cancelled_Title.ToLower()))   //  ("cancelled")) 
                ////////////{
                ////////////    ////UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:In case of series checking syncing of individually edited items");
                
                ////////////    IsyncManager syncManager = new SyncManager();
                //////////// syncManager.SyncIndiviudallyEditedItems(currApp, false, CurrentModule, true, false);
                ////////////   // syncManager.SyncAppointmentWithCondeco(currApp);
                ////////////    //System.Collections.Generic.List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceBookingHasDST(currApp, true);
                ////////////    //foreach (CondecoMeeting currmeet in currMeeting)
                ////////////    //{
                ////////////    //    syncManager.SyncOccurrenceBooking(currmeet, currApp);
                ////////////    //}
                ////////////    syncManager = null;
                ////////////   // currApp.Save();
                ////////////    //IsyncManager syncManager = new SyncManager();
                ////////////    //syncManager.SyncAppointmentWithCondeco(currApp);
                ////////////    //syncManager = null;
                ////////////}

				 if (IsOccurenceChangesDiscard)
                {
                    e.Cancel = true;
                    IsAlertDisplayed = false;
                    IsOccurenceChangesDiscard = false;
                    return;
                }
                if (!isSelectedChanged)
                {
                    //CurrentModule.itemEvents.Remove(this);
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in ProcessSend :" + ex.Message + " " + ex.StackTrace);
            }
            IsEnableWriteEventCheck = true;
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:*********Finished********");
        }
        
        
        private bool HandleAppointmentMove(Outlook.AppointmentItem curApp)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            // UtilityManager.LogMessage("2D1- Inside handle appointment move with Subject:" + curApp.Subject + "StartDate:" + curApp.Start + ":End Date:" + curApp.End);
            Outlook.AppointmentItem singleAppointment = null;
            // MessageBox.Show("Going to Move Appointment with Start:" + origStartDate + "to :" + curApp.Start);
            // UtilityManager.LogMessage("2D2- checking if appontment is recurring series");
            DateTime? MeetingMoveOrigStartDate = null;
            if (curApp.MeetingStatus != Outlook.OlMeetingStatus.olNonMeeting)
            {
                MeetingMoveOrigStartDate = (DateTime)AppointmentDataInfo.GetOriginalMeetingMoveStartDate(curApp);
            }

          

            //////if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
            //////{
            //////    Outlook.RecurrencePattern masterRecPattern = curApp.GetRecurrencePattern();
            //////    Outlook.Exceptions oExps = masterRecPattern.Exceptions;
            //////    if (oExps.Count > 0)
            //////    {
            //////        if (MeetingMoveOrigStartDate != null)
            //////        {
            //////        for (int ex = 0; ex < oExps.Count; ex++)
            //////          {
            //////            Outlook.Exception currExp = oExps[ex + 1];
            //////            if (currExp != null)
            //////            {
            //////                if (!currExp.Deleted && currExp.OriginalDate.Date.CompareTo(((DateTime) MeetingMoveOrigStartDate).Date) == 0)
            //////                {
            //////                        // UtilityManager.LogMessage("2D2- single occurrent deleted exceptionfound for date:" + origStartDate.Date);
            //////                        singleAppointment = currExp.AppointmentItem;
            //////                        UtilityManager.FreeCOMObject(currExp);
            //////                        break;
                                
            //////                }
            //////                UtilityManager.FreeCOMObject(currExp);
            //////            }
            //////         }
            //////       }
            //////    }
            //////    UtilityManager.FreeCOMObject(oExps);
            //////}

            IbookingHelper bookingHelper = new BookingHelper();
            CondecoMeeting currMeeting = new CondecoMeeting();
            if (singleAppointment != null)
            {
                //////////string postID = appHelper.GetAppointmentPostID(curApp);
                //////////if (UtilityManager.SyncBookingEnabled().Equals(1) && (string.IsNullOrEmpty(postID) || (appHelper.IsSyncAppointment(curApp))))
                //////////{
                //////////    postID = appHelper.SyncGetAppointmentPostID(curApp);
                //////////}
                //////////if (MeetingMoveOrigStartDate != null)
                //////////{
                //////////    currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, false, (DateTime)MeetingMoveOrigStartDate);
                //////////    AppointmentDataInfo.SetOriginalMeetingMoveStartDate(curApp, null);
                //////////}
                //////////curApp = singleAppointment;
                // UtilityManager.LogMessage("2D3A- Getting condeco appointment for deleted single occurrence meeting for date:" + origStartDate);
            }
            else if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
            {
                string postID = AppointmentHelper.GetAppointmentPostID(curApp);
                if (UtilityManager.SyncBookingEnabled().Equals(1) && (string.IsNullOrEmpty(postID) || (AppointmentHelper.IsSyncAppointment(curApp))))
                {
                    postID = AppointmentHelper.SyncGetAppointmentPostID(curApp);
                }
                //issue 5964 4April2015
                if (MeetingMoveOrigStartDate == null)
                {
                    currMeeting = bookingHelper.GetCondecoMeetingOccurrenceByDate(postID, curApp, false, curApp.Start);
                }
                else
                {
                    currMeeting = bookingHelper.GetCondecoMeetingOccurrenceByDate(postID, curApp, false,  (DateTime)MeetingMoveOrigStartDate);
                    //currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, false, (DateTime)MeetingMoveOrigStartDate);
                   // AppointmentDataInfo.SetOriginalMeetingMoveStartDate(curApp, null);
                }

                //curApp = singleAppointment;
            }
            else if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptNotRecurring)
            {
                currMeeting = bookingHelper.GetCurrentCondecoMeeting(curApp, false);

            }
            // UtilityManager.LogMessage("2D4- Boking Found in condeco:" + currMeeting.BookingID);
            if (currMeeting.BookingID == 0) return false;

            AppointmentDataInfo.AddAppointmentInCollection(curApp, currMeeting.BookingID);

            int result = 0;
            // UtilityManager.LogMessage("2D5- checking for past booking:" + currMeeting.BookingID);
            // Below appItem.End check to fix the CRRI 6550 ,TP-19968 by Paritosh 
            long meetingTicks1 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
            DateTime meetingStartDateTime1 = new DateTime(meetingTicks1);


            ////DateTime condecoStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime1,
            ////                                                                                    currMeeting.LocationID,
            ////                                                                                    currMeeting.OriginalTZ);
            DateTime condecoStartTimeInLocalTz = currMeeting.BookingDateStartToLocalTZ;

            ////////long meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
            ////////DateTime meetingEndDateTime = new DateTime(meetingTicks);




            ////////DateTime condecoEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime,
            ////////                                                                                       currMeeting.LocationID,
            ////////                                                                                       currMeeting.OriginalTZ);
            DateTime condecoEndTimeInLocalTz = currMeeting.BookingDateEndToLocalTZ;
            if (condecoStartTimeInLocalTz.CompareTo(DateTime.MinValue) != 0 && curApp.Start.CompareTo(condecoStartTimeInLocalTz) != 0 && (condecoStartTimeInLocalTz.CompareTo(DateTime.Now) < 0) && (curApp.Start.CompareTo(condecoStartTimeInLocalTz) < 0))
            {
                // MessageBox.Show("date change to past");
                result = -4;
            }
            else if (condecoEndTimeInLocalTz.CompareTo(DateTime.MinValue) != 0 && (condecoEndTimeInLocalTz.CompareTo(DateTime.Now) < 0))
            {
                result = -6;
            }

            else if (DateTime.Now.CompareTo(curApp.Start) >= 0 && DateTime.Now.CompareTo(curApp.End) > 0)
            {
                // UtilityManager.LogMessage("2D5A- booking is in past can not be modified:" + DateTime.Now);
                result = -200;
            }
            else if (curApp.AllDayEvent)
            {

                result = -400;
            }
            else
            {

                result = bookingHelper.UpdateCondecoBooking(currMeeting, curApp);

            }
            string message = CondecoResources.AppointmentMove_Services_NotAvailable;
            if (result == -1 || result == -999 || result == -100 || result == -200 || result == -300 || result == -400 ||
                result == -2 || result == -3 || result == -4 || result == -5 || result == -100 || result == -99 ||
                result == -97 || result == -98 || result == -81 || result == -82 || result == -83 || result == -84 || result == -85 || result == -86 || result == -6)
            {

                if (result == -1)
                    message = CondecoResources.AppointmentMove_Room_NotAvailable;
                else if (result == -81)
                    message = CondecoResources.Group_SingleDayBookingOnly;
                else if (result == -82)
                    message = CondecoResources.Group_BusinessHourOverLap;
                else if (result == -83)
                    message = CondecoResources.Group_AdvancedBookingPeriod;
                else if (result == -84)
                    message = CondecoResources.Group_NoticeRequired;
                else if (result == -97 || result == -98)
                {
                    // result == -98  for   EN-6795 ADDIN 6.2.4|[GIC] Booking updated from outlook add-in doesn't get updated in TMS.
                    //When Multiple Room With Attendee .
                   // message = CondecoResources.Condeco_Update_Failed_VCBooking;
                  //  User Story 4399:Move non-recurrent meeting with multi room booking in Outlook calendar grid
                    //User Story 4405:Move recurrent meeting (instance) with multi room booking in Outlook calendar grid
                    StringBuilder message1 = new StringBuilder();
                    // message1.AppendLine(CondecoResources.V6UpdateOnlyFromBookingTab);
                    message1.AppendLine(CondecoResources.V6ReSubmitBookingForm);
                    message1.AppendLine();
                    message1.AppendLine(CondecoResources.V6UpdateBookingOKCancel);
                    DialogResult response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                    if (response == DialogResult.OK)
                    {
                        AppointmentDataInfo.SetStartDateChanged(curApp, true);
                        AppointmentDataInfo.SetHavingPermission(curApp, true);
                        object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                        AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                        
                        CurrentModule.IsUndoMeetingChanges = true;
                        CurrentModule.RibbonButtons_OnClick(obj, null, true);
                    }
                    else
                    {
                        AppointmentDataInfo.SetHavingPermission(curApp, true);
                        ////long meetingTicks2 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                        ////DateTime meetingStartDateTime2 = new DateTime(meetingTicks2);
                        ////DateTime condecomeetingStartDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime2,
                        ////                                                                          currMeeting.LocationID,
                        ////                                                                          currMeeting.OriginalTZ);

                        DateTime condecomeetingStartDateTime2LTZ = currMeeting.BookingDateStartToLocalTZ;

                        ////meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                        ////DateTime meetingEndDateTime2 = new DateTime(meetingTicks);
                        ////DateTime condecomeetingEndDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime2,
                        ////                                                                          currMeeting.LocationID,
                        ////                                                                          currMeeting.OriginalTZ);

                        DateTime condecomeetingEndDateTime2LTZ = currMeeting.BookingDateEndToLocalTZ;
                        curApp.Start = condecomeetingStartDateTime2LTZ;
                        curApp.End = condecomeetingEndDateTime2LTZ;
                        curApp.Subject = currMeeting.MeetingTitle;
                        //if( !(curApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && !(curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)))
                        //{
                        curApp.Save();
                        //}
                        //curApp.Start = origStartDate;
                        //curApp.End = origEndDate;
                        //curApp.Save();
                    }
                   
                    return false;
                }
                else if (result == -99)
                {
                    StringBuilder message1 = new StringBuilder();
                    DialogResult response;
                    if ( (curApp.MeetingStatus==Outlook.OlMeetingStatus.olMeeting) &&  (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && currMeeting.IsMappedToExchange)
                    {
                      //  User Story 4402:Move recurrent meeting (instance) with single room booking in Outlook calendar grid
                       /// When  Single day sync boom booking move 
                        message1.AppendLine(CondecoResources.V6SyncBookingSeriesUpdate);
                        message1.AppendLine();
                        message1.AppendLine(CondecoResources.V6UpdateSyncBookingNotAvaliable);
                        response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        if (response == DialogResult.OK)
                        {

                            //Outlook.RecurrencePattern rpItem = curApp.GetRecurrencePattern();
                            //long meetingTicks2 = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            //DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
                            //DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                            //                                                                             currMeeting.LocationID,
                            //                                                                             currMeeting.OriginalTZ);
                            //if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
                            //{
                            //    rpItem.StartTime = condecoSeriesStartTimeInLocalTz;
                            //}

                            //meetingTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            //DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
                            //DateTime condecoSeriesEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesEnd,
                            //                                                                             currMeeting.LocationID,
                            //                                                                             currMeeting.OriginalTZ);
                            //if (condecoSeriesEndTimeInLocalTz.TimeOfDay != rpItem.EndTime.TimeOfDay)
                            //{
                            //    rpItem.EndTime = condecoSeriesEndTimeInLocalTz;

                            //}

                            //curApp.Subject = currMeeting.MeetingTitle;
                            //curApp.Save();
                           
                            //UtilityManager.FreeCOMObject(rpItem);
                            //UtilityManager.FreeCOMObject(curApp);
                        }
                    } 
                    else  if ( (curApp.MeetingStatus==Outlook.OlMeetingStatus.olMeeting) &&  (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && !currMeeting.IsMappedToExchange)
                    {
                        //  User Story 4402:Move recurrent meeting (instance) with single room booking in Outlook calendar grid
                        message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                        message1.AppendLine();
                        message1.AppendLine(CondecoResources.Sync_ConflictRoomMessageAlternative);
                        response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                        if (response == DialogResult.OK)
                        {
                            AppointmentDataInfo.SetStartDateChanged(curApp, true);
                            AppointmentDataInfo.SetHavingPermission(curApp, true);
                            object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                            AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                            CurrentModule.IsUndoMeetingChanges = true;
                            CurrentModule.RibbonButtons_OnClick(obj, null, true);
                        }
                        else
                        {
                            ////long meetingTicks2 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            ////DateTime meetingStartDateTime2 = new DateTime(meetingTicks2);

                            ////DateTime condecomeetingStartDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime2,
                            ////                                                                     currMeeting.LocationID,
                            ////                                                                     currMeeting.OriginalTZ);

                            DateTime condecomeetingStartDateTime2LTZ = currMeeting.BookingDateStartToLocalTZ;

                            ////meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            ////DateTime meetingEndDateTime2 = new DateTime(meetingTicks);

                            ////DateTime condecomeetingEndDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime2,
                            ////                                                                    currMeeting.LocationID,
                            ////                                                                    currMeeting.OriginalTZ);

                            DateTime condecomeetingEndDateTime2LTZ = currMeeting.BookingDateEndToLocalTZ;
                            curApp.Start = condecomeetingStartDateTime2LTZ;
                            curApp.End = condecomeetingEndDateTime2LTZ;
                            curApp.Subject = currMeeting.MeetingTitle;
                            //curApp.Save();
                        }
                    }

                    else
                    {
                        //Meeting Non sync single room Booking 
                        if (curApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && !currMeeting.IsMappedToExchange)
                        {
                           // User Story 4394:Move non-recurrent meeting with single room booking in Outlook calendar grid
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomMessageAlternative);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetStartDateChanged(curApp, true);
                                AppointmentDataInfo.SetHavingPermission(curApp, true);
                                object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                                AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                                CurrentModule.IsUndoMeetingChanges = true;
                                CurrentModule.RibbonButtons_OnClick(obj, null, true);
                            }
                            else
                            {
                                //Bug 5780::Add-in V6.0.5978/Win7/Outlook 2013 - While moving non recurrent meeting with single room booking on unavailable 
                                ////long meetingTicks2 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                                ////DateTime meetingStartDateTime2 = new DateTime(meetingTicks2);
                                ////DateTime condecoMeetingStartDateTimeLTZ = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime2,
                                ////                                                                    currMeeting.LocationID,
                                ////                                                                    currMeeting.OriginalTZ);
                                DateTime condecoMeetingStartDateTimeLTZ = currMeeting.BookingDateStartToLocalTZ;

                                ////meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                                ////DateTime meetingEndDateTime2 = new DateTime(meetingTicks);
                                ////DateTime condecomeetingEndDateTimeLTZ = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime2,
                                ////                                    currMeeting.LocationID,
                                ////                                    currMeeting.OriginalTZ);

                                DateTime condecomeetingEndDateTimeLTZ = currMeeting.BookingDateEndToLocalTZ; 
                                curApp.Start = condecoMeetingStartDateTimeLTZ;
                                curApp.End = condecomeetingEndDateTimeLTZ;
                                curApp.Subject = currMeeting.MeetingTitle;
                                curApp.Save();
                            
                            }

                        }
                        else if (curApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && currMeeting.IsMappedToExchange) 
                        {
                            // // User Story 4394:Move non-recurrent meeting with single room booking in Outlook calendar grid
                            // //Meeting  sync single room Booking 
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.V6UpdateSyncBookingNotAvaliable);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                CurrentModule.IsUndoMeetingChanges = true;
                                //long meetingTicks2 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                                //DateTime meetingStartDateTime2 = new DateTime(meetingTicks2);
                                //meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                                //DateTime meetingEndDateTime1 = new DateTime(meetingTicks);
                                //curApp.Start = meetingStartDateTime2;
                                //curApp.End = meetingEndDateTime;
                                //curApp.Subject = currMeeting.MeetingTitle;
                                //curApp.Save();
                            }

                        }
                        else
                        {
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomMessageForMeeting);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetStartDateChanged(curApp, true);
                                AppointmentDataInfo.SetHavingPermission(curApp, true);
                                object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                                AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                                CurrentModule.IsUndoMeetingChanges = true;
                                CurrentModule.RibbonButtons_OnClick(obj, null, true);
                                //AppointmentDataInfo.SetAppItemSkipSync(curApp, true);
                                //curApp.Display();
                                //CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                            }
                        }
                    }
                  
                    return false;

                }
                else if (result == -200)
                    message = CondecoResources.Booking_Moved_Past;
                else if (result == -400)
                    message = CondecoResources.AllDayEvent_NotSupported;
                else if (result == -999) //added by Anand for permission verification
                    message = CondecoResources.Check_Booking_Permission;

                else if (result == -4)
                {
                    //  message = CondecoResources.BookingStartDateUneditable;
                    message = CondecoResources.Booking_Moved_Past;
                }
                else if (result == -85)
                {
                    //MessageBox.Show("Meeting in progress cant edit");
                    message = CondecoResources.BookingIsInProgress;
                }
                else if (result == -86)
                {
                    message = CondecoResources.AppointmentIsAlreadyClosed;
                }
                else if (result == -6 || result == -3)
                {
                    UtilityManager.LogMessage("Handle appoitnment move past bookign result =" + result);
                    message = CondecoResources.Booking_Update_Past;
                }

                UtilityManager.ShowErrorMessage(message);
                // MessageBox.Show(message, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                //curApp.Start = origStartDate;
                //curApp.End = origEndDate;

                // start changes Fixes issue 8224	Add-in V6.0.6721/Win7/Outlook 2010-The meeting is saved out of business hour on the outlook calendar while Business hour rule is applied

                //long meetingTicksValidateFAIL = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                //DateTime meetingStartDateTimeValidateFAIL = new DateTime(meetingTicksValidateFAIL);
                //DateTime condecomeetingStartDateTimeLTZValidateFAIL = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTimeValidateFAIL,
                //                                                                          currMeeting.LocationID,
                //                                                                          currMeeting.OriginalTZ);

                DateTime condecomeetingStartDateTimeLTZValidateFAIL = currMeeting.BookingDateStartToLocalTZ;
                //meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                //DateTime meetingEndDateTimeValidateFAIL = new DateTime(meetingTicks);
                //DateTime condecomeetingEndDateTimeLTZValidateFAIL = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTimeValidateFAIL,
                //                                                                          currMeeting.LocationID,
                //                                                                          currMeeting.OriginalTZ);

                DateTime condecomeetingEndDateTimeLTZValidateFAIL = currMeeting.BookingDateEndToLocalTZ;
                curApp.Start = condecomeetingStartDateTimeLTZValidateFAIL;
                curApp.End = condecomeetingEndDateTimeLTZValidateFAIL;
                curApp.Subject = currMeeting.MeetingTitle;
          
                //End change
                curApp.Save();

                return false;
            }
            else if (result == 1)
            {
                //Outlook.AppointmentItem appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                UtilityManager.LogMessage("CondecoItemsEventsClass.cs- HandleAppointmentMove(Outlook.AppointmentItem curApp)  (result == 1)");
                string newLocSimple = bookingHelper.GetBookingLocation("", curApp);
                string newLoc = "";
                if (!AppointmentHelper.SyncIsOwaInitiatedBooking(curApp))
                {
                    newLoc = AppointmentHelper.FormatLocationInCondecoStyle(newLocSimple);
                }
                else
                {
                    newLoc = curApp.Location;
                }
                if (curApp.Location == null)
                    curApp.Location = " ";
                if (!String.IsNullOrEmpty(newLoc) && !String.IsNullOrEmpty(curApp.Location))
                {
                    if (!curApp.Location.ToLower().Contains(newLoc.ToLower()))
                    {
                        AppointmentHelper.SetAppointmentLocation(curApp, newLocSimple);
                        // curApp.Location = newLoc;

                        curApp.Save();
                    }
                }

                MessageBox.Show(CondecoResources.Sync_BookingMoved, CondecoResources.Sync_Confirmation, MessageBoxButtons.OK, MessageBoxIcon.None);
                return true;
            }
            return false; 

        }

        public override void ProcessWrite(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            try
            {
                if (!IsEnableWriteEventCheck) return;
                Outlook.AppointmentItem appItem = ItemObj as Outlook.AppointmentItem;
                bool isPartialBooking = false;
                //changed by Anand on 27-Feb-2013 for TP11197 to handle past occurance and exception
                if (appItem != null)
                {
                    //By Anand on 11-March-2013 Moved here as above it might get appItem as null.
                    if (AppointmentDataInfo.GetSkipWriteCheck(appItem)) return;

                    string postId = AppointmentHelper.GetAppointmentPostID(appItem);
                    if (UtilityManager.SyncBookingEnabled().Equals(1) && (string.IsNullOrEmpty(postId) || (AppointmentHelper.IsSyncAppointment(appItem))))
                    {
                        postId = AppointmentHelper.SyncGetAppointmentPostID(appItem);
                    }
                    if (!string.IsNullOrEmpty(postId))
                    {
                        if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            if (!CheckBookingIsInSyncWithCondeco(false, out isPartialBooking))
                            {
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            //not cancelled as having post id
                            if (appItem.Start.CompareTo(appItem.End) == 0)
                            {
                                isAppointmentMoved = false;
                                UtilityManager.ShowErrorMessage(CondecoResources.MeetingDate_Start_End_Same);
                                e.Cancel = true;
                            }
                            else if (origStart < DateTime.Now && !appItem.Location.ToLower().Contains("cancelled"))
                            {
                                //show message that updated is not allowed
                                if (origStart == DateTime.MinValue)
                                {
                                    UtilityManager.ShowErrorMessage(CondecoResources.ObjectIssue_NeedToReopen);
                                    e.Cancel = true;
                                }
                                // Below End date  check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                                else if (origEnd < DateTime.Now)
                                {
                                    UtilityManager.LogMessage("ProcessWrite origEnd=" + Convert.ToString(origEnd));
                                    if (!(Module as AddinModule).IsDatePastUpdateMsgShow)
                                    {
                                        if ((Module as AddinModule).HostMajorVersion > 12)
                                        {
                                          //  UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                                        }
                                        else
                                        {
                                            appItem.Start = OrigStart;
                                            appItem.End = OrigEnd;
                                            appItem.Subject = OrigSubject;
                                            appItem.Save();
                                            e.Cancel = true;
                                        }

                                        // isAppointmentMoved = false;
                                        // UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                                        // IsDatePastMsgShow = true;
                                       
                                    }

                                }
                            }

                            //having start and end delimiter
                        }
                    }
                }
                //if (!CheckBookingIsInSyncWithCondeco())
                //{

                //    e.Cancel = true;
                //}
                /* else if (IsSubjectChanged && bookingHelper.IsCondecoBooking(currApp))
               {
                   MessageBox.Show("The recent changes in the item subject may result in your Calendar and the Room Booking Application being out of synchronisation." + Environment.NewLine + "Please make sure you update your changes using the Book Room Tab and then click on Save and Close (or Send Update if the meeting has attendees).", "Condeco Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   e.Cancel = true;
               }
             */
            }
            catch(Exception ex)
            {
                UtilityManager.LogMessage("CondecoItemEventsClass - ProcessWrite- " + ex.Message + " --Stack ---" + ex.StackTrace );
            }

        }

        public override void ProcessBeforeDelete(object item, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            //Added by Anand on 27-Feb-2013 for TP 11992
            Outlook.AppointmentItem appItem = item as Outlook.AppointmentItem;
            IbookingHelper bookingHelper = new BookingHelper();
          
            if (appItem != null)
            {
                //Changed by Anand : Returning as received mails should not cancel the booking. TP12345
                UtilityManager.LogMessage("1-Checking meeting is not olMeetingReceived");
                if (appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceived) return;
                //End Change
                string postId = AppointmentHelper.GetAppointmentPostID(appItem);
                if (UtilityManager.SyncBookingEnabled().Equals(1) && string.IsNullOrEmpty(postId) && (AppointmentHelper.IsSyncAppointment(appItem, true)))
                {
                    postId = AppointmentHelper.SyncGetAppointmentPostID(appItem);
                }

                if (!string.IsNullOrEmpty(postId))
                {

                    if (!UtilityManager.IsSharedCalendarItem(appItem) && appItem.IsRecurring && (appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster) && (String.IsNullOrEmpty(appItem.Location) || appItem.Location == " ") && (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence))
                    {
                        return;
                    }
                    // Below appItem.End check to fix the CRRI 6550 ,TP-19968 by Paritosh
                    //!appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster is added for past booking delete 
                    if ((appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster) && appItem.Start < DateTime.Now && appItem.Location.Contains(UtilityManager.LocationStart)
                      && appItem.End < DateTime.Now && appItem.Location.Contains(UtilityManager.LocationEnd) && !appItem.Location.ToLower().Contains("cancelled"))
                    {
                        //show message that updated is not allowed
                        UtilityManager.LogMessage("process before delete  past booking appItem.RecurrenceState " + appItem.RecurrenceState);
                        // this below message commented .this msg pop up in olk 2010 when cancel past meeting instance  as well as series
                        //UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                        // e.Cancel = true;
                        return;
                    }
                    if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                    {
                        if (!UtilityManager.IsSSOTimeOutMsgShow)
                        {

                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                        }
                        //UtilityManager.IsSSOTimeOutMsgShow = false;
                        e.Cancel = true;
                        return;
                    }
                    bool permcheck = UtilityManager.PermissionCheck(postId, UtilityManager.GetCurrentUserForPost());

                    // Added Meeting status condition by Paritosh Mishra , to fix CRI2 6507 & CRI2 -6554
                    // if (!(permcheck) )
                    if (!(permcheck) && !((appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled) || (appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceivedAndCanceled)))
                    {
                        UtilityManager.ShowWarningMessage(CondecoResources.Check_Booking_Permission);
                        e.Cancel = true;
                        return;
                    }
                    //chandra for #18802 IsVCWithin1hourCheck in case of reccurance booking 27-Oct-13
                    if (appItem.IsRecurring && appItem.Start.Subtract(DateTime.Now).TotalMinutes < 60)
                    {
                        int bookingID = bookingHelper.GetBookingID(postId, appItem);
                        bookingHelper.IsVCWithin1hourCheck(bookingID, UtilityManager.GetCurrentUserForPost());
                        if (bookingHelper.IsVCWithin1hour)
                        {
                            UtilityManager.ShowErrorMessage(CondecoResources.VcNotDeletedWithinAnHour);
                            e.Cancel = true;
                            return;
                        }
                    }
                    //End- chandra for #18802 IsVCWithin1hourCheck in case of reccurance booking 27-Oct-13










                    try
                    {
                        bool isSharedCalendar = false;
                        if (!UtilityManager.IsSharedCalendarItem(appItem))
                        {
                            return;
                        }
                        else
                        {
                            UtilityManager.SharedCalendarUsed();
                            isSharedCalendar = true;
                        }
                        if (UtilityManager.sharedCalendarVersionList.Count == 0)
                        {
                            return;
                        }
                        // appItem = (this.ItemObj as Outlook.AppointmentItem);

                        if (!isSharedCalendar || !UtilityManager.sharedCalendarVersionList.Contains(appItem.OutlookVersion))
                        {
                            return;
                        }

                        //if (currFolder.Name.ToLower().Equals("deleted items") || currFolder.DefaultItemType == Outlook.OlItemType.olMailItem)
                        //{
                        //   
                        //}
                        if (appItem.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting && !appItem.IsRecurring)
                        {
                            // bookingHelper = new BookingHelper();
                            bookingHelper.DeleteCondecoBooking(appItem);
                            bookingHelper = null;
                            return;
                        }
                        bool deleteSeries = false;
                        if ((appItem.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting) && (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException))
                        {
                            string msg = CondecoAddinV2.App_Resources.CondecoResources.SharedCalendarAppDeleteMessage;
                            msg = String.Format(msg, Environment.NewLine);
                            // DialogResult response = MessageBox.Show("The meeting you are removing is linked to a recurring room booking in the Room Booking system.  " + Environment.NewLine + " Click 'Yes' to cancel all instances of the room booking. " + Environment.NewLine + " Click 'No' if you wish to just remove just this instance of the room booking. " + Environment.NewLine + " Alternatively click 'Cancel' to cancel this operation. ", CondecoResources.Condeco_Error_Caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                            DialogResult response = MessageBox.Show(msg, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                            if (response == DialogResult.Yes)
                            {
                                deleteSeries = true;
                            }
                            else if (response == DialogResult.Cancel)
                            {
                                e.Cancel = true;
                                return;
                            }
                            else if (response == DialogResult.No)
                            {
                                deleteSeries = false;
                            }
                            try
                            {
                                if (!deleteSeries)
                                {
                                    // Outlook.AppointmentItem ItemtoDelete = item as Outlook.AppointmentItem;
                                    bookingHelper.DeleteCondecoBooking(appItem, false);
                                    string location = bookingHelper.GetBookingLocation(postId, appItem);
                                    appItem.Location = location;
                                    if (String.IsNullOrEmpty(location))
                                    {
                                        // Outlook.AppointmentItem ItemtoDelete = appItem as Outlook.AppointmentItem;

                                        CurrentModule.SendMessage(WM_MYToolBar, IntPtr.Zero, IntPtr.Zero);
                                        CurrentModule.curritemAppTobeDeleted = appItem;
                                        UtilityManager.LogMessage("CondecoItemEventsClass.ProcessBeforeDelete shared calendar with nolocation booking");
                                        // appItem.Delete();
                                        e.Cancel = true;
                                        return;

                                    }
                                    else
                                    {
                                        try
                                        {
                                            appItem.Save();
                                            e.Cancel = true;
                                        }
                                        catch
                                        {
                                        }
                                        return;
                                    }
                                    // UtilityManager.FreeCOMObject(ItemtoDelete);


                                }
                                else
                                {
                                    Outlook.AppointmentItem ItemtoDelete = appItem.Parent as Outlook.AppointmentItem;
                                    bookingHelper.DeleteCondecoBooking(ItemtoDelete, true);

                                    CurrentModule.curritemAppTobeDeleted = ItemtoDelete;
                                    CurrentModule.SendMessage(WM_MYToolBar, IntPtr.Zero, IntPtr.Zero);



                                    //UtilityManager.FreeCOMObject(ItemtoDelete);
                                }
                            }
                            catch (Exception ex)
                            {
                                UtilityManager.LogMessage("Excetion at CondecoItemEventsClass.ProcessBeforeDelete" + ex.Message);
                            }
                        }

                    }
                    catch (Exception)
                    {

                    }
                    bookingHelper = null;

                }
            }
            //end

            //MessageBox.Show("Item Before Delete Fires");




        }

        public override void ProcessAttachmentRemove(object attachment)
        {
            ////Below code are added by Paritosh to resolve CRD-6520
            //Outlook.AppointmentItem currApp = this.ItemObj as Outlook.AppointmentItem;
            //if (bookingHelper.IsCondecoBooking(currApp))
            //{
            //    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
            //    {
            //        if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
            //        {
            //            try
            //            {
            //                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessAttachmentRemove before remove attachment");
            //                currApp.Attachments.Remove(1);
            //                currApp.Save();
            //                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessAttachmentRemove after remove attachment");
            //            }
            //            catch
            //            {
            //                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessAttachmentRemove catch");
            //            }
            //        }
            //    }
            //}
            //// end // 
        }

        public override void ProcessBeforeAttachmentAdd(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAttachmentPreview(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAttachmentRead(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAttachmentWriteToTempFile(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessUnload()
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAutoSave(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeRead()
        {
            // TODO: Add some code
        }

        public override void ProcessAfterWrite()
        {
            // TODO: Add some code
        }

        private bool CheckBookingIsInSyncWithCondeco( out bool isPartialBooking )
        {
            return CheckBookingIsInSyncWithCondeco(true,out isPartialBooking);
        }

        private bool CheckBookingIsInSyncWithCondeco(bool returnTrueForCancel , out bool isPartialBooking )
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco:*********Started********");
            Outlook.AppointmentItem currApp = null;
            IbookingHelper bookingHelper = new BookingHelper();
            bool result = true;
            try{
             currApp = this.ItemObj as Outlook.AppointmentItem;
            
                }
            catch
            {
                // Added by Ravi Goyal - if item is olApptException, com object was released,recreate it from inspector object
                Outlook.Inspector ins = CurrentModule.OutlookApp.ActiveInspector();
                if (ins != null)
                {
                    object item = ins.CurrentItem;
                    if (item != null)
                    {
                        if (item is Outlook.AppointmentItem)
                        {
                            currApp = item as Outlook.AppointmentItem;
                        }
                    }
                }
            }
            //Outlook.UserProperties uProps = currApp.UserProperties;
            //bool bookRoomClicked = false;
            //if (uProps != null)
            //{
            //    Outlook.UserProperty propBookRoom = uProps.Find("BookRoomClicked", true);

            //    if (propBookRoom != null)
            //    {
            //        bookRoomClicked = Convert.ToBoolean(propBookRoom.Value.ToString());
            //        UtilityManager.FreeCOMObject(propBookRoom);
            //    }

            //}
            //Outlook.UserProperty prop = uProps.Find("AppointmentLocationSynced", true);
            bool bookRoomClicked = false;
            string namedValue = "";
            namedValue = UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.BookRoomClicked);

            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco - UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.BookRoomClicked) NamedValue :" + namedValue);
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco - AddinModule.IsBookRoomClicked :" + AddinModule.IsBookRoomClicked);

            if (!String.IsNullOrEmpty(namedValue))
            {
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco - UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.BookRoomClicked) NamedValue is not null");
                bookRoomClicked = Convert.ToBoolean(namedValue.ToString());
            }
            else//TP#21109 Added by Vineet Yadav on 15 Feb 2014
            {
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco - UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.BookRoomClicked) NamedValue is NullOrEmpty");
                bookRoomClicked = Convert.ToBoolean(AddinModule.IsBookRoomClicked);
            }

            bool isCondecoBooking = bookingHelper.IsCondecoBooking(currApp);

            if (isCondecoBooking)
            {
                isPartialBooking = false;
                if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                {
                    if (!UtilityManager.IsSSOTimeOutMsgShow)
                    {

                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    }
                    //UtilityManager.IsSSOTimeOutMsgShow = false;
                    return true;
                }
            }
            else
            {
                isPartialBooking = true;
            }
            bool appointmentCancelled = false;
            //if (prop != null)
            //{
            //    appointmentCancelled = true;u
            //    UtilityManager.FreeCOMObject(prop);
            //}
            //UtilityManager.FreeCOMObject(uProps);
            string namedValue1 = "";
            namedValue1 = UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.AppointmentLocationSynced);
            if (!String.IsNullOrEmpty(namedValue1))
            {
                appointmentCancelled = true;
            }

            if (!isCondecoBooking || appointmentCancelled) return true;
            if (isCondecoBooking && IsRecurrenceRemoved)
            {
                // bookingHelper.DeleteCondecoBooking(currApp);
                bookingHelper.RemoveSeriesFromCondeco(currApp);
                return true;
            }
         

            if (bookRoomClicked || CurrentModule.IsMeetingSaveFromWeb)
                IsCondecoBookingSaved = bookingHelper.IsCondecoBookingSaved(currApp);

            if ((bookRoomClicked || CurrentModule.IsMeetingSaveFromWeb) && isCondecoBooking && IsCondecoBookingSaved)
            {
                CurrentModule.IsMeetingSaveFromWeb = false;
              //  int i = 0;
            }
            else
            {
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(currApp, true);
                CurrentModule.IsMeetingSaveFromWeb = false;
                if (currMeeting.BookingID > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    // || IsSubjectChanged
                    bool subjectReallyChanged = true;
                    bool startDateReallyChanged = true;
                    bool endDateReallyChanged = true;
                    if ((IsSubjectChanged && !IsCondecoBookingSaved))
                    {
                        //Added by Anand On 1-Feb-2013 if setting subject empty its coming null and giving error below.
                        if (currApp.Subject == null) currApp.Subject = " ";
                        //if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        // {
                        if (currApp.Subject.ToLower().Equals(currMeeting.MeetingTitle.ToLower()))
                        {
                            subjectReallyChanged = false;
                        }
                        // }
                        if (subjectReallyChanged)
                            sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_Subject);
                        // IsSubjectChanged = false;
                    }
                    //|| IsStartDateChanged
                    //bool seriesChanged = false;
                    long currentTicks;
                    Outlook.RecurrencePattern rpItem = null;
                    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        rpItem = currApp.GetRecurrencePattern();
                        //if (!bookRoomClicked && !IsCondecoBookingSaved && (IsStartDateChanged || IsEndDateChanged))
                        // {
                        //     seriesChanged = true;
                        // }
                    }

                    if ((IsStartDateChanged && !IsCondecoBookingSaved))
                    {

                        DateTime condecoStart = DateTime.MinValue;
                        DateTime itemStart = DateTime.MinValue;
                        if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            rpItem = currApp.GetRecurrencePattern();
                            currentTicks = rpItem.PatternStartDate.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
                            itemStart = new DateTime(currentTicks);
                            itemStart = UtilityManager.ConvertDateToTZ(itemStart, currMeeting.LocationID, currMeeting.OriginalTZ);
                            //Changed by Anand for Issue 5 for Case TC10 & TC17
                            //currentTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            //changed again currentTicks = OrigStart.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            currentTicks = origSeriesStart.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            //Changed end
                            condecoStart = new DateTime(currentTicks);


                        }
                        else
                        {
                            currentTicks = currApp.Start.Date.Ticks + currApp.Start.TimeOfDay.Ticks;
                            itemStart = new DateTime(currentTicks);
                            itemStart = UtilityManager.ConvertDateToTZ(itemStart, currMeeting.LocationID);
                            currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            condecoStart = new DateTime(currentTicks);
                        }

                        if (itemStart.CompareTo(condecoStart) == 0)
                        {
                            startDateReallyChanged = false;
                        }


                        if (startDateReallyChanged)
                            sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_StartDate);
                        // IsStartDateChanged = false;
                    }
                    // || IsEndDateChanged

                    if ((IsEndDateChanged && !IsCondecoBookingSaved))
                    {
                        DateTime condecoEnd = DateTime.MinValue;
                        DateTime itemEnd = DateTime.MinValue;
                        if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            rpItem = currApp.GetRecurrencePattern();
                            currentTicks = rpItem.PatternEndDate.Date.Ticks + rpItem.EndTime.TimeOfDay.Ticks;
                            itemEnd = new DateTime(currentTicks);
                            itemEnd = UtilityManager.ConvertDateToTZ(itemEnd, currMeeting.LocationID, currMeeting.OriginalTZ);
                            //Changed by Anand for Issue 5 for Case TC10 & TC17
                            //currentTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            //changed again currentTicks = OrigEnd.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            currentTicks = origSeriesEnd.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            //Changed end
                            condecoEnd = new DateTime(currentTicks);


                        }
                        else
                        {
                            currentTicks = currApp.End.Date.Ticks + currApp.End.TimeOfDay.Ticks;
                            itemEnd = new DateTime(currentTicks);
                            itemEnd = UtilityManager.ConvertDateToTZ(itemEnd, currMeeting.LocationID);
                            currentTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            condecoEnd = new DateTime(currentTicks);
                        }
                        if (rpItem != null)
                            UtilityManager.FreeCOMObject(rpItem);

                        if (itemEnd.CompareTo(condecoEnd) == 0)
                        {
                            endDateReallyChanged = false;
                        }

                        if (endDateReallyChanged)
                            sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_EndDate);
                        // IsEndDateChanged = false;
                    }



                    if (!string.IsNullOrEmpty(sb.ToString()))
                    {

                        //Please make sure you update your changes using the Book Room Tab and then click on Save and Close (or Send Update if the meeting has attendees)
                        if (!IsAlertDisplayed)
                        {
                            if (currApp.IsRecurring && currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                            {
                                if (startDateReallyChanged || endDateReallyChanged)
                                    (Module as AddinModule).isItemNotInSync = true;
                                if (!returnTrueForCancel)
                             {
                                // AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currApp, false);
                                 result = V6HandleSeriesUpdate();
                             }
                              else
                             {
                                result = HandleSeriesRequest2(sb.ToString(), returnTrueForCancel);
                             }
                              
                            }
                            else
                            {
                                if (startDateReallyChanged || endDateReallyChanged)
                                    (Module as AddinModule).isItemNotInSync = true;

                                if (!IsDatePastMsgShow)
                                {
                                    if (!AppointmentDataInfo.GetAppItemSkipSync(currApp) &&   AppointmentDataInfo.GetAppItemIsAppItemSaveAndCloseClick(currApp))
                                    {
                                        AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currApp, false);
                                        result = HandleNormalRequest(currMeeting, currApp, sb.ToString());
                                    }

                                }
                                else
                                {
                                    IsDatePastMsgShow = false;
                                }
                            }
                            if (result) IsAlertDisplayed = true;
                        }

                    }
                }
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco:*********Finished********");
            }
            return result;
        }

        private bool HandleNormalRequest(CondecoMeeting currMeeting, Outlook.AppointmentItem currApp, string currentMessage)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:*********Started********");
            bool result = true;
            (Module as AddinModule).IsDatePastUpdateMsgShow = true;
            IbookingHelper bookingHelper = new BookingHelper();
            //Commented by Paritosh Mishra for V6 Addin .Make a Update booking have same workflow as appointment move on calender

            //StringBuilder message = new StringBuilder();
            //message.AppendLine(CondecoResources.OutofSync_Message_Start);
            //message.AppendLine(currentMessage);
            //message.AppendLine(CondecoResources.OutofSync_Message_Options);
            //UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:SyncMessage:" + message.ToString());
            //DialogResult response = MessageBox.Show(message.ToString(), CondecoResources.Condeco_Error_Caption,
            //    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            //if (response == DialogResult.Yes)
            //{
                if (!UtilityManager.PermissionCheck(currMeeting.PostID, UtilityManager.GetCurrentUserForPost()))
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.Check_Booking_Permission);
                    return result = false;
                }

                // Below  check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                if (currApp.Start.CompareTo(currApp.End) == 0)
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.MeetingDate_Start_End_Same);
                  //  MessageBox.Show(CondecoResources.MeetingDate_Start_End_Same, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;
                }


                long meetingTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                DateTime meetingStartDateTime = new DateTime(meetingTicks);
                meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                DateTime meetingEndDateTime = new DateTime(meetingTicks);
                DateTime currentDateTime = UtilityManager.ConvertDateToTZ(DateTime.Now, currMeeting.LocationID);
                // Below replace || to && check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                // !currApp.IsRecurring is added for past booking reucrrance 
                if (currentDateTime.CompareTo(meetingStartDateTime) >= 0 && currentDateTime.CompareTo(meetingEndDateTime) > 0)
                {
                    UtilityManager.LogMessage("handleNormalrequest past booking");
                    UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                   // MessageBox.Show(CondecoResources.Booking_Update_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;
                }
                if (DateTime.Now.CompareTo(currApp.Start) >= 0 && DateTime.Now.CompareTo(currApp.End) > 0)
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.Booking_Moved_Past);
                    //MessageBox.Show(CondecoResources.Booking_Moved_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;
                }
                //fix the CRRI2 6550 ,CRI26043,TP-19968 by Paritosh
                //if (meetingStartDateTime.CompareTo(DateTime.Now) < 0)
                //{
                //    if (meetingStartDateTime.CompareTo(currApp.Start) != 0)
                //    {
                //        MessageBox.Show(CondecoResources.Booking_Update_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        MessageBox.Show("Dont change start date");
                //        return result = false;
                //    }
                //}
                //
                //24/jan/2004
                //  if (meetingStartDateTime.CompareTo(DateTime.MinValue) != 0 && currApp.Start.CompareTo(meetingStartDateTime) != 0 &&  (meetingStartDateTime.CompareTo(DateTime.Now) < 0))
                if (meetingStartDateTime.CompareTo(DateTime.MinValue) != 0 && currApp.Start.CompareTo(meetingStartDateTime) != 0 && (meetingStartDateTime.CompareTo(DateTime.Now) < 0) && (meetingStartDateTime.CompareTo(DateTime.Now) < 0) && (currApp.Start.CompareTo(meetingStartDateTime) < 0))
                {
                    // MessageBox.Show("date change to past");
                    UtilityManager.ShowErrorMessage(CondecoResources.Booking_Moved_Past);
                   // MessageBox.Show(CondecoResources.Booking_Moved_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;

                }

                if (currApp.AllDayEvent)
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.AllDayEvent_NotSupported);
                   // MessageBox.Show(CondecoResources.AllDayEvent_NotSupported, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;
                }


             //   if (!bookingHelper.CheckCrossDateBookingDueToTimeZoneDifference(currMeeting, currApp, true)) return false;


                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:Going to Uupdate in condeco:" + currApp.Start + ":subject:" + currApp.Subject);

                //Added by Anand on 29 Jan 2013 for TP issue 9947 in one scenario when save failed but updadatecondecobooking passed
                //Save the appointment before sending to update so no inconsistency if further save failed
                if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting) //added on 20Jan2015
                {
                    currApp.Save();
                }
               
                //end change

                //Added by Anand on 1-Feb-2013 for TP10850
                //Validation code to prevent the invalid data update on direct save and close
                result = ValidationManager.ValidateAppointment(currApp, true);
                if (!result) return false;
                //End change
                string messageToDisplay = string.Empty;
                int updateResponse = bookingHelper.UpdateCondecoBooking(currMeeting, currApp);
                if (updateResponse == 1)
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:update is successful in condeco");
                   // bool dataFound = false;
                    string postid = AppointmentHelper.GetAppointmentPostID(currApp);
                    if (UtilityManager.SyncBookingEnabled().Equals(1) && (string.IsNullOrEmpty(postid) || (AppointmentHelper.IsSyncAppointment(currApp))))
                    {
                        postid = AppointmentHelper.SyncGetAppointmentPostID(currApp);
                    }
                  //  DataSet condecoDataSet = UtilityManager.GetMeetingFromCondeco(postid);
                    DataSet condecoDataSet = UtilityManager.GetMeetingFromCondeco(postid, currApp);
                    if (condecoDataSet.Tables.Count < 7)
                    {
                        //dataFound = false;
                    }
                    else
                    {
                        if (condecoDataSet.Tables[6].Rows.Count > 0)
                        {
                            AppointmentHelper.SaveAppointmentRecord(currApp, condecoDataSet.GetXml());
                           // dataFound = true;
                        }
                    }
                    CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(currApp, false);
                    string newLocSimple = curMeeting.MainLocation;
                    string newLoc = "";

                    if (!AppointmentHelper.SyncIsOwaInitiatedBooking(currApp))
                    {
                        newLoc = AppointmentHelper.FormatLocationInCondecoStyle(newLocSimple);
                    }
                    else
                    {
                        newLoc = currApp.Location;
                    }
                    if (currApp.Location == null)
                        currApp.Location = " ";
                    if (!String.IsNullOrEmpty(newLoc) && !String.IsNullOrEmpty(currApp.Location))
                    {
                        if (!currApp.Location.ToLower().Contains(newLoc.ToLower()))
                        {
                            //    currApp.Location = newLoc;
                            AppointmentHelper.SetAppointmentLocation(currApp, newLocSimple);

                        }
                    }
                    if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                    {
                        currApp.Save();
                    }
                    //Feature  4150: Outlook Add-in tactical fixes Ref 01
                    MessageBox.Show(CondecoResources.Sync_BookingMoved, CondecoResources.Sync_Confirmation, MessageBoxButtons.OK, MessageBoxIcon.None);
                    result = true;
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:appointment is updated");
                }
                else if (updateResponse == -97 || updateResponse == -98)
                {
                    // result == -98  for   EN-6795 ADDIN 6.2.4|[GIC] Booking updated from outlook add-in doesn't get updated in TMS.
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:Update failed");
                  //  UtilityManager.ShowErrorMessage(CondecoResources.Condeco_Update_Failed_VCBooking);
                    //User Story 4413:Change recurrent appointment (instance) with multi room booking in Appointment window
                    //Multiple room  Update
                    //message = CondecoResources.Condeco_Update_Failed_VCBooking;
                    //User Story 4174:Change non-recurrent appointment with multi room booking in Appointment
                    // User Story 4401:Change non-recurrent meeting with multi room booking in Appointment
                   // User Story 4407:Change recurrent meeting (instance) with multi room booking in Appointment
                   // User Story 4413:Change recurrent appointment (instance) with multi room booking in Appointment window
                    StringBuilder message1 = new StringBuilder();
                    ////////////////////////////message1.AppendLine(CondecoResources.V6UpdateBookingOKCancel);
                    message1.AppendLine(CondecoResources.V6ReSubmitBookingForm);
                    message1.AppendLine();
                    message1.AppendLine(CondecoResources.V6UpdateBookingOKCancel);
                    //27Jan2015
                    //message1.AppendLine(CondecoResources.V6NoUpdateRoomBookingNoCancel);
                   // message1.AppendLine(CondecoResources.Recurrence_Series_Discard_Instruction);

                    DialogResult response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                    if (response == DialogResult.OK)
                    {
                        AppointmentDataInfo.SetStartDateChanged(currApp, true);
                        AppointmentDataInfo.SetHavingPermission(currApp, true);
                        object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                        AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                        CurrentModule.RibbonButtons_OnClick(obj, null, true);
                        AppointmentDataInfo.SetAppItemSkipSync(currApp, true);
                        if (currApp.MeetingStatus != Outlook.OlMeetingStatus.olNonMeeting)
                        {
                            CurrentModule.IsUndoMeetingChanges = true;
                        }

                    }
                    else
                    {
                        ////meetingTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                        ////DateTime meetingStartDateTime1 = new DateTime(meetingTicks);
                        ////DateTime condecomeetingStartDateTime1LTZ = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime1,
                        ////                                                                         currMeeting.LocationID,
                        ////                                                                         currMeeting.OriginalTZ);
                        ////meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                        ////DateTime meetingEndDateTime1 = new DateTime(meetingTicks);
                        ////DateTime condecomeetingEndDateTime1LTZ = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime1,
                        ////                                                                         currMeeting.LocationID,
                        ////                                                                         currMeeting.OriginalTZ);
                        DateTime condecomeetingStartDateTime1LTZ = currMeeting.BookingDateStartToLocalTZ;
                        DateTime condecomeetingEndDateTime1LTZ = currMeeting.BookingDateEndToLocalTZ;
                        currApp.Start = condecomeetingStartDateTime1LTZ;
                        currApp.End = condecomeetingEndDateTime1LTZ;
                        currApp.Subject = currMeeting.MeetingTitle;
                        currApp.Save();
                    }
                    message1 = null;
                    return false;
                }
                else if (updateResponse == -81)
                    messageToDisplay = CondecoResources.Group_SingleDayBookingOnly;
                else if (updateResponse == -82)
                    messageToDisplay = CondecoResources.Group_BusinessHourOverLap;
                else if (updateResponse == -83)
                    messageToDisplay = CondecoResources.Group_AdvancedBookingPeriod;
                else if (updateResponse == -84)
                    messageToDisplay = CondecoResources.Group_NoticeRequired;
                else if (updateResponse == -85)
                {

                    messageToDisplay = CondecoResources.BookingIsInProgress;
                }
                else if (updateResponse == -86)
                {
                    messageToDisplay = CondecoResources.AppointmentIsAlreadyClosed;
                }
                else if (updateResponse == -999) //added by Anand for permission verification
                {
                    messageToDisplay = CondecoResources.Check_Booking_Permission;
                }
                //Added  updateResponse ==-99 switch when  Room in  unavaliability     by Paritosh Mishra for V6 Addin .Make a Update booking have same workflow as appointment move on calender
               else if(updateResponse ==-99)
                {
                     StringBuilder message1 = new StringBuilder();
                    DialogResult response;
                    if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting  && (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && currMeeting.IsMappedToExchange)
                    {
                        //User Story 4410:Change recurrent appointment (instance) with single room booking in Appointment window
                        //When  Single day sync boom booking move 
                        message1.AppendLine(CondecoResources.V6SyncBookingSeriesUpdate);
                        message1.AppendLine();
                        message1.AppendLine(CondecoResources.V6UpdateSyncBookingNotAvaliable);
                        
                        response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        if (response == DialogResult.OK)
                        {
                            AppointmentDataInfo.SetAppItemSkipSync(currApp, true);
                            //CurrentModule.IsUndoMeetingChanges = true;
                            //long meetingTicks1 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            //DateTime meetingStartDateTime1 = new DateTime(meetingTicks);
                            //meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            //DateTime meetingEndDateTime1 = new DateTime(meetingTicks);
                            //currApp.Start = meetingStartDateTime;
                            //currApp.End = meetingEndDateTime;
                            //currApp.Subject = currMeeting.MeetingTitle;
                            //currApp.Save();
                        }
                    }
                    else if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting && (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && !currMeeting.IsMappedToExchange)
                    {
                        //User Story 4410:Change recurrent appointment (instance) with single room booking in Appointment window
                        message1.AppendLine(CondecoResources.V6SyncBookingSeriesUpdate);
                        message1.AppendLine();
                        message1.AppendLine(CondecoResources.Sync_ConflictRoomMessageForMeeting);

                        response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        if (response == DialogResult.OK)
                        {
                           // bool bln = AppointmentDataInfo.GetAppItemSkipSync(currApp);
                            AppointmentDataInfo.SetStartDateChanged(currApp, true);
                            AppointmentDataInfo.SetHavingPermission(currApp, true);
                            object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                            AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                            //  CurrentModule.IsUndoMeetingChanges = true;
                            CurrentModule.RibbonButtons_OnClick(obj, null, true);
                            AppointmentDataInfo.SetAppItemSkipSync(currApp, true);

                        }
                    }
                    else if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting &&(currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && !currMeeting.IsMappedToExchange)
                    {
                       //  User Story 4404:Change recurrent meeting (instance) with single room booking in Appointment
                        message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                        message1.AppendLine();
                        message1.AppendLine(CondecoResources.Sync_ConflictRoomMessageAlternative);
                        response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                        if (response == DialogResult.OK)
                        {
                            bool bln = AppointmentDataInfo.GetAppItemSkipSync(currApp);
                            AppointmentDataInfo.SetStartDateChanged(currApp, true);
                            AppointmentDataInfo.SetHavingPermission(currApp, true);
                            object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                            AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                            CurrentModule.IsUndoMeetingChanges = true;
                            CurrentModule.RibbonButtons_OnClick(obj, null, true);
                            AppointmentDataInfo.SetAppItemSkipSync(currApp, true);

                        }
                        else
                        {
                            ////long meetingTicks2 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            ////DateTime meetingStartDateTime2 = new DateTime(meetingTicks2);
                            ////DateTime condecomeetingStartDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime2,
                            ////                                                                    currMeeting.LocationID,
                            ////                                                                    currMeeting.OriginalTZ);
                            ////meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            ////DateTime meetingEndDateTime2 = new DateTime(meetingTicks);
                            ////DateTime condecomeetingEndDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime2,
                            ////                                                                   currMeeting.LocationID,
                            ////                                                                   currMeeting.OriginalTZ);

                            DateTime condecomeetingStartDateTime2LTZ = currMeeting.BookingDateStartToLocalTZ;
                            DateTime condecomeetingEndDateTime2LTZ = currMeeting.BookingDateEndToLocalTZ;

                            currApp.Start = condecomeetingStartDateTime2LTZ;
                            currApp.End = condecomeetingStartDateTime2LTZ;
                            currApp.Subject = currMeeting.MeetingTitle;
                            //currApp.Save();
                        }
                    }
                    else if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && currMeeting.IsMappedToExchange)
                    {
                        //  User Story 4404:Change recurrent meeting (instance) with single room booking in Appointment
                        message1.AppendLine(CondecoResources.V6SyncBookingSeriesUpdate);
                        message1.AppendLine();
                        message1.AppendLine(CondecoResources.V6UpdateSyncBookingNotAvaliable);

                        response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        if (response == DialogResult.OK)
                        {
                            AppointmentDataInfo.SetAppItemSkipSync(currApp, true);
                            CurrentModule.IsUndoMeetingChanges = true;
                           
                        }
                        
                    }
                   
                    else
                    {
                        if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && !currMeeting.IsMappedToExchange)
                        {
                            //User Story 4396:Change non-recurrent meeting with single room booking in Appointment window
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomMessageAlternative);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                bool bln = AppointmentDataInfo.GetAppItemSkipSync(currApp);
                                AppointmentDataInfo.SetStartDateChanged(currApp, true);
                                AppointmentDataInfo.SetHavingPermission(currApp, true);
                                object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                                AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                                CurrentModule.IsUndoMeetingChanges = true;
                                CurrentModule.RibbonButtons_OnClick(obj, null, true);
                                AppointmentDataInfo.SetAppItemSkipSync(currApp, true);
                               
                            }
                            else
                            {
                                ////long meetingTicks2 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                                ////DateTime meetingStartDateTime2 = new DateTime(meetingTicks2);
                                ////DateTime condecomeetingStartDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime2,
                                ////                                                               currMeeting.LocationID,
                                ////                                                               currMeeting.OriginalTZ);
                                ////meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                                ////DateTime meetingEndDateTime2 = new DateTime(meetingTicks);
                                ////DateTime condecomeetingEndDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime2,
                                ////                                                               currMeeting.LocationID,
                                ////                                                               currMeeting.OriginalTZ);
                                DateTime condecomeetingStartDateTime2LTZ   =currMeeting.BookingDateStartToLocalTZ;
                                DateTime condecomeetingEndDateTime2LTZ = currMeeting.BookingDateEndToLocalTZ;

                                currApp.Start = condecomeetingStartDateTime2LTZ;
                                currApp.End = condecomeetingEndDateTime2LTZ;
                                currApp.Subject = currMeeting.MeetingTitle;
                                currApp.Save();
                            }
                        }
                        else if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && currMeeting.IsMappedToExchange)
                        {
                            //User Story 4396:Change non-recurrent meeting with single room booking in Appointment window
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.V6UpdateSyncBookingNotAvaliable);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                //bool bln = AppointmentDataInfo.GetAppItemSkipSync(currApp);
                                //AppointmentDataInfo.SetStartDateChanged(currApp, true);
                                //AppointmentDataInfo.SetHavingPermission(currApp, true);
                                //object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                                //AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                                //CurrentModule.RibbonButtons_OnClick(obj, null, true);
                                CurrentModule.IsUndoMeetingChanges = true;
                                AppointmentDataInfo.SetAppItemSkipSync(currApp, true);

                            }
                        }
                        else if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting && !currMeeting.IsMappedToExchange)
                        {
                          //  User Story 4162:Change non-recurrent appointment with single room booking in Appointment
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomMessageAlternative);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                bool bln = AppointmentDataInfo.GetAppItemSkipSync(currApp);
                                AppointmentDataInfo.SetStartDateChanged(currApp, true);
                                AppointmentDataInfo.SetHavingPermission(currApp, true);
                                object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                                AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                                //  CurrentModule.IsUndoMeetingChanges = true;
                                CurrentModule.RibbonButtons_OnClick(obj, null, true);
                                AppointmentDataInfo.SetAppItemSkipSync(currApp, true);
                            }
                            else
                            {
                                ////long meetingTicks2 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                                ////DateTime meetingStartDateTime2 = new DateTime(meetingTicks2);
                                ////DateTime condecomeetingStartDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime2,
                                ////                                                             currMeeting.LocationID,
                                ////                                                             currMeeting.OriginalTZ);
                                ////meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                                ////DateTime meetingEndDateTime2 = new DateTime(meetingTicks);
                                ////DateTime condecomeetingEndDateTime2LTZ = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime2,
                                ////                                                            currMeeting.LocationID,
                                ////                                                            currMeeting.OriginalTZ);

                                DateTime condecomeetingStartDateTime2LTZ = currMeeting.BookingDateStartToLocalTZ;
                                DateTime condecomeetingEndDateTime2LTZ = currMeeting.BookingDateEndToLocalTZ; 

                                currApp.Start = condecomeetingStartDateTime2LTZ;
                                currApp.End = condecomeetingEndDateTime2LTZ;
                                currApp.Subject = currMeeting.MeetingTitle;
                                currApp.Save();
                            }
                        }
                        else if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting && currMeeting.IsMappedToExchange)
                        {
                            //  User Story 4162:Change non-recurrent appointment with single room booking in Appointment
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.V6UpdateSyncBookingNotAvaliable);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {

                                AppointmentDataInfo.SetAppItemSkipSync(currApp, true);

                            }
                        }
                        else
                        {
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomMessageForMeeting);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                bool bln = AppointmentDataInfo.GetAppItemSkipSync(currApp);
                                AppointmentDataInfo.SetStartDateChanged(currApp, true);
                                AppointmentDataInfo.SetHavingPermission(currApp, true);
                                object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                                AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                                //  CurrentModule.IsUndoMeetingChanges = true;
                                CurrentModule.RibbonButtons_OnClick(obj, null, true);
                                if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                                {
                                    AppointmentDataInfo.SetAppItemSkipSync(currApp, true);
                                }
                                else
                                {
                                    AppointmentDataInfo.SetAppItemSkipSync(currApp, true);
                                    CurrentModule.IsUndoMeetingChanges = true;
                                }
                                //curApp.Display();
                                //CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                            }
                        }
                    }
                    message1 = null;
                    result = false;
                }
                else
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.Condeco_Update_Failed);
                    // MessageBox.Show(CondecoResources.Condeco_Update_Failed, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    result = false;
                }
                if (!string.IsNullOrEmpty(messageToDisplay))
                {
                    UtilityManager.ShowErrorMessage(messageToDisplay);
                    //MessageBox.Show(messageToDisplay, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    result = false;
                }

           // }
            //Changed by Anand on 14-Feb-2013 and add the condition for Cancel
            //else if (response == DialogResult.No)


            ////////else if (response == DialogResult.No || response == DialogResult.Cancel)
            ////////{
            ////////    try
            ////////    {
            ////////        UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:Reverting the changes");
            ////////        IsEnableWriteEventCheck = false;
            ////////        if (origStart != DateTime.MinValue)
            ////////        {
            ////////            currApp.Start = OrigStart;
            ////////            currApp.End = OrigEnd;
            ////////            currApp.Subject = OrigSubject;
            ////////            currApp.Save();
            ////////        }
            ////////    }
            ////////    catch
            ////////    {
            ////////    }
            ////////    finally
            ////////    {
            ////////        IsEnableWriteEventCheck = true;
            ////////    }
            ////////    result = true;
            ////////}
            ////////else
            ////////{
            ////////    result = false;
            ////////}
            (Module as AddinModule).IsDatePastUpdateMsgShow = false;
            bookingHelper = null;
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:*********Finished********");
            return result;
        }

        private bool HandleSeriesRequest2(string currentMessage)
        {
            int version = CurrentModule.HostMajorVersion;
            if (version > 12)
            {
                return V6HandleSeriesUpdate();
            }
            else
            {
                return HandleSeriesRequest2(currentMessage, true);
            }
        }
        private bool V6HandleSeriesUpdate()
        {
            V6SeriesUpdateUndoWhileCancel = false;
            IbookingHelper bookingHelper = new BookingHelper();
            Outlook.AppointmentItem currApp = this.ItemObj as Outlook.AppointmentItem;
            StringBuilder message = new StringBuilder();
            message.AppendLine(CondecoResources.V6UpdateOnlyFromBookingTabSeries);
            //27Jan2015
            //message.AppendLine(CondecoResources.V6NoUpdateRoomBookingNoCancel);
            //message.AppendLine(CondecoResources.Recurrence_Series_Discard_Instruction);
            DialogResult response = MessageBox.Show(message.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (response == DialogResult.Cancel)
            {

                Outlook.RecurrencePattern rpItem = currApp.GetRecurrencePattern();
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(currApp, false);
                ////long meetingTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                ////DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
                ////DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                ////                                                                             currMeeting.LocationID,
                ////                                                                             currMeeting.OriginalTZ);
                DateTime condecoSeriesStartTimeInLocalTz = currMeeting.BookingDateStartToLocalTZ;

                if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
                {
                    rpItem.StartTime = condecoSeriesStartTimeInLocalTz;
                }

                //meetingTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                //DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
                //DateTime condecoSeriesEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesEnd,
                //                                                                             currMeeting.LocationID,
                //                                                                             currMeeting.OriginalTZ);
                DateTime condecoSeriesEndTimeInLocalTz = currMeeting.BookingDateEndToLocalTZ;
                if (condecoSeriesEndTimeInLocalTz.TimeOfDay != rpItem.EndTime.TimeOfDay)
                {
                    rpItem.EndTime = condecoSeriesEndTimeInLocalTz;

                }

                currApp.Subject = currMeeting.MeetingTitle;
                currApp.Save();
             
                UtilityManager.FreeCOMObject(rpItem);
                UtilityManager.FreeCOMObject(currApp);
            }
            else
            {
              
                AppointmentDataInfo.SetStartDateChanged(currApp, true);
                AppointmentDataInfo.SetHavingPermission(currApp, true);
               // V6SeriesUpdateUndoWhileCancel = true;
                object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                CurrentModule.RibbonButtons_OnClick(obj, null, true);
            }
            bookingHelper= null;
            return false;
        }

        private bool HandleSeriesRequest2(string currentMessage, bool returnTrueForCancel)
        {
            int version = CurrentModule.HostMajorVersion;
            //Bug 5800:Add-in V6.0.5978/Win7/Outlook 2013 - On just closing the appointment window, recurrence booking(appointment/meeting) is getting saved in calender.... Commented Below lines 2Feb2014
            if (version ==11 )
            {
                return V6HandleSeriesUpdate();
            }
            else
            {
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:*********Started********");
                StringBuilder message = new StringBuilder();
                message.AppendLine(CondecoResources.OutofSync_Message_Start);
                message.AppendLine(currentMessage);
                message.AppendLine(CondecoResources.Room_Booking_Tab_Instructions);


                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:message" + message);
                //Changed by Anand : Used OkCancel message box and based on cancel its allowing user to close the inspector but prompt message that need to open again to get sync
                MessageBoxButtons options = MessageBoxButtons.OK;
               
              
                options = MessageBoxButtons.OKCancel;
                message.AppendLine(CondecoResources.Recurrence_Series_Discard_Instruction);
                DialogResult response = MessageBox.Show(message.ToString(), CondecoResources.Condeco_Error_Caption, options, MessageBoxIcon.Information);
                IsOLK2007RecCancelExp = false;
                if (response == DialogResult.Cancel)
                {
                    //CRD 6754 Addded flag  isOLK2007RecCancelExp only for OLK 2007 by paritosh
                    if (version == 12)
                    {
                        IsOLK2007RecCancelExp = true;
                    }
                    //MessageBox.Show(Resources.Room_Booking_Tab_InstructionsForCancel, Resources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);              
                    return returnTrueForCancel;

                }
                return false;

            }
        }
        //private bool HandleSeriesRequest(CondecoMeeting currMeeting, Outlook.AppointmentItem currApp, string currentMessage)
        //{
        //    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:*********Started********");
        //    bool result = true;
        //    StringBuilder message = new StringBuilder();
        //    message.AppendLine(CondecoResources.OutofSync_Message_Start);
        //    message.AppendLine(currentMessage);
        //    message.AppendLine(CondecoResources.Room_Booking_Tab_Instructions);
        //    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:message" + message);
        //    DialogResult response = MessageBox.Show(message.ToString(), CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        //    if (response == DialogResult.Cancel)
        //    {
        //        MessageBox.Show(CondecoResources.Room_Booking_Tab_InstructionsForCancel, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        return true;

        //    }
        //    /*if (currApp.Start.CompareTo(DateTime.Now) >= 0 || UtilityManager.IsSeriesSaveAllowed())
        //    {
        //        MessageBox.Show(message.ToString(), Resources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }
        //    else
        //    {
        //        //The recent times changes are discarded as the series time can not be updated as some of the instances are in past.
        //        DialogResult response = MessageBox.Show(Resources.Recurrence_Series_PastSave, Resources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        if (response == DialogResult.OK)
        //        {

        //            try
        //            {
        //                SyncManager sManager = new SyncManager();
        //                //sManager.SyncAppointmentWithCondeco(currApp);
        //                sManager.SyncSeriesBooking(currApp, false);
        //                sManager.SyncIndiviudallyEditedItems(currApp);
        //            }
        //            catch
        //            {
        //                UtilityManager.LogMessage("Error Occurred while syncing the series appointment." + message);
        //            }
        //            currApp.Close(Outlook.OlInspectorClose.olDiscard);
                 
        //         result = false;
        //        }
        //    }*/
        //    result = false;
        //    //    if (response == DialogResult.OK)
        //    //     {
        //    //         UtilityManager.ShowErrorMessage(Resources.Room_Booking_Tab_Instructions);
        //    //         UtilityManager.ShowErrorMessage("Your appointment is not in sync with the room booking application. Please open it again to synchronize.");
        //    //         currApp.Close(Outlook.OlInspectorClose.olDiscard);
        //    //         result = false;
        //    //     }
        //    //     else if (response == DialogResult.Cancel)
        //    //     {
        //    //         try
        //    //         {
        //    //             /*Outlook.RecurrencePattern recPattern = currApp.GetRecurrencePattern();
        //    //             if (recPattern != null)
        //    //                 recPattern.PatternStartDate = OrigStart.Date;
        //    //                 recPattern.StartTime = OrigStart;
        //    //                 recPattern.PatternEndDate = OrigEnd.Date;
        //    //                 recPattern.EndTime = OrigEnd;

        //    //             //currApp.Start = OrigStart;
        //    //             //currApp.End = OrigEnd;
        //    //                 currApp.Subject = OrigSubject;
        //    //                 currApp.Save();
        //    //                 UtilityManager.FreeCOMObject(recPattern);
        //    //            */


        //    //         }
        //    //         catch
        //    //         {
        //    //         }
        //    //         result = true;
        //    //     }
        //    //    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:*********Finished********");
        //    return result;
        //}
    }
}

