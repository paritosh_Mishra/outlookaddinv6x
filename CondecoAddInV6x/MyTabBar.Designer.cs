namespace CondecoAddinV2
{
    partial class MyTabBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
  
  
        /// <summary>
        /// Clean uppreparation[n++] = " any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
  
        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.webBrowserControl = new System.Windows.Forms.WebBrowser();
            this.picLoader = new System.Windows.Forms.PictureBox();
            this.ProgressText = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.webBrowserControl);
            this.panel1.Location = new System.Drawing.Point(3, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(523, 0);
            this.panel1.TabIndex = 0;
            // 
            // webBrowserControl
            // 
            this.webBrowserControl.Location = new System.Drawing.Point(19, 45);
            this.webBrowserControl.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserControl.Name = "webBrowserControl";
            this.webBrowserControl.Size = new System.Drawing.Size(86, 55);
            this.webBrowserControl.TabIndex = 12;
            this.webBrowserControl.Visible = false;
            // 
            // picLoader
            // 
            this.picLoader.BackColor = System.Drawing.Color.White;
            this.picLoader.Image = global::CondecoAddinV2.App_Resources.CondecoResources.preloader64X64;
            this.picLoader.Location = new System.Drawing.Point(396, 154);
            this.picLoader.Name = "picLoader";
            this.picLoader.Size = new System.Drawing.Size(64, 64);
            this.picLoader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLoader.TabIndex = 13;
            this.picLoader.TabStop = false;
            this.picLoader.Visible = false;
            // 
            // ProgressText
            // 
            this.ProgressText.AutoSize = true;
            this.ProgressText.BackColor = System.Drawing.Color.White;
            this.ProgressText.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.ProgressText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.ProgressText.Location = new System.Drawing.Point(-50, -11);
            this.ProgressText.Margin = new System.Windows.Forms.Padding(0);
            this.ProgressText.Name = "ProgressText";
            this.ProgressText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ProgressText.Size = new System.Drawing.Size(0, 15);
            this.ProgressText.TabIndex = 14;
            this.ProgressText.Visible = false;
            // 
            // MyTabBar
            // 
            this.AutoScroll = false;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(0, 0);
            this.Controls.Add(this.ProgressText);
            this.Controls.Add(this.picLoader);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MyTabBar";
            this.Resize += new System.EventHandler(this.MyTabBar_Resize);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picLoader;
        private System.Windows.Forms.WebBrowser webBrowserControl;
        public System.Windows.Forms.Label ProgressText;





    }
}
