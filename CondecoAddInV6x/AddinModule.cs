using System;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Collections;
using System.Collections.Generic;
using Exception = System.Exception;
using CondecoAddinV2.Repository;
using System.Globalization;
using CondecoAddinV2.App_Resources;
using AddinExpress.MSO;
using CondecoAddinV2.DeskBooking;
using System.Drawing;
using System.Reflection;
using System.Text;
using CondecoAddinV2.Constants;
using System.Net.NetworkInformation;


namespace CondecoAddinV2
{

    /// <summary>
    ///   Add-in Express Add-in Module
    /// </summary>
    [GuidAttribute("3F567CC8-72F9-4087-80E9-71DCAF3575F9"), ProgId("CondecoAddinV2.AddinModule")]
    public class AddinModule : AddinExpress.MSO.ADXAddinModule
    {
        public enum RibbonButtonEnum
        {
            MeetingDetails = 0,
            RoomSearch = 1,
            RoomGrid = 2,
            CancelBooking = 3,
            NewDeskBooking = 11,
            MyDeskBooking = 12,
            FindaColleague = 13,
            RoomBooking = 4
        }
        public AddinModule()
        {

            InitializeComponent();

            // Please add any initialization code to the AddinInitialize event handler
        }
        //This IsDatePastUpdateMsgShow  is introduce to supress two alert message in OLK2007 when updaing Past appointment
        public bool IsDatePastUpdateMsgShow { get; set; }
        internal string StaticribbonXML = string.Empty;
        internal object staticaBuiltinRibbonTab = null;
        //Issue TFS Bugs 8400
        public bool IsMeetingSaveFromWeb { get; set; }

        public AddinExpress.OL.ADXOlFormsManager adxOlFormsManager1;
        public AddinExpress.OL.ADXOlFormsCollectionItem AppointmentCollectionItem;
        public AddinExpress.MSO.ADXOutlookAppEvents adxOutlookEvents;
        //public AddinExpress.OL.ADXOlFormsCollectionItem CheckRoomGridCollectionItem;
        //public AddinExpress.OL.ADXOlFormsCollectionItem CancelRoomCollectionItem;
        //public AddinExpress.OL.ADXOlFormsCollectionItem MyBookingsCollectionItem;
        //Re-ach by paritosh private static AppointmentHelper appHelper = new AppointmentHelper();
        ////Re-ach by paritosh private static BookingHelper bookingHelper = new BookingHelper();
        private static string lastCaption = string.Empty;
        public bool isNewTimeProposed = false;
        private static bool IsRecurranceClick = false;

        //Added by Paritosh to Undo all user changes when User Move meeting from calendra , finds conflict , open room booking , and user closes appoitment window.
        public bool IsUndoMeetingChanges = false;

        // To handle deleted Items
        public DeletedItemsEventsClass deletedItem = null;
        private AddinExpress.MSO.ADXRibbonCommand adxRecurrenceCommand;

        private ImageList imageList1;
        //public AddinExpress.MSO.ADXRibbonCommand adxAppointmentCommand;
        public Boolean isItemNotInSync = new Boolean();
        public Boolean isItemOpen = new Boolean();

        //TP#21109 Added by Vineet Yadav on 15 Feb 2014
        public static Boolean IsBookRoomClicked = false;
        //Add below Flag by Paritosh to delete the Blind Condeco Booking which are created by clicking Room Grid
        public static Boolean IsRoomGridClicked = false;



        // private List<KeyValuePair<Guid, CondecoItemsEventsClass>> itemsEventsClassKeyValueList = null;
        //Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402 (Only for >outlook 2007, not for outlook 2007 or lesser versions.
        //---List to hold Exclusion items at the time of booking is ##exclusionlist##
        public List<Outlook.AppointmentItem> exclusionlist = null;
        //---List to hold Exclusion items at the time of edit booking is ##exclusioneditedlist##
        public List<Outlook.AppointmentItem> exclusioneditedlist = null;

        //tuple list to hold information about exclusionoccurences.
        public Tuple<DateTime, DateTime, string> exclusionoccurencesinfo = null;
        //---List to hold MasterAppointment location at the time of delete booking is ##AppMasterLoc##
        public List<string> AppMasterLoc = null;   //checklater/26/10 
        //---variable to hold sendupdate button to send the exclusion items at Inspector Close;
        public bool IsSendPressed = new Boolean();
        //---List to hold EditedOccurenceDates, at Edit this list will be used to get the Edited items in list
        public List<DateTime> EditedOccurenceDate = null;
        //End Added by Ravi Goyal
        //EN-11279
        private List<string> OutFoxFolderEntries = null;
        //EN-11279
        public List<DateTime> ModuleMissingDates = null;
        public string OutboxFolderEntryID = string.Empty;
        private AddinExpress.MSO.ADXRibbonGroup CondecoBookingGroup;
        private AddinExpress.MSO.ADXRibbonButton RibbonRoomSearchButton;
        private AddinExpress.MSO.ADXRibbonTab CondecoRibbonTab;
        private AddinExpress.OL.ADXOlFormsCollectionItem MyTabBar;
        private AddinExpress.MSO.ADXRibbonButton RibbonRoomGridButton;
        private AddinExpress.MSO.ADXRibbonCommand RibbonShowAppointmentPage;
        private AddinExpress.MSO.ADXRibbonCommand RibbonShowSchedulingPage;
        private ImageList imgListRibbon;
        private AddinExpress.MSO.ADXRibbonButton RibbonCancelBookingButton;

        #region Desk Booking
        //Re-ach by paritosh private static DeskBooking.DeskBookingHelper deskBookingHelper = new DeskBooking.DeskBookingHelper();
        private static DeskBooking.DeskBookingHelper deskBookingHelper;
        private AddinExpress.OL.ADXOlFormsCollectionItem adxOlFormsCollectionItem_NavigationPaneDesks;

        private Outlook.NavigationModule PreviouNavigationModule;

        #endregion

        private ImageList imgListRibbon2013;
        private ImageList imgListRibbon2010;
        private ImageList imageList2007;
        private AddinExpress.MSO.ADXRibbonCommand adxInviteAttendeesCommand;
        private AddinExpress.MSO.ADXRibbonCommand adxCancelInvitationCommand;
        private AddinExpress.MSO.ADXOLSolutionModule adxolSolutionModule_Desk;
        private ADXRibbonCommand RibbonButtonAppDelete;
        private ADXRibbonCommand adxRibbonRemoveFrmCalender;
        private ADXRibbonCommand adxCmd_OpenInNewWin;
        private ADXRibbonCommand adxRibbonComSaveClose;
        private ADXRibbonButton RibbonRoomBookingButton;
        private ADXRibbonCommand adxRibbonComSendUpdate;
        //private List<CondecoFoldersEventsClass> folderEvents = null; //new List<CondecoFoldersEventsClass>();
        private List<CondecoItemsEventsClass> itemsEvents = null;// new List<CondecoItemsEventsClass>();
        private CondecoItemEventsClass selectedItemEvents = null;
        private List<CondecoItemEventsClass> itemEvents = null;//new List<CondecoItemEventsClass>();

        // public bool IsMeetingCancelled { get; set; }
        //This IsMeetingSaveNCloseOnly valiable is used to send message only when msg_saveed send from web .
        public bool IsMeetingSaveNCloseOnly { get; set; }
        public bool IsMeetingSeriesCanceled { get; set; }
        public Object ObjForm { get; set; }
        // public bool IsMeetingInstancesCanceled { get; set; }


        private const int WM_USER = 0x0400;
        private const int WM_MYMESSAGE = WM_USER + 1000;
        private const int WM_MYMyAppDeleted = WM_USER + 150;
        private const int WM_MYToolBar = WM_USER + 100;
        public Outlook.AppointmentItem curritemAppTobeDeleted = null;
        #region Component Designer generated code
        /// <summary>
        /// Required by designer
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Required by designer support - do not modify
        /// the following method
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddinModule));
            this.adxOlFormsManager1 = new AddinExpress.OL.ADXOlFormsManager(this.components);
            this.MyTabBar = new AddinExpress.OL.ADXOlFormsCollectionItem(this.components);
            this.AppointmentCollectionItem = new AddinExpress.OL.ADXOlFormsCollectionItem(this.components);
            this.adxOutlookEvents = new AddinExpress.MSO.ADXOutlookAppEvents(this.components);
            this.adxRecurrenceCommand = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.CondecoRibbonTab = new AddinExpress.MSO.ADXRibbonTab(this.components);
            this.CondecoBookingGroup = new AddinExpress.MSO.ADXRibbonGroup(this.components);
            this.RibbonRoomBookingButton = new AddinExpress.MSO.ADXRibbonButton(this.components);
            this.RibbonRoomSearchButton = new AddinExpress.MSO.ADXRibbonButton(this.components);
            this.RibbonRoomGridButton = new AddinExpress.MSO.ADXRibbonButton(this.components);
            this.RibbonCancelBookingButton = new AddinExpress.MSO.ADXRibbonButton(this.components);
            this.imgListRibbon = new System.Windows.Forms.ImageList(this.components);
            this.RibbonShowAppointmentPage = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.RibbonShowSchedulingPage = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.imageList2007 = new System.Windows.Forms.ImageList(this.components);
            this.imgListRibbon2013 = new System.Windows.Forms.ImageList(this.components);
            this.imgListRibbon2010 = new System.Windows.Forms.ImageList(this.components);
            this.adxInviteAttendeesCommand = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.adxCancelInvitationCommand = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.adxolSolutionModule_Desk = new AddinExpress.MSO.ADXOLSolutionModule(this.components);
            this.RibbonButtonAppDelete = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.adxRibbonRemoveFrmCalender = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.adxCmd_OpenInNewWin = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.adxRibbonComSaveClose = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            this.adxRibbonComSendUpdate = new AddinExpress.MSO.ADXRibbonCommand(this.components);
            // 
            // adxOlFormsManager1
            // 
            this.adxOlFormsManager1.Items.Add(this.MyTabBar);
            this.adxOlFormsManager1.SetOwner(this);
            // 
            // MyTabBar
            // 
            this.MyTabBar.FormClassName = "CondecoAddinV2.MyTabBar";
            this.MyTabBar.InspectorItemTypes = AddinExpress.OL.ADXOlInspectorItemTypes.olAppointment;
            this.MyTabBar.InspectorLayout = AddinExpress.OL.ADXOlInspectorLayout.TopSubpane;
            this.MyTabBar.InspectorMode = AddinExpress.OL.ADXOlInspectorMode.Compose;
            this.MyTabBar.IsHiddenStateAllowed = false;
            this.MyTabBar.IsMinimizedStateAllowed = false;
            this.MyTabBar.RegionBorder = AddinExpress.OL.ADXRegionBorderStyle.None;
            this.MyTabBar.Splitter = AddinExpress.OL.ADXOlSplitterBehavior.None;
            // 
            // adxOutlookEvents
            // 
            this.adxOutlookEvents.Startup += new System.EventHandler(this.adxOutlookEvents_Startup);
            this.adxOutlookEvents.Quit += new System.EventHandler(this.adxOutlookEvents_Quit);
            this.adxOutlookEvents.NewInspector += new AddinExpress.MSO.ADXOlInspector_EventHandler(this.adxOutlookEvents_NewInspector);
            this.adxOutlookEvents.InspectorActivate += new AddinExpress.MSO.ADXOlInspector_EventHandler(this.adxOutlookEvents_InspectorActivate);
            this.adxOutlookEvents.InspectorClose += new AddinExpress.MSO.ADXOlInspector_EventHandler(this.adxOutlookEvents_InspectorClose);
            this.adxOutlookEvents.NewExplorer += new AddinExpress.MSO.ADXOlExplorer_EventHandler(this.adxOutlookEvents_NewExplorer);
            this.adxOutlookEvents.ExplorerActivate += new AddinExpress.MSO.ADXOlExplorer_EventHandler(this.adxOutlookEvents_ExplorerActivate);
            this.adxOutlookEvents.ExplorerFolderSwitch += new AddinExpress.MSO.ADXOlExplorer_EventHandler(this.adxOutlookEvents_ExplorerFolderSwitch);
            this.adxOutlookEvents.ExplorerClose += new AddinExpress.MSO.ADXOlExplorer_EventHandler(this.adxOutlookEvents_ExplorerClose);
            this.adxOutlookEvents.ExplorerBeforeFolderSwitch += new AddinExpress.MSO.ADXOlExplorerBeforeFolderSwitch_EventHandler(this.adxOutlookEvents_ExplorerBeforeFolderSwitch);
            this.adxOutlookEvents.ExplorerBeforeViewSwitch += new AddinExpress.MSO.ADXOlExplorerBeforeViewSwitch_EventHandler(this.adxOutlookEvents_ExplorerBeforeViewSwitch);
            this.adxOutlookEvents.ExplorerSelectionChange += new AddinExpress.MSO.ADXOlExplorer_EventHandler(this.adxOutlookEvents_ExplorerSelectionChange);
            this.adxOutlookEvents.ExplorerBeforeItemCut += new AddinExpress.MSO.ADXHostAction_EventHandler(this.adxOutlookEvents_ExplorerBeforeItemCut);
            // 
            // adxRecurrenceCommand
            // 
            this.adxRecurrenceCommand.ActionTarget = AddinExpress.MSO.ADXRibbonCommandTarget.ToggleButton;
            this.adxRecurrenceCommand.Id = "adxRibbonCommand_a66270b1352149c3b98bf6d878f95c23";
            this.adxRecurrenceCommand.IdMso = "Recurrence";
            this.adxRecurrenceCommand.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.adxRecurrenceCommand.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.adxRecurrenceCommand_OnAction);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Condeco.ico");
            this.imageList1.Images.SetKeyName(1, "48.ico");
            this.imageList1.Images.SetKeyName(2, "availability.ico");
            this.imageList1.Images.SetKeyName(3, "bookRoom.ico");
            this.imageList1.Images.SetKeyName(4, "cancel.ico");
            this.imageList1.Images.SetKeyName(5, "my_bookings.ico");
            this.imageList1.Images.SetKeyName(6, "room_booking.ico");
            this.imageList1.Images.SetKeyName(7, "room.png");
            this.imageList1.Images.SetKeyName(8, "grid_new.jpg");
            this.imageList1.Images.SetKeyName(9, "meeting_details.png");
            this.imageList1.Images.SetKeyName(10, "Desk_Booking.ico");
            this.imageList1.Images.SetKeyName(11, "Find_Colleague.ico");
            this.imageList1.Images.SetKeyName(12, "My_Desk_Bookings.ico");
            this.imageList1.Images.SetKeyName(13, "Desk_Search.bmp");
            this.imageList1.Images.SetKeyName(14, "Your_Bookings.bmp");
            this.imageList1.Images.SetKeyName(15, "FindAColleague.bmp");
            // 
            // CondecoRibbonTab
            // 
            this.CondecoRibbonTab.Controls.Add(this.CondecoBookingGroup);
            this.CondecoRibbonTab.Id = "d";
            this.CondecoRibbonTab.IdMso = "TabAppointment";
            this.CondecoRibbonTab.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            // 
            // CondecoBookingGroup
            // 
            this.CondecoBookingGroup.Controls.Add(this.RibbonRoomBookingButton);
            this.CondecoBookingGroup.Controls.Add(this.RibbonRoomSearchButton);
            this.CondecoBookingGroup.Controls.Add(this.RibbonRoomGridButton);
            this.CondecoBookingGroup.Controls.Add(this.RibbonCancelBookingButton);
            this.CondecoBookingGroup.Id = "adxRibbonGroup_aa673d6c-1053-4b36-b993-02ded504a9b5";
            this.CondecoBookingGroup.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.CondecoBookingGroup.InsertAfterIdMso = "GroupShow";
            this.CondecoBookingGroup.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            // 
            // RibbonRoomBookingButton
            // 
            this.RibbonRoomBookingButton.Caption = "Room Booking";
            this.RibbonRoomBookingButton.Enabled = false;
            this.RibbonRoomBookingButton.Id = "adxRibbonButton_ba832e8e9b564fb3934df38a5a34c72a";
            this.RibbonRoomBookingButton.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.RibbonRoomBookingButton.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.RibbonRoomBookingButton.Size = AddinExpress.MSO.ADXRibbonXControlSize.Large;
            this.RibbonRoomBookingButton.Tag = 4;
            this.RibbonRoomBookingButton.ToggleButton = true;
            this.RibbonRoomBookingButton.Visible = false;
            this.RibbonRoomBookingButton.OnClick += new AddinExpress.MSO.ADXRibbonOnAction_EventHandler(this.RibbonButtons_OnClick);
            // 
            // RibbonRoomSearchButton
            // 
            this.RibbonRoomSearchButton.Id = "adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe";
            this.RibbonRoomSearchButton.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.RibbonRoomSearchButton.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.RibbonRoomSearchButton.Size = AddinExpress.MSO.ADXRibbonXControlSize.Large;
            this.RibbonRoomSearchButton.Tag = 1;
            this.RibbonRoomSearchButton.ToggleButton = true;
            this.RibbonRoomSearchButton.OnClick += new AddinExpress.MSO.ADXRibbonOnAction_EventHandler(this.RibbonButtons_OnClick);
            this.RibbonRoomSearchButton.PropertyChanging += new AddinExpress.MSO.ADXRibbonPropertyChanging_EventHandler(this.RibbonRoomSearchButton_PropertyChanging);
            // 
            // RibbonRoomGridButton
            // 
            this.RibbonRoomGridButton.Id = "adxRibbonButton_28af3e5a-ad46-4552-a566-ebcf422c0185";
            this.RibbonRoomGridButton.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.RibbonRoomGridButton.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.RibbonRoomGridButton.Size = AddinExpress.MSO.ADXRibbonXControlSize.Large;
            this.RibbonRoomGridButton.Tag = 2;
            this.RibbonRoomGridButton.ToggleButton = true;
            this.RibbonRoomGridButton.OnClick += new AddinExpress.MSO.ADXRibbonOnAction_EventHandler(this.RibbonButtons_OnClick);
            this.RibbonRoomGridButton.PropertyChanging += new AddinExpress.MSO.ADXRibbonPropertyChanging_EventHandler(this.RibbonRoomGridButton_PropertyChanging);
            // 
            // RibbonCancelBookingButton
            // 
            this.RibbonCancelBookingButton.Id = "adxRibbonButton_a04c913437644d45b1d34f7acabec93b";
            this.RibbonCancelBookingButton.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.RibbonCancelBookingButton.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.RibbonCancelBookingButton.Size = AddinExpress.MSO.ADXRibbonXControlSize.Large;
            this.RibbonCancelBookingButton.Tag = 3;
            this.RibbonCancelBookingButton.ToggleButton = true;
            this.RibbonCancelBookingButton.OnClick += new AddinExpress.MSO.ADXRibbonOnAction_EventHandler(this.RibbonButtons_OnClick);
            // 
            // imgListRibbon
            // 
            this.imgListRibbon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListRibbon.ImageStream")));
            this.imgListRibbon.TransparentColor = System.Drawing.Color.Transparent;
            this.imgListRibbon.Images.SetKeyName(0, "Room_Search_Normal");
            this.imgListRibbon.Images.SetKeyName(1, "Grid_Search_Normal");
            this.imgListRibbon.Images.SetKeyName(2, "Booking_Delete_Normal");
            this.imgListRibbon.Images.SetKeyName(3, "Cancel_Booking.bmp");
            this.imgListRibbon.Images.SetKeyName(4, "Room_Booking.bmp");
            this.imgListRibbon.Images.SetKeyName(5, "Room_Grid.bmp");
            this.imgListRibbon.Images.SetKeyName(6, "Room_Booking2010.bmp");
            this.imgListRibbon.Images.SetKeyName(7, "Room_Booking2013.bmp");
            // 
            // RibbonShowAppointmentPage
            // 
            this.RibbonShowAppointmentPage.ActionTarget = AddinExpress.MSO.ADXRibbonCommandTarget.ToggleButton;
            this.RibbonShowAppointmentPage.Id = "adxRibbonCommand_d3c14baa-ad8e-492c-8a58-8360372b8d1c";
            this.RibbonShowAppointmentPage.IdMso = "ShowAppointmentPage";
            this.RibbonShowAppointmentPage.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.RibbonShowAppointmentPage.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.RibbonStandardButtons_OnAction);
            // 
            // RibbonShowSchedulingPage
            // 
            this.RibbonShowSchedulingPage.ActionTarget = AddinExpress.MSO.ADXRibbonCommandTarget.ToggleButton;
            this.RibbonShowSchedulingPage.Id = "adxRibbonCommand_9bf4e242-40bc-464c-80c3-7b0995f601b0";
            this.RibbonShowSchedulingPage.IdMso = "ShowSchedulingPage";
            this.RibbonShowSchedulingPage.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.RibbonShowSchedulingPage.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.RibbonStandardButtons_OnAction);
            // 
            // imageList2007
            // 
            this.imageList2007.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2007.ImageStream")));
            this.imageList2007.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2007.Images.SetKeyName(0, "Desk_Search.bmp");
            this.imageList2007.Images.SetKeyName(1, "Your_Bookings.bmp");
            this.imageList2007.Images.SetKeyName(2, "FindAColleague.bmp");
            // 
            // imgListRibbon2013
            // 
            this.imgListRibbon2013.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListRibbon2013.ImageStream")));
            this.imgListRibbon2013.TransparentColor = System.Drawing.Color.Transparent;
            this.imgListRibbon2013.Images.SetKeyName(0, "Desk_Search.bmp");
            this.imgListRibbon2013.Images.SetKeyName(1, "Your_Bookings.bmp");
            this.imgListRibbon2013.Images.SetKeyName(2, "FindAColleague.bmp");
            this.imgListRibbon2013.Images.SetKeyName(3, "Room_Booking.bmp");
            this.imgListRibbon2013.Images.SetKeyName(4, "Room_Grid.bmp");
            this.imgListRibbon2013.Images.SetKeyName(5, "Cancel_Booking.bmp");
            // 
            // imgListRibbon2010
            // 
            this.imgListRibbon2010.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListRibbon2010.ImageStream")));
            this.imgListRibbon2010.TransparentColor = System.Drawing.Color.Transparent;
            this.imgListRibbon2010.Images.SetKeyName(0, "Desk_Search.bmp");
            this.imgListRibbon2010.Images.SetKeyName(1, "FindAColleague.bmp");
            this.imgListRibbon2010.Images.SetKeyName(2, "Your_Bookings.bmp");
            this.imgListRibbon2010.Images.SetKeyName(3, "Room_Booking.bmp");
            this.imgListRibbon2010.Images.SetKeyName(4, "Room_Grid.bmp");
            this.imgListRibbon2010.Images.SetKeyName(5, "Cancel_Booking.bmp");
            // 
            // adxInviteAttendeesCommand
            // 
            this.adxInviteAttendeesCommand.ActionTarget = AddinExpress.MSO.ADXRibbonCommandTarget.ToggleButton;
            this.adxInviteAttendeesCommand.Id = "adxRibbonCommand_c9cff6b721ac4515a5112d7f97bc1561";
            this.adxInviteAttendeesCommand.IdMso = "InviteAttendees";
            this.adxInviteAttendeesCommand.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.adxInviteAttendeesCommand.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.adxInviteAttendeesCommand_OnAction);
            // 
            // adxCancelInvitationCommand
            // 
            this.adxCancelInvitationCommand.Id = "adxRibbonCommand_26eaba7549084bc3a3bfd1ed589867fb";
            this.adxCancelInvitationCommand.IdMso = "CancelInvitation";
            this.adxCancelInvitationCommand.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.adxCancelInvitationCommand.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.adxCancelInvitationCommand_OnAction);
            // 
            // adxolSolutionModule_Desk
            // 
            this.adxolSolutionModule_Desk.NavigationPaneDisplayedModuleCount = 5;
            this.adxolSolutionModule_Desk.NavigationPanePosition = 4;
            // 
            // RibbonButtonAppDelete
            // 
            this.RibbonButtonAppDelete.Id = "adxRibbonCommand_4b1e515c8eb5479a87d46fdef4256238";
            this.RibbonButtonAppDelete.IdMso = "Delete";
            this.RibbonButtonAppDelete.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.RibbonButtonAppDelete.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.RibbonButtonAppDelete_OnAction);
            // 
            // adxRibbonRemoveFrmCalender
            // 
            this.adxRibbonRemoveFrmCalender.Id = "adxRibbonCommand_8fca1ae4ff7b4820967b7df48a3379c4";
            this.adxRibbonRemoveFrmCalender.IdMso = "RemoveFromCalendar";
            this.adxRibbonRemoveFrmCalender.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.adxRibbonRemoveFrmCalender.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.adxRibbonRemoveFrmCalender_OnAction);
            // 
            // adxCmd_OpenInNewWin
            // 
            this.adxCmd_OpenInNewWin.Id = "adxRibbonCommand_87133835c9f847aa98567f50e10b9b1a";
            this.adxCmd_OpenInNewWin.IdMso = "WebOpenInNewWindow";
            // 
            // adxRibbonComSaveClose
            // 
            this.adxRibbonComSaveClose.Id = "adxRibbonCommand_92018f9307f5499ab155bdf7c2567a3d";
            this.adxRibbonComSaveClose.IdMso = "SaveAndClose";
            this.adxRibbonComSaveClose.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookAppointment;
            this.adxRibbonComSaveClose.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.adxRibbonComSaveClose_OnAction);
            // 
            // adxRibbonComSendUpdate
            // 
            this.adxRibbonComSendUpdate.Id = "adxRibbonCommand_7d89339680e44361a0bc58933f110772";
            this.adxRibbonComSendUpdate.IdMso = "SendUpdate";
            this.adxRibbonComSendUpdate.Ribbons = ((AddinExpress.MSO.ADXRibbons)((((AddinExpress.MSO.ADXRibbons.msrOutlookMeetingRequestRead | AddinExpress.MSO.ADXRibbons.msrOutlookMeetingRequestSend) 
            | AddinExpress.MSO.ADXRibbons.msrOutlookAppointment) 
            | AddinExpress.MSO.ADXRibbons.msrOutlookResend)));
            this.adxRibbonComSendUpdate.OnAction += new AddinExpress.MSO.ADXRibbonCommand_EventHandler(this.adxRibbonComSendUpdate_OnAction);
            // 
            // AddinModule
            // 
            this.AddinName = "Condeco Outlook Add-in 6.4";
            this.Description = "To create room bookings";
            this.HandleShortcuts = true;
            this.PageTitle = global::CondecoAddinV2.App_Resources.CondecoResources.Condeco;
            this.PageType = "CondecoAddinV2.CondecoPropertyPage";
            this.SupportedApps = AddinExpress.MSO.ADXOfficeHostApp.ohaOutlook;
            this.AddinInitialize += new AddinExpress.MSO.ADXEvents_EventHandler(this.AddinModule_AddinInitialize);
            this.AddinStartupComplete += new AddinExpress.MSO.ADXEvents_EventHandler(this.AddinModule_AddinStartupComplete);
            this.AddinBeginShutdown += new AddinExpress.MSO.ADXEvents_EventHandler(this.AddinModule_AddinBeginShutdown);
            this.OnSendMessage += new AddinExpress.MSO.ADXSendMessage_EventHandler(this.AddinModule_OnSendMessage);
            this.OnRibbonBeforeCreate += new AddinExpress.MSO.ADXRibbonBeforeCreate_EventHandler(this.AddinModule_OnRibbonBeforeCreate);
            this.OnRibbonBeforeLoad += new AddinExpress.MSO.ADXRibbonBeforeLoad_EventHandler(this.AddinModule_OnRibbonBeforeLoad);

        }

        void adxOutlookEvents_InspectorActivate(object sender, object inspector, string folderName)
        {

            //System.Threading.Thread.Sleep(100);
            UtilityManager.LogMessage("adxOutlookEvents_InspectorActivate: *******Started**************** ");
            if (inspector == null) return;


            // CondecoRibbonTab.Visible = true;
            try
            {
                if (IsRecurranceClick)
                {
                    if ((RibbonRoomSearchButton.Pressed == true) || (RibbonRoomGridButton.Pressed == true))
                    {
                        BookingManager_Collapse(null);
                        CheckRoom_Collapse(null);
                        // CancelRoom_Collapse(null);
                        //|| (RibbonCancelBookingButton.Pressed == true)
                    }
                    else
                    {
                        IsRecurranceClick = false;
                    }

                }
                else
                {

                    IsRecurranceClick = false;
                }
                //Added  for PRB0043359 and PRB0043845
                try
                {
                    if (inspector != null)
                    {
                        Outlook.Inspectors inspectors = OutlookApp.Inspectors;
                        foreach (Outlook.Inspector ins in inspectors)
                        {

                            if (ins != null)
                            {
                                object item = ins.CurrentItem;
                                if (item != null)
                                {
                                    if (item is Outlook.MeetingItem || item is Outlook.AppointmentItem)
                                    {
                                        if (!string.IsNullOrEmpty(StaticribbonXML) && staticaBuiltinRibbonTab != null)
                                        {
                                            ReplaceGetVisibleWithVisible(StaticribbonXML, staticaBuiltinRibbonTab as ADXRibbonTab, true);
                                            CondecoRibbonTab.Visible = true;
                                        }
                                    }
                                    if (item is Outlook.AppointmentItem)
                                    {
                                        string bookingid = UserPropertiesExtension.GetNamedPropertyRecord(item as Outlook.AppointmentItem, CustomProperty.CondecoBookingID);
                                        if (!string.IsNullOrEmpty(bookingid))
                                        {
                                            if ((RibbonRoomBookingButton.Pressed == false && RibbonRoomGridButton.Pressed == false && AppointmentDataInfo.GetAppItemCondecoBookingOpen(item as Outlook.AppointmentItem) == false))
                                            {
                                                HideTabBar();
                                            }
                                        }
                                    }
                                    ValidateRibbonButtonsPressedState(CondecoRibbonTab);
                                }
                                UtilityManager.FreeCOMObject(item);
                                UtilityManager.FreeCOMObject(ins);
                            }
                        }
                        UtilityManager.FreeCOMObject(inspectors);
                    }

                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorActivate: error while setting BookingManager_Collapse " + ex.Message);
                }
                //End Added for PRB0043359 and PRB0043845
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage(" adxOutlookEvents_InspectorActivate" + ex.Message);
            }
            finally
            {
                //EN - 7700

            }
            UtilityManager.LogMessage("adxOutlookEvents_InspectorActivate: *******Finished**************** ");

        }


        void RibbonStandardButtons_OnAction(object sender, AddinExpress.MSO.IRibbonControl control, bool pressed, AddinExpress.MSO.ADXCancelEventArgs e)
        {

            UtilityManager.LogMessage("AddinModule.RibbonStandardButtons_OnAction:*********Started********");
            //OfflineUrlBasedData.ClearOfflineData();
            // AppointmentDataInfo.ClearAppointmentInCollection();
            RibbonRoomBookingButton.Pressed = false;
            RibbonRoomGridButton.Pressed = false;
            RibbonCancelBookingButton.Pressed = false;
            RibbonRoomBookingButton.Pressed = false;
            HideTabBar();
            if (sender != null)
            {
                try
                {
                    BookingManager obj = sender as BookingManager;
                    if (obj != null)
                    {
                        obj.Collapse -= BookingManager_Collapse;
                        if (!obj.IsDisposed)
                        {
                            obj = null;
                        }
                    }
                }
                catch
                {
                }
                try
                {
                    CheckRoomGrid obj1 = sender as CheckRoomGrid;
                    if (obj1 != null)
                    {
                        obj1.Collapse -= CheckRoom_Collapse;
                        if (!obj1.IsDisposed)
                        {
                            obj1 = null;
                        }
                    }
                }
                catch
                {
                }
                try
                {
                    CancelRoom obj2 = sender as CancelRoom;
                    if (obj2 != null)
                    {
                        obj2.Collapse -= CancelRoom_Collapse;
                        if (!obj2.IsDisposed)
                        {
                            obj2 = null;
                        }
                    }
                }
                catch
                {
                }
                ValidateRibbonButtonsPressedState(CondecoRibbonTab);
            }
            UtilityManager.LogMessage("AddinModule.RibbonStandardButtons_OnAction:*********Finished********");
        }


        void adxRecurrenceCommand_OnAction(object sender, AddinExpress.MSO.IRibbonControl control, bool pressed, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            //throw new Exception("The method or operation is not implemented.");
            UtilityManager.LogMessage("AddinModule.adxRecurrenceCommand_OnAction:*********Started********");

            //TP#21109 Added by Vineet Yadav on 15 Feb 2014
            IsBookRoomClicked = false;
            IsRoomGridClicked = false;

            Outlook.Inspector cInsp = OutlookApp.ActiveInspector();
            if (cInsp == null) return;
            if (!(cInsp.CurrentItem is Outlook.AppointmentItem)) return;
            Outlook.AppointmentItem currentItem = cInsp.CurrentItem as Outlook.AppointmentItem;
            if (currentItem == null) return;

            IbookingHelper bookingHelper = new BookingHelper();
            // if (!pressed && bookingHelper.IsCondecoBooking(currentItem) && currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && currentItem.Start.CompareTo(DateTime.Now) < 0 && !UtilityManager.IsSeriesSaveAllowed())
            if (!pressed && bookingHelper.IsCondecoBooking(currentItem) && !UtilityManager.IsSeriesSaveAllowed() && currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)// && currentItem.Start.CompareTo(DateTime.Now) < 0)
            {
                UtilityManager.ShowErrorMessage(CondecoResources.Recurrence_Series_PastSave);
                // MessageBox.Show(CondecoResources.Recurrence_Series_PastSave, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
            else if (pressed && !currentItem.IsRecurring && bookingHelper.IsCondecoBooking(currentItem))
            {
                UtilityManager.ShowErrorMessage(CondecoResources.Appointment_Conversion_NotAllowed);
                //  MessageBox.Show(CondecoResources.Appointment_Conversion_NotAllowed, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
            bookingHelper = null;
            UtilityManager.FreeCOMObject(currentItem);
            //UtilityManager.FreeCOMObject(cInsp);

            if ((RibbonRoomSearchButton.Pressed == true) || (RibbonRoomGridButton.Pressed == true) || (RibbonCancelBookingButton.Pressed == true))
            {
                IsRecurranceClick = true;

            }
            UtilityManager.LogMessage("AddinModule.adxRecurrenceCommand_OnAction:*********Finished********");
        }
        #endregion

        #region Add-in Express automatic code

        // Required by Add-in Express - do not modify
        // the methods within this region

        public override System.ComponentModel.IContainer GetContainer()
        {
            if (components == null)
                components = new System.ComponentModel.Container();
            return components;
        }

        [ComRegisterFunctionAttribute]
        public static void AddinRegister(Type t)
        {
            AddinExpress.MSO.ADXAddinModule.ADXRegister(t);
        }

        [ComUnregisterFunctionAttribute]
        public static void AddinUnregister(Type t)
        {
            AddinExpress.MSO.ADXAddinModule.ADXUnregister(t);
        }

        public override void UninstallControls()
        {
            base.UninstallControls();
        }

        #endregion


        public Outlook._Application OutlookApp
        {
            get
            {
                return (HostApplication as Outlook._Application);
            }
        }

        private void adxOutlookEvents_InspectorClose(object sender, object inspector, string folderName)
        {
            UtilityManager.LogMessage("adxOutlookEvents_InspectorClose: *******Started**************** ");
            //Outlook.Inspector cInsp = null;
            bool IsItemAppointment = true;
            try
            {

                isItemOpen = false;
                //try
                //{
                //    cInsp = inspector as Outlook.Inspector;
                //}
                //catch (Exception)
                //{

                //}
                ////  if (!(inspector is Outlook.Inspector)) return;
                //if (cInsp == null) return;

                //Added By Ritesh for TP-9471
                //In certain senario, if value set "true" and opening a new appointment and clicking on on Room Booking
                //Giving the msg "Subject Not specefied".               
                this.isItemNotInSync = false;
                //End By Ritesh/Anand for TP-9471

                object cItem = (inspector as Outlook.Inspector).CurrentItem;
                if (!(cItem is Outlook.AppointmentItem))
                {
                    UtilityManager.FreeCOMObject(cItem);
                    IsBookRoomClicked = false;
                    IsRoomGridClicked = false;
                    IsItemAppointment = false;
                    return;
                }


                Outlook.AppointmentItem currentItem = cItem as Outlook.AppointmentItem;

                if (currentItem == null) return;

                try
                {
                    //Below are added by paritosh to send mail of edited series and no locationn  booking 
                    if (!this.IsMeetingSaveNCloseOnly && currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        IbookingHelper bookingHelper = new BookingHelper();
                        IsyncManager syncManager = new SyncManager();
                        List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceMeetingEditedOnly(currentItem);
                        Outlook.RecurrencePattern recPattern = currentItem.GetRecurrencePattern();
                        syncManager.HanldeIndiviudalyEditedItems(currentItem, recPattern, currMeeting, this, true, false, true);
                        if (exclusionlist != null)
                        {
                            if (exclusionlist.Count > 0)
                            {
                                try { SendExclusionEmails(exclusionlist); }
                                catch { }
                                finally { exclusionlist.Clear(); }
                            }
                        }
                        if (this.IsMeetingSeriesCanceled)
                        {
                            AppointmentHelper.ClearAppointmentData(currentItem);
                            this.IsMeetingSeriesCanceled = false;
                        }
                        UtilityManager.FreeCOMObject(recPattern);
                        bookingHelper = null;
                        syncManager = null;
                    }
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("Inspextorclose edited mail send catch ex= " + ex.Message);
                }

                try
                {
                    if (currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        if (this.ModuleMissingDates != null && this.ModuleMissingDates.Count > 0)
                        {
                            IsyncManager syncManager = new SyncManager();
                            Outlook.RecurrencePattern recPattern1 = null;
                            recPattern1 = currentItem.GetRecurrencePattern();

                            syncManager.HanldeMissingItems(recPattern1, this.ModuleMissingDates, this, MissingMeeting: true);
                            try
                            {
                                exclusioneditedlist = new List<Outlook.AppointmentItem>();
                                foreach (DateTime EditedappItemdate in this.EditedOccurenceDate)
                                {
                                    try
                                    {
                                        Outlook.AppointmentItem tempappItem = null;
                                        if (recPattern1 != null)
                                            tempappItem = recPattern1.GetOccurrence(EditedappItemdate);
                                        if (tempappItem != null)
                                            exclusioneditedlist.Add(tempappItem);
                                    }
                                    catch (Exception ex)
                                    {
                                        UtilityManager.LogMessage("Inspectorclose catch ex=" + ex.Message);
                                    }
                                }
                                UtilityManager.FreeCOMObject(recPattern1);
                                if (exclusioneditedlist != null)
                                {
                                    if (exclusioneditedlist.Count > 0)
                                    {
                                        try
                                        {
                                            UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:missing date **Exclusioneditedlist** Count of Edited Appointments" + exclusioneditedlist.Count);
                                            SendExclusionEmails(exclusioneditedlist, true);
                                            UtilityManager.LogMessage("adxOutlookEvents_InspectorClose: missing date**Exclusioneditedlist**SendExclusionEmails-Done");
                                        }
                                        catch (Exception ex)
                                        {
                                            UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:missing date **Exclusioneditedlist**SendExclusionEmails-catch " + ex.Message);
                                        }
                                    }
                                }
                            }
                            catch (Exception exc)
                            {
                                UtilityManager.LogMessage("adxOutlookEvents_InspectorClose: error occured in creation of Edited Appointments");
                                UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:" + exc.Message);
                                UtilityManager.LogMessage("Stack Trace adxOutlookEvents_InspectorClose:" + exc.StackTrace);
                                UtilityManager.FreeCOMObject(recPattern1);
                                EditedOccurenceDate.Clear();
                            }
                            finally
                            {
                                if (exclusioneditedlist != null)
                                {
                                    exclusioneditedlist.Clear();
                                    exclusioneditedlist = null;
                                }
                                if (EditedOccurenceDate != null)
                                {
                                    EditedOccurenceDate.Clear();
                                    EditedOccurenceDate = null;
                                }
                                if (ModuleMissingDates != null)
                                {
                                    ModuleMissingDates.Clear();
                                    ModuleMissingDates = null;
                                }
                                UtilityManager.FreeCOMObject(recPattern1);
                                syncManager = null;
                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("Inspectorclose edited missing booking  mail send catch ex= " + ex.Message);
                }

                // end 

                if (!AppointmentDataInfo.GetAppRecuDeleteToolbarClick(currentItem))
                {
                    AppointmentDataInfo.RemoveAppointmentFromCollection(currentItem);
                }


                UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Getting Post ID");
                string iID = AppointmentHelper.GetAppointmentPostID(currentItem);
                //UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Post ID Found:" + iID);
                if (string.IsNullOrEmpty(iID))
                {
                    if (UtilityManager.SyncBookingEnabled() == 1)
                    {
                        return;
                    }
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:appointment is not booked.");
                    //Delete calendar item if a room is not saved
                    //if (IsBookRoomClicked || IsRoomGridClicked)
                    //{
                    //    this.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                    //    curritemAppTobeDeleted = currentItem;
                    //    IsBookRoomClicked = false;
                    //    IsRoomGridClicked = false;
                    //}
                    return;
                }
                //////////For EN-2547
                //////IsMeetingCancelled = false;
                //////if (currentItem.IsRecurring && (currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting || currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled))
                //////{
                //////    IsMeetingCancelled = true;
                //////}
                //////////end //
                IsBookRoomClicked = false;
                IsRoomGridClicked = false;
                UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:checking if it is a condeco booking or not");
                // IbookingHelper bookingHelper1 = new BookingHelper();
                // if (!bookingHelper.IsCondecoBooking(currentItem))
                if ((AppointmentDataInfo.GetBookingId(currentItem) == 0))
                {
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:No condeco booking found so returning");
                    UtilityManager.FreeCOMObject(currentItem);
                    return;
                }


                UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:checking if it is a cancelled appointment");
                if (currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                {
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Meeting is cancelled so closing the insp");
                    //if (condecoItem != null)
                    //{
                    //    condecoItem.RemoveConnection();
                    //    condecoItem.Dispose();
                    //}
                    (inspector as Outlook._Inspector).Close(Outlook.OlInspectorClose.olSave);
                    UtilityManager.FreeCOMObject(currentItem);
                }
                //Added by Ravi Goyal-(Only for >outlook 2007, not for outlook 2007 or lesser versions), this will send emails to all exception is series
                //& update the invitee calendar.
                //////////if (EditedOccurenceDate != null && IsSendPressed && IsItemAppointment && this.HostMajorVersion > 12)
                //////////{
                //////////    if (EditedOccurenceDate.Count > 0)
                //////////    {
                //////////        UtilityManager.LogMessage("adxOutlookEvents_InspectorClose: EditedOccurence**Date** Count of Edited Appointments" + EditedOccurenceDate.Count);


                //////////    }
                //////////}
                //End Added by Ravi Goyal
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:" + ex.Message);
                UtilityManager.LogMessage("Stack Trace adxOutlookEvents_InspectorClose:" + ex.StackTrace);
            }
            finally
            {
                try
                {
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block() processing initiated");
                    // 	CRD-7376- Below lines are added by Paritosh to fix the misalign in Outlook 2007 when user click schedule button then  close appoitment window .Issue reported by Alex 
                    // 	PRB0040840 - Added by Ravi Goyal to fix the Outlook 2007 email minimize issues. 
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block()-checking if Outlook version is 2007");
                    if (this.HostMajorVersion == 12 && IsItemAppointment)
                    {
                        UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block- **Outlook version 2007 detected** ");
                        Outlook._Explorer activeExp = OutlookApp.ActiveExplorer();
                        UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block- **Outlook version 2007 Explorer object created** ");
                        if (activeExp != null)
                        {
                            UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block- **Explorer Activate() called** ");
                            activeExp.Activate();
                            UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block- **Explorer Activate() completed successfully** ");
                        }
                        UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block- **Explorer Activate() release called** ");
                        UtilityManager.FreeCOMObject(activeExp);
                        UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block- **Explorer Activate() released successfully** ");
                    }
                    else
                    {
                        UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block- Outlook version is:-" + this.HostMajorVersion.ToString());
                        UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Finally Block- check for Item type appointment is : " + IsItemAppointment.ToString());
                    }
                }
                catch (Exception excep)
                {
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose:Catch() in Finally Block" + excep.Message);
                    UtilityManager.LogMessage("Stack Trace adxOutlookEvents_InspectorClose:Catch() in Finally Block" + excep.StackTrace);
                }
                try
                {
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose: Finally *******Calling CleanExclusionVariables()**************** ");
                    CleanExclusionVariables();
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose: Finally *******Finished CleanExclusionVariables()**************** ");
                    UtilityManager.LogMessage("adxOutlookEvents_InspectorClose: Finally *******Finished**************** ");
                    // CleanUpProcess();


                }
                catch
                {
                }
            }
            UtilityManager.LogMessage("adxOutlookEvents_InspectorClose: *******Finished**************** ");
            // CleanUpProcess();
        }

        private void AddinModule_AddinStartupComplete(object sender, EventArgs e)
        {
            //folderEvents = new List<CondecoFoldersEventsClass>();
            itemsEvents = new List<CondecoItemsEventsClass>();
            itemEvents = new List<CondecoItemEventsClass>();
            ConnectToFolder();
            UtilityManager.LogMessage("AddinModule_AddinStartupComplete: *******Started**************** ");
            UtilityManager.LogMessage("Condeco Version()" + CondecoAddinV2.App_Resources.CondecoResources.Addin_Version);
            try
            {

                //  string currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                // string twoLetterCurrentCulture = currentCulture.Split('-')[0];

                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB"); //CultureInfo.CreateSpecificCulture(twoLetterCurrentCulture);

                UtilityManager.LogMessage("AddinModule_AddinStartupComplete: Going to add Calendar Item class");
                //calendarItem = new CondecoItemsEventsClass(this);
                //calendarItem.ConnectTo(AddinExpress.MSO.ADXOlDefaultFolders.olFolderCalendar, true);
                UtilityManager.LogMessage("AddinModule_AddinStartupComplete: Checking if outlook version is 2000 0r 2003 then attached listener to deleted items");
                //if (this.HostMajorVersion <= 14)
                //{
                UtilityManager.LogMessage("AddinModule_AddinStartupComplete: listener attached to deleted items folder");
                //deletedItem = new DeletedItemsEventsClass(this);
                //deletedItem.ConnectTo(AddinExpress.MSO.ADXOlDefaultFolders.olFolderDeletedItems, true);
                //}

                UtilityManager.LogMessage("AddinModule_AddinStartupComplete: Calendar Item Attached");

                //condecoItem = new CondecoMeetingItemEventsClass(this);

                UtilityManager.LogMessage("AddinModule_AddinStartupComplete: Getting Condeco Host Name");
                UtilityManager.GetCondecoHostName();

                this.AddinName = global::CondecoAddinV2.App_Resources.CondecoResources.AddIn_Name;
                //Added by Ravi Goyal for PRB0043778
                if (UtilityManager.ConnectionMode().Equals(2))
                {
                    NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(UtilityManager.NetworkChange_NetworkAvailabilityChanged);
                }
                //End added by Ravi Goyal for PRB0043778
                // Added by Ravi Goyal to write the user Outlook navigation preferences-PRB0041123
                if (this.HostMajorVersion >= 14 && UtilityManager.DeskBookingEnabled().Equals(1))
                {
                    try
                    {
                        this.OutlookApp.Application.ActiveExplorer().Deactivate += new Outlook.ExplorerEvents_10_DeactivateEventHandler(AddinModule_Deactivate);
                    }
                    catch { }
                }
                //End Added by Ravi Goyal to write the user Outlook navigation preferences-PRB0041123

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("AddinModule_AddinStartupComplete" + ex.Message);
            }

            UtilityManager.LogMessage("AddinModule_AddinStartupComplete: *******Finished**************** ");
        }
        // Added by Ravi Goyal to write the user Outlook navigation preferences-PRB0041123
        internal void AddinModule_Deactivate()
        {
            try
            {
                UtilityManager.LogMessage("AddinModule_AddinModule_Deactivate():**Setting Navigation pane preferences for Outlook in Settings");
                UtilityManager.SetDeskNavPropertyOnClose(this.OutlookApp.ActiveExplorer().NavigationPane as Outlook.NavigationPane, this.HostMajorVersion);
                UtilityManager.LogMessage("AddinModule_AddinModule_Deactivate():**Setting Navigation pane preferences for Outlook in Settings--Completed");
            }
            catch { }
        }
        //---Continue new method for PRB0041123
        private void SetOutlookNavigationPreferences()
        {
            try
            {
                UtilityManager.LogMessage("AddinModule_SetOutlookNavigationPreferences():**Setting Navigation pane preferences for Outlook");
                string NavigationPreferences = string.Empty;

                NavigationPreferences = UtilityManager.ReadNavigationPreferences();
                if (!String.IsNullOrEmpty(NavigationPreferences))
                {
                    string[] valuesArray = NavigationPreferences.Split(',');
                    this.adxolSolutionModule_Desk.NavigationPanePosition = Convert.ToInt16(valuesArray[0]);
                }
                if (this.HostMajorVersion == 14)
                {
                    string[] valuesArray = NavigationPreferences.Split(',');
                    this.adxolSolutionModule_Desk.Visible = Convert.ToBoolean(valuesArray[1]);
                }
                else
                {
                    this.adxolSolutionModule_Desk.Visible = true;
                }
                UtilityManager.LogMessage("AddinModule_SetOutlookNavigationPreferences():**Setting Navigation pane preferences for Outlook-Completed");
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("AddinModule_SetOutlookNavigationPreferences():**exception in reading Navigation pane preferences for Outlook" + ex.Message);
                UtilityManager.LogMessage("AddinModule_SetOutlookNavigationPreferences():**settings default values now");
                this.adxolSolutionModule_Desk.NavigationPanePosition = 4;
                this.adxolSolutionModule_Desk.Visible = true;
            }
        }
        //End Added by Ravi Goyal to read & write the user Outlook navigation preferences-PRB0041123
        private void adxOutlookEvents_NewInspector(object sender, object inspector, string folderName)
        {

            UtilityManager.LogMessage("adxOutlookEvents_NewInspector: *******Started**************** ");
            if (inspector == null) return;
            try
            {
                Outlook._Inspector olInsp = inspector as Outlook._Inspector;
                object item = olInsp.CurrentItem;
                if (item is Outlook.MeetingItem)
                {
                    Outlook.MeetingItem currentMeetingItem = item as Outlook.MeetingItem;

                    if (currentMeetingItem.MessageClass.StartsWith("IPM.Schedule.Meeting.Resp"))
                    {
                        lastCaption = currentMeetingItem.Subject;
                    }
                }
                else if (item is Outlook.AppointmentItem)
                {

                    if (!string.IsNullOrEmpty(lastCaption))
                    {
                        Outlook.AppointmentItem curAppItem = item as Outlook.AppointmentItem;

                        if (lastCaption.ToLower().StartsWith("new time proposed") && lastCaption.Contains(curAppItem.Subject))
                        {
                            isNewTimeProposed = true;
                        }
                        lastCaption = string.Empty;

                    }
                }
                //SyncBooking start
                if (item != null && item is Outlook._AppointmentItem)
                {
                    UtilityManager.LogMessage("AddinModule- adxOutlookEvents_NewInspector() Before SYNCorCondecoIsNewAppointment");
                    //  this.SendMessage(WM_MYToolBar, IntPtr.Zero, IntPtr.Zero);
                    //  if (!appHelper.IsNewAppointment(item as Outlook.AppointmentItem) || appHelper.IsSyncAppointment(item as Outlook.AppointmentItem,true))
                    if (!AppointmentHelper.SYNCorCondecoIsNewAppointment(item as Outlook.AppointmentItem, true))
                    {
                        try
                        {
                            CondecoItemEventsClass itemEventSink = new CondecoItemEventsClass(this, false);
                            itemEventSink.ConnectTo(item, true);
                            itemEvents.Add(itemEventSink);

                            if (selectedItemEvents != null)
                                if (CompareItem(item, selectedItemEvents.ItemObj))
                                {
                                    selectedItemEvents.Dispose();
                                    //selectedItemEvents = null;
                                }
                        }
                        catch (Exception ex)
                        {
                            UtilityManager.LogMessage("AddinModule- adxOutlookEvents_NewInspector() event added catch =" + ex.StackTrace);
                        }
                        //Room booking id adxRibbonButton_ba832e8e9b564fb3934df38a5a34c72a
                        //object obj = (this).FindRibbonControl("adxRibbonButton_ba832e8e9b564fb3934df38a5a34c72a");
                        //AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                        //if (ribbonRoomBooking != null)
                        //{
                        //    ribbonRoomBooking.Enabled = true;
                        //}  
                    }
                    else
                    {

                        //object obj = (this).FindRibbonControl("adxRibbonButton_ba832e8e9b564fb3934df38a5a34c72a");
                        //AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                        //if (ribbonRoomBooking != null)
                        //{
                        //    ribbonRoomBooking.Enabled = false;
                        //}  
                        UtilityManager.FreeCOMObject(item);
                    }
                }
                //SyncBooking End

                //if (item != null && item is Outlook._AppointmentItem && !appHelper.IsNewAppointment(item as Outlook.AppointmentItem))
                //{

                //    CondecoItemEventsClass itemEventSink = new CondecoItemEventsClass(this, false);
                //    itemEventSink.ConnectTo(item, true);
                //    //itemEventSink.IsAppointmentMoved = false;
                //    itemEvents.Add(itemEventSink);
                //    if (selectedItemEvents != null)
                //        if (CompareItem(item, selectedItemEvents.ItemObj))
                //        {
                //            selectedItemEvents.Dispose();
                //            selectedItemEvents = null;
                //        }
                //}

                //else
                //{
                //    UtilityManager.FreeCOMObject(item);
                //}
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                //this.SendMessage(WM_MYToolBar, IntPtr.Zero, IntPtr.Zero);
                isItemOpen = true;
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage(" adxOutlookEvents_NewInspector" + ex.Message);
                UtilityManager.LogMessage(" adxOutlookEvents_NewInspector StackTrace" + ex.StackTrace);
            }
            UtilityManager.LogMessage("adxOutlookEvents_NewInspector: *******Finished**************** ");
        }

        private void AddinModule_AddinBeginShutdown(object sender, EventArgs e)
        {
            UtilityManager.LogMessage("AddinModule_AddinBeginShutdown: *******Started**************** ");
            CleanUpProcess();
            UtilityManager.LogMessage("AddinModule_AddinBeginShutdown: *******Finished**************** ");
        }
        public void CleanUpProcess()
        {

            //OfflineUrlBasedData.ClearOfflineData();
            //AppointmentDataInfo.ClearAppointmentInCollection();
            if (SSOFormAuthentication.IsInstanceCreated)
            {
                SSOFormAuthentication.DisposeSSO(true);
            }
            if (deletedItem != null)
            {
                deletedItem.RemoveConnection();
                deletedItem.Dispose();
                UtilityManager.LogMessage("AddinModule_AddinBeginShutdown: Connection removed from deleted items folder ");
            }
            // WriteToLog("  =  AddinModule.AddinBeginShutdown", "Node_AddinBeginShutdown");
            if (selectedItemEvents != null)
                selectedItemEvents.Dispose();
            //if (folderEvents != null)
            //{
            //    foreach (CondecoFoldersEventsClass folderEventSink in folderEvents)
            //        folderEventSink.Dispose();
            //    folderEvents.Clear();
            //}
            if (itemsEvents != null)
            {
                foreach (CondecoItemsEventsClass itemEventSink in itemsEvents)
                    itemEventSink.Dispose();
                itemsEvents.Clear();
            }
            if (itemEvents != null)
            {
                foreach (CondecoItemEventsClass itemEventSink in itemEvents)
                    itemEventSink.Dispose();
                itemEvents.Clear();
            }
            StaticribbonXML = null;
            staticaBuiltinRibbonTab = null;
            UtilityManager.LogMessage("AddinModule_AddinBeginShutdown: *******Finished**************** ");
        }

        private void adxOutlookEvents_ExplorerBeforeItemCut(object sender, AddinExpress.MSO.ADXHostActionEventArgs e)
        {

        }

        private void adxOutlookEvents_ExplorerSelectionChange(object sender, object explorer)
        {
            ////Below commented to fixed  6876

            ////Outlook.Explorer exp = explorer as Outlook.Explorer;
            ////if (exp == null) return;
            ////if (exp.Selection.Count == 0) return;
            ////if (!(exp.Selection.Item(1) is Outlook.AppointmentItem)) return;
            ////Outlook.AppointmentItem currItem = exp.Selection.Item(1) as Outlook.AppointmentItem;
            ////if (currItem == null) return;
            ////folderItem.ConnectTo(currItem, true);
            if (UtilityManager.SharedCalendarUsed())
            {
                try
                {
                    // string s = "  =  ADXOutlookAppEvents.ExplorerSelectionChange. ";
                    Outlook.Explorer currentExplorer = explorer as Outlook.Explorer;
                    if (currentExplorer != null)
                    {
                        Outlook.NameSpace ns = currentExplorer.Session as Outlook.NameSpace;
                        if (ns != null)
                            try
                            {
                                Outlook.MAPIFolder folder = currentExplorer.CurrentFolder;
                                if (folder != null && folder.DefaultItemType == Outlook.OlItemType.olAppointmentItem)
                                    try
                                    {
                                        Outlook.MAPIFolder folder2 = ns.GetFolderFromID(folder.EntryID, folder.StoreID);
                                        if (folder2 != null)
                                        {
                                            // s += "Current Folder name is '" + folder.Name + "', ";
                                            bool flagFound = false;
                                            for (int i = 0; i < this.itemsEvents.Count; i++)
                                            {
                                                try
                                                {
                                                    object foldobj = this.itemsEvents[i].FolderObj as Outlook.MAPIFolder;
                                                    if (foldobj != null)
                                                    {
                                                        if (((Outlook.MAPIFolder)foldobj).EntryID == folder.EntryID)
                                                        {
                                                            if (this.itemsEvents[i].IsConnected)
                                                            {
                                                                flagFound = true;
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    this.itemsEvents[i].RemoveConnection();
                                                                }
                                                                catch
                                                                {
                                                                }
                                                                this.itemsEvents[i].ConnectTo(folder2, true, false);

                                                            }
                                                            flagFound = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                catch
                                                {
                                                    UtilityManager.LogMessage("Catch adxOutlookEvents_ExplorerSelectionChange");
                                                    // CleanUpProcess();
                                                }
                                            }

                                            if (!flagFound)
                                            {
                                                //System.Diagnostics.Debug.WriteLine(" Binding " + folder.Name);
                                                CondecoItemsEventsClass eventSink = new CondecoItemsEventsClass(this);
                                                eventSink.ConnectTo(folder2, true, false);
                                                this.itemsEvents.Add(eventSink);
                                            }


                                        }
                                    }
                                    finally
                                    {
                                        Marshal.ReleaseComObject(folder);
                                    }
                                // s += "Explorer caption is '" + currentExplorer.Caption + "'";
                            }

                            finally
                            {
                                Marshal.ReleaseComObject(ns);
                            }
                    }
                    // WriteToLog(s, "Node_ExplorerSelectionChange");

                }
                catch
                {
                }
            }
            ConnectToSelectedItem(explorer);
        }


        private void adxOutlookEvents_ExplorerFolderSwitch(object sender, object explorer)
        {
            #region Desk Booking
            if (UtilityManager.DeskBookingEnabled().Equals(1))
            {
                if (this.HostMajorVersion < 14)
                {
                    ShowNavigationPaneDesksForm();
                }
            }
            #endregion
        }


        private void ConnectToFolder()
        {
            UtilityManager.LogMessage("ConnectToFolder: *******Started**************** ");
            Outlook._NameSpace ns = OutlookApp.GetNamespace("MAPI");
            if (ns != null)
                try
                {
                    Outlook.MAPIFolder inboxFolder = ns.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox); // WHY INBOX: TODO
                    if (inboxFolder != null)
                        try
                        {
                            Outlook.MAPIFolder rootFolder = inboxFolder.Parent as Outlook.MAPIFolder;
                            if (rootFolder != null)
                                try
                                {
                                    Outlook.MAPIFolder folder1 = ns.GetFolderFromID(rootFolder.EntryID, rootFolder.StoreID);
                                    if (folder1 != null)
                                    {
                                        CondecoItemsEventsClass itemsEventSink = new CondecoItemsEventsClass(this);
                                        itemsEventSink.ConnectTo(folder1, true, false);
                                        itemsEvents.Add(itemsEventSink);
                                        //itemsEventsClassKeyValueList.Add(new KeyValuePair<Guid, CondecoItemsEventsClass>(Guid.NewGuid(), itemsEventSink));
                                        // itemsEventSink = null;
                                    }

                                    //Outlook.MAPIFolder folder2 = ns.GetFolderFromID(rootFolder.EntryID, rootFolder.StoreID);
                                    //if (folder2 != null)
                                    //{
                                    //    CondecoFoldersEventsClass foldersEventSink = new CondecoFoldersEventsClass(this);
                                    //    foldersEventSink.ConnectTo(folder2, true, false);
                                    //    folderEvents.Add(foldersEventSink);
                                    //    foldersEventSink = null;
                                    //   // folderEvents[folderEvents.Count +1].ConnectTo(folder2, true, false);

                                    //}
                                    ConnectToFolders(ns, rootFolder);
                                }
                                catch (Exception ex)
                                {
                                    UtilityManager.LogMessage("ConnectToFolder:Catch  Error3:" + ex.Message + ":Stacktrace3 =" + ex.StackTrace);
                                }
                                finally
                                {
                                    Marshal.ReleaseComObject(rootFolder);
                                }

                        }
                        catch (Exception ex)
                        {
                            UtilityManager.LogMessage("ConnectToFolder:Catch  Error2:" + ex.Message + ":Stacktrace2 =" + ex.StackTrace);
                        }
                        finally
                        {
                            Marshal.ReleaseComObject(inboxFolder);
                        }
                    Outlook.MAPIFolder outboxFolder = ns.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderOutbox);
                    if (outboxFolder != null)
                        try
                        {
                            OutboxFolderEntryID = outboxFolder.EntryID;
                        }
                        catch (Exception ex)
                        {
                            UtilityManager.LogMessage("ConnectToFolder:Catch  Error1:" + ex.Message + ":Stacktrace1 =" + ex.StackTrace);
                        }
                        finally
                        {
                            Marshal.ReleaseComObject(outboxFolder);
                        }
                    //EN-11279
                    OutFoxFolderEntries = new List<string>();
                    OutFoxFolderEntries = EnumerateOutboxStores();
                    //EN-11279
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("ConnectToFolder:Catch outer Error:" + ex.Message + ":Stacktrace =" + ex.StackTrace);
                }

                finally
                {
                    Marshal.ReleaseComObject(ns);
                }
            UtilityManager.LogMessage("ConnectToFolder: *******Finished**************** ");
        }
        private void ConnectToFolders(Outlook._NameSpace ns, Outlook.MAPIFolder rootFolder)
        {
            UtilityManager.LogMessage("ConnectToFolders: *******Started**************** ");
            try
            {
                Outlook.Folders folders = rootFolder.Folders;
                Outlook.MAPIFolder folder = null;
                if (folders != null)
                    try
                    {
                        for (int i = 1; i <= folders.Count; i++)
                        {
                            //Outlook.MAPIFolder folder = folders.Item(i);
                            try
                            {
                                folder = folders[i];
                                if (folder != null && folder.DefaultItemType == Outlook.OlItemType.olAppointmentItem)
                                    try
                                    {
                                        Outlook.MAPIFolder folder1 = ns.GetFolderFromID(folder.EntryID, folder.StoreID);
                                        if (folder1 != null)
                                        {
                                            CondecoItemsEventsClass itemsEventSink = new CondecoItemsEventsClass(this);
                                            itemsEventSink.ConnectTo(folder1, true, false);
                                            itemsEvents.Add(itemsEventSink);
                                            //itemsEventSink = null;
                                        }
                                        //Outlook.MAPIFolder folder2 = ns.GetFolderFromID(folder.EntryID, folder.StoreID);
                                        //if (folder2 != null)
                                        //{

                                        //    CondecoFoldersEventsClass foldersEventSink = new CondecoFoldersEventsClass(this);
                                        //    foldersEventSink.ConnectTo(folder2, true, false);
                                        //    folderEvents.Add(foldersEventSink);
                                        //    foldersEventSink = null;
                                        //}
                                        ConnectToFolders(ns, folder);
                                    }
                                    catch
                                    {
                                        UtilityManager.LogMessage("ConnectToFolders:Catch  Error3:");
                                    }
                                    finally
                                    {
                                        Marshal.ReleaseComObject(folder);
                                    }
                            }
                            catch
                            {
                                UtilityManager.LogMessage("ConnectToFolders:Catch  Error2:");
                            }
                        }
                    }
                    catch
                    {
                        UtilityManager.LogMessage("ConnectToFolders:Catch  Error1:");
                    }
                    finally
                    {
                        Marshal.ReleaseComObject(folders);
                    }
            }
            catch
            {
                UtilityManager.LogMessage("ConnectToFolders:Catch outer Error:");
            }

            UtilityManager.LogMessage("ConnectToFolders: *******Finished**************** ");
        }
        //public void DoFolderAdd(Outlook.MAPIFolder folder)
        //{
        //    if (folder != null)
        //    {
        //        bool newFolderAdded = true;
        //        for (int i = folderEvents.Count - 1; i >= 0; i--)
        //        {
        //            try
        //            {
        //                Outlook.MAPIFolder listFolder = folderEvents[i].FolderObj as Outlook.MAPIFolder;
        //                if (folder.EntryID == listFolder.EntryID)
        //                {
        //                    newFolderAdded = false;
        //                    break;
        //                }
        //            }
        //            catch
        //            {
        //                folderEvents[i].RemoveConnection();
        //                folderEvents[i].Dispose();
        //            }
        //        }
        //        if (newFolderAdded)
        //        {
        //            Outlook.MAPIFolder newFolder = null;
        //            Outlook._NameSpace ns = OutlookApp.GetNamespace("MAPI");
        //            if (ns != null)
        //                try
        //                {
        //                    newFolder = ns.GetFolderFromID(folder.EntryID, folder.StoreID);
        //                }
        //                finally
        //                {
        //                    Marshal.ReleaseComObject(ns);
        //                }
        //            if (newFolder != null)
        //            {
        //                CondecoItemsEventsClass itemsEventSink = new CondecoItemsEventsClass(this);
        //                itemsEventSink.ConnectTo(newFolder, true, false);
        //                itemsEvents.Add(itemsEventSink);
        //                itemsEventSink = null;

        //                CondecoFoldersEventsClass foldersEventSink = new CondecoFoldersEventsClass(this);
        //                foldersEventSink.ConnectTo(newFolder, true, false);
        //                folderEvents.Add(foldersEventSink);
        //                foldersEventSink = null;
        //            }
        //        }
        //    }
        //}
        public void WriteToLog(string StringRes, string NodeName)
        {
            //MessageBox.Show(StringRes);
        }

        //private void ConnectToSelectedItem(object explorer)
        //{
        //    Outlook.MAPIFolder currentFolder = null;
        //    Outlook.Explorer currentExplorer = explorer as Outlook.Explorer;
        //    if (currentExplorer != null)
        //        try
        //        {
        //            currentFolder = currentExplorer.CurrentFolder;
        //            if (currentFolder != null)
        //                if (currentFolder.EntryID != OutboxFolderEntryID && currentFolder.DefaultItemType == OlItemType.olAppointmentItem)
        //                {
        //                    Outlook.Selection selection = null;
        //                    try
        //                    {
        //                        selection = currentExplorer.Selection as Outlook.Selection;
        //                        if (selection != null)
        //                            if (selection.Count > 0)
        //                            {
        //                                object item = selection.Item(1);
        //                                if (!isItemEventsConnected(item) && item is Outlook.AppointmentItem)
        //                                {
        //                                    if (selectedItemEvents == null)
        //                                        selectedItemEvents = new OutlookItemEventsClass1(this, true);
        //                                    selectedItemEvents.ConnectTo(item, true);
        //                                }
        //                            }
        //                    }
        //                    catch
        //                    {
        //                        // The Explorer has been closed and cannot be used for further operations. Review your code and restart Outlook.
        //                    }
        //                    finally
        //                    {
        //                        if (selection != null)
        //                            Marshal.ReleaseComObject(selection);
        //                    }
        //                }
        //        }
        //        finally
        //        {
        //            if (currentFolder != null)
        //                Marshal.ReleaseComObject(currentFolder);
        //        }
        //}
        //public string ItemInfo(object item)   //This is Commented by Paritosh .This method is not called anywhere 
        //{
        //    string s = string.Empty;

        //    if (item is Outlook.MailItem)
        //    {
        //        Outlook.MailItem mail = null;
        //        mail = item as Outlook.MailItem;
        //        if (mail != null)
        //            try
        //            {
        //                s += " MailItem with subject '";
        //                s += mail.Subject;
        //                s += "'. ";
        //                return s;
        //            }
        //            catch
        //            {
        //                return "";
        //            }
        //    }

        //    if (item is Outlook.AppointmentItem)
        //    {
        //        Outlook.AppointmentItem appointment = null;
        //        appointment = item as Outlook.AppointmentItem;
        //        if (appointment != null)
        //            try
        //            {
        //                s += " AppointmentItem with subject '";
        //                s += appointment.Subject;
        //                s += "'. ";
        //                return s;
        //            }
        //            catch
        //            {
        //                return "";
        //            }
        //    }

        //    if (item is Outlook.TaskItem)
        //    {
        //        Outlook.TaskItem task = null;
        //        task = item as Outlook.TaskItem;
        //        if (task != null)
        //            try
        //            {
        //                s += " TaskItem with subject '";
        //                s += task.Subject;
        //                s += "'. ";
        //                return s;
        //            }
        //            catch
        //            {
        //                return "";
        //            }
        //    }

        //    if (item is Outlook.JournalItem)
        //    {
        //        Outlook.JournalItem journal = null;
        //        journal = item as Outlook.JournalItem;
        //        if (journal != null)
        //            try
        //            {
        //                s += " JournalItem with subject '";
        //                s += journal.Subject;
        //                s += "'. ";
        //                return s;
        //            }
        //            catch
        //            {
        //                return "";
        //            }
        //    }

        //    if (item is Outlook.ContactItem)
        //    {
        //        Outlook.ContactItem contact = null;
        //        contact = item as Outlook.ContactItem;
        //        if (contact != null)
        //            try
        //            {
        //                s += " ContactItem with subject '";
        //                s += contact.Subject;
        //                s += "'. ";
        //                return s;
        //            }
        //            catch
        //            {
        //                return "";
        //            }
        //    }

        //    if (item is Outlook.PostItem)
        //    {
        //        Outlook.PostItem post = null;
        //        post = item as Outlook.PostItem;
        //        if (post != null)
        //            try
        //            {
        //                s += " PostItem with subject '";
        //                s += post.Subject;
        //                s += "'. ";
        //                return s;
        //            }
        //            catch
        //            {
        //                return "";
        //            }
        //    }

        //    if (item is Outlook.NoteItem)
        //    {
        //        Outlook.NoteItem note = null;
        //        note = item as Outlook.NoteItem;
        //        if (note != null)
        //            try
        //            {
        //                s += " NoteItem with subject '";
        //                s += note.Subject;
        //                s += "'. ";
        //                return s;
        //            }
        //            catch
        //            {
        //                return "";
        //            }
        //    }

        //    if (item is Outlook.DistListItem)
        //    {
        //        Outlook.DistListItem distList = null;
        //        distList = item as Outlook.DistListItem;
        //        if (distList != null)
        //            try
        //            {
        //                s += " DistListItem with subject '";
        //                s += distList.Subject;
        //                s += "'. ";
        //                return s;
        //            }
        //            catch
        //            {
        //                return "";
        //            }
        //    }
        //    return s;
        //}   
        private bool CompareItem(object item, object ItemObj)
        {
            try
            {
                if (itemEvents.Count > 0)
                {
                    if ((item is Outlook.MailItem) && (ItemObj is Outlook.MailItem))
                    {
                        try
                        {
                            if ((item as Outlook.MailItem).EntryID == (ItemObj as Outlook.MailItem).EntryID)
                                return true;
                        }
                        catch { return false; }
                    }


                    if ((item is Outlook.AppointmentItem) && (ItemObj is Outlook.AppointmentItem))
                    {
                        try
                        {
                            if ((item as Outlook.AppointmentItem).EntryID == (ItemObj as Outlook.AppointmentItem).EntryID)
                                return true;
                        }
                        catch { return false; }
                    }


                    if ((item is Outlook.TaskItem) && (ItemObj is Outlook.TaskItem))
                        try
                        {
                            if ((item as Outlook.TaskItem).EntryID == (ItemObj as Outlook.TaskItem).EntryID)
                                return true;
                        }
                        catch { return false; }

                    if ((item is Outlook.JournalItem) && (ItemObj is Outlook.JournalItem))
                        try
                        {
                            if ((item as Outlook.JournalItem).EntryID == (ItemObj as Outlook.JournalItem).EntryID)
                                return true;
                        }
                        catch { return false; }

                    if ((item is Outlook.ContactItem) && (ItemObj is Outlook.ContactItem))
                        try
                        {
                            if ((item as Outlook.ContactItem).EntryID == (ItemObj as Outlook.ContactItem).EntryID)
                                return true;
                        }
                        catch { return false; }

                    if ((item is Outlook.PostItem) && (ItemObj is Outlook.PostItem))
                        try
                        {
                            if ((item as Outlook.PostItem).EntryID == (ItemObj as Outlook.PostItem).EntryID)
                                return true;
                        }
                        catch { return false; }

                    if ((item is Outlook.NoteItem) && (ItemObj is Outlook.NoteItem))
                        try
                        {
                            if ((item as Outlook.NoteItem).EntryID == (ItemObj as Outlook.NoteItem).EntryID)
                                return true;
                        }
                        catch { return false; }

                    if ((item is Outlook.DistListItem) && (ItemObj is Outlook.DistListItem))
                        try
                        {
                            if ((item as Outlook.DistListItem).EntryID == (ItemObj as Outlook.DistListItem).EntryID)
                                return true;
                        }
                        catch { return false; }
                }
                return false;
            }
            catch (SystemException ex)
            {
                UtilityManager.LogMessage("Exception recorded in Method AddinModule.cs CompareItem() " + ex.Message);
                //MessageBox.Show(ex.Message);
                return false;
            }

        }

        private bool isItemEventsConnected(object item)
        {
            for (int i = 0; i < itemEvents.Count; i++)
            {
                if (CompareItem(item, itemEvents[i].ItemObj))
                {
                    return true;
                }
            }
            return false;
        }

        public void ConnectToSelectedItem(object explorer) //New One
        {
            UtilityManager.LogMessage("ConnectToSelectedItem(object explorer)");
            Outlook.MAPIFolder currentFolder = null;
            Outlook.Explorer currentExplorer = explorer as Outlook.Explorer;
            if (currentExplorer != null)
                try
                {
                    /* In case of outlook 2010 if the user opens outlook and open one email and dont close it. AFter 
                     * that if user tries to close the outlook from task bar then explorer will not be available due to outlook 2010
                     * lazy processing feature in the background. We need to check if explorer folder is there 
                     *and if explorer is not available in case outlook is shutting down then handle it gracefuly.
                     */
                    //if (currentExplorer.CurrentFolder != null)
                    //{
                    currentFolder = currentExplorer.CurrentFolder;
                    if (currentFolder != null)
                        try
                        {
                            //EN-11279
                            if (OutFoxFolderEntries != null && OutFoxFolderEntries.Count >= 2)
                            {
                                int index = -1;
                                try
                                {
                                    index = OutFoxFolderEntries.IndexOf(currentFolder.EntryID);
                                    if (index > -1)
                                    {
                                        OutboxFolderEntryID = OutFoxFolderEntries[index].ToString();
                                    }
                                }
                                catch { }
                            }
                        }
                        catch (Exception ex)
                        {
                            UtilityManager.LogMessage("ConnectToSelectedItem(object explorer) : Exception while reading OutFoxFolderEntries -" + ex.Message);
                        }
                    //EN-11279
                    if (currentFolder.EntryID != OutboxFolderEntryID)
                    {

                        Outlook.Selection selection = null;
                        try
                        {
                            selection = currentExplorer.Selection as Outlook.Selection;
                            if (selection != null)
                                if (selection.Count > 0) //DragnDropTesting
                                {
                                    //object item = selection.Item(1); //Print
                                    object item = selection[1];
                                    if (!isItemEventsConnected(item))
                                    //if (item is Outlook._AppointmentItem)
                                    {
                                        System.Diagnostics.Debug.WriteLine(" Binding ");
                                        if (selectedItemEvents == null)
                                            selectedItemEvents = new CondecoItemEventsClass(this, true);
                                        selectedItemEvents.ConnectTo(item, true);
                                    }
                                    else
                                    {
                                        UtilityManager.FreeCOMObject(item);
                                    }
                                    //Added by Paritosh for chevron to remove memory leaks
                                    //if (item != null) 
                                    //    UtilityManager.FreeCOMObject(item); 
                                }
                        }
                        catch
                        {
                            // The Explorer has been closed and cannot be used for further operations. Review your code and restart Outlook.
                        }
                        finally
                        {
                            if (selection != null)
                            {
                                Marshal.ReleaseComObject(selection);
                                selection = null;
                            }
                        }
                    }
                    //}
                }
                catch (Exception ex)
                {
                    /* In case of outlook 2010 if the user open outlook and open one email and dont close it. AFter 
                     * that if user tries to close the outlook from task bar then explorer will not be available due to outlook 2010
                     * lazy processing feature in the background. We need to  handle the exception
                     * if explorer is not available in case outlook is shutting down.
                     */
                    UtilityManager.LogMessage(" Connect To Selected Items Failure:" + ex.Message);
                }
                finally
                {
                    if (currentFolder != null)
                    {
                        Marshal.ReleaseComObject(currentFolder);
                        currentFolder = null;
                    }
                }
        }
        //EN-11279
        private List<string> EnumerateOutboxStores()
        {
            List<string> outboxentries = null;
            try
            {
                UtilityManager.LogMessage("AddinModule.cs (EnumerateOutboxStores) ****Started****");
                Outlook.Stores stores = this.OutlookApp.Session.Stores;
                if (stores != null)
                {
                    outboxentries = new List<string>();
                    foreach (Outlook.Store store in stores)
                    {
                        if (store.IsDataFileStore == true)
                        {
                            Outlook.MAPIFolder outboxFolder = store.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderOutbox);
                            if (outboxFolder != null && !string.IsNullOrEmpty(outboxFolder.EntryID))
                            {
                                if (!outboxentries.Contains(outboxFolder.EntryID))
                                {
                                    outboxentries.Add(outboxFolder.EntryID);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("AddinModule.cs (EnumerateOutboxStores) Exception  " + ex.Message);
            }
            return outboxentries;
        }
        //EN-11279
        private void AddinModule_AddinInitialize(object sender, EventArgs e)
        {
            try
            {
                UtilityManager.LogMessage("AddinModule_AddinInitialize: *******Started****************");
                UtilityManager.GetCondecoUserLanguage();
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(UtilityManager.UserLanguage);
                this.adxCmd_OpenInNewWin.Ribbons = ((AddinExpress.MSO.ADXRibbons)((AddinExpress.MSO.ADXRibbons.msrOutlookMailRead | AddinExpress.MSO.ADXRibbons.msrOutlookMailCompose)));
                if (this.HostMajorVersion >= 14)
                {
                    #region Desk Booking
                    if (UtilityManager.DeskBookingEnabled().Equals(1))
                    {
                        deskBookingHelper = new DeskBooking.DeskBookingHelper();
                        try
                        {
                            ADXOLSolutionFolder rootFolder = new ADXOLSolutionFolder();
                            rootFolder.FolderName = CondecoAddinV2.App_Resources.CondecoResources.Condeco_Desks;
                            rootFolder.FolderType = ADXOLSolutionFolderType.Notes;
                            rootFolder.ImageList = imgListRibbon2013;
                            rootFolder.Image = 1;
                            rootFolder.ImageTransparentColor = Color.Fuchsia;
                            adxolSolutionModule_Desk.Folders.Add(rootFolder);
                            Outlook.Explorer activeExp = OutlookApp.ActiveExplorer();
                            // MessageBox.Show(activeExp.NavigationPane.CurrentModule.Name);
                            if (activeExp != null)
                            {
                                PreviouNavigationModule = activeExp.NavigationPane.CurrentModule;
                            }
                            UtilityManager.FreeCOMObject(activeExp);
                            if (UtilityManager.ConnectionMode() == 2)
                            {
                                //EN - 8396
                                //  this.adxCmd_OpenInNewWin.Ribbons = AddinExpress.MSO.ADXRibbons.msrOutlookExplorer; 
                            }
                            //Added by Ravi Goyal to write the user Outlook navigation preferences-PRB0041123
                            if (this.HostMajorVersion >= 14)
                            {
                                try
                                {
                                    UtilityManager.LogMessage("AddinModule_AddinInitialize:Desk Booking is set to True in App.config, setting Outlook Navigation preferences ");
                                    SetOutlookNavigationPreferences();
                                    UtilityManager.LogMessage("AddinModule_AddinModule_AddinInitialize:Setting Outlook Navigation preferences *Completed*");
                                }
                                catch (Exception ex)
                                {
                                    UtilityManager.LogMessage("AddinModule_AddinInitialize:While Desk Booking is set to True in App.config, getting Outlook Navigation preferences is failed due to:- " + ex.Message);
                                }
                            }
                            //End Added by Ravi Goyal to write the user Outlook navigation preferences-PRB0041123
                        }
                        catch (Exception ex)
                        {
                            UtilityManager.LogMessage("AddinModule_AddinInitialize Failure:" + ex.Message);

                        }
                    }
                    #endregion
                }
                this.CondecoBookingGroup.Caption = global::CondecoAddinV2.App_Resources.CondecoResources.Room_Booking_Group_Text;
                this.CondecoRibbonTab.Caption = global::CondecoAddinV2.App_Resources.CondecoResources.AddIn_Name;
                this.RibbonRoomSearchButton.Caption = global::CondecoAddinV2.App_Resources.CondecoResources.Room_Booking;
                this.RibbonRoomGridButton.Caption = global::CondecoAddinV2.App_Resources.CondecoResources.RibbonRoomGridText;
                this.RibbonCancelBookingButton.Caption = global::CondecoAddinV2.App_Resources.CondecoResources.Cancel_Booking;
                this.RibbonRoomSearchButton.SuperTip = global::CondecoAddinV2.App_Resources.CondecoResources.Room_Booking_Ribbon_SuperTip;
                this.RibbonRoomSearchButton.ScreenTip = global::CondecoAddinV2.App_Resources.CondecoResources.Room_Booking_Ribbon_ScreenTip;
                this.RibbonRoomGridButton.SuperTip = global::CondecoAddinV2.App_Resources.CondecoResources.RoomGrid_Ribbon_SuperTip;
                this.RibbonRoomGridButton.ScreenTip = global::CondecoAddinV2.App_Resources.CondecoResources.RoomGrid_Ribbon_ScreenTip;
                this.RibbonCancelBookingButton.SuperTip = global::CondecoAddinV2.App_Resources.CondecoResources.CancelBooking_Ribbon_SuperTip;
                this.RibbonCancelBookingButton.ScreenTip = global::CondecoAddinV2.App_Resources.CondecoResources.CancelBooking_Ribbon_ScreenTip;
                this.CondecoBookingGroup.ScreenTip = global::CondecoAddinV2.App_Resources.CondecoResources.Room_Booking_Group_Text;


                //--start-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402-check for outlook 2013 version and this event will be created for outlook 2010 & above versions, not applicable for outlook 2007.
                if (this.HostMajorVersion > 12)
                {
                    this.adxOutlookEvents.ItemSend += new ADXOlItemSend_EventHandler(adxOutlookEvents_ItemSend);
                    UtilityManager.LogMessage("AddinModule_AddinInitialize: adxOutlookEvents_ItemSend event created");
                }
                //--end-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402
                UtilityManager.LogMessage("AddinModule_AddinInitialize: *******Finished****************");
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("AddinModule_AddinInitialize:Catch -" + ex.Message + ":Stack trace =" + ex.StackTrace);
            }
        }
        //--start-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402 (Only for >outlook 2007, not for outlook 2007 or lesser versions.
        private void adxOutlookEvents_ItemSend(object sender, ADXOlItemSendEventArgs e)
        {
            try
            {
                UtilityManager.LogMessage("adxOutlookEvents_ItemSend *** -Started- ***");
                Outlook.AppointmentItem currApp = null;
                Outlook.Inspector ins = OutlookApp.ActiveInspector();
                if (ins != null)
                {
                    object item = ins.CurrentItem;
                    if (item != null)
                    {
                        if (item is Outlook.AppointmentItem)
                        {
                            currApp = item as Outlook.AppointmentItem;
                            if (UtilityManager.IsSharedCalendarItem(currApp))
                            {
                                if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                                {
                                    IbookingHelper bookingHelper = new BookingHelper();
                                    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                                    {
                                        UtilityManager.LogMessage("adxOutlookEvents_ItemSend *** -cancel meeting on shared calendar started- ***");
                                        bookingHelper.DeleteCondecoBooking(currApp, true);
                                        UtilityManager.LogMessage("adxOutlookEvents_ItemSend *** -cancel meeting on shared calendar finished- ***");
                                    }
                                }
                            }
                            else
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("adxOutlookEvents_ItemSend:Catch -" + ex.Message + ":Stack trace =" + ex.StackTrace);
            }
        }
        private void CleanExclusionVariables()
        {
            try
            {
                UtilityManager.LogMessage("**AddinModule_CleanExclusionVariables started**");
                if (exclusionlist != null)
                {
                    exclusionlist.Clear();
                    UtilityManager.LogMessage("AddinModule_CleanExclusionVariables-exclusionlist clear done");
                }
                if (exclusioneditedlist != null)
                {
                    exclusioneditedlist.Clear();
                    UtilityManager.LogMessage("AddinModule_CleanExclusionVariables-exclusioneditedlist clear done");
                }
                if (AppMasterLoc != null)
                {
                    AppMasterLoc.Clear();
                    UtilityManager.LogMessage("AddinModule_CleanExclusionVariables-AppMasterLoc clear done");
                }
                if (exclusionoccurencesinfo != null)
                {
                    exclusionoccurencesinfo = null;
                    UtilityManager.LogMessage("AddinModule_CleanExclusionVariables-exclusionoccurencesinfo clear done");
                }
                if (ModuleMissingDates != null)
                {
                    ModuleMissingDates.Clear();
                }
                IsSendPressed = false;
                UtilityManager.LogMessage("AddinModule_CleanExclusionVariables-IsSendPressed-Set to **False** done");
                UtilityManager.LogMessage("**AddinModule_CleanExclusionVariables finished**");
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("AddinModule_CleanExclusionVariables:Error-" + ex.Message);
            }
        }
        private void SendExclusionEmails(List<Outlook.AppointmentItem> ExclusionListItems, bool IsSeriesEdited = false)
        {
            if (IsSeriesEdited)
            {
                UtilityManager.LogMessage("AddinModule.SendExclusionEmails *******Series is in Edit mode,Sending Exclusion emails**************** ");
            }
            else
            {
                UtilityManager.LogMessage("AddinModule.SendExclusionEmails *******New Series created,Sending Exclusion emails**************** ");
            }
            if (ExclusionListItems != null)
            {
                try
                {
                    foreach (Outlook.AppointmentItem appItem in ExclusionListItems)
                    {
                        UtilityManager.LogMessage("AddinModule.SendExclusionEmails *******Started**************** ");
                        UtilityManager.LogMessage("AddinModule.SendExclusionEmails: Exclusion meeting subject:-**" + appItem.Subject.ToString() + "** for location-" + appItem.Location.ToString() + "**sent started**");
                        ((Outlook._AppointmentItem)appItem).Send();
                        UtilityManager.LogMessage("AddinModule.SendExclusionEmails: Exclusion meeting subject:-**" + appItem.Subject.ToString() + "** for location-" + appItem.Location.ToString() + "sent successfully");
                    }
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage(" adxOutlookEvents_SendExclusionEmails" + ex.Message);
                }
                finally
                {
                    UtilityManager.LogMessage(" adxOutlookEvents_SendExclusionEmails Finally() started");
                    ExclusionListItems.Clear();
                    UtilityManager.LogMessage(" adxOutlookEvents_SendExclusionEmails Finally() completed");
                }
            }
        }
        //--end-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402
        private void adxOutlookEvents_ExplorerActivate(object sender, object explorer)
        {
            try
            {
                //string s = "  =  ADXOutlookAppEvents.ExplorerActivate. ";
                //  s += sExplorerInfo(explorer);
                // WriteToLog(s, "Node_ExplorerActivate");

                ConnectToSelectedItem(explorer);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage(" AddinModule_AddinInitialize Failure:" + ex.Message);

            }
        }

        public void RibbonButtons_OnClick(object sender, AddinExpress.MSO.IRibbonControl control, bool pressed)
        {


            AddinExpress.MSO.ADXRibbonButton button = null;

            button = (AddinExpress.MSO.ADXRibbonButton)sender;
            //adxRibbonButton2.Enabled = !adxRibbonButton2.Enabled;

            if (button == null)
                return;

            AddinExpress.OL.ADXOlForm visibleForm = GetTabBar();

            if (visibleForm == null)
                return;
            WebBrowser browserControl = null;
            PictureBox loaderPicture = null;
            Label loaderProgress = null;
            IsRecurranceClick = false;

            //TP#21109 Added by Vineet Yadav on 15 Feb 2014
            IsBookRoomClicked = false;
            IsRoomGridClicked = false;
            if (!MsgPopUpSyncInitiateBooking())
            {
                return;
            }

            switch ((RibbonButtonEnum)button.Tag)
            {
                case RibbonButtonEnum.RoomBooking:
                    //IsBookRoomClicked = true;
                    RibbonRoomGridButton.Pressed = false;
                    RibbonRoomSearchButton.Pressed = false;
                    RibbonCancelBookingButton.Pressed = false;
                    RibbonRoomBookingButton.Pressed = true;
                    visibleForm.Location = new Point(-1, -1);
                    visibleForm.Height = 1000;
                    visibleForm.Visible = true;
                    browserControl = visibleForm.Controls.Find("webBrowserControl", true).Length == 1 ? (WebBrowser)visibleForm.Controls.Find("webBrowserControl", true)[0] : null;
                    loaderPicture = visibleForm.Controls.Find("picLoader", true).Length == 1 ? (PictureBox)visibleForm.Controls.Find("picLoader", true)[0] : null;
                    loaderProgress = visibleForm.Controls.Find("ProgressText", true).Length == 1 ? (Label)visibleForm.Controls.Find("ProgressText", true)[0] : null;
                    if (browserControl != null && loaderPicture != null)
                    {

                        BookingManager bookingManager = new BookingManager(browserControl, visibleForm.OutlookAppObj, visibleForm.AddinModule, loaderPicture, loaderProgress, true);
                        bookingManager.RoomGridClickStopBrowserEvent = false;
                        bookingManager.Collapse += new BookingManager.CollapseHandler(BookingManager_Collapse);
                        bookingManager.BookingManager_Activated(null, null);
                        ObjForm = bookingManager;
                    }
                    RibbonRoomBookingButton.Pressed = true;
                    break;

                case RibbonButtonEnum.RoomSearch:
                    //TP#21109 Added by Vineet Yadav on 15 Feb 2014
                    IsBookRoomClicked = true;
                    RibbonRoomBookingButton.Pressed = false;
                    RibbonRoomGridButton.Pressed = false;
                    RibbonRoomSearchButton.Pressed = true;
                    RibbonCancelBookingButton.Pressed = false;

                    //this.FindRibbonControl("ShowAppointmentPage")
                    visibleForm.Location = new Point(-1, -1);
                    visibleForm.Height = 1000;
                    visibleForm.Visible = true;
                    browserControl = visibleForm.Controls.Find("webBrowserControl", true).Length == 1 ? (WebBrowser)visibleForm.Controls.Find("webBrowserControl", true)[0] : null;
                    loaderPicture = visibleForm.Controls.Find("picLoader", true).Length == 1 ? (PictureBox)visibleForm.Controls.Find("picLoader", true)[0] : null;
                    loaderProgress = visibleForm.Controls.Find("ProgressText", true).Length == 1 ? (Label)visibleForm.Controls.Find("ProgressText", true)[0] : null;
                    if (browserControl != null && loaderPicture != null)
                    {
                        UtilityManager.LogMessage(" BookingManager bookingManager");
                        BookingManager bookingManager = new BookingManager(browserControl, visibleForm.OutlookAppObj, visibleForm.AddinModule, loaderPicture, loaderProgress, false, ObjForm);
                        bookingManager.RoomGridClickStopBrowserEvent = false;
                        bookingManager.Collapse += new BookingManager.CollapseHandler(BookingManager_Collapse);
                        UtilityManager.LogMessage(" bookingManager.BookingManager_Activated");
                        bookingManager.BookingManager_Activated(null, null);
                        ObjForm = bookingManager;
                        // bookingManager.validateFail 
                    }
                    RibbonRoomSearchButton.Pressed = true;


                    break;

                case RibbonButtonEnum.RoomGrid:
                    RibbonRoomBookingButton.Pressed = false;
                    RibbonRoomGridButton.Pressed = true;
                    RibbonRoomSearchButton.Pressed = false;
                    RibbonCancelBookingButton.Pressed = false;
                    IsRoomGridClicked = true;

                    visibleForm.Height = 1000;
                    visibleForm.Visible = true;
                    browserControl = visibleForm.Controls.Find("webBrowserControl", true).Length == 1 ? (WebBrowser)visibleForm.Controls.Find("webBrowserControl", true)[0] : null;
                    loaderPicture = visibleForm.Controls.Find("picLoader", true).Length == 1 ? (PictureBox)visibleForm.Controls.Find("picLoader", true)[0] : null;
                    loaderProgress = visibleForm.Controls.Find("ProgressText", true).Length == 1 ? (Label)visibleForm.Controls.Find("ProgressText", true)[0] : null;

                    if (browserControl != null && loaderPicture != null)
                    {
                        CheckRoomGrid checkRoomGrid = new CheckRoomGrid(browserControl, visibleForm.OutlookAppObj, visibleForm.AddinModule, visibleForm.InspectorObj, loaderPicture, loaderProgress, ObjForm);
                        checkRoomGrid.Collapse += new CheckRoomGrid.CollapseHandler(CheckRoom_Collapse);
                        checkRoomGrid.RoomBookingClickStopBrowserEvent = false;
                        checkRoomGrid.CheckRoomGrid_Activated(null, null);
                        ObjForm = checkRoomGrid;
                    }
                    break;

                case RibbonButtonEnum.CancelBooking:
                    RibbonRoomBookingButton.Pressed = false;
                    RibbonRoomGridButton.Pressed = false;
                    RibbonRoomSearchButton.Pressed = false;
                    RibbonCancelBookingButton.Pressed = true;



                    browserControl = visibleForm.Controls.Find("webBrowserControl", true).Length == 1 ? (WebBrowser)visibleForm.Controls.Find("webBrowserControl", true)[0] : null;
                    if (browserControl != null)
                    {
                        CancelRoom cancelRoom = new CancelRoom(browserControl, visibleForm.OutlookAppObj, visibleForm.AddinModule, visibleForm.InspectorObj);
                        if (cancelRoom.ValidateCancelRoomRequest())
                        {
                            //Below line Commented  and Cursor wait  added by Paritosh  to resolve  TP #20471 While cancelling the meeting from outlook UI get distorted.
                            // HideTabBar();
                            cancelRoom.Collapse += new CancelRoom.CollapseHandler(CancelRoom_Collapse);
                            Cursor.Current = Cursors.WaitCursor;
                            cancelRoom.CancelMeeting();
                            Cursor.Current = Cursors.Default;
                        }
                        else
                        {
                            cancelRoom = null;
                        }
                    }

                    break;
            }
        }
        public bool MsgPopUpSyncInitiateBooking()
        {
            bool ret = false;
            try
            {
                Outlook.Inspector cInsp = OutlookApp.ActiveInspector();
                if (cInsp == null) return ret;
                if (!(cInsp.CurrentItem is Outlook.AppointmentItem)) return ret;
                Outlook.AppointmentItem currentItem = cInsp.CurrentItem as Outlook.AppointmentItem;
                if (currentItem == null) return ret;
                if (AppointmentHelper.IsSyncAppointment(currentItem as Outlook.AppointmentItem))
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.SyncBookingValidation);
                    UtilityManager.FreeCOMObject(currentItem);
                    return ret;
                }
                UtilityManager.FreeCOMObject(currentItem);
                UtilityManager.FreeCOMObject(cInsp);
                return true;
            }
            catch
            {
                return true;
            }
        }

        public AddinExpress.OL.ADXOlForm GetTabBar()
        {

            AddinExpress.OL.ADXOlForm visibleForm = null;
            try
            {
                visibleForm = MyTabBar.GetCurrentForm(AddinExpress.OL.EmbeddedFormStates.Visible);
                if (visibleForm == null)
                    MyTabBar.GetForm(this.OutlookApp.ActiveInspector()).Visible = true;

                visibleForm = MyTabBar.GetCurrentForm(AddinExpress.OL.EmbeddedFormStates.Visible);
                if (visibleForm != null)
                    visibleForm.Location = new System.Drawing.Point(0, 0);
            }
            catch { }

            return visibleForm;

        }



        public void HideTabBar()
        {

            AddinExpress.OL.ADXOlForm visibleForm = null;
            try
            {
                visibleForm = MyTabBar.GetCurrentForm(AddinExpress.OL.EmbeddedFormStates.Visible);
                if (visibleForm != null)
                {
                    visibleForm.Visible = false;
                    visibleForm.Location = new System.Drawing.Point(-1000, -1000);
                }
            }
            catch { }

        }

        void BookingManager_Collapse(object sender)
        {
            try
            {
                BookingManager manager = (BookingManager)sender;
                if (manager != null)
                {
                    manager.Deactivate();

                }

                RibbonStandardButtons_OnAction(manager, null, true, null);
                if (manager != null)
                {
                    manager = null;
                }
                //En-40
                ////////////////////////Outlook._Inspector  olInsp = OutlookApp.ActiveInspector();


                ////////////////////////if (olInsp != null)
                ////////////////////////{
                ////////////////////////    object item = olInsp.CurrentItem;
                ////////////////////////    Outlook.AppointmentItem appitem = item as Outlook.AppointmentItem;
                ////////////////////////    if (appitem != null)
                ////////////////////////    {
                ////////////////////////        CondecoItemEventsClass itemEventSink = new CondecoItemEventsClass(this, false);
                ////////////////////////        itemEventSink.ConnectTo(appitem, true);
                ////////////////////////        itemEvents.Add(itemEventSink);

                ////////////////////////        if (selectedItemEvents != null)
                ////////////////////////            if (CompareItem(appitem, selectedItemEvents.ItemObj))
                ////////////////////////            {
                ////////////////////////                selectedItemEvents.Dispose();
                ////////////////////////                selectedItemEvents = null;
                ////////////////////////            }
                ////////////////////////    }
                ////////////////////////}
            }
            catch { }
        }

        void CheckRoom_Collapse(object sender)
        {
            try
            {
                CheckRoomGrid grid = (CheckRoomGrid)sender;
                if (grid != null)
                    grid.Deactivate();

                RibbonStandardButtons_OnAction(null, null, true, null);

                if (grid != null)
                {
                    grid = null;
                }
            }
            catch { }
        }


        void CancelRoom_Collapse(object sender)
        {
            try
            {
                CancelRoom cancelRoom = (CancelRoom)sender;
                if (cancelRoom != null)
                    cancelRoom.Deactivate();

                RibbonStandardButtons_OnAction(null, null, true, null);

                if (cancelRoom != null)
                {
                    cancelRoom = null;
                }
            }
            catch { }
        }

        private void AddinModule_OnRibbonBeforeCreate(object sender, string ribbonId)
        {
            if (this.HostMajorVersion == 15)
            {
                // this.adxOlExplorerCommandBar_DeskBooking.UseForRibbon = false;
                RibbonRoomBookingButton.ImageList = imgListRibbon2013;
                RibbonRoomBookingButton.Image = 3;
                RibbonRoomSearchButton.ImageList = imgListRibbon2013;
                RibbonRoomSearchButton.Image = 3;
                RibbonRoomGridButton.ImageList = imgListRibbon2013;
                RibbonRoomGridButton.Image = 4;
                RibbonCancelBookingButton.ImageList = imgListRibbon2013;
                RibbonCancelBookingButton.Image = 5;

            }
            else if (this.HostMajorVersion == 14)
            {
                //   this.adxOlExplorerCommandBar_DeskBooking.UseForRibbon = false;
                RibbonRoomBookingButton.ImageList = imgListRibbon2013;
                RibbonRoomBookingButton.Image = 3;
                RibbonRoomSearchButton.ImageList = imgListRibbon2010;
                RibbonRoomSearchButton.Image = 3;
                RibbonRoomGridButton.ImageList = imgListRibbon2010;
                RibbonRoomGridButton.Image = 4;
                RibbonCancelBookingButton.ImageList = imgListRibbon2010;
                RibbonCancelBookingButton.Image = 5;
            }
            else if (this.HostMajorVersion == 12)
            {
                // this.adxOlExplorerCommandBar_DeskBooking.UseForRibbon = false;
                RibbonRoomSearchButton.ImageList = imgListRibbon;
                RibbonRoomSearchButton.Image = 0;
                RibbonRoomBookingButton.ImageList = imgListRibbon;
                RibbonRoomBookingButton.Image = 0;
                RibbonRoomGridButton.ImageList = imgListRibbon;
                RibbonRoomGridButton.Image = 1;
                RibbonCancelBookingButton.ImageList = imgListRibbon;
                RibbonCancelBookingButton.Image = 2;

            }
            else
            {

                //this.adxOlExplorerCommandBar_DeskBooking.UseForRibbon = false;

                RibbonRoomSearchButton.ImageList = imgListRibbon2013;
                RibbonRoomSearchButton.Image = 3;
                RibbonRoomGridButton.ImageList = imgListRibbon2013;
                RibbonRoomGridButton.Image = 4;
                RibbonCancelBookingButton.ImageList = imgListRibbon2013;
                RibbonCancelBookingButton.Image = 5;

            }


        }



        private void adxInviteAttendeesCommand_OnAction(object sender, AddinExpress.MSO.IRibbonControl control, bool pressed, AddinExpress.MSO.ADXCancelEventArgs e)
        {

            try
            {
                ////Outlook.Inspector cInsp = OutlookApp.ActiveInspector();
                ////if (cInsp == null) return;
                ////if (!(cInsp.CurrentItem is Outlook.AppointmentItem)) return;
                ////Outlook.AppointmentItem currentItem = cInsp.CurrentItem as Outlook.AppointmentItem;
                ////if (currentItem == null) return;


                ////if ((RibbonRoomSearchButton.Pressed == true || RibbonRoomGridButton.Pressed == true) && AppointmentDataInfo.GetWebBookedclick(currentItem))
                ////{
                ////    AppointmentDataInfo.SetOlkAttandeeOrTitleChanged(currentItem, true);
                ////}
                if (RibbonRoomSearchButton.Pressed == true)
                {
                    BookingManager_Collapse(null);

                }
                else if (RibbonRoomGridButton.Pressed == true)
                {
                    CheckRoom_Collapse(null);
                }
                else if (RibbonCancelBookingButton.Pressed == true)
                {
                    CancelRoom_Collapse(null);
                }
            }
            catch
            {
            }

        }

        private void adxCancelInvitationCommand_OnAction(object sender, AddinExpress.MSO.IRibbonControl control, bool pressed, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            try
            {
                Outlook.Inspector cInsp = OutlookApp.ActiveInspector();
                if (cInsp == null) return;
                if (!(cInsp.CurrentItem is Outlook.AppointmentItem)) return;
                Outlook.AppointmentItem currentItem = cInsp.CurrentItem as Outlook.AppointmentItem;
                if (currentItem == null) return;


                ////// Outlook.AppointmentItem appItemTemp = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                ////if ((RibbonRoomSearchButton.Pressed == true || RibbonRoomGridButton.Pressed == true) && AppointmentDataInfo.GetWebBookedclick(currentItem))
                ////{
                ////    AppointmentDataInfo.SetOlkAttandeeOrTitleChanged(currentItem, true);
                ////}

                if (RibbonRoomSearchButton.Pressed == true)
                {
                    BookingManager_Collapse(null);
                    if (currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                    {
                        AppointmentHelper.IsCancelAppInvitees = true;
                        AppointmentHelper.ClearAppointmentData(currentItem);
                        IsMeetingSaveNCloseOnly = false;
                    }

                }
                else if (RibbonRoomGridButton.Pressed == true)
                {
                    CheckRoom_Collapse(null);
                    if (currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                    {
                        AppointmentHelper.IsCancelAppInvitees = true;
                        AppointmentHelper.ClearAppointmentData(currentItem);
                        IsMeetingSaveNCloseOnly = false;
                    }

                }
                else if (RibbonCancelBookingButton.Pressed == true)
                {
                    IsMeetingSaveNCloseOnly = false;
                    // CancelRoom_Collapse(null);
                }
                UtilityManager.FreeCOMObject(currentItem);
            }
            catch
            {
            }

        }

        private void AddinModule_OnRibbonBeforeLoad(object sender, AddinExpress.MSO.ADXRibbonBeforeLoadEventArgs e)
        {
            // Below Lines added by Paritosh to  visible Appointment Ribbon always  
            try
            {
                e.Xml = ReplaceGetVisibleWithVisible(e.Xml, CondecoRibbonTab, true);
            }
            catch
            {
            }
        }

        private string ReplaceGetVisibleWithVisible(string ribbonXML, ADXRibbonTab aBuiltinRibbonTab, bool visible)
        {
            string result = string.Empty;
            try
            {
                StaticribbonXML = ribbonXML.ToString();
                staticaBuiltinRibbonTab = aBuiltinRibbonTab as ADXRibbonTab;
                string[] lines = ribbonXML.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                for (int i = 0; i < lines.Length; i++)
                {
                    if (lines[i].Contains(aBuiltinRibbonTab.IdMso))
                    {
                        lines[i] = lines[i].Replace(@"getVisible=""getVisible_Callback""",
                            @"visible=""" + visible.ToString().ToLower() + @"""");
                        break;
                    }
                }

                if (lines.Length == 1)
                {
                    result = "";
                }
                else
                {
                    for (int i = 0; i < lines.Length; i++)
                        result += lines[i] + Environment.NewLine;
                }
            }
            catch
            {
            }
            return result;
        }

        private void adxOutlookEvents_ExplorerBeforeViewSwitch(object sender, ADXOlExplorerBeforeViewSwitchEventArgs e)
        {

        }
        //EN - 7700
        private void ValidateRibbonButtonsPressedState(ADXRibbonTab aBuiltinRibbonTab)
        {
            AddinExpress.MSO.ADXRibbonButton button = null;
            try
            {
                if (aBuiltinRibbonTab != null && aBuiltinRibbonTab.Controls.Contains(CondecoBookingGroup))
                {
                    if (CondecoBookingGroup != null && CondecoBookingGroup.Controls.Count > 1)
                    {
                        foreach (object objcontrol in CondecoBookingGroup.Controls)
                        {
                            button = objcontrol as ADXRibbonButton;
                            if (button != null && button.Pressed == true)
                            {
                                button.Pressed = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Exception in AddinModule.cs:validateButtonState-  " + ex.Message);
            }
            finally
            {
                button = null;
            }

        }
        //EN - 7700
        private void adxOutlookEvents_Quit(object sender, EventArgs e)
        {
            CleanUpProcess();
            if (SSOFormAuthentication.IsInstanceCreated)
            {
                SSOFormAuthentication.DisposeSSO(true);
            }
        }


        private void adxOutlookEvents_ExplorerClose(object sender, object explorer)
        {
            int count = 0;
            Outlook._Explorers expls = null;

            try
            {
                expls = OutlookApp.Explorers;
                count = expls.Count;
            }
            catch
            {
            }
            finally
            {
                if (expls != null)
                    Marshal.ReleaseComObject(expls);
            }
        }

        #region Desk Booking
        private void adxOutlookEvents_ExplorerBeforeFolderSwitch(object sender, ADXOlExplorerBeforeFolderSwitchEventArgs e)
        {
            try
            {
                if (this.HostMajorVersion >= 14 && UtilityManager.DeskBookingEnabled().Equals(1))
                {
                    bool IsSSOUserNotAuth = false;
                    Outlook.Explorer activeExp = OutlookApp.ActiveExplorer();
                    if (activeExp != null)
                    {
                        if (activeExp.NavigationPane.CurrentModule.Name == "Solutions" || activeExp.NavigationPane.CurrentModule.Name.ToLower() == "add-ins")
                        {
                            //ADDED BY CHANDRA FOR CASE WHERE USER IS NOT ENTERED IN TO CONDECO IN fORM aUTHENTICATION
                            if (UtilityManager.UserName.Trim().Equals("") && UtilityManager.ConnectionMode() == 1 && UtilityManager.IsCondecoContactable())
                            {
                                UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2010_2013);
                            }
                            else
                            {
                                //DeskBooking SSO Implmentation 
                                if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsDeskBookingSSOAuthentication())
                                {
                                    if (!UtilityManager.IsSSOTimeOutMsgShow)
                                    {

                                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                                    }

                                    IsSSOUserNotAuth = true;
                                    //   e.Cancel = true;
                                    // return;
                                }
                                if (!IsSSOUserNotAuth)
                                {
                                    this.DeskFormOpen();
                                }
                            }
                            e.Cancel = true;
                            if (PreviouNavigationModule != null)
                            {
                                if (PreviouNavigationModule.Name.ToString().ToLower() == "mail")
                                {
                                    Outlook.NavigationModule objNavigationModule = activeExp.NavigationPane.Modules.GetNavigationModule(Outlook.OlNavigationModuleType.olModuleNotes);
                                    activeExp.NavigationPane.CurrentModule = objNavigationModule;
                                    activeExp.NavigationPane.CurrentModule = PreviouNavigationModule;
                                }
                                else
                                {
                                    Outlook.NavigationModule obj = activeExp.NavigationPane.Modules.GetNavigationModule(Outlook.OlNavigationModuleType.olModuleMail);
                                    activeExp.NavigationPane.CurrentModule = obj;
                                    activeExp.NavigationPane.CurrentModule = PreviouNavigationModule;

                                }
                            }

                        }
                        else
                        {
                            PreviouNavigationModule = activeExp.NavigationPane.CurrentModule;

                        }
                    }
                    Marshal.ReleaseComObject(activeExp);
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("adxOutlookEvents_ExplorerBeforeFolderSwitch Failure:" + ex.Message);
            }
        }

        private void adxOutlookEvents_NewExplorer(object sender, object explorer)
        {
            try
            {
                if (this.HostMajorVersion >= 14 && UtilityManager.DeskBookingEnabled().Equals(1))
                {
                    bool IsSSOuserNotAuth = false;
                    Outlook._Explorer currentExplorer = explorer as Outlook.Explorer;
                    if (currentExplorer != null)
                    {
                        if (currentExplorer.CurrentFolder.Name == CondecoAddinV2.App_Resources.CondecoResources.Condeco_Desks)
                        {
                            try
                            {
                                //ADDED BY CHANDRA FOR CASE WHERE USER IS NOT ENTERED IN TO CONDECO PART IN fORM aUTHENTICATION
                                if (UtilityManager.UserName.Trim().Equals("") && UtilityManager.ConnectionMode() == 1 && UtilityManager.IsCondecoContactable())
                                {
                                    //nothing to do //case of non mapped user
                                }
                                else
                                {
                                    //DeskBooking SSO Implmentation 
                                    if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsDeskBookingSSOAuthentication())
                                    {
                                        IsSSOuserNotAuth = true;
                                        if (!UtilityManager.IsSSOTimeOutMsgShow)
                                        {

                                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                                        }

                                    }
                                    if (!IsSSOuserNotAuth)
                                    {
                                        this.DeskFormOpen();
                                    }
                                }
                                currentExplorer.Close();
                            }
                            catch (Exception ex)
                            {
                                UtilityManager.LogMessage(" DeskFormOpen() Failure:" + ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("adxOutlookEvents_NewExplorer Failure:" + ex.Message);
            }
        }

        private void ShowNavigationPaneDesksForm()
        {
            try
            {

                if (adxOlFormsCollectionItem_NavigationPaneDesks == null)
                {
                    adxOlFormsCollectionItem_NavigationPaneDesks = new AddinExpress.OL.ADXOlFormsCollectionItem(this.components);
                    this.adxOlFormsManager1.Items.Add(adxOlFormsCollectionItem_NavigationPaneDesks);


                    adxOlFormsCollectionItem_NavigationPaneDesks.UseOfficeThemeForBackground = true;
                    adxOlFormsCollectionItem_NavigationPaneDesks.Cached = AddinExpress.OL.ADXOlCachingStrategy.OneInstanceForAllFolders;

                    adxOlFormsCollectionItem_NavigationPaneDesks.ExplorerItemTypes = ((AddinExpress.OL.ADXOlExplorerItemTypes)((((((((AddinExpress.OL.ADXOlExplorerItemTypes.olMailItem | AddinExpress.OL.ADXOlExplorerItemTypes.olAppointmentItem)
                    | AddinExpress.OL.ADXOlExplorerItemTypes.olContactItem)
                    | AddinExpress.OL.ADXOlExplorerItemTypes.olTaskItem)
                    | AddinExpress.OL.ADXOlExplorerItemTypes.olJournalItem)
                    | AddinExpress.OL.ADXOlExplorerItemTypes.olNoteItem)
                    | AddinExpress.OL.ADXOlExplorerItemTypes.olPostItem)
                    | AddinExpress.OL.ADXOlExplorerItemTypes.olDistributionListItem)));


                    adxOlFormsCollectionItem_NavigationPaneDesks.FormClassName = "CondecoAddinV2.DeskBooking.NavigationPaneDesks";
                    adxOlFormsCollectionItem_NavigationPaneDesks.ExplorerLayout = AddinExpress.OL.ADXOlExplorerLayout.BottomNavigationPane;

                    Outlook._Explorer activeExplorer = OutlookApp.ActiveExplorer();
                    adxOlFormsCollectionItem_NavigationPaneDesks.ApplyTo(activeExplorer);
                    Marshal.ReleaseComObject(activeExplorer);
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                UtilityManager.LogMessage(" ShowNavigationPaneDesksForm() Failure:" + ex.Message);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage(" ShowNavigationPaneDesksForm() Failure:" + ex.Message);
            }

        }

        private void DeskFormOpen()
        {
            try
            {
                //  object OutlookAppObj, object AddinModule, PictureBox loaderPicture
                //PictureBox loaderPicture = visibleForm.Controls.Find("picLoader", true).Length == 1 ? (PictureBox)visibleForm.Controls.Find("picLoader", true)[0] : null;
                //  PictureBox loaderPicture = new PictureBox();
                deskBookingHelper.OpenDeskModel(OutlookApp);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage(" DeskFormOpen() Failure:" + ex.Message);
            }
        }
        #endregion


        private void adxRibbonRemoveFrmCalender_OnAction(object sender, IRibbonControl control, bool pressed, ADXCancelEventArgs e)
        {

        }

        public void AddinModule_OnSendMessage(object sender, ADXSendMessageEventArgs e)
        {
            UtilityManager.LogMessage("AddinModule_OnSendMessage:: *******Started****************");
            if (e.Message == WM_MYMESSAGE)
            {
                try
                {
                    //For sync V6 Open Appointment when move appoitment to already alooted slot 
                    object obj = (this).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                    AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                    RibbonButtons_OnClick(obj, null, true);
                    // curritemAppTobeDeleted.Delete();
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("AddinModule_OnSendMessage:Catch -" + ex.Message);
                }
                finally
                {

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            if (e.Message == WM_MYToolBar)
            {
                try
                {
                    //System.Threading.Thread.Sleep(1000);
                    //if (!CondecoRibbonTab.Visible)
                    // CondecoRibbonTab.Visible = true;
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("AddinModule_OnSendMessage:Catch -" + ex.Message);
                }
                finally
                {

                }
            }

            if (e.Message == WM_MYMyAppDeleted)
            {
                try
                {
                    if (curritemAppTobeDeleted != null)
                    {
                        curritemAppTobeDeleted.Delete();
                    }

                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("AddinModule_OnSendMessage:Catch -" + ex.Message);
                }
                finally
                {
                    if (curritemAppTobeDeleted != null)
                    {
                        UtilityManager.FreeCOMObject(curritemAppTobeDeleted);
                    }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                }
            }
            UtilityManager.LogMessage("AddinModule_OnSendMessage:: *******end****************");
        }

        //private void SaveCloseUpdate(Outlook.AppointmentItem currApp)
        //{
        //    currApp.Save();
        //    CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(currApp, true);
        //  if (currMeeting.BookingID > 0)
        //        {
        //            StringBuilder sb = new StringBuilder();
        //            // || IsSubjectChanged
        //            bool subjectReallyChanged = true;
        //            bool startDateReallyChanged = true;
        //            bool endDateReallyChanged = true;

        //                if (currApp.Subject == null) currApp.Subject = " ";

        //                if (currApp.Subject.ToLower().Equals(currMeeting.MeetingTitle.ToLower()))
        //                {
        //                    subjectReallyChanged = false;
        //                }

        //                if (subjectReallyChanged)
        //                    sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_Subject);

        //            long currentTicks;
        //            Outlook.RecurrencePattern rpItem = null;
        //            if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
        //            {
        //                rpItem = currApp.GetRecurrencePattern(); 
        //            }

        //            DateTime origSeriesStart=DateTime.MinValue;;
        //            DateTime origSeriesEnd = DateTime.MinValue;

        //                DateTime condecoStart = DateTime.MinValue;
        //                DateTime itemStart = DateTime.MinValue;
        //                if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
        //                {
        //                    rpItem = currApp.GetRecurrencePattern();
        //                    currentTicks = rpItem.PatternStartDate.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
        //                    itemStart = new DateTime(currentTicks);
        //                    itemStart = UtilityManager.ConvertDateToTZ(itemStart, currMeeting.LocationID, currMeeting.OriginalTZ);
        //                    //Changed by Anand for Issue 5 for Case TC10 & TC17
        //                    //currentTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //                    //changed again currentTicks = OrigStart.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //                    currentTicks = origSeriesStart.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //                    //Changed end
        //                    condecoStart = new DateTime(currentTicks);


        //                }
        //                else
        //                {
        //                    currentTicks = currApp.Start.Date.Ticks + currApp.Start.TimeOfDay.Ticks;
        //                    itemStart = new DateTime(currentTicks);
        //                    itemStart = UtilityManager.ConvertDateToTZ(itemStart, currMeeting.LocationID);
        //                    currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //                    condecoStart = new DateTime(currentTicks);
        //                }

        //                if (itemStart.CompareTo(condecoStart) == 0)
        //                {
        //                    startDateReallyChanged = false;
        //                }


        //                if (startDateReallyChanged)
        //                    sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_StartDate);
        //                // IsStartDateChanged = false;

        //            // || IsEndDateChanged


        //                DateTime condecoEnd = DateTime.MinValue;
        //                DateTime itemEnd = DateTime.MinValue;
        //                if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
        //                {
        //                    rpItem = currApp.GetRecurrencePattern();
        //                    currentTicks = rpItem.PatternEndDate.Date.Ticks + rpItem.EndTime.TimeOfDay.Ticks;
        //                    itemEnd = new DateTime(currentTicks);
        //                    itemEnd = UtilityManager.ConvertDateToTZ(itemEnd, currMeeting.LocationID, currMeeting.OriginalTZ);
        //                    //Changed by Anand for Issue 5 for Case TC10 & TC17
        //                    //currentTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
        //                    //changed again currentTicks = OrigEnd.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
        //                    currentTicks = origSeriesEnd.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
        //                    //Changed end
        //                    condecoEnd = new DateTime(currentTicks);


        //                }
        //                else
        //                {
        //                    currentTicks = currApp.End.Date.Ticks + currApp.End.TimeOfDay.Ticks;
        //                    itemEnd = new DateTime(currentTicks);
        //                    itemEnd = UtilityManager.ConvertDateToTZ(itemEnd, currMeeting.LocationID);
        //                    currentTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
        //                    condecoEnd = new DateTime(currentTicks);
        //                }
        //                if (rpItem != null)
        //                    UtilityManager.FreeCOMObject(rpItem);

        //                if (itemEnd.CompareTo(condecoEnd) == 0)
        //                {
        //                    endDateReallyChanged = false;
        //                }

        //                if (endDateReallyChanged)
        //                    sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_EndDate);
        //                // IsEndDateChanged = false;




        //            if (!string.IsNullOrEmpty(sb.ToString()))
        //            {

        //                MessageBox.Show(sb.ToString());
        //                ////Please make sure you update your changes using the Book Room Tab and then click on Save and Close (or Send Update if the meeting has attendees)
        //                //if (!IsAlertDisplayed)
        //                //{
        //                //    if (currApp.IsRecurring && currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
        //                //    {
        //                //        if (startDateReallyChanged || endDateReallyChanged)
        //                //            isItemNotInSync = true;
        //                //        result = HandleSeriesRequest2(sb.ToString(), returnTrueForCancel);
        //                //    }
        //                //    else
        //                //    {
        //                //        if (startDateReallyChanged || endDateReallyChanged)
        //                //           isItemNotInSync = true;

        //                //        if (!IsDatePastMsgShow)
        //                //        {
        //                //            result = HandleNormalRequest(currMeeting, currApp, sb.ToString());

        //                //        }
        //                //        else
        //                //        {
        //                //            IsDatePastMsgShow = false;
        //                //        }
        //                //    }
        //                //    if (result) IsAlertDisplayed = true;
        //                //}

        //            }
        //        }
        //        UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco:*********Finished********");
        //    }

        //private void CleanUpProcess()
        //{

        //    if (deletedItem != null)
        //    {
        //        deletedItem.RemoveConnection();
        //        deletedItem.Dispose();
        //        UtilityManager.LogMessage("AddinModule_AddinBeginShutdown: Connection removed from deleted items folder ");
        //    }
        //    if (selectedItemEvents != null)
        //        selectedItemEvents.Dispose();
        //    if (folderEvents != null)
        //    {
        //        foreach (CondecoFoldersEventsClass folderEventSink in folderEvents)
        //            folderEventSink.Dispose();
        //        folderEvents.Clear();
        //    }
        //    if (itemsEvents != null)
        //    {
        //        foreach (CondecoItemsEventsClass itemEventSink in itemsEvents)
        //            itemEventSink.Dispose();
        //        itemsEvents.Clear();
        //    }
        //    if (itemEvents != null)
        //    {
        //        foreach (CondecoItemEventsClass itemEventSink in itemEvents)
        //            itemEventSink.Dispose();
        //        itemEvents.Clear();
        //    }
        //}

        private void adxOutlookEvents_Startup(object sender, EventArgs e)
        {

        }

        private void adxRibbonComSendUpdate_OnAction(object sender, IRibbonControl control, bool pressed, ADXCancelEventArgs e)
        {
            // MessageBox.Show("Hello");
        }
        private void adxRibbonComSaveClose_OnAction(object sender, IRibbonControl control, bool pressed, ADXCancelEventArgs e)
        {
            Outlook.Inspector cInsp = OutlookApp.ActiveInspector();
            if (cInsp == null) return;
            if (!(cInsp.CurrentItem is Outlook.AppointmentItem)) return;
            Outlook.AppointmentItem currentItem = cInsp.CurrentItem as Outlook.AppointmentItem;
            if (currentItem == null) return;
            IbookingHelper bookingHelper = new BookingHelper();
            if (bookingHelper.IsCondecoBooking(currentItem))
            {
                if (AppointmentDataInfo.GetAppItemCondecoBookingOpen(currentItem))
                {
                    AppointmentDataInfo.SetAppItemSkipSync(currentItem, true);
                }
                else
                {
                    AppointmentDataInfo.SetAppItemSkipSync(currentItem, false);
                }
                AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currentItem, true);


                //////if ((RibbonRoomSearchButton.Pressed == true || RibbonRoomGridButton.Pressed == true) && AppointmentDataInfo.GetWebBookedclick(currentItem) && (AppointmentDataInfo.GetOlkAttandeeOrTitleChanged(currentItem) || AppointmentDataInfo.GetAfterSaveSubjectchanges(currentItem)))
                //////{
                //////  DialogResult clickOk=  MessageBox.Show(CondecoResources.AddingInviteeOrTitleAfterBooking, CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //////  if (clickOk==DialogResult.OK)
                //////  {
                //////      object obj = (this).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                //////      this.IsUndoMeetingChanges = true;
                //////      this.RibbonButtons_OnClick(obj, null, true);
                //////  }
                //////}
            }
            UtilityManager.FreeCOMObject(currentItem);
            UtilityManager.FreeCOMObject(cInsp);
            bookingHelper = null;
            //Outlook.Inspector cInsp = OutlookApp.ActiveInspector();
            //if (cInsp == null) return;
            //if (!(cInsp.CurrentItem is Outlook.AppointmentItem)) return;
            //Outlook.AppointmentItem currentItem = cInsp.CurrentItem as Outlook.AppointmentItem;
            //if (currentItem == null) return;
            //if (bookingHelper.IsCondecoBooking(currentItem))
            //{
            //    SaveCloseUpdate(currentItem);
            //}
            //UtilityManager.FreeCOMObject(currentItem);
            //UtilityManager.FreeCOMObject(cInsp);
        }
        private void RibbonButtonAppDelete_OnAction(object sender, IRibbonControl control, bool pressed, ADXCancelEventArgs e)
        {
            Outlook.Inspector cInsp = OutlookApp.ActiveInspector();
            if (cInsp == null) return;
            if (!(cInsp.CurrentItem is Outlook.AppointmentItem)) return;
            Outlook.AppointmentItem currentItem = cInsp.CurrentItem as Outlook.AppointmentItem;
            if (currentItem == null) return;
            IbookingHelper bookingHelper = new BookingHelper();
            if (bookingHelper.IsCondecoBooking(currentItem))
            {
                if (currentItem.IsRecurring && (currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence))
                {
                    AppointmentDataInfo.SetAppRecuDeleteToolbarClick(currentItem, true);
                }
                else
                {
                    AppointmentDataInfo.SetAppRecuDeleteToolbarClick(currentItem, false);
                }
                //if (AppointmentDataInfo.GetAppItemCondecoBookingOpen(currentItem))
                //{
                //    AppointmentDataInfo.SetAppItemSkipSync(currentItem, true);
                //}
                //else
                //{
                //    AppointmentDataInfo.SetAppItemSkipSync(currentItem, false);
                //}
                //AppointmentDataInfo.SetAppItemIsAppItemSaveAndCloseClick(currentItem, true);
            }
            UtilityManager.FreeCOMObject(currentItem);
            UtilityManager.FreeCOMObject(cInsp);
            bookingHelper = null;

        }

        private void RibbonRoomSearchButton_PropertyChanging(object sender, ADXRibbonPropertyChangingEventArgs e)
        {
            try
            {
                UtilityManager.LogMessage("RibbonRoomSearchButton_PropertyChanging: Started");
                Outlook.AppointmentItem appItem = AppointmentHelper.GetCurrentAppointment(OutlookApp);
                if (appItem != null && appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && e.PropertyType.ToString().ToLower() == "supertip")
                {
                    SendKeys.Send("{TAB}");
                }

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Exception in AddinModule_RibbonRoomSearchButton_PropertyChanging: " + ex.Message);
            }
        }

        private void RibbonRoomGridButton_PropertyChanging(object sender, ADXRibbonPropertyChangingEventArgs e)
        {
            try
            {
                UtilityManager.LogMessage("RibbonRoomGridButton_PropertyChanging: Started");
                Outlook.AppointmentItem appItem = AppointmentHelper.GetCurrentAppointment(OutlookApp);
                if (appItem != null && appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting && e.PropertyType.ToString().ToLower() == "supertip")
                {
                    SendKeys.Send("{TAB}");
                }

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Exception in RibbonRoomGridButton_PropertyChanging: " + ex.Message);
            }
        }

    }
}