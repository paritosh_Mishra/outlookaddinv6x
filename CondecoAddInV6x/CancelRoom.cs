using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using Outlook = Microsoft.Office.Interop.Outlook;
using CondecoAddinV2.Repository;
using CondecoAddinV2.App_Resources;
using CondecoAddinV2.Constants;

 
namespace CondecoAddinV2
{
    /// <summary>
    /// Summary description for CancelRoom.
    /// </summary>
    public class CancelRoom : Form
    {
        public delegate void CollapseHandler(object sender);
        public event CollapseHandler Collapse;

        //private Button button1;
        //private Label label1;
        //private Button btnNo; 
        private System.ComponentModel.IContainer components = null;
       // private static AppointmentHelper appHelper = new AppointmentHelper();
      //  private static SyncManager syncManager = new SyncManager();
        //private Button btnClose;
        //private GroupBox groupBox1;
      //  private static BookingHelper bookingHelper = new BookingHelper();
        private WebBrowser webBrowser1;
        //bool condition = true;

        object OutlookAppObj = null;
        object AddinModule = null;
        object InspectorObj = null;

        public CancelRoom(WebBrowser browserObject, object OutlookAppObj, object AddinModule, object InspectorObj)
 	     {
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            this.OutlookAppObj = OutlookAppObj;
            this.AddinModule = AddinModule;
            this.InspectorObj = InspectorObj;

            //string iconpath = UtilityManager.IconPath + "\\cancel.ico";
            //System.Drawing.Icon ico = new System.Drawing.Icon(iconpath);
            //this.Icon = ico;


        }

        ~CancelRoom()
        {
          //  Deactivate();
        }


        public new void Deactivate()
        {
            if (this.webBrowser1 != null)
            {
                this.webBrowser1.DocumentCompleted -= webBrowser1_DocumentCompleted;
                // this.gridBrowser.Navigating -= gridBrowser_Navigating;
                this.webBrowser1 = null;
            }

        }
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if(components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }
 
        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CancelRoom));
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(863, 296);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(71, 52);
            this.webBrowser1.TabIndex = 5;
            this.webBrowser1.Visible = false;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // CancelRoom
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(975, 400);
            this.Controls.Add(this.webBrowser1);
           // this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CancelRoom";
            this.Text = App_Resources.CondecoResources.Cancel_Room;// "Cancel Room";
            this.Activated += new System.EventHandler(this.CancelRoom_Activated);
            this.Click += new System.EventHandler(this.CancelRoom_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    UtilityManager.CollapseCondecoRegion(this);
        //    UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
        //}

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public void CancelMeeting()
        {
            UtilityManager.LogMessage("CancelMeeting() Method  *************Started************ ");
            IbookingHelper bookingHelper = new BookingHelper();
            if (this.InspectorObj != null)
            {
                Outlook.Inspector currInsp = this.InspectorObj as Outlook.Inspector;
                object item = currInsp.CurrentItem;
                if (item == null) return;
                Outlook.AppointmentItem delAppItem = null;
                try
                {
                    if (item is Outlook.AppointmentItem)
                    {
                        delAppItem = item as Outlook.AppointmentItem;
                        if (delAppItem != null)
                        {
                            UtilityManager.LogMessage("CancelMeeting() Method delAppItem");
                            if (bookingHelper.IsCondecoBooking(delAppItem))
                            {
                                if (delAppItem.Location == null)
                                    delAppItem.Location = " ";
                                if (!delAppItem.Location.ToLower().Contains("cancelled"))
                                {
                                    //string postID = appHelper.GetAppointmentPostID(delAppItem);
                                    UtilityManager.LogMessage("CancelMeeting()  Going to Delete from DB");
                                    bookingHelper.DeleteCondecoBooking(delAppItem);
                                    UtilityManager.LogMessage("CancelMeeting()   Delete from DB Done");
                                    if (!bookingHelper.IsVCWithin1hour)
                                    {
                                        //groupBox1.Visible = false;
                                        UtilityManager.LogMessage("CancelMeeting()   Is Not VC created within an hour");
                                        CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(delAppItem, true ,Canceledseies :true);
                                        if (UtilityManager.SyncBookingEnabled().Equals(1) && AppointmentHelper.IsSyncAppointment(delAppItem) && !delAppItem.Location.Contains(UtilityManager.LocationStart))
                                        {
                                            string synLoc = curMeeting.MainLocation.Substring(curMeeting.MainLocation.LastIndexOf('-'));
                                            delAppItem.Location = delAppItem.Location + synLoc;
                                        }
                                        else
                                        {

                                            AppointmentHelper.SetAppointmentLocation(delAppItem, curMeeting.MainLocation);
                                        }
                                        UtilityManager.LogMessage("CancelMeeting()   Set Cancelled Sufix done ");
                                        string namedValue = "";
                                        namedValue = UserPropertiesExtension.GetNamedPropertyRecord(delAppItem, CustomProperty.AppointmentLocationSynced);
                                        if (String.IsNullOrEmpty(namedValue))
                                        {
                                            UserPropertiesExtension.SetNamedPropertyRecord(delAppItem, CustomProperty.AppointmentLocationSynced, Convert.ToString("True"));
                                        }
                                        UtilityManager.LogMessage("CancelMeeting()   AppointmentLocationSynced set on SetNamedPropertyRecord ");
                                        if (delAppItem.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                                            delAppItem.Save();

                                        if (delAppItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                                        {
                                            (this.AddinModule as AddinModule).IsMeetingSeriesCanceled = true;
                                            //Disable below function as it already call on function -bookingHelper.DeleteCondecoBooking(delAppItem);
                                            //////IsyncManager syncManager = new SyncManager();
                                            //////syncManager.SyncIndiviudallyEditedItems(delAppItem, true);
                                            //////syncManager = null;
                                            UtilityManager.LogMessage("CancelMeeting()   SyncIndiviudallyEditedItems done ");
                                         
                                        }
                                        else
                                        {
                                            AppointmentHelper.ClearAppointmentData(delAppItem);
                                        }
                                        // delAppItem.Save();
                                        UtilityManager.LogMessage("CancelMeeting()   ClearAppointmentData  ");
                                        //Added below line to Fix  issue  20779
                                        Collapse(this);
                                        UtilityManager.LogMessage("CancelMeeting()   Collapse Done ");
                                    }
                                }
                                if (delAppItem != null)
                                {
                                    UtilityManager.FreeCOMObject(delAppItem);
                                    delAppItem = null;

                                    GC.Collect();
                                    GC.WaitForPendingFinalizers();

                                }

                            }

                        }
                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("CancelMeeting() Method  Catch ex=" + ex.Message + " # Ex.stack =" + ex.StackTrace);
                }
                finally
                {
                    if (delAppItem != null)
                    {
                        if (delAppItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            UtilityManager.LogMessage("CancelMeeting():Finally- Item is olApptMaster, will be released on send event ");
                        }
                        else
                        {
                            UtilityManager.FreeCOMObject(delAppItem);
                            delAppItem = null;
                            UtilityManager.LogMessage("CancelMeeting(): Finally- UtilityManager.FreeCOMObject(delAppItem) completed successully");
                        }
                        bookingHelper = null;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                    }
                }
                UtilityManager.LogMessage("CancelMeeting() Method  *************End************ ");
            }
        }

        public void CancelRoom_Activated(object sender, EventArgs e)
        {
            try
            {
                //groupBox1.Visible = false;
                if (!ValidateCancelRoomRequest())
                    Collapse(this);
                
                
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred in CancelRoom" + ex.Message + " " + ex.StackTrace);
            }
        }

        public bool ValidateCancelRoomRequest()
        {
            bool result = false;
            string message = "";

            if (!UtilityManager.IsCondecoContactable())
            {
                message = CondecoResources.Condeco_NotContactable;
                DisplayAlert(message);
                return result;
                
            }
            else
            {
                if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                {
                    // User_Not_Authorized
                    if (!UtilityManager.IsSSOTimeOutMsgShow)
                    {

                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    }
                    //UtilityManager.IsSSOTimeOutMsgShow = false;
                    
                    return result;
                }
            }
            //ADDED BY CHANDRA FOR CASE WHERE USER IS NOT ENTERED IN TO CONDECO PART IN fORM aUTHENTICATION
            if (UtilityManager.UserName.Trim().Equals("") && UtilityManager.ConnectionMode() == 1)
            {
                UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized_RoomBooking);
                return result;
            }
            //END-CHANDRA
            
            Outlook.AppointmentItem appItem = null;
            UtilityManager.LogMessage("CancelRoom- ValidateCancelRoomRequest");
            appItem = AppointmentHelper.GetCurrentAppointment(this.OutlookAppObj);
            if (appItem == null) return result;

            //Below validation  added to resolved the TFS bug -Bug 3258:Add-in V6/Win7/Outlook 2010 - Getting wrong alert while clicking on Cancel booking button for web initiated booking
            if (AppointmentHelper.SyncIsWebInitiated(appItem))
            {
                UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.Sync_WebInitatedBooking);
                return result;
            }

            if (DateTime.Now.CompareTo(appItem.Start) >= 0 && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                message = CondecoResources.Cancelled_PastBooking;
                DisplayAlert(message);
                return result;
            }
            IbookingHelper bookingHelper = new BookingHelper();
            CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
            bookingHelper = null;
           
            // Past Booking check
            if (curMeeting.BookingID == 0)
            {
                DisplayAlert(CondecoResources.Booking_NotavailableTo_Cancell);
                return false;
            }
            long meetingTicks = curMeeting.DateFrom.Date.Ticks + curMeeting.TimeFrom.TimeOfDay.Ticks;            
            DateTime meetingDateTime = new DateTime(meetingTicks);
            DateTime currentDateTime = UtilityManager.ConvertDateToTZ(DateTime.Now, curMeeting.LocationID);
            if (currentDateTime.CompareTo(meetingDateTime) >= 0 && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                message = CondecoResources.Cancelled_PastBooking;
            }
            // Meeting in Progress check
            if (curMeeting.RealStartTime.CompareTo(currentDateTime) >= 0)
            {
                message = CondecoResources.Cancelled_StartedBooking;
            }
            if (curMeeting.BookingRejected)
            {
                message = CondecoResources.Booking_Already_Rejected;
            }
            // Booking is Cancelled
            if (curMeeting.DeleteBooking || curMeeting.BookingRejected)
            {
                message = CondecoResources.Booking_Already_Cancelled;
            }
            if (!string.IsNullOrEmpty(message))
            {
                DisplayAlert(message);
            }
            else
            {
                //All check passed so return true
                result = true;
            }
            UtilityManager.FreeCOMObject(appItem);
                //Added By ritesh for TP - 10503
            //string postID = appHelper.GetAppointmentPostID(appItem);
            //if (!bookingHelper.PermissionCheck(postID, UtilityManager.GetCurrentUserForPost()))
            if(!AppointmentDataInfo.GetHavingPermission(appItem))
            {
                message = CondecoResources.Check_Booking_Permission;
                DisplayAlert(message);
                result = false;
            }
            //End By ritesh
            
            return result;

        }
        private void DisplayAlert(string message)
        {
             if (!string.IsNullOrEmpty(message))
            {
                UtilityManager.ShowWarningMessage(message);
                //this.GetType().InvokeMember("HideFloating", System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, null, this, new object[] { });
               // UtilityManager.CollapseCondecoRegion(this);
                //UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
                //this.groupBox1.Visible = false;
                this.webBrowser1.Url = new Uri(UtilityManager.GetSplashPath());
                // this.btnNo.PerformClick();
                 
                //this.Visible = false;
               // UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
               // this.Visible = true;
            }
        }

        //private void CancelRoom_ADXBeforeFormShow()
        //{
        //    this.Visible = appHelper.IsCondecoRegionVisible(this.InspectorObj as Outlook.Inspector);
        //}

        private void CancelRoom_Click(object sender, EventArgs e)
        {
           
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if(Collapse !=null)
            Collapse(this);
        }
    }
}

