using System;
using System.Collections.Generic;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Globalization;
using CondecoAddinV2.App_Resources;
using CondecoAddinV2.Constants;
using CondecoAddinV2.Repository;

namespace CondecoAddinV2
{
    interface IsyncManager
    {
       void SyncIndiviudallyEditedItems(Outlook.AppointmentItem curritem, bool isCondecoBookingcheckPrev = false, AddinModule CurrentAddinModule =null, bool IsSeriesEdited =false, bool IsSeriesDeleted= false );
       void SyncAppointmentWithCondeco(Outlook.AppointmentItem curritem);
      // void SyncSeriesBookingHasDST(Outlook._AppointmentItem appitem, CondecoMeeting currMeeting);
       void SyncOccurrenceBooking(CondecoMeeting currMeeting,Outlook.AppointmentItem appitem);
       void HanldeIndiviudalyEditedItems(Outlook.AppointmentItem appItem, Outlook.RecurrencePattern recPattern, List<CondecoMeeting> editedMeetings, AddinModule CurrentAddinModule = null, bool IsSeriesEdited = false, bool IsSeriesDeleted = false,bool IsMeetingCloseMailSend =false);
       void HanldeMissingItems(Outlook.RecurrencePattern recPattern, List<DateTime> missingAppointments, AddinModule CurrentAddinModule = null, bool MissingMeeting =true);
    }

    public sealed class SyncManager : IsyncManager
    {
       // private static AppointmentHelper appHelper = new AppointmentHelper();
        //private static BookingHelper bookingHelper = new BookingHelper();
        public  bool IsNormalAppSyncToDBShowNoMsg { get;set;}
        public int OutlookVersion { get; set; }
        public SyncManager()
        {
        }

        public void SyncAppointmentWithCondeco(Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco:*********Started********");
            UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco:Syncing item with Subject" + appItem.Subject + ":Start Date:" + appItem.Start + "End Date" + appItem.End);
            IbookingHelper bookingHelper = new BookingHelper();
            try
            {
                if (appItem == null) return;
                if (AppointmentHelper.IsNewAppointment(appItem)) return;
                //if (!bookingHelper.IsCondecoBookingV2(appItem)) return;
                  if (!bookingHelper.IsCondecoBooking(appItem)) return;

                //if (UtilityManager.IsForceCultureENGB())
                //{
                //    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                //}

                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
                if (!IsSyncingRequired(currMeeting, appItem)) return;

                switch (appItem.RecurrenceState)
                {
                    case Outlook.OlRecurrenceState.olApptNotRecurring:
                        SyncNormalBooking(currMeeting, appItem);
                        break;
                    case Outlook.OlRecurrenceState.olApptMaster:
                        //SyncSeriesBooking(appItem, true);
                        SyncSeriesBooking(appItem, true, appItem);
                        break;
                    case Outlook.OlRecurrenceState.olApptOccurrence:
                    case Outlook.OlRecurrenceState.olApptException:
                        if (bookingHelper.IsNonCondecoOccurrence(appItem)) return;
                        bool seriesUpdated = false;
                        Outlook.AppointmentItem parentItem = appItem.Parent as Outlook.AppointmentItem;
                        if (parentItem != null)
                        {
                            //Changed by Anand : Checking return value and if series master changed then open instance is invalid and no further action possible
                            //seriesUpdated = SyncSeriesBooking(parentItem, false);
                            seriesUpdated = SyncSeriesBooking(parentItem, false, appItem);
                            UtilityManager.FreeCOMObject(parentItem);
                        }
                        if (seriesUpdated)
                        {
                            UtilityManager.ShowWarningMessage(CondecoResources.Series_Synced);
                            UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco: ShowWarningMessage: " + CondecoResources.Series_Synced);
                            //added by Anand on 6-Mar-2013 for issue 12179
                            try
                            {
 //Added by Ravi Goyal (13-Oct-2015) to Fix PRB0041450- Outlook Crashed when open the manage room booking in single instance after series approved by Admin through web
                                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence)
                                {
                                    if (parentItem != null)
                                    {
                                        appItem.Location = parentItem.Location;
                                        appItem.Start = parentItem.Start;
                                        appItem.End = parentItem.End;
                                        appItem.Subject = parentItem.Subject;
                                        UtilityManager.FreeCOMObject(parentItem);
                                        UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco-Series sync completed successfully after opening occurence of meeting"); 
                                    }
                                }
                                else
                                {
                                    UtilityManager.FreeCOMObject(parentItem);
                                }
                                //End Added by Ravi Goyal (13-Oct-2015)
                               //check 27/10/15 Outlook.Inspector ins = appItem.GetInspector;
                              //check 27/10/15 ((Outlook._Inspector)ins).Close(Outlook.OlInspectorClose.olDiscard);
                               
                            }
                            catch (System.Exception ex)
                            {
                                UtilityManager.LogMessage("Error on closing inspector " + ex.Message);
                            }

                        }
                        //End Change
                        else
                        {  
						 UtilityManager.FreeCOMObject(parentItem);
                        }
                            bool isappsync = AppointmentDataInfo.GetAppItemSkipSync(appItem);
                            if (!isappsync)
                            {
                                SyncOccurrenceBooking(currMeeting, appItem);
                            }
                          if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                            {
                                try
                                {
                                    UtilityManager.LogMessage("SyncAppointmentWithCondeco:Item RecurrenceState is olApptException, releasing com the object");
                                   // UtilityManager.FreeCOMObject(appItem);
                                    UtilityManager.LogMessage("SyncAppointmentWithCondeco:com the object release successfully");
                                }
                                catch 
                                {
                                }
                            }
                        break;
                    default:
                        //SyncOccurrenceBooking(appItem); 
                        break;
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred while syning appontment:" + ex.Message);
                UtilityManager.LogMessage("Error stacked trace while syning appontment:" + ex.StackTrace);
            }
            bookingHelper = null;
            UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco:*********Finished********");
        }

        private void SyncNormalBooking(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("SyncManager.SyncNormalBooking:*********Started********");
            UtilityManager.LogMessage("SyncManager.SyncNormalBooking:Syncing item with Subject: " + appItem.Subject + ":Start Date:" + appItem.Start + "End Date" + appItem.End);

            bool itemSynced = false;
           
            if (currMeeting.BookingID == 0) return;
            bool isappsync = AppointmentDataInfo.GetAppItemSkipSync(appItem); 
             if (!isappsync)
            {
                itemSynced = SyncItem(currMeeting, appItem);
                ClearAppointmentDataIfRequired(currMeeting, appItem);
            }
            
            if (itemSynced)
            {
                
                UtilityManager.LogMessage("SyncManager.SyncNormalBooking: Appointment has been synced");
                if (!IsNormalAppSyncToDBShowNoMsg)
                {
                    UtilityManager.ShowWarningMessage(CondecoResources.Appointment_Synced);
                }
            }

            UtilityManager.LogMessage("SyncManager.SyncNormalBooking:*********Finished********");
        }

        //Changed by Anand : Added bool return type to identify that series master had been modified.
        public bool SyncSeriesBooking(Outlook.AppointmentItem appItem, bool showMessage,Outlook.AppointmentItem MainAppointment)
        {
            IbookingHelper bookingHelper = new BookingHelper();
            UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:*********Started********");
            UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Syncing item with Subject: " + appItem.Subject + ":Start Date:" + appItem.Start + "End Date" + appItem.End);
            bool itemUpdated = false;
            // if (appItem.RecurrenceState == OlRecurrenceState.olApptException || appItem.RecurrenceState == OlRecurrenceState.olApptOccurrence)
            CondecoMeeting currMeeting = bookingHelper.GetCondecoSeriesMeeting(appItem, false);
            // need to implement rejected bookings


            //Sync the Subject
            //Changed by Anand : Added trim in comparison as most of the time its invalidating the object due to of space
            if (!string.IsNullOrEmpty(currMeeting.MeetingTitle) && !appItem.Subject.Trim().ToLower().Equals(currMeeting.MeetingTitle.Trim().ToLower()))
            {
                appItem.Subject = currMeeting.MeetingTitle;
                itemUpdated = true;
                UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Syncing item Subject to " + currMeeting.MeetingTitle);
            }//End Change
            if (appItem.Location == null)
                appItem.Location = " ";

            //Sync the Location
            string newLoc = "";

            if (!AppointmentHelper.SyncIsOwaInitiatedBooking(MainAppointment))
            {
                 newLoc = AppointmentHelper.FormatLocationInCondecoStyle(currMeeting.MainLocation);
            }
            else
            {
                newLoc = appItem.Location;
            }
            if (!string.IsNullOrEmpty(newLoc) && !appItem.Location.ToLower().Contains(newLoc.ToLower()))
            {
                //appItem.Location = currMeeting.MainLocation;
                bool locationUpdated=  AppointmentHelper.SetAppointmentLocation(appItem, currMeeting.MainLocation);
                if(locationUpdated) itemUpdated = true;
                UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Syncing item location to " + currMeeting.MainLocation);
            }

            //bool isCrossDayBooking = false;
            //isCrossDayBooking = bookingHelper.isCrossDayRecurranceBooking(appItem);
            //List<CondecoMeeting> currMeetinglist = bookingHelper.GetRecurranceBookingWithLocalTimeConversion(appItem, isCrossDayBooking);
            List<CondecoMeeting> currMeetinglist = bookingHelper.GetRecurranceBookingHasDST(appItem, true,true,false);
            if (currMeetinglist.Count > 0)
            {
                //ON web Outlook booking date time doesn't edit
                foreach (CondecoMeeting cMeeting in currMeetinglist)
                {
                   // bool bln = UpdateSeriesBookingHasDST(appItem, cMeeting, isCrossDayBooking);
                    bool bln = UpdateSeriesBookingHasDST(appItem, cMeeting);
                    if (bln)
                    {
                        itemUpdated = true;
                    }
                }
               
            }
            else
            {
                Outlook.RecurrencePattern rpItem = appItem.GetRecurrencePattern();
                if (UpdateStartDateTimeInSeries(rpItem, currMeeting))
                    itemUpdated = true;
                if (UpdateEndDateTimeInSeries(rpItem, currMeeting))
                    itemUpdated = true;

                UtilityManager.FreeCOMObject(rpItem);
            }

            currMeetinglist = null;

            if (itemUpdated)
            {
                try
                {
                    appItem.Save();
                    UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Series Item changes saved sucessfully");
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred while SyncSeriesBooking appontment:" + ex.Message);
                    UtilityManager.LogMessage("Error stacked trace while SyncSeriesBooking appontment:" + ex.StackTrace);
                }

                ClearAppointmentDataIfRequired(currMeeting, appItem);

                if (showMessage)
                    UtilityManager.ShowWarningMessage(CondecoResources.Series_Synced);
            }
            bookingHelper = null;
            UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:*********Finished********");
            return itemUpdated;
        }

        private static bool UpdateEndDateTimeInSeries(Outlook.RecurrencePattern rpItem, CondecoMeeting currMeeting)
        {
            bool itemUpdated = false;
            //long meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
          //  long meetingTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
          //  DateTime currMeetingItemSeriesEnd;// = new DateTime(meetingTicks);
            ////DateTime condecoSeriesEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesEnd,
            ////                                                                             currMeeting.LocationID,
            ////                                                                             currMeeting.OriginalTZ);

            DateTime condecoSeriesEndTimeInLocalTz = currMeeting.BookingDateEndToLocalTZ;
            if (condecoSeriesEndTimeInLocalTz.TimeOfDay != rpItem.EndTime.TimeOfDay)
            {
                rpItem.EndTime = condecoSeriesEndTimeInLocalTz;
                itemUpdated = true;
            }
            //if (condecoSeriesEndTimeInLocalTz.Date != rpItem.PatternEndDate.Date)
            //{
            //    rpItem.PatternEndDate = condecoSeriesEndTimeInLocalTz.Date;
            //    itemUpdated = true;
            //}
            return itemUpdated;
        }

        private static bool DSTUpdateEndDateTimeInSeries(Outlook.RecurrencePattern rpItem, CondecoMeeting currMeeting)
        {
            bool itemUpdated = false;
            long currentTicks = rpItem.PatternEndDate.Date.Ticks + rpItem.EndTime.TimeOfDay.Ticks;
            DateTime itemSeriesEnd = new DateTime(currentTicks);
            ////itemSeriesEnd = UtilityManager.ConvertDateToTZ(itemSeriesEnd, currMeeting.LocationID, currMeeting.OriginalTZ);
            ////currentTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
            ////DateTime condecoSeriesEnd = new DateTime(currentTicks);
            DateTime condecoSeriesEnd = currMeeting.BookingDateEndToLocalTZ;
            if (itemSeriesEnd.CompareTo(condecoSeriesEnd) != 0)
            {
               // DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeTo, currMeeting.LocationID, currMeeting.OriginalTZ);
                DateTime newDt = currMeeting.BookingDateEndToLocalTZ;
                if (newDt.TimeOfDay.CompareTo(rpItem.EndTime.TimeOfDay) != 0)
                {
                    rpItem.EndTime = newDt;
                    itemUpdated = true;
                }
            }
            return itemUpdated;
        }

        private static bool UpdateStartDateTimeInSeries(Outlook.RecurrencePattern rpItem, CondecoMeeting currMeeting)
        {
            bool itemUpdated = false;
            //long meetingTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
            long meetingTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
            DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
            //DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
            //                                                                             currMeeting.LocationID,
            //                                                                             currMeeting.OriginalTZ);

            DateTime condecoSeriesStartTimeInLocalTz = currMeeting.BookingDateStartToLocalTZ;
            if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
            {
                rpItem.StartTime = condecoSeriesStartTimeInLocalTz;
                itemUpdated = true;
            }
            //if (condecoSeriesStartTimeInLocalTz.Date != rpItem.PatternStartDate.Date)
            //{
            //    rpItem.PatternStartDate = condecoSeriesStartTimeInLocalTz.Date;
            //    itemUpdated = true;
            //}
            return itemUpdated;
        }

        //private static bool UpdateStartDateTimeInSeries(RecurrencePattern rpItem, CondecoMeeting currMeeting)
        //{
        //    bool itemUpdated = false;
        //    long currentTicks = rpItem.PatternStartDate.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
        //    DateTime itemSeriesStart = new DateTime(currentTicks);
        //    itemSeriesStart = UtilityManager.ConvertDateToTZ(itemSeriesStart, currMeeting.LocationID);
        //    currentTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //    DateTime condecoSeriesStart = new DateTime(currentTicks);

        //    if (itemSeriesStart.CompareTo(condecoSeriesStart) != 0)
        //    {
        //        DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeFrom, currMeeting.LocationID);
        //        if (newDt.TimeOfDay.CompareTo(rpItem.StartTime.TimeOfDay) != 0)
        //        {
        //            rpItem.StartTime = newDt;
        //            itemUpdated = true;
        //            UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Syncing item series start time to " + newDt);
        //        }
        //    }
        //    return itemUpdated;
        //}

        public void SyncOccurrenceBooking(CondecoMeeting curMeeting, Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("SyncManager.SyncOccurrenceBooking:*********Started********");
            IbookingHelper bookingHelper = new BookingHelper();
            //EN - 10968
            //CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
            CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
            //EN - 10968
            bool itemSynced = false;

            bool result = false;
            if (currMeeting.DeleteBooking || currMeeting.Edited || currMeeting.BookingRejected|| currMeeting.Approved )
            {
                result = true;
            }
            //23jan2015
            if (currMeeting.IsMappedToExchange)
            {
                result = true;
            }

            if (!result) return;
            //Sync the Subject
            itemSynced = this.SyncItem(currMeeting, appItem);
            if (itemSynced)
            {
                UtilityManager.LogMessage("SyncManager.SyncOccurrenceBooking:Single Occurrence Synced sucessfully");
                ClearAppointmentDataIfRequired(currMeeting, appItem);
                if (!IsNormalAppSyncToDBShowNoMsg)
                {
                    UtilityManager.ShowWarningMessage(CondecoResources.Appointment_Synced);
                }
            }
            bookingHelper = null;
            UtilityManager.LogMessage("SyncManager.SyncOccurrenceBooking:*********Finished********");
        }

        public bool SyncItem(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            //bool doSyncing = false;
            bool itemUpdated = false;
            bool itemlocationUpdated = false;
            //below is disable in V6.3
            //bool isCrossDayBooking = false;
            ////////////////////////////////////////////IbookingHelper bookingHelper = new BookingHelper();
            ////////////////////////////////////////////if (bookingHelper.isCrossDayRecurranceBooking(appItem))
            ////////////////////////////////////////////{
            ////////////////////////////////////////////    isCrossDayBooking = true;
            ////////////////////////////////////////////}
            UtilityManager.LogMessage("SyncManager.SyncItem:*********Started********");
            //Added by Anand On 1-Feb-2013 if setting subject empty its coming null and giving error below.
            if (appItem.Subject == null) appItem.Subject = " ";
            if (!string.IsNullOrEmpty(currMeeting.MeetingTitle) && !appItem.Subject.Trim().ToLower().Equals(currMeeting.MeetingTitle.Trim().ToLower()))
            {
                appItem.Subject = currMeeting.MeetingTitle.Replace("&#39;", "'").Replace("&#38;", "&");
                itemUpdated = true;
            }

            if (appItem.Location == null)
                appItem.Location = " ";

            string newLoc = "";
            if (!AppointmentHelper.SyncIsOwaInitiatedBooking(appItem))
            {
                newLoc = AppointmentHelper.FormatLocationInCondecoStyle(currMeeting.MainLocation);
            }
            else
            {
                newLoc = appItem.Location;
            }
            //Sync the Location
            UtilityManager.LogMessage("SyncManager.SyncItem:Going to Compare Appointment Location: " + appItem.Location + "With condeco Meeting Lcoation:" + currMeeting.MainLocation);
            if ((!string.IsNullOrEmpty(newLoc) && !appItem.Location.ToLower().Contains(newLoc.ToLower())) || appItem.IsRecurring)
            {
                //Make the item updated to false if location are same but need to change in the UI
                if (appItem.Location.ToLower().Contains(newLoc.ToLower()) && !itemUpdated)
                {
                    itemlocationUpdated = false;
                }
                if (!itemlocationUpdated)
                {
                    //appItem.Location = currMeeting.MainLocation;
                    bool locationUpdated = AppointmentHelper.SetAppointmentLocation(appItem, currMeeting.MainLocation);
                    //Changed by Anand 10_Jan_2013 for issue as converting occurance as exception when opening occurance TP 10476 issue.
                    if(locationUpdated) itemlocationUpdated = true;
                    UtilityManager.LogMessage("SyncManager.SyncItem:Syncing item location to " + currMeeting.MainLocation);
                }
            }
            //On web Outlook booking doesn't edit 
             DateTime DatetoDBDateFrom;
             DateTime DatetoDBTimeFrom;
             ////////////////Below code changed for TP issue 9198
             //////////////if (isCrossDayBooking)
             //////////////{
             //////////////  //  DatetoDBDateFrom = UtilityManager.ConvertDateToLocalTZ(currMeeting.DateFrom, currMeeting.LocationID);
             //////////////  //  DatetoDBTimeFrom = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeFrom, currMeeting.LocationID);
             //////////////    DatetoDBDateFrom = currMeeting.DateFrom;
             //////////////    DatetoDBTimeFrom = currMeeting.TimeFrom;
             //////////////}
             //////////////else
             //////////////{
             //////////////    DatetoDBDateFrom = currMeeting.DateFrom;
             //////////////    DatetoDBTimeFrom = currMeeting.TimeFrom;
             //////////////}
             DatetoDBDateFrom = currMeeting.CrossdateFrom;
             DatetoDBTimeFrom = currMeeting.TimeFrom;
             DateTime meetingStart = currMeeting.BookingDateStartToLocalTZ; // new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);
            // DateTime meetingStart = currMeeting.BookingDateStartToLocalTZ;//new DateTime(DatetoDBDateFrom.Year, appItem.Start.Month, appItem.Start.Day, appItem.Start.Hour, appItem.Start.Minute, appItem.Start.Second);
            // DateTime appStart = UtilityManager.ConvertDateToTZ(appItem.Start,currMeeting.LocationID);//For crossday need to check
             DateTime appStart = appItem.Start;
             if (meetingStart != DateTime.MinValue && appStart.CompareTo(meetingStart) != 0)
             {
                 //appItem.Start = meetingStartTimeInLocalTz;
                // appItem.Start = UtilityManager.ConvertDateToLocalTZ(meetingStart, currMeeting.LocationID);
                 appItem.Start = meetingStart;// UtilityManager.ConvertDateToLocalTZ(currMeeting.BookingDateStartToLocalTZ, currMeeting.LocationID);
                 itemUpdated = true;
                 UtilityManager.LogMessage("SyncManager.SyncItem:Syncing item Start Time");
             }
             DateTime DatetoDBDateTo;
             DateTime DatetoDBTimeTO;
             ////////if (isCrossDayBooking)
             ////////{
             ////////   // DatetoDBDateTo = UtilityManager.ConvertDateToLocalTZ(currMeeting.DateTo, currMeeting.LocationID);
             ////////    //DatetoDBTimeFrom = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeTo, currMeeting.LocationID);
             ////////    DatetoDBDateTo = currMeeting.DateTo;
             ////////    DatetoDBTimeTO = currMeeting.TimeTo;
             ////////}
             ////////else
             ////////{
             ////////    DatetoDBDateTo = currMeeting.DateTo;
             ////////    DatetoDBTimeTO = currMeeting.TimeTo;
             ////////}
             DatetoDBDateTo = currMeeting.CrossdateTo;
             DatetoDBTimeTO = currMeeting.TimeTo;
             DateTime meetingEnd = currMeeting.BookingDateEndToLocalTZ;//new DateTime(DatetoDBDateTo.Year, DatetoDBDateTo.Month, DatetoDBDateTo.Day, DatetoDBTimeTO.Hour, DatetoDBTimeTO.Minute, DatetoDBTimeTO.Second);
             ////////DateTime appEnd = UtilityManager.ConvertDateToTZ(appItem.End, currMeeting.LocationID);
             DateTime appEnd = appItem.End;
             if (meetingEnd != DateTime.MinValue && appEnd.CompareTo(meetingEnd) != 0)
             {
                // appItem.End = UtilityManager.ConvertDateToLocalTZ(meetingEnd, currMeeting.LocationID);
                 appItem.End = meetingEnd;//UtilityManager.ConvertDateToLocalTZ(currMeeting.BookingDateEndToLocalTZ, currMeeting.LocationID);
                 itemUpdated = true;
                 UtilityManager.LogMessage("SyncManager.SyncItem:Syncing item End Time");
             }

            if (itemUpdated || itemlocationUpdated)
            {
                try
                {
                    //To fix 8102   issue only for Olk2010 below check in required
                    if(! (AppointmentDataInfo.GetAppItemCondecoBookingOpen(appItem) && OutlookVersion == 14))
                    {
                        appItem.Save();
                    }
                    
                    UtilityManager.LogMessage("Item changes Synced sucessfully");

                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred while SyncItem appontment:" + ex.Message);
                    UtilityManager.LogMessage("Error stacked trace while SyncItem appontment:" + ex.StackTrace);
                }
            }
            //bookingHelper = null;
            UtilityManager.LogMessage("SyncManager.SyncItem:*********Finished********");
            return itemUpdated;
        }
       

        public void SyncIndiviudallyEditedItems(Outlook.AppointmentItem appItem,bool isCondecoBookingcheckPrev =false,  AddinModule CurrentAddinModule = null, bool IsSeriesEdited = false, bool IsSeriesDeleted = false)
        {
            UtilityManager.LogMessage("SyncManager-> Inside SyncIndiviudallyEditedItems");
            IbookingHelper bookingHelper = new BookingHelper();
            //bool result = true;
            //if (!bookingHelper.IsCondecoBookingV2(appItem)) return;
            if (!isCondecoBookingcheckPrev && !bookingHelper.IsCondecoBooking(appItem))
            {
                return;
            }
           
            UtilityManager.LogMessage("SyncManager-> Current Item is a condeco Booking");
            if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
            {
               // List<CondecoMeeting> editedMeetings = bookingHelper.GetIndividuallyEditedItems(appItem);
                List<DateTime> missingDates = new List<DateTime>();
                int locationID = 0;
                UtilityManager.LogMessage("SyncManager-> Going to Get IndividualyEdited and Missing Items");
                bool loadfromDb = false;
                if (IsSeriesDeleted)
                {
                    //IsSeriesDeleted means this function call from condecodeleteseries otherwise from bookingmanager and checkgrid(data already get from db)
                   // loadfromDb = true;
                    loadfromDb = false;
                }
               
                List<CondecoMeeting> editedMeetings = bookingHelper.GetIndividuallyEditedAndMissingItems(appItem, ref missingDates, ref locationID, loadfromDb);
                int count = missingDates.Count;
           
                if (editedMeetings.Count == 0 && missingDates.Count == 0) return;
                UtilityManager.LogMessage("SyncManager-> Number of Missing Dates Found" + count + "::Number of Edited Items " + editedMeetings.Count);
                Outlook.RecurrencePattern recPattern = appItem.GetRecurrencePattern();

                //changed by Anand 22-Jan-13 for TP issue 9870/10220  
                if (missingDates.Count > 0)
                {
                    CurrentAddinModule.EditedOccurenceDate = new List<DateTime>();
                    CurrentAddinModule.ModuleMissingDates = new List<DateTime>();
                    HanldeMissingItems(recPattern, missingDates, CurrentAddinModule);

                    missingDates.ForEach(item => { CurrentAddinModule.ModuleMissingDates.Add((DateTime)item); });
                    
                }
                //end changed by anand
             //   List<DateTime> tempMissingNolocationdate = new List<DateTime>(CurrentAddinModule.EditedOccurenceDate.Count);
              //  CurrentAddinModule.EditedOccurenceDate.ForEach(item => { tempMissingNolocationdate.Add((DateTime)item); });
                
                if (editedMeetings.Count > 0)
                {
                  //  HanldeIndiviudalyEditedItems(appItem, recPattern, editedMeetings);
                    //if (!bookingHelper.isCrossDayRecurranceBooking(appItem))
                    //{
                    HanldeIndiviudalyEditedItems(appItem, recPattern, editedMeetings, CurrentAddinModule);
                   // }
                }
                //if (CurrentAddinModule.EditedOccurenceDate.Count > 0)
                //{
                //    tempMissingNolocationdate.ForEach(item => { CurrentAddinModule.EditedOccurenceDate.Add((DateTime)item); });
                //}
                //changed by Anand 22-Jan-13 for TP issue 9870/10220  
                //if (missingDates.Count > 0)
                //    HanldeMissingItems(appItem, recPattern, missingDates, locationID);
                //end changed by anand
                bookingHelper = null;
                UtilityManager.FreeCOMObject(recPattern);
            }
        }
        public void HanldeIndiviudalyEditedItems(Outlook.AppointmentItem appItem, Outlook.RecurrencePattern recPattern, List<CondecoMeeting> editedMeetings, AddinModule CurrentAddinModule = null, bool IsSeriesEdited = false, bool IsSeriesDeleted = false,bool IsMeetingCloseMailSend =false)
        {
           // bool isCrossDayBooking = false;
            IbookingHelper bookingHelper = new BookingHelper();
            ////if (bookingHelper.isCrossDayRecurranceBooking(appItem))
            ////{
            ////    isCrossDayBooking = true;
            ////}

            if (CurrentAddinModule != null && CurrentAddinModule.HostMajorVersion >= 14 && IsMeetingCloseMailSend)
            {
                CurrentAddinModule.exclusionlist = new List<Outlook.AppointmentItem>();
               // CurrentAddinModule.EditedOccurenceDate = new List<DateTime>();
                CurrentAddinModule.AppMasterLoc = new List<string>();
            }
            
            
            foreach (CondecoMeeting cMeeting in editedMeetings)
            {
                DateTime occurrenceData = DateTime.MinValue;
                DateTime DatetoDB;
                Outlook.AppointmentItem currAppointment = null;
                long currentTicks;
                // case 2 when room is updated
                try
                {
                    try
                    {
                        //12Feb2016  //to increase performance  of addin on recurrance booking 12Feb2016
                        ////////if (isCrossDayBooking)
                        ////////{
                        ////////    DatetoDB = UtilityManager.ConvertDateToLocalTZ(cMeeting.OriginalDateFrom, cMeeting.LocationID);
                        ////////}
                        ////////else
                        ////////{
                        ////////    DatetoDB = cMeeting.OriginalDateFrom;
                        ////////}
                        DatetoDB = cMeeting.CrossdateFrom;
                        // currentTicks = cMeeting.OriginalDateFrom.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                        currentTicks = DatetoDB.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                        occurrenceData = new DateTime(currentTicks);
                        //occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, cMeeting.LocationID);
                        currAppointment = recPattern.GetOccurrence(occurrenceData);
                    }
                    catch (System.Exception ex)
                    {
                        UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                        UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                    }

                    //case 1 When Series Time is changed
                    if (currAppointment == null)
                    {
                        try
                        {
                            //12Feb2016  //to increase performance  of addin on recurrance booking 12Feb2016
                            ////////if (isCrossDayBooking)
                            ////////{
                            ////////    DatetoDB = UtilityManager.ConvertDateToLocalTZ(cMeeting.DateFrom, cMeeting.LocationID);
                            ////////}
                            ////////else
                            ////////{
                            ////////    DatetoDB = cMeeting.DateFrom;
                            ////////}
                            DatetoDB=cMeeting.CrossdateFrom;
                            // currentTicks = cMeeting.DateFrom.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                            //////////////currentTicks = DatetoDB.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                            //////////////occurrenceData = new DateTime(currentTicks);
                            //////////////occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, cMeeting.LocationID);
                            occurrenceData = cMeeting.BookingDateStartToLocalTZ;
                            currAppointment = recPattern.GetOccurrence(occurrenceData);
                            //currAppointment = GetAppointmentFromRecPatternBasedOnDateAndTime(recPattern, currentTicks, cMeeting.LocationID);
                        }
                        catch (System.Exception ex)
                        {
                            UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                            UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                        }
                    }
                    // case 4 When room and times are changed acroos countries booking
                    if (currAppointment == null)
                    {
                        try
                        {
                            ////12Feb2016  //to increase performance  of addin on recurrance booking 12Feb2016
                            ////////if (isCrossDayBooking)
                            ////////{
                            ////////    DatetoDB = UtilityManager.ConvertDateToLocalTZ(cMeeting.DateFrom, cMeeting.LocationID);
                            ////////}
                            ////////else
                            ////////{
                            ////////    DatetoDB = cMeeting.DateFrom;
                            ////////}
                            DatetoDB = cMeeting.CrossdateFrom;
                            //currentTicks = cMeeting.DateFrom.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                            currentTicks = DatetoDB.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                            occurrenceData = new DateTime(currentTicks);
                            // occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, cMeeting.LocationID);
                            currAppointment = recPattern.GetOccurrence(occurrenceData);
                        }
                        catch (System.Exception ex)
                        {
                            UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                            UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                        }
                    }

                    // case 2 when room is updated
                    if (currAppointment == null)
                    {
                        try
                        {
                            ////12Feb2016  //to increase performance  of addin on recurrance booking 12Feb2016
                            ////////if (isCrossDayBooking)
                            ////////{
                            ////////    DatetoDB = UtilityManager.ConvertDateToLocalTZ(cMeeting.DateFrom, cMeeting.LocationID);
                            ////////}
                            ////////else
                            ////////{
                            ////////    DatetoDB = cMeeting.DateFrom;
                            ////////}
                            DatetoDB = cMeeting.CrossdateFrom;
                            //currentTicks = cMeeting.DateFrom.Date.Ticks + cMeeting.TimeFrom.TimeOfDay.Ticks;
                            //////////////////currentTicks = DatetoDB.Date.Ticks + cMeeting.TimeFrom.TimeOfDay.Ticks;
                            //////////////////occurrenceData = new DateTime(currentTicks);
                            //////////////////occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, cMeeting.LocationID);
                            occurrenceData = cMeeting.BookingDateStartToLocalTZ;
                            currAppointment = recPattern.GetOccurrence(occurrenceData);
                        }
                        catch (System.Exception ex)
                        {
                            UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                            UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                        }

                    }

                    if (currAppointment != null)
                    {
                        try
                        {
                            //Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402 (Only for >outlook 2007, not for outlook 2007 or lesser versions.
                            bool IsException = false;
                            if (currAppointment.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                            {
                               IsException = true;
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in Edit/delete mode,existing RecurrenceState state is olApptException");
                            }
                            if (IsSeriesEdited)
                            {
                                //Added by Ravi Goyal- creating exception forcefully so that item should be exclude from update during entire series edit
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in Edit mode, RecurrenceState going to change olApptException forcefully");
                                IsException = true;
                                //12dec2015
                                currAppointment.Location = null;
                    //currAppointment.Start = currAppointment.Start.AddMinutes(5.0);
                    //currAppointment.End = currAppointment.End.AddMinutes(5.0);
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in Edit mode, RecurrenceState state is changed to olApptException");
                            }
                            if (!IsSeriesEdited && !IsSeriesDeleted && currAppointment.Location != null)
                            {
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in Edit mode, RecurrenceState going to change olApptException forcefully");
                                IsException = true;
                                //12dec2015
                                currAppointment.Location = null;
                ////currAppointment.Start = currAppointment.Start.AddMinutes(5.0);
                ////currAppointment.End = currAppointment.End.AddMinutes(5.0);
                            }
                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-SyncItem Started");
                            if (IsSeriesDeleted && !IsException)
                            {
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in delete mode, RecurrenceState state is olApptOccurence");
                            }
                            else
                            {
                                SyncItem(cMeeting, currAppointment);
                            }
                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-SyncItem completed");
                            if (CurrentAddinModule != null)
                            {
                                if (currAppointment.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                                {
                                    if (CurrentAddinModule.HostMajorVersion < 14)
                                    {
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Outlook version is" + CurrentAddinModule.HostMajorVersion);
                                        UtilityManager.FreeCOMObject(currAppointment);
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-UtilityManager.FreeCOMObject() completed,object released successfully");
                                    }
                                    else if (CurrentAddinModule.HostMajorVersion >= 14 )//21dec2015 && !IsSeriesEdited && !IsSeriesDeleted)
                                    {
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Outlook version is" + CurrentAddinModule.HostMajorVersion);
                                       /// commented by paritosh on12/dec2015 for duplicate mail
                                        if (IsMeetingCloseMailSend)
                                        {
                                            CurrentAddinModule.exclusionlist.Add(currAppointment);
                                        }
                                        //////if (currAppointment.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && IsMeetingCloseMailSend)
                                        //////{
                                        //////    CurrentAddinModule.exclusionlist.Add(currAppointment);
                                        //////}
                                        ////// if(currAppointment.RecurrenceState == Outlook.OlRecurrenceState.olApptException || currAppointment.RecurrenceState==Outlook.OlRecurrenceState.olApptOccurrence)
                                        //////{
                                        //////    CurrentAddinModule.exclusionlist.Add(currAppointment);
                                        //////}
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Items added to exclusion list (not sent yet)");
                                    }
                                    else if (IsSeriesDeleted && !IsException)
                                    {
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series deleted and Item in not exception");
                                        CurrentAddinModule.AppMasterLoc.Add(cMeeting.MainLocation);
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-AppMasterLoc set as" + cMeeting.MainLocation);
                                    }
                                    //Added by Ravi Goyal to fix Recurring series edit issue, so incase if not only location is updated, addin will send seperate emails for each edited meeting occurence
                                    else if (IsSeriesEdited && (IsException || currAppointment.RecurrenceState == Outlook.OlRecurrenceState.olApptException || currAppointment.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence))
                                    {
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in edit mode and Item is Exception");
                                        //.. so if not only location updated, send seperate email for each edited meeting to sync the Attendees and host calendar.
                                        if (!AppointmentDataInfo.GetIsOnlySeriesLocationUpdated(appItem))
                                        {
                                           //Dated 22dec2015 CurrentAddinModule.EditedOccurenceDate.Add(currAppointment.Start);
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in edit mode and Item added to List<> EditedOccurenceDate ");
                                        }
                                        //..if only location updated do nothing, outlook will send seperate email.
                                        else
                                        {
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in edit mode ,only location updated so do nothing, outlook will send seperate email.");
                                            UtilityManager.FreeCOMObject(currAppointment);
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in edit mode ,UtilityManager.FreeCOMObject() completed,object released successfully");
                                        }
                                    }
                                    else
                                    {
                                        UtilityManager.FreeCOMObject(currAppointment);
                                    }
                                }
                                else
                                {
                                    UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Appointment is not meeting (no attendee), releasing the com Object");
                                    UtilityManager.FreeCOMObject(currAppointment);
                                    UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-object released successfully");
                                }
                            }
                            else
                            {
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-CurrentAddinModule is Null, releasing the com Object");
                                UtilityManager.FreeCOMObject(currAppointment);
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-object released successfully");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                            UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                        }
                    }
                }
                finally
                {
                    //UtilityManager.FreeCOMObject(currAppointment);
                }

            }
            bookingHelper = null;
        }

       // public void HanldeMissingItems(Outlook.RecurrencePattern recPattern, List<DateTime> missingAppointments, int locationID, AddinModule CurrentAddinModule = null)
        public void HanldeMissingItems(Outlook.RecurrencePattern recPattern, List<DateTime> missingAppointments,  AddinModule CurrentAddinModule = null,bool MissingMeeting =false)
        {
           
            UtilityManager.LogMessage("HanldeMissingItems-> Inside Missing Items for total items:"+missingAppointments.Count + "for location");
            try
            {
                foreach (var missingDate in missingAppointments)
                {
                    DateTime occurrenceData = DateTime.MinValue;
                    Outlook.AppointmentItem currAppointment = null;
                    UtilityManager.LogMessage("Going to check appointment for" + missingDate.ToString());
                    int CompareValue = 0;
                    //case 1 When Series Time is changed
                    long currentTicks;
                    try
                    {
                        currentTicks = missingDate.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                        occurrenceData = new DateTime(currentTicks);
                        //commented below by Anand 22-Jan-13 for TP issue 9870/10220 
                        //as its using recpattern so should check current appointmeht
                        //occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, locationID);
                        //also check for passed bookings conditions added for issue TP 10699
                        //if (DateTime.Compare(occurrenceData, DateTime.Now) == -1)
                        //    currAppointment = recPattern.GetOccurrence(occurrenceData);

                        CompareValue = DateTime.Compare(occurrenceData, DateTime.Now);
                        currAppointment = recPattern.GetOccurrence(occurrenceData);

                        //end comment by anand

                    }
                    catch (System.Exception ex)
                    {
                        UtilityManager.LogMessage("Error Occurred while HanldeMissingItems appontment:" + ex.Message);
                        UtilityManager.LogMessage("Error stacked trace while HanldeMissingItems appontment:" + ex.StackTrace);
                    }


                    if (currAppointment != null)
                    {
                        try
                        {
                            UtilityManager.LogMessage("Going to Set location for appointment:" + missingDate.ToString());
                            // this.SyncItem(cMeeting, currAppointment);
                            //Changed below as its making location to blank Anand 22-Jan-13 for TP issue 9870/10220  
                            // old code currAppointment.Location = string.Empty;
                            if (CompareValue < 0) //condition for passed booking
                            {
                                currAppointment.Location = " ";//currAppointment.Location;
                            }
                            else if (CompareValue >= 0) //condition for skipped booking
                            {
                                currAppointment.Location = " ";
                               
                            }
                            if (currAppointment.MeetingStatus != Outlook.OlMeetingStatus.olNonMeeting && MissingMeeting)
                            {
                                CurrentAddinModule.EditedOccurenceDate.Add(currAppointment.Start);
                            }
                            //End Added by Anand 22-Jan-13 for TP issue 9870/10220
                            currAppointment.Save();
                            UtilityManager.FreeCOMObject(currAppointment);
                            UtilityManager.LogMessage("location set for appointment:" + missingDate.ToString());
                        }
                        catch (System.Exception ex)
                        {
                            UtilityManager.LogMessage("Error Occurred while HanldeMissingItems appontment:" + ex.Message);
                            UtilityManager.LogMessage("Error stacked trace while HanldeMissingItems appontment:" + ex.StackTrace);
                        }
                    }

                }
            }
            catch
            {
                return;
            }
            UtilityManager.LogMessage("HanldeMissingItems-> Finish Missing Items" );
        }
        public bool IsSyncingRequired(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            bool result = true;
            bool doSyncing = false;
            if (currMeeting.DeleteBooking || currMeeting.BookingRejected)
            {
                if (currMeeting.BookingRejected)
                {
                    if (!appItem.Location.ToLower().Contains(CondecoResources.Appointment_Rejected_Title.ToLower()))
                    {
                        doSyncing = true;

                    }
                    //return itemUpdated;
                }
                if (currMeeting.DeleteBooking)
                {
                    if (!appItem.Location.ToLower().Contains(CondecoResources.Appointment_Cancelled_Title.ToLower()))
                    {
                        doSyncing = true;
                    }
                    // return itemUpdated;
                }

                string namedValue = "";
                namedValue = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                //Below Commented by Paritosh to fix #20475
                if (String.IsNullOrEmpty(namedValue))
                {
                    UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced, Convert.ToString("True"));
                 // appItem.Save();
                }
                //else
                //{
                //    doSyncing = false;
                //}
                if (doSyncing)
                {

                    result = true;
                }
                else
                {
                    result = false;
                }

                // return itemUpdated;
            }
            if (result)
            {
                //if (UtilityManager.PastbookingSeriesUpdate() == 1)
                //{
                    //if ((appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence))
                    //{
                    //    result = false;
                    //}
                //}
                //else
                //{
                //    if ((appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence) && DateTime.Now >= appItem.Start)
                //    {
                //        result = false;
                //    }
                //}
            }
            return result;
        }

        public bool ClearAppointmentDataIfRequired(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            bool result = false;
            if (currMeeting.BookingRejected || currMeeting.DeleteBooking)
            {
                AppointmentHelper.ClearAppointmentData(appItem);
                result = true;
            }
            return result;
        }

        ////public void SyncSeriesBookingHasDST(Outlook._AppointmentItem appitem, CondecoMeeting currMeeting)
        ////{
        ////   // ,DataSet occdata,bool IsEdit
        ////    DateTime occurrenceData = DateTime.MinValue;
        ////    DateTime condecoSeriesStartTimeInLocalTz = DateTime.MinValue;
        ////    Outlook.AppointmentItem currAppointment = null;
        ////    Outlook.RecurrencePattern rpItem = null;
        ////    try
        ////    {
        ////        UtilityManager.LogMessage("SyncManager.SyncSeriesBookingHasDST:*********Started********");
        ////        rpItem = appitem.GetRecurrencePattern();
        ////        long currentTicks;
        ////        try
        ////        {
        ////            ////////////////currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        ////            ////////////////DateTime currMeetingItemSeriesStart = new DateTime(currentTicks);
        ////            ////////////////condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
        ////            ////////////////                                                                      currMeeting.LocationID,
        ////            ////////////////                                                                      currMeeting.OriginalTZ);
        ////            condecoSeriesStartTimeInLocalTz = currMeeting.BookingDateStartToLocalTZ;

        ////            //  if (IsEdit)
        ////            //  {

        ////            //for (int i = 0; i < occdata.Tables[0].Rows.Count; i++)
        ////            //{
        ////            //if (Convert.ToInt32(occdata.Tables[0].Rows[i]["BookingID"]) == currMeeting.BookingID)
        ////            //{
        ////            // DateTime dtime = Convert.ToDateTime(occdata.Tables[0].Rows[i]["OccuranceDate"]);
        ////            if (condecoSeriesStartTimeInLocalTz != DateTime.MinValue)
        ////            {
        ////                currentTicks = currMeeting.DateFrom.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
        ////                DateTime Instancedate = new DateTime(currentTicks);
        ////                currAppointment = rpItem.GetOccurrence(Instancedate);
        ////                currAppointment.Start = UtilityManager.ConvertDateToLocalTZ(condecoSeriesStartTimeInLocalTz,currMeeting.LocationID,currMeeting.OriginalTZ);
        ////            }
        ////            //  break;
        ////            //}
        ////            //}
        ////            //}
        ////            //else
        ////            //{



        ////            //    currentTicks = currMeeting.DateFrom.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
        ////            //    occurrenceData = new DateTime(currentTicks);
        ////            //    currAppointment = rpItem.GetOccurrence(occurrenceData);
        ////            //    currAppointment.Start = condecoSeriesStartTimeInLocalTz;

        ////            //}
        ////        }
        ////        catch
        ////        {

        ////            //currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        ////            //occurrenceData = new DateTime(currentTicks);
        ////            //DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(occurrenceData,currMeeting.LocationID,currMeeting.OriginalTZ);
        ////            //currAppointment = rpItem.GetOccurrence(occurrenceData);
        ////            //currAppointment.Start = condecoSeriesStartTimeInLocalTz;
        ////        }
        ////        try
        ////        {
        ////            //////currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
        ////            //////DateTime currMeetingEnddate = new DateTime(currentTicks);
        ////            //////DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeetingEnddate, currMeeting.LocationID, currMeeting.OriginalTZ);
        ////            if (currMeeting.BookingDateEndToLocalTZ != DateTime.MinValue)
        ////            {
        ////                DateTime newDt = currMeeting.BookingDateEndToLocalTZ;
        ////                currAppointment.End = newDt;
        ////            }
        ////        }
        ////        catch
        ////        {
        ////        }
        ////        currAppointment.Save();
        ////    }
        ////    catch
        ////    {
        ////    }
        ////    finally
        ////    {
        ////        UtilityManager.FreeCOMObject(rpItem);
        ////        UtilityManager.FreeCOMObject(currAppointment);
        ////    }
        ////   // return condecoSeriesStartTimeInLocalTz;
        ////}

        //change CrossDayBooking to remove processcall for V6.3
       // private static bool UpdateSeriesBookingHasDST(Outlook.AppointmentItem appitem, CondecoMeeting currMeeting ,bool isCrossDayBooking )
        private static bool UpdateSeriesBookingHasDST(Outlook.AppointmentItem appitem, CondecoMeeting currMeeting  )
        {
            ////bool isCrossDayBooking = false;
            ////IbookingHelper bookingHelper = new BookingHelper();
            ////if (bookingHelper.isCrossDayRecurranceBooking(appitem))
            ////{
            ////    isCrossDayBooking = true;
            ////}
            
            bool itemUpdated = false;
            UtilityManager.LogMessage("SyncManager.UpdateSeriesBookingHasDST:*********Started********");
            Outlook.AppointmentItem currAppointment = null;
            Outlook.RecurrencePattern rpItem = appitem.GetRecurrencePattern();
            DateTime occurrenceData = DateTime.MinValue;
            long currentTicks;
            try
            {
                DateTime CrossdateFrom;
                ////////if (isCrossDayBooking)
                ////////{
                ////////    CrossdateFrom = UtilityManager.ConvertDateToLocalTZ(currMeeting.DateFrom, currMeeting.LocationID);
                ////////}
                ////////else
                ////////{
                ////////    CrossdateFrom = currMeeting.DateFrom;
                ////////}

                CrossdateFrom = currMeeting.CrossdateFrom;
                //currentTicks = CrossdateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                //DateTime currMeetingItemSeriesStart = new DateTime(currentTicks);

                ////////DateTime currMeetingItemSeriesStart = new DateTime(currentTicks);
                ////////DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                ////////                                                                        currMeeting.LocationID,
                ////////                                                                        currMeeting.OriginalTZ);


                DateTime condecoSeriesStartTimeInLocationTz = currMeeting.BookingDateStartToLocalTZ;
                currentTicks = CrossdateFrom.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
                occurrenceData = new DateTime(currentTicks);
                currAppointment = rpItem.GetOccurrence(occurrenceData);
                if (condecoSeriesStartTimeInLocationTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
                {
                    currAppointment.Start = condecoSeriesStartTimeInLocationTz;//UtilityManager.ConvertDateToLocalTZ(condecoSeriesStartTimeInLocationTz, currMeeting.LocationID, currMeeting.OriginalTZ); ;
                    itemUpdated = true;
                }

                try
                {

                   //// DateTime CrossdateTo;
                    ////////if (isCrossDayBooking)
                    ////////{
                    ////////    CrossdateTo = UtilityManager.ConvertDateToLocalTZ(currMeeting.DateFrom, currMeeting.LocationID);
                    ////////}
                    ////////else
                    ////////{
                    ////////    CrossdateTo = currMeeting.DateFrom;
                    ////////}
                    //CrossdateTo = currMeeting.CrossdateTo;
                    ///CrossdateTo = currMeeting.CrossdateTo;
                    ////////if(CrossdateTo.Date !=CrossdateFrom.Date){
                    ////////currentTicks = CrossdateTo.Date.Ticks + rpItem.EndTime.TimeOfDay.Ticks;
                    ////////DateTime currMeetingEnddate = new DateTime(currentTicks);
                    ////////DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeetingEnddate, currMeeting.LocationID, currMeeting.OriginalTZ);
                    DateTime newDt = currMeeting.BookingDateEndToLocalTZ;
                    if (newDt.TimeOfDay.CompareTo(rpItem.EndTime.TimeOfDay) != 0)
                    {
                        currAppointment.End = newDt;
                        itemUpdated = true;
                    }

                  

                    //if (itemSeriesEnd.CompareTo(condecoSeriesEnd) != 0)
                    //{
                    //    DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeTo, currMeeting.LocationID, currMeeting.OriginalTZ);
                    //    if (newDt.TimeOfDay.CompareTo(rpItem.EndTime.TimeOfDay) != 0)
                    //    {
                    //        rpItem.EndTime = newDt;
                    //        itemUpdated = true;
                    //    }
                    //}
                    if (itemUpdated)
                    {
                        currAppointment.Save();
                    }
                    

                }
                catch
                {
                }  
            }
            catch
            {
                
            }
            finally
            {
                UtilityManager.FreeCOMObject(rpItem);
                UtilityManager.FreeCOMObject(currAppointment);
                
            }
           // bookingHelper = null;
            return itemUpdated; 
        }

       
       
    }
}
