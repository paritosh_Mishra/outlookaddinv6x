using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CondecoAddinV2.App_Resources;
using Outlook = Microsoft.Office.Interop.Outlook;
using CondecoAddinV2.Repository;
using System.Text;

namespace CondecoAddinV2
{
    /// <summary>
    /// Add-in Express Outlook Items Events Class
    /// </summary>
    public class CondecoItemsEventsClass : AddinExpress.MSO.ADXOutlookItemsEvents
    {
        private AddinModule CurrentModule = null;
        // static AppointmentHelper appHelper = new AppointmentHelper();
        //static BookingHelper bookingHelper = new BookingHelper();
        private DateTime origStartDate = DateTime.MinValue;
        private DateTime origEndDate = DateTime.MaxValue;
        private string itemToMove = string.Empty;
        private string origSubject = string.Empty;
        public bool isAppointmentMoved = false;

        private const int WM_USER = 0x0400;
        private const int WM_MYMESSAGE = WM_USER + 1000;

        public CondecoItemsEventsClass(AddinExpress.MSO.ADXAddinModule module)
            : base(module)
        {
            if (CurrentModule == null)
                CurrentModule = module as AddinModule;
        }

        public override void ProcessItemAdd(object item)
        {
            UtilityManager.LogMessage("Calendar Item Add called");
        }
        //Added by Ravi Goyal for PRB0040856
        private bool GetCurrentView(object item)
        {
            UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView():*********Started********");
            bool viewrequired = false;
            try
            {
                object WindowType = this.CurrentModule.OutlookApp.ActiveWindow();
                if (WindowType is Outlook.Explorer)
                {
                    UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: Window type is Outlook explorer");
                    Outlook.View currentview = this.CurrentModule.OutlookApp.ActiveExplorer().CurrentView as Outlook.View;
                    if (currentview.Name == "Calendar")
                    {
                        UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: current view on explorer is calendar");
                        viewrequired = true;
                    }
                    if (currentview.Name == "Preview")
                    {
                        if (item == null) viewrequired = false;
                        if (item is Outlook.AppointmentItem || item is Outlook.MeetingItem) viewrequired = true;
                        UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: selected item is Appointment");
                    }
                    UtilityManager.FreeCOMObject(currentview);
                }
                else if (WindowType is Outlook.Inspector)
                {
                    UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: Window type is Outlook Inspector");
                    if (item == null) viewrequired = false;
                    if (item is Outlook.AppointmentItem || item is Outlook.MeetingItem) viewrequired = true;
                    UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: Inspector item is Appointment");
                }
                UtilityManager.FreeCOMObject(WindowType);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView():*********Exception :-********" + ex.Message);
            }
            return viewrequired;
        }
        //End Added by Ravi Goyal for PRB0040856
        public override void ProcessItemChange(object item)
        {
            //Added by Ravi Goyal for PRB0040856
            bool Goahead = GetCurrentView(item);
            if (Goahead)
            {
                UtilityManager.LogMessage("**CondecoItemsEventsClass.ProcessItemChange:*Goahead method returned true**");
            }
            else
            {
                UtilityManager.LogMessage("**CondecoItemsEventsClass.ProcessItemChange:*Goahead method returned false**");
                return;
            }
            //End Added by Ravi Goyal for PRB0040856

            UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessItemChange:*********Started********");
            // UtilityManager.LogMessage("2A-Calendar Item changed called"+item.GetType().FullName);
            if (item == null) return;
            if (!(item is Outlook.AppointmentItem)) return;
            Outlook.AppointmentItem currAppointment = item as Outlook.AppointmentItem;

            if (currAppointment == null) return;

            try
            {
                // if appointment is move on the calendar
                string cPostID = AppointmentHelper.GetAppointmentPostID(currAppointment);
                //string appSYNCPostID = "";

                //appSYNCPostID = UserPropertiesExtension.GetNamedPropertyRecord(currAppointment, CondecoAddinV2.Constants.CustomProperty.SyncPostID);
                if (UtilityManager.SyncBookingEnabled().Equals(1) && string.IsNullOrEmpty(cPostID))
                {
                    cPostID = AppointmentHelper.SyncGetAppointmentPostID(currAppointment);
                }
                //  UtilityManager.LogMessage("2B- Item going to move for postid:" + cPostID);
                if (string.IsNullOrEmpty(cPostID)) return;

                if (isAppointmentMoved)
                {
                    string currentID = itemToMove;
                    itemToMove = "";
                    //   UtilityManager.LogMessage("2C- Item moved called for postid:" + cPostID);
                    IbookingHelper bookingHelper = new BookingHelper();
                    if (!bookingHelper.IsCondecoBooking(currAppointment) || !currAppointment.EntryID.Equals(currentID))
                    {
                        isAppointmentMoved = false;

                        //  UtilityManager.LogMessage("2CC- Item is not a condeco booking for postid:" + cPostID);
                        // UtilityManager.FreeCOMObject(currAppointment);
                        return;
                    }

                    if (currAppointment.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                    {
                        // UtilityManager.LogMessage("2D- Item is going to moved in condeco  with Subject:" + currAppointment.Subject + "StartDate:" + currAppointment.Start + ":End Date:" + currAppointment.End);
                        this.HandleAppointmentMove(currAppointment);
                        //  UtilityManager.LogMessage("2E- Item moved in condeco  with Subject:" + currAppointment.Subject + "StartDate:" + currAppointment.Start + ":End Date:" + currAppointment.End);
                    }
                    //isCondecoBookingChanged = false;
                    // UtilityManager.FreeCOMObject(currAppointment);
                    isAppointmentMoved = false;
                    return;
                }
                // Try to delete single occurrence


                if (currAppointment.IsRecurring && (currAppointment.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting || currAppointment.MeetingStatus == Outlook.OlMeetingStatus.olMeeting))
                {
                    string PostID = AppointmentHelper.GetAppointmentPostID(currAppointment);

                    if (UtilityManager.SyncBookingEnabled().Equals(1) && (string.IsNullOrEmpty(cPostID) || (AppointmentHelper.IsSyncAppointment(currAppointment))))
                    {
                        PostID = AppointmentHelper.SyncGetAppointmentPostID(currAppointment);
                    }
                    if (string.IsNullOrEmpty(PostID)) return;
                    this.HandleOccurrenceDelete(currAppointment);



                }




            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("CondecoItemsEvents.ProcessItemChange Catch " + ex.Message);
            }
            finally
            {
                UtilityManager.FreeCOMObject(currAppointment);


                // GC.Collect();
                // GC.WaitForPendingFinalizers();

                //  UtilityManager.FreeCOMObject(currAppointment);
            }
            UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessItemChange:*********Finished********");
        }

        public override void ProcessItemRemove()
        {

        }

        public override void ProcessBeforeFolderMove(object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeItemMove(object item, object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {

            try
            {
                if (item is Outlook.AppointmentItem)
                {
                    UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessBeforeItemMove- started");
                    Outlook.AppointmentItem cItem = item as Outlook.AppointmentItem;
                    IbookingHelper bookingHelper = new BookingHelper();
                    string postID = AppointmentHelper.GetAppointmentPostID(cItem);
                    //EN - 8750
                    if (string.IsNullOrEmpty(postID))
                    {
                        UtilityManager.LogMessage("1BB- Item is not a condeco appointment");
                        return;
                    }
                    if(!string.IsNullOrEmpty(postID) && cItem.Location.Contains(CondecoResources.Appointment_Initial_Location))
                    {
                        UtilityManager.LogMessage("2BB- Item is not a condeco appointment");
                        return;
                    }
                    //if (!bookingHelper.IsCondecoBooking(cItem))
                    //{
                    //    UtilityManager.LogMessage("1BB- Item is not a condeco appointment");

                    //    return;
                    //}
                    //EN - 8750
                    if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                    {
                        if (!UtilityManager.IsSSOTimeOutMsgShow)
                        {

                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                        }
                        //UtilityManager.IsSSOTimeOutMsgShow = false;
                        e.Cancel = true;
                        return;
                    }
                    Outlook.MAPIFolder currFolder = null;
                    if (moveTo == null)
                    {
                        UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessBeforeItemMove- moveTo folder is null, calling method HandleDeleteConecoBooking()");
                        HandleDeleteConecoBooking(cItem);
                    }

                    if (moveTo is Outlook.MAPIFolder)
                    {
                        currFolder = moveTo as Outlook.MAPIFolder;

                        if (currFolder.Name.ToLower().StartsWith("calendar") || currFolder.DefaultItemType == Outlook.OlItemType.olAppointmentItem)
                        {


                            string currentPostID = AppointmentHelper.GetAppointmentPostID(cItem);
                            UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessBeforeItemMove- post id for appointment is -" + currentPostID);
                            if (UtilityManager.SyncBookingEnabled().Equals(1) && (string.IsNullOrEmpty(currentPostID) || (AppointmentHelper.IsSyncAppointment(cItem))))
                            {
                                currentPostID = AppointmentHelper.SyncGetAppointmentPostID(cItem);
                            }
                            //&& cItem.MeetingStatus != Outlook.OlMeetingStatus.olMeeting
                            if (!AppointmentHelper.IsNewAppointment(cItem) && !String.IsNullOrEmpty(currentPostID))
                            {
                                if (cItem.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                                {
                                    StringBuilder message1 = new StringBuilder();
                                    // message1.AppendLine(CondecoResources.V6UpdateOnlyFromBookingTab);
                                    message1.AppendLine(CondecoResources.AppointmentDragDropIntimate);
                                    DialogResult response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                                    if (response == DialogResult.Cancel)
                                    {
                                        e.Cancel = true;
                                        return;
                                    }
                                }

                                // bool dragnDropAllowed = true;
                                AppointmentDataInfo.AddAppointmentInCollection(cItem, true, 0);
                                if (cItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                                {
                                    //  isAppointmentMoved = false;
                                    AppointmentDataInfo.SetAppItemSkipSync(cItem, true);
                                }




                                isAppointmentMoved = true;
                                itemToMove = cItem.EntryID;
                                origSubject = cItem.Subject;
                                origStartDate = cItem.Start;
                                origEndDate = cItem.End;

                                //MessageBox.Show("BeforeItemMove  From Condeco" + origEndDate);




                                if (cItem.MeetingStatus != Outlook.OlMeetingStatus.olNonMeeting)
                                {
                                    UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessBeforeItemMove- calling SetOriginalMeetingMoveStartDate");
                                    AppointmentDataInfo.SetOriginalMeetingMoveStartDate(cItem, (DateTime?)origStartDate);
                                    AppointmentDataInfo.SetOriginalMeetingMoveEndDate(cItem, (DateTime?)origEndDate);
                                    UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessBeforeItemMove- SetOriginalMeetingMoveStartDate completed");
                                }


                                //MessageBox.Show(origStartDate.ToString());
                            }

                        }


                        if (currFolder.Name.ToLower().Equals("deleted items") || currFolder.DefaultItemType == Outlook.OlItemType.olMailItem)
                        {
                            HandleDeleteConecoBooking(cItem);



                            if (bookingHelper.IsVCWithin1hour)
                            {
                                e.Cancel = true;
                            }
                        }

                    }
                    UtilityManager.FreeCOMObject(currFolder);
                    UtilityManager.FreeCOMObject(cItem);
                    bookingHelper = null;
                    //if (!e.Cancel && !isBookingdeleted)
                    //{
                    //    StringBuilder message = new StringBuilder();
                    //    message.AppendLine(CondecoResources.Sync_ConflictRoomReadytoMoveSubject);
                    //    message.AppendLine();
                    //    message.AppendLine(CondecoResources.Sync_ConflictRoomReadytoMoveMsg);
                    //    DialogResult response = MessageBox.Show(message.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                    //    if (response == DialogResult.Cancel)
                    //    {
                    //        e.Cancel = true;
                    //    }
                    //}



                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("ProcessBeforeItemMove " + ex.Message + " " + ex.StackTrace);
            }
            finally
            {


            }
            //  UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessBeforeItemMove:*********Finished********");
        }
        //private bool AppointmentCanMove(Outlook.AppointmentItem curApp)
        //{
        //    bool ret = false;
        //    StringBuilder message1 = new StringBuilder();
        //    // message1.AppendLine(CondecoResources.V6UpdateOnlyFromBookingTab);
        //    message1.AppendLine(CondecoResources.AppointmentDragDropIntimate);
        //    DialogResult response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
        //    if (response == DialogResult.OK)
        //    {
        //        ret = true;
        //    }
        //    else
        //    {
        //        ret = false;
        //    }
        //    return ret;

        //}
        private void HandleDeleteConecoBooking(Outlook.AppointmentItem cItem)
        {
            if (!AppointmentHelper.IsNewAppointment(cItem))
            {
                // Dont delete if its a dummy item deleted by microsoft live meeting,
                bool deleteCondecoBooking = true;
                bool wasItemOpened = (this.Module as AddinModule).isItemOpen;
                //cItem.MessageClass.ToLower().Equals("ipm.appointment.live meeting request") && !wasLiveMeeting
                Outlook.Inspector olInsp = null;
                string curretnCaption = string.Empty;
                try
                {
                    olInsp = cItem.GetInspector;
                    if (olInsp != null)
                    {
                        curretnCaption = olInsp.Caption;
                    }

                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("ProcessBeforeItemMove GettingInspector" + ex.Message);
                }
                finally
                {
                    UtilityManager.FreeCOMObject(olInsp);
                }
                //  string itemCaption = cItem.Subject + " - Conferencing Request";
                if (wasItemOpened && cItem.MeetingStatus != Outlook.OlMeetingStatus.olMeetingCanceled && curretnCaption.ToLower().Contains("conferencing request"))
                    deleteCondecoBooking = false;

                if (deleteCondecoBooking)
                {
                    IbookingHelper bookingHelper = new BookingHelper();
                    bookingHelper.DeleteCondecoBooking(cItem);
                    bookingHelper = null;
                }
            }
        }
        private void HandleAppointmentMove(Outlook.AppointmentItem curApp)
        {

            // UtilityManager.LogMessage("2D1- Inside handle appointment move with Subject:" + curApp.Subject + "StartDate:" + curApp.Start + ":End Date:" + curApp.End);
            Outlook.AppointmentItem singleAppointment = null;
            IbookingHelper bookingHelper = new BookingHelper();
            CondecoMeeting currMeeting = null;
            try
            {
                // MessageBox.Show("Going to Move Appointment with Start:" + origStartDate + "to :" + curApp.Start);
                // UtilityManager.LogMessage("2D2- checking if appontment is recurring series");
                ////////DateTime? occurChangeStartDate = null;
                ////////DateTime? occurChangeEndDate = null;
                currMeeting = new CondecoMeeting();
                if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    Outlook.RecurrencePattern masterRecPattern = curApp.GetRecurrencePattern();
                    Outlook.Exceptions oExps = masterRecPattern.Exceptions;
                    if (oExps.Count > 0)
                    {
                        //for (int ex = 0; ex < oExps.Count; ex++)
                        //{
                        // Outlook.Exception currExp = oExps[ex + 1];
                        foreach (Outlook.Exception currExp in oExps)
                        {
                            if (currExp != null)
                            {
                                if (!currExp.Deleted && currExp.OriginalDate.Date.CompareTo(origStartDate.Date) == 0)
                                {
                                    UtilityManager.LogMessage("CondecoItemsEventsClass : AddinExpress.MSO.ADXOutlookItemsEvents ::HandleAppointmentMove :currExp.OriginalDate=" + currExp.OriginalDate);
                                    singleAppointment = currExp.AppointmentItem;
                                    UtilityManager.LogMessage("CondecoItemsEventsClass : AddinExpress.MSO.ADXOutlookItemsEvents ::HandleAppointmentMove :singleAppointment.Start=" + singleAppointment.Start);
                                    UtilityManager.FreeCOMObject(currExp);
                                    break;
                                }

                            }
                            UtilityManager.FreeCOMObject(currExp);
                        }
                        // MessageBox.Show("For Condeco =" + singleAppointment.Start.ToString());
                        // MessageBox.Show("For Condeco =" + singleAppointment.End.ToString());
                    }
                    UtilityManager.FreeCOMObject(oExps);
                    UtilityManager.FreeCOMObject(masterRecPattern);
                }
                //if (singleAppointment == null)
                //{
                //    if (DateTime.Compare(singleAppointment.Start, curApp.Start) == 0 && DateTime.Compare(singleAppointment.End, curApp.End) == 0)
                //    {
                //        UtilityManager.ShowErrorMessage(CondecoResources.AppointmentDragDropHaveProblem);

                //        AppointmentDataInfo.SetStartDateChanged(curApp, true);
                //        AppointmentDataInfo.SetHavingPermission(curApp, true);
                //        object obj = (CurrentModule).FindRibbonControl("adxRibbonButton_c870b073-e4f1-4082-9a0a-622f91295ebe");
                //        AddinExpress.MSO.ADXRibbonButton ribbonRoomBooking = obj as AddinExpress.MSO.ADXRibbonButton;
                //        CurrentModule.IsUndoMeetingChanges = true;
                //        CurrentModule.RibbonButtons_OnClick(obj, null, true);
                //        return;
                //    }
                //}
                string postID = AppointmentHelper.GetAppointmentPostID(curApp);
                if (UtilityManager.SyncBookingEnabled().Equals(1) && (string.IsNullOrEmpty(postID) || (AppointmentHelper.IsSyncAppointment(curApp))))
                {
                    postID = AppointmentHelper.SyncGetAppointmentPostID(curApp);
                }
                if (singleAppointment != null && curApp.IsRecurring)
                {
                    //EN - 8750
                    //currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, false, origStartDate);
                    currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, true, origStartDate);
                    //EN-8750
                    curApp = singleAppointment;
                    // UtilityManager.LogMessage("2D3A- Getting condeco appointment for deleted single occurrence meeting for date:" + origStartDate);
                }
                else if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                {
                    currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, true, curApp.Start);
                    singleAppointment = curApp;
                }
                else if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptNotRecurring)
                {
                    //EN - 8750
                    //currMeeting = bookingHelper.GetCurrentCondecoMeeting(curApp, false);
                    currMeeting = bookingHelper.GetCurrentCondecoMeeting(curApp, true);
                    //EN - 8750
                }
                else if (curApp.IsRecurring)
                {
                    //currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, false, curApp.Start);
                    Outlook.RecurrencePattern recPattern = curApp.GetRecurrencePattern();
                    DateTime occurrenceData = DateTime.MinValue;
                    long currentTicks;
                    currentTicks = origStartDate.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                    occurrenceData = new DateTime(currentTicks);
                    singleAppointment = recPattern.GetOccurrence(occurrenceData);
                    if (singleAppointment != null)
                    {
                        //  curApp = (Outlook.AppointmentItem)singleAppointment.Parent;
                        //Dated by Mar2016 - EN-4998 Popup is displayed (APPOINTMENT CAN NOT BE MOVE INTO PAST DATE/TIME) when user drag the future instance of the recurrence booking which have some past instance across the tramline.
                        //currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, false, origStartDate);
                        currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, true, singleAppointment.Start);
                        curApp = singleAppointment;
                    }
                    UtilityManager.FreeCOMObject(recPattern);
                }
                // UtilityManager.LogMessage("2D4- Boking Found in condeco:" + currMeeting.BookingID);
                if (currMeeting.BookingID == 0) return;
                AppointmentDataInfo.AddAppointmentInCollection(curApp, currMeeting.BookingID);
                int result = 0;
                // UtilityManager.LogMessage("2D5- checking for past booking:" + currMeeting.BookingID);
                // Below appItem.End check to fix the CRRI 6550 ,TP-19968 by Paritosh
                
                long meetingTicks1 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                DateTime meetingStartDateTime1 = new DateTime(meetingTicks1);
                DateTime condecoStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime1, currMeeting.LocationID, currMeeting.OriginalTZ);
                long meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                DateTime meetingEndDateTime = new DateTime(meetingTicks);
                DateTime condecoEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime, currMeeting.LocationID, currMeeting.OriginalTZ);
                
                if (curApp.IsRecurring && DateTime.Compare(singleAppointment.Start, condecoStartTimeInLocalTz) == 0 && DateTime.Compare(singleAppointment.End, condecoEndTimeInLocalTz) == 0)
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.AppointmentDragDropHaveProblem);
                    //AppointmentDataInfo.SetAppItemSkipSync(singleAppointment, true);
                    singleAppointment.Start = condecoStartTimeInLocalTz;
                    singleAppointment.End = condecoEndTimeInLocalTz;
                    singleAppointment.Save();
                    //EN-8750
                    //singleAppointment.Display();
                    //-EN-8750
                    //CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                    return;
                }
                //24/jan/2004
                //if (meetingStartDateTime1.CompareTo(DateTime.Now) < 0 && meetingStartDateTime1.CompareTo(curApp.Start) != 0)
                //{
                //    result = -4;
                //}
                if (condecoStartTimeInLocalTz.CompareTo(DateTime.MinValue) != 0 && curApp.Start.CompareTo(condecoStartTimeInLocalTz) != 0 && (condecoStartTimeInLocalTz.CompareTo(DateTime.Now) < 0) && (curApp.Start.CompareTo(condecoStartTimeInLocalTz) < 0))
                {
                    // MessageBox.Show("date change to past");
                    result = -4;
                }
                else if (condecoEndTimeInLocalTz.CompareTo(DateTime.MinValue) != 0 && (condecoEndTimeInLocalTz.CompareTo(DateTime.Now) < 0))
                {
                    result = -6;
                }
                else if (DateTime.Now.CompareTo(curApp.Start) >= 0 && DateTime.Now.CompareTo(curApp.End) > 0)
                {
                    // UtilityManager.LogMessage("2D5A- booking is in past can not be modified:" + DateTime.Now);
                    result = -200;
                }
                else if (curApp.AllDayEvent)
                {
                    result = -400;
                }
                else
                {
                    result = bookingHelper.UpdateCondecoBooking(currMeeting, curApp);

                }
                string message = CondecoResources.AppointmentMove_Services_NotAvailable;
                if (result == 1)
                {
                    //Outlook.AppointmentItem appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                    UtilityManager.LogMessage("CondecoItemsEventsClass.cs- HandleAppointmentMove(Outlook.AppointmentItem curApp)  (result == 1)");
                    string newLocSimple = bookingHelper.GetBookingLocation("", curApp);
                    string newLoc = "";
                    if (!AppointmentHelper.SyncIsOwaInitiatedBooking(curApp))
                    {
                        newLoc = AppointmentHelper.FormatLocationInCondecoStyle(newLocSimple);
                    }
                    else
                    {
                        newLoc = curApp.Location;
                    }
                    if (curApp.Location == null)
                        curApp.Location = " ";
                    if (!String.IsNullOrEmpty(newLoc) && !String.IsNullOrEmpty(curApp.Location))
                    {
                        if (!curApp.Location.ToLower().Contains(newLoc.ToLower()))
                        {
                            AppointmentHelper.SetAppointmentLocation(curApp, newLocSimple);
                            // curApp.Location = newLoc;
                            curApp.Save();
                        }
                    }
                    MessageBox.Show(CondecoResources.Sync_BookingMoved, CondecoResources.Sync_Confirmation, MessageBoxButtons.OK, MessageBoxIcon.None);
                    //if (result == 1)
                    //{
                    //    HandleOccurrenceResyncAfterMove(curApp, currMeeting);
                    //}
                    //CurrentModule.CleanUpProcess();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                else if (result == -1 || result == -999 || result == -100 || result == -200 || result == -300 || result == -400 ||
                    result == -2 || result == -3 || result == -4 || result == -5 || result == -100 || result == -99 ||
                    result == -97 || result == -98 || result == -81 || result == -82 || result == -83 || result == -84 || result == -85 || result == -86 || result == -6)
                {

                    if (result == -1)
                        message = CondecoResources.AppointmentMove_Room_NotAvailable;
                    else if (result == -81)
                        message = CondecoResources.Group_SingleDayBookingOnly;
                    else if (result == -82)
                        message = CondecoResources.Group_BusinessHourOverLap;
                    else if (result == -83)
                        message = CondecoResources.Group_AdvancedBookingPeriod;
                    else if (result == -84)
                        message = CondecoResources.Group_NoticeRequired;
                    else if (result == -97 || result == -98)
                    {
                     // result == -98  for   EN-6795 ADDIN 6.2.4|[GIC] Booking updated from outlook add-in doesn't get updated in TMS.
                        //multiple room booking Move
                        StringBuilder message1 = new StringBuilder();
                        if (!curApp.IsRecurring && curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptNotRecurring && currMeeting.IsMappedToExchange)
                        {
                            message1.AppendLine(CondecoResources.V6ReSubmitBookingForm);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.V6UpdateBookingOKCancel);
                            DialogResult response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetAppItemSkipSync(curApp, true);
                                curApp.Display();
                                CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                            }
                            else
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                            return;
                        }
                        else if (!curApp.IsRecurring && curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptNotRecurring && !currMeeting.IsMappedToExchange)
                        {
                            //User Story 4171:Move non-recurrent appointment with multi room booking in Outlook calendar grid
                            message1.AppendLine(CondecoResources.V6ReSubmitBookingForm);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.V6UpdateBookingOKCancel);
                            DialogResult response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetAppItemSkipSync(curApp, true);
                                curApp.Display();
                                CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                            }
                            else
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                            return;
                        }
                        else if (curApp.IsRecurring && (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException))
                        {
                            //User Story 4411:Move recurrent appointment (instance) with multi room booking in Outlook calendar grid
                            message1.AppendLine(CondecoResources.V6ReSubmitBookingForm);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.V6UpdateBookingOKCancel);
                            DialogResult response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetAppItemSkipSync(curApp, true);
                                curApp.Display();
                                CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                                CurrentModule.IsUndoMeetingChanges = true;
                            }
                            else
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                            return;
                        }
                        else
                        {
                            message1.AppendLine(CondecoResources.V6UpdateBookingOKCancel);
                            //27Jan2015
                            // message1.AppendLine(CondecoResources.Recurrence_Series_Discard_Instruction);
                            DialogResult response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetAppItemSkipSync(curApp, true);
                                curApp.Display();
                                CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                            }
                            else
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                            return;
                        }
                    }
                    else if (result == -99)
                    {
                        //When Appointment/ or single instance of series  move  from calendra .
                        //User Story 4408:Move recurrent appointment (instance) with single room booking in Outlook calendar grid
                        StringBuilder message1 = new StringBuilder();
                        DialogResult response;
                        if ((curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && !currMeeting.IsMappedToExchange)
                        {
                            //When  Single day sync boom booking move 
                            //message1.AppendLine(CondecoResources.V6SyncBookingSeriesUpdate);
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomMessage);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetAppItemSkipSync(curApp, true);
                                curApp.Display();
                                CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                            }
                            else
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                        }
                        //else if ((curApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting) && (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && !currMeeting.IsMappedToExchange)
                        //{
                        //}
                        else if ((curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException) && currMeeting.IsMappedToExchange)
                        {
                            //User Story 4408:Move recurrent appointment (instance) with single room booking in Outlook calendar grid
                            //Sync_ConflictRoomSubject
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                        }
                        else if (!curApp.IsRecurring && currMeeting.IsMappedToExchange)
                        {
                            //User Story 4152:Move non-recurrent appointment with single room booking in Outlook calendar grid
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.V6UpdateSyncBookingNotAvaliable);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                        }
                        else if (!curApp.IsRecurring && !currMeeting.IsMappedToExchange)
                        {
                            //User Story 4152:Move non-recurrent appointment with single room booking in Outlook calendar grid
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomMessage);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetAppItemSkipSync(curApp, true);
                                curApp.Display();
                                CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                            }
                            else
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                        }
                        else
                        {
                            //When  appoitment move without sync room
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomSubject);
                            message1.AppendLine();
                            message1.AppendLine(CondecoResources.Sync_ConflictRoomMessage);
                            response = MessageBox.Show(message1.ToString(), CondecoResources.Sync_RoomAlertCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                            if (response == DialogResult.OK)
                            {
                                AppointmentDataInfo.SetAppItemSkipSync(curApp, true);
                                curApp.Display();
                                CurrentModule.SendMessage(WM_MYMESSAGE, IntPtr.Zero, IntPtr.Zero);
                            }
                            else
                            {
                                curApp.Start = origStartDate;
                                curApp.End = origEndDate;
                                curApp.Save();
                            }
                        }
                        return;
                        // message = CondecoResources.Appointment_Cannot_Moved;
                    }

                    else if (result == -200)
                        message = CondecoResources.Booking_Moved_Past;
                    else if (result == -400)
                        message = CondecoResources.AllDayEvent_NotSupported;
                    else if (result == -999) //added by Anand for permission verification
                        message = CondecoResources.Check_Booking_Permission;

                    else if (result == -4)
                    {
                        //  message = CondecoResources.BookingStartDateUneditable;
                        message = CondecoResources.Booking_Moved_Past;
                    }
                    else if (result == -85)
                    {
                        //MessageBox.Show("Meeting in progress cant edit");
                        message = CondecoResources.BookingIsInProgress;
                    }
                    else if (result == -86)
                    {
                        message = CondecoResources.AppointmentIsAlreadyClosed;
                    }
                    else if (result == -6 || result == -3)
                    {
                        UtilityManager.LogMessage("handleAppointment move past booking");
                        message = CondecoResources.Booking_Update_Past;
                    }
                    UtilityManager.ShowErrorMessage(message);
                    // MessageBox.Show(message, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    curApp.Start = origStartDate;
                    curApp.End = origEndDate;
                    //End change
                    curApp.Save();
                }

            }
            catch (SystemException ex)
            {
                UtilityManager.LogMessage("CondecoItemsEventsClass.HandleAppointmentMove Catch :+" + ex.Message);
            }
            finally
            {

                bookingHelper = null;
                AppointmentDataInfo.RemoveAppointmentFromCollection(curApp);
                UtilityManager.FreeCOMObject(curApp);
                UtilityManager.FreeCOMObject(singleAppointment);
                currMeeting = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

        }
        private void HandleOccurrenceDelete(Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("CondecoItemsEventsClass.HandleOccurrenceDelete:*********Started********");
            Outlook.RecurrencePattern rPattern = null;
            Outlook.Exceptions recExps = null;
            // Outlook.Exception mExp = null;
            Outlook.AppointmentItem newItem = null;
            Outlook.NameSpace ns = null;
            try
            {

                //////if (CurrentModule.IsMeetingCancelled)
                //////{
                //////    CurrentModule.IsMeetingCancelled = false;  
                //////}
                //////else
                //////{
                //////    int bookingId = AppointmentDataInfo.GetBookingId(appItem);
                //////    if (bookingId == 0) return;
                //////}
                string currentId = appItem.EntryID;

                Marshal.ReleaseComObject(appItem);
                appItem = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();


                ns = CurrentModule.OutlookApp.Session;
                object currentItem = ns.GetItemFromID(currentId, null);
                newItem = currentItem as Outlook.AppointmentItem;
                rPattern = newItem.GetRecurrencePattern();
                recExps = rPattern.Exceptions;

                if (recExps.Count == 0)
                {
                    //Commented and changed By anand for TP10181 issue as they are already in finally

                    UtilityManager.FreeCOMObject(rPattern);
                    UtilityManager.FreeCOMObject(recExps);
                    UtilityManager.FreeCOMObject(newItem);
                    UtilityManager.FreeCOMObject(ns);
                    //  UtilityManager.FreeCOMObject(mExp);
                    return;
                }
                string postID = AppointmentHelper.GetAppointmentPostID(newItem);
                //string appSYNCPostID = "";

                //appSYNCPostID = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CondecoAddinV2.Constants.CustomProperty.SyncPostID);
                if (UtilityManager.SyncBookingEnabled().Equals(1) && string.IsNullOrEmpty(postID) && (AppointmentHelper.IsSyncAppointment(newItem, true)))
                {
                    postID = AppointmentHelper.SyncGetAppointmentPostID(newItem);
                }
                string getBookingData = "iID=" + postID + "&iSwitch=7";
                UtilityManager.LogMessage("6a-Getting user booking Data with parm: " + getBookingData);
                string bookingData = UtilityManager.PostDataToServer(getBookingData);

                IbookingHelper bookingHelper = new BookingHelper();



                //  for (int i = 0; i < recExps.Count; i++)


                foreach (Outlook.Exception curexp in recExps)
                {
                    //mExp = recExps.Item(i + 1);
                    // mExp = recExps[i + 1];
                    try
                    {
                        if (curexp.Deleted)
                        {














                            bookingHelper.DeleteSingleOccurrenceFromCondeco(newItem, curexp.OriginalDate, postID, bookingData);

                        }
                        else if (AppointmentDataInfo.GetAppRecuDeleteToolbarClick(newItem))
                        {
                            //Outlook._AppointmentItem app = rPattern.GetOccurrence(curexp.OriginalDate);
                            //if (app != null && app.End < DateTime.Now)
                            //{
                            //    break;
                            //}
                            bookingHelper.DeleteSingleOccurrenceFromCondeco(newItem, curexp.OriginalDate, postID, bookingData);
                            // UtilityManager.FreeCOMObject(app);
                        }
                    }
                    catch (Exception ex)
                    {
                        UtilityManager.LogMessage("Going to Delete exp for" + curexp.OriginalDate + "::" + ex.Message + " " + ex.StackTrace);
                        // bookingHelper.DeleteSingleOccurrenceFromCondeco(newItem, curexp.OriginalDate, postID, bookingData);
                    }
                    finally
                    {

                        // UtilityManager.FreeCOMObject(mExp);
                        Marshal.ReleaseComObject(curexp);
                    }
                }
                bookingHelper = null;
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred in Occurrence Delete" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(recExps);
                UtilityManager.FreeCOMObject(rPattern);
                UtilityManager.FreeCOMObject(newItem);
                UtilityManager.FreeCOMObject(ns);
                //UtilityManager.FreeCOMObject(mExp);
            }
            UtilityManager.LogMessage("CondecoItemsEventsClass.HandleOccurrenceDelete:*********Finished********");
        }


    }
}

