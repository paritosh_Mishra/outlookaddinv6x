using System;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace CondecoAddinV2
{
	/// <summary>
	/// Add-in Express Outlook Folders Events Class
	/// </summary>
	public class CondecoFoldersEventsClass : AddinExpress.MSO.ADXOutlookFoldersEvents
	{
		private AddinModule CurrentModule = null;

		public CondecoFoldersEventsClass(AddinExpress.MSO.ADXAddinModule module)
			: base(module)
		{
			if (CurrentModule == null)
				CurrentModule = module as AddinModule;
		}

		public override void ProcessFolderAdd(object folder)
		{
          
			//CurrentModule.DoFolderAdd(folder as Outlook.MAPIFolder);
		}

		public override void ProcessFolderChange(object folder)
		{
          
           
		}

		public override void ProcessFolderRemove()
		{

		}
	}
}

