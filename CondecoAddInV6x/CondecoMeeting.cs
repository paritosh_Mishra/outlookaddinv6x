using System;
using System.Collections.Generic;
using System.Text;

namespace CondecoAddinV2
{
    public class CondecoMeeting
    {
        private string postID;
        private int bookingID;
        private int fkRecurrenceID;
        private string allLocation;
        private string mainLocation;
        private bool deployment;
        private bool approved;
        private DateTime dateFrom;
        private DateTime timeFrom;
        private DateTime dateTo;
        private DateTime timeTo;
        private string itemID;
        private int resourceItemID;
        private string meetingTitle;
        private bool edited;
        private DateTime realStartTime;
        private DateTime realEndTime;
        private bool noShow;
        private int locationID;
        private bool deleteBooking;
        private string outlookID;
        private int cancelBeforeDays;
        private DateTime cancelBeforeTime;
        private DateTime businessDayStart;
        private bool bookingRejected;
        private bool isSeriesRejected;
        private bool isSeriesDeleted;
        private DateTime originalDateFrom;
        private DateTime originalTimeFrom;
        private DateTime originalDateTo;
        private DateTime originalTimeTo;
        private string originalTZ;
        //added by anand for Making timezone UTC based
        public string LocationTZ { get; set; }
       // Added by Paritosh for V6 to getExchange Room is in Booking 
        public bool IsMappedToExchange { get; set; }
        public int InstanceIsPast { get; set; }
        public bool IsBookingHasDST { get; set; }

        //Below four property are added to increase recurance bookig edit on V3.1 enterprise 
        public DateTime CrossdateFrom { get; set; }
        public DateTime CrossdateTo { get; set; }
        public DateTime BookingDateStartToLocalTZ { get; set; }
        public DateTime BookingDateEndToLocalTZ { get; set; }
        // End 
        public int BookingID
        {
            get { return bookingID; }
            set { bookingID = value; }
        }
        public string PostID
        {
            get { return postID; }
            set { postID = value; }
        }

        public int FkRecurrenceID
        {
            get { return fkRecurrenceID; }
            set { fkRecurrenceID = value; }
        }
       

        public string AllLocation
        {
            get { return allLocation; }
            set { allLocation = value; }
        }


        public string MainLocation
        {
            get { return mainLocation; }
            set { mainLocation = value; }
        }


        public bool Deployment
        {
            get { return deployment; }
            set { deployment = value; }
        }
     

        public bool Approved
        {
            get { return approved; }
            set { approved = value; }
        }
     

        public DateTime DateFrom
        {
            get { return dateFrom; }
            set { dateFrom = value; }
        }
     

        public DateTime TimeFrom
        {
            get { return timeFrom; }
            set { timeFrom = value; }
        }
   

        public DateTime DateTo
        {
            get { return dateTo; }
            set { dateTo = value; }
        }
    

        public DateTime TimeTo
        {
            get { return timeTo; }
            set { timeTo = value; }
        }
     

        public string ItemID
        {
            get { return itemID; }
            set { itemID = value; }
        }
     

        public int ResourceItemID
        {
            get { return resourceItemID; }
            set { resourceItemID = value; }
        }
    

        public string MeetingTitle
        {
            get { return meetingTitle; }
            set { meetingTitle = value; }
        }
   

        public bool Edited
        {
            get { return edited; }
            set { edited = value; }
        }
    

        public DateTime RealStartTime
        {
            get { return realStartTime; }
            set { realStartTime = value; }
        }
   

        public DateTime RealEndTime
        {
            get { return realEndTime; }
            set { realEndTime = value; }
        }
    

        public bool NoShow
        {
            get { return noShow; }
            set { noShow = value; }
        }

        public int LocationID
        {
            get { return locationID; }
            set { locationID = value; }
        }
    

        public bool DeleteBooking
        {
            get { return deleteBooking; }
            set { deleteBooking = value; }
        }


        public string OutlookID
        {
            get { return outlookID; }
            set { outlookID = value; }
        }
    

        public int CancelBeforeDays
        {
            get { return cancelBeforeDays; }
            set { cancelBeforeDays = value; }
        }
    

        public DateTime CancelBeforeTime
        {
            get { return cancelBeforeTime; }
            set { cancelBeforeTime = value; }
        }


        public DateTime BusinessDayStart
        {
            get { return businessDayStart; }
            set { businessDayStart = value; }
        }


        public bool BookingRejected
        {
            get { return bookingRejected; }
            set { bookingRejected = value; }
        }
      

        public bool IsSeriesRejected
        {
            get { return isSeriesRejected; }
            set { isSeriesRejected = value; }
        }
  

        public bool IsSeriesDeleted
        {
            get { return isSeriesDeleted; }
            set { isSeriesDeleted = value; }
        }
      

        public DateTime OriginalDateFrom
        {
            get { return originalDateFrom; }
            set { originalDateFrom = value; }
        }
     

        public DateTime OriginalTimeFrom
        {
            get { return originalTimeFrom; }
            set { originalTimeFrom = value; }
        }
 

        public DateTime OriginalDateTo
        {
            get { return originalDateTo; }
            set { originalDateTo = value; }
        }
  

        public DateTime OriginalTimeTo
        {
            get { return originalTimeTo; }
            set { originalTimeTo = value; }
        }

        public String OriginalTZ
        {
            get { return originalTZ; }
            set { originalTZ = value; }
        }
    }
}
