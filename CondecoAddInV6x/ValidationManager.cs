using System;
using System.Collections.Generic;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;
using CondecoAddinV2.Repository;
using CondecoAddinV2.App_Resources;

namespace CondecoAddinV2
{
    public sealed class ValidationManager
    {
        //private static BookingHelper bookingHelper = new BookingHelper();
        public static bool ValidateAppointment(Outlook.AppointmentItem appItem, bool isItemNotInSync)
        {
            if (appItem == null) return true;
            bool result = false;
            try
            {
                switch (appItem.RecurrenceState)
                {
                    case Outlook.OlRecurrenceState.olApptNotRecurring:
                        if (PerformNormalChecks(appItem,isItemNotInSync))
                        {
                            result = true;
                        }
                        break;
                    case Outlook.OlRecurrenceState.olApptMaster:
                        if (PerformNormalChecks(appItem,isItemNotInSync))
                        {
                            if (PerformRecurringChecks(appItem))
                            {
                                result = true;
                            }
                        }
                        break;
                    case Outlook.OlRecurrenceState.olApptOccurrence:
                    case Outlook.OlRecurrenceState.olApptException:
                        if (PerformNormalChecks(appItem,isItemNotInSync))
                        {
                            if (PerformRecurringChecks(appItem))
                            {
                                if (PeformOccurrenceChecks(appItem))
                                    result = true;
                            }
                        }
                        break;
                }
            }
            catch(System.Exception ex)
            {
                UtilityManager.LogMessage("Error occurred whilde validating the appointment" + ex.Message);
                UtilityManager.LogMessage("Error trace while validating :" + ex.StackTrace);
            }
            return result;
            

        }

        public static bool PerformNormalChecks(Outlook.AppointmentItem appItem, bool isItemNotInSync)
        {
            bool errorFound = false;
            StringBuilder message = new StringBuilder();

            //Added by Anand on 1-Feb-2013 for TP10850, TP10845
            if (appItem.Start.CompareTo(appItem.End) == 0)
            {
                message.AppendLine(CondecoResources.MeetingDate_Start_End_Same);
                UtilityManager.ShowErrorMessage(message.ToString());
                return false;
            }
            // Below appItem.End check to fix the CRRI  6550 ,TP-19968 by Paritosh 
            if (DateTime.Now.CompareTo(appItem.Start) >= 0 && DateTime.Now.CompareTo(appItem.End) > 0 && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                UtilityManager.LogMessage("performnormalchecks past booking");
                message.AppendLine(CondecoResources.Booking_Update_Past);

                UtilityManager.ShowErrorMessage(message.ToString());
                return false;
            }
           
            //End
             //Below is commented By Paritosh to solve TP 17638
            //if (!bookingHelper.CheckCrossDateBookingDueToTimeZoneDifference(null, appItem, false))
            //{
              
            //    message.AppendLine(CondecoResources.Location_DateDiff_As_Timezone_Not_Same);

            //    UtilityManager.ShowErrorMessage(message.ToString());
            //    return false;
            //}

            message.AppendLine(CondecoResources.Message_Base);
            message.AppendLine("");
            if (true)
            {
                if (!isItemNotInSync)
                {
                    //Changed by Anand on 23-Jan-13 for TP10604 old code appItem.Save();
                    try
                    {
                        //Below flag is introduce on 25 Feb 2015 , for v6 , to avoid send and update outlook window  when meeting drag on calender,  window opens and by clicking Book Booking button 
                       // if (!(AppointmentDataInfo.GetBookingId(appItem) > 0 && appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting))
                        if (!(AppointmentDataInfo.GetBookingId(appItem) > 0) && appItem.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                        {
                            appItem.Save();
                        }
                      
                        
                    }
                    catch (System.Runtime.InteropServices.COMException ex)
                    {
                        if (ex.ErrorCode == -2147352567)
                        {
                            errorFound = true;
                            message.AppendLine(CondecoResources.Message_starter + ex.Message);
                        }
                    }
                    //End Change
                }
                if (appItem.Subject == null || appItem.Subject == " " || appItem.Subject == string.Empty)
                {
                    appItem.Subject = CondecoAddinV2.App_Resources.CondecoResources.DefaultAppointmentSubject;

                    //errorFound = true;
                    //message.AppendLine(CondecoResources.Message_starter + CondecoResources.MeetingTitle_NotValid);
                }
                //Added by anand on 20-Feb-2013 for 10501, 10503 to show message if not having permission on booking
                if (!AppointmentDataInfo.GetHavingPermission(appItem))
                {
                   // errorFound = true;
                    //message.AppendLine(CondecoResources.Message_starter + CondecoResources.Check_Booking_Permission);
                    //Remove CondecoResources.Message_starter below to fixes issue IN TFS 3208 
                    UtilityManager.ShowErrorMessage(CondecoResources.Check_Booking_Permission);
                    return false;
                }
                //end
                
            }
            if (appItem.AllDayEvent)
            {
                errorFound = true;
                message.AppendLine(CondecoResources.Message_starter + CondecoResources.AllDayEvent_NotSupported);
            }
            // TO Do dont check in case Of outlook 2003
            object startTimeZone = null;
            object endTimeZone = null;  
            try
            {
                startTimeZone = appItem.GetType().InvokeMember("StartTimeZone", System.Reflection.BindingFlags.GetProperty, null, appItem, new object[] { });
                endTimeZone = appItem.GetType().InvokeMember("EndTimeZone", System.Reflection.BindingFlags.GetProperty, null, appItem, new object[] { });  
            }
            catch
            {
            }
            if (startTimeZone != null && endTimeZone != null)
            {
                if(!startTimeZone.ToString().ToLower().Equals(endTimeZone.ToString().ToLower()))
                {
                    errorFound = true;
                    message.AppendLine(CondecoResources.Message_starter+CondecoResources.Timezone_Not_Same);
                }

            }


            if(errorFound)
            {
                UtilityManager.ShowErrorMessage(message.ToString());
                return false;
            }
            return true;
            
        }

        public static bool PerformRecurringChecks(Outlook.AppointmentItem appItem)
        {
           IbookingHelper bookingHelper = new BookingHelper();
            bool errorFound = false;
            bool result = true;
            StringBuilder message = new StringBuilder();
            message.AppendLine(CondecoResources.Message_Base);
            message.AppendLine("");
            Outlook.RecurrencePattern recPattern = appItem.GetRecurrencePattern();
            Outlook.Exceptions recExps = recPattern.Exceptions;
            if (recExps.Count > 0 && appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !bookingHelper.IsCondecoBooking(appItem))
            {
                errorFound = true;
                message.AppendLine(CondecoResources.Message_starter + CondecoResources.Recurrence_Contains_Exceptions);

            }
            else
            {
                if (recPattern.NoEndDate)
                {
                    errorFound = true;
                    message.AppendLine(CondecoResources.Message_starter + CondecoResources.Recurrence_NoEndDate);

                }
                if (recPattern.Occurrences > 52)
                {
                    errorFound = true;
                    message.AppendLine(CondecoResources.Message_starter + CondecoResources.Recurrence_Maximum_Allowed);
                }
            }

            if (errorFound)
            {
                UtilityManager.ShowErrorMessage(message.ToString());
                result = false;
            }
            bookingHelper = null;
            UtilityManager.FreeCOMObject(recExps);
            UtilityManager.FreeCOMObject(recPattern);
            return result;
            
        }
        public static bool PeformOccurrenceChecks(Outlook.AppointmentItem appItem)
        {
            bool errorFound = false;
            StringBuilder message = new StringBuilder();
            message.AppendLine(CondecoResources.Message_Base);
            message.AppendLine("");
            IbookingHelper bookingHelper = new BookingHelper();
            //if (!bookingHelper.IsCondecoBookingV2(appItem))
              if (!bookingHelper.IsCondecoBooking(appItem))
            {
                errorFound = true;
                message.AppendLine(CondecoResources.Message_starter + CondecoResources.Recurrence_NoSeriesBooking_Exsist);

            }
          
            else if (bookingHelper.IsNonCondecoOccurrence(appItem))
            {
                errorFound = true;
                message.AppendLine(CondecoResources.Message_starter + CondecoResources.Recurrence_NoOccurrenceBooking_Exsist);
            }
            if (errorFound)
            {
                UtilityManager.ShowErrorMessage(message.ToString());
                return false;
            }
            bookingHelper = null;
            return true;
        }
    }
}
