﻿using Outlook = Microsoft.Office.Interop.Outlook;

namespace CondecoAddinV2
{
    public static class UserPropertiesExtension 
    {
        //Changed  by Paritosh :Replace User Property to named Property ,to disable custom property to print :CRI2-6019 
        //Start//
        public static bool SetNamedPropertyRecord(Outlook.AppointmentItem appitem, string propertyName, string value)
        {
            UtilityManager.LogMessage("UserPropertiesExtension :: SetNamedPropertyRecord Called  propertyName" + propertyName + " :value=" + value);
            bool retval = false;
            try
            {
                appitem.PropertyAccessor.SetProperty(propertyName, value);
                retval = true;
                return retval;
            }
            catch
            {
                UtilityManager.LogMessage("UserPropertiesExtension ::Catch SetNamedPropertyRecord  propertyName" + propertyName + " :value=" + value);
                retval = false;
                return retval;

            }

        }
        public static string GetNamedPropertyRecord(Outlook.AppointmentItem appitem, string propertyName)
        {
            UtilityManager.LogMessage("UserPropertiesExtension :: GetNamedPropertyRecord Called  propertyName" + propertyName);
            string NamedValue = "";
            try
            {
                object obj = appitem.PropertyAccessor.GetProperty(propertyName);
                if (obj != null)
                    NamedValue = obj.ToString();

                UtilityManager.LogMessage("UserPropertiesExtension :: GetNamedPropertyRecord  propertyName-" + propertyName + " :NamedValue=" + NamedValue);
                return NamedValue;
            }
            catch
            {
                UtilityManager.LogMessage("UserPropertiesExtension ::Catch GetNamedPropertyRecord  propertyName" + propertyName + " :NamedValue=" + NamedValue);
                bool bln= SetNamedPropertyRecord(appitem,propertyName, "");
                UtilityManager.LogMessage("UserPropertiesExtension ::Catch GetNamedPropertyRecord (SetNamedPropertyRecord)  propertyName" + propertyName + " :NamedValue=" + NamedValue + ":bln =" + bln);
                //if (bln)
                //{
                //    return "true";
                //}
                return "";
               
            }


        }
        public static void DeleteNamedPropertyRecord(Outlook.AppointmentItem appitem, string propertyName)
        {
            try
            {
                appitem.PropertyAccessor.DeleteProperty(propertyName);
            }
            catch
            {
                UtilityManager.LogMessage("UserPropertiesExtension ::Catch DeleteNamedPropertyRecord  propertyName" + propertyName + " ");
            }

        }

        //End//

        public static void RemoveProperty(Outlook.UserProperties properties, string propertyName)
        {
            Outlook.UserProperty appRecordProperty = null;
            appRecordProperty = properties.Find(propertyName, true);
            if (appRecordProperty != null)
                appRecordProperty.Delete();
            UtilityManager.FreeCOMObject(appRecordProperty);
        }

        //public static void SetBoolPropertyRecord(UserProperties properties, string propertyName, bool value)
        //{
        //    UserProperty appRecordProperty = null;
        //    appRecordProperty = properties.Find(propertyName, true);
        //    if (appRecordProperty == null)
        //        appRecordProperty = properties.Add(propertyName, OlUserPropertyType.olYesNo, false, false);

        //    //prevent same value write again
        //    if (appRecordProperty.Value != null)
        //    {
        //        bool currentvalue;
        //        if (bool.TryParse(appRecordProperty.Value.ToString(), out currentvalue))
        //        {
        //            if (currentvalue != value) appRecordProperty.Value = value;
        //        }
        //    }
        //    UtilityManager.FreeCOMObject(appRecordProperty);
        //}

        //public static bool SetPropertyRecord(UserProperties properties, string propertyName, string value)
        //{
        //    UserProperty appRecordProperty = null;
        //    bool existingProperty = false;
        //    bool skipPropertySetting = false;
        //    appRecordProperty = properties.Find(propertyName, true);
        //    if (appRecordProperty == null)
        //        appRecordProperty = properties.Add(propertyName, OlUserPropertyType.olText, false, false);
        //    else
        //        existingProperty = true;

        //    //prevent same value write again
        //    if (existingProperty && appRecordProperty.Value != null)
        //    {
        //        string currentContent = appRecordProperty.Value.ToString();
        //        if(string.Compare(currentContent.Trim(), value.Trim(), true) == 0)
        //            skipPropertySetting = true;
        //    }
            
        //    if(!skipPropertySetting) appRecordProperty.Value = value;

        //    UtilityManager.FreeCOMObject(appRecordProperty);

        //    return !skipPropertySetting;
        //}

        //public static string GetPropertyRecord(UserProperties properties, string propertyName)
        //{
        //    UserProperty appRecordProperty = null;
        //    string appRecord = string.Empty;
        //    appRecordProperty = properties.Find(propertyName, true);
        //    if (appRecordProperty != null && appRecordProperty.Value != null)
        //        appRecord = appRecordProperty.Value.ToString();
        //    UtilityManager.FreeCOMObject(appRecordProperty);
        //    return appRecord;
        //}

        //public static bool GetBoolPropertyRecord(UserProperties properties, string propertyName)
        //{
        //    UserProperty appRecordProperty = null;
        //    bool appRecord = false;
        //    appRecordProperty = properties.Find(propertyName, true);
        //    if (appRecordProperty != null && appRecordProperty.Value != null)
        //        bool.TryParse(appRecordProperty.Value.ToString(), out appRecord);
        //    UtilityManager.FreeCOMObject(appRecordProperty);        
        //    return appRecord;
        //}

        //public static void RemoveProperty(UserProperties properties, string propertyName)
        //{
        //    UserProperty appRecordProperty = null;
        //    appRecordProperty = properties.Find(propertyName, true);
        //    if (appRecordProperty != null)
        //        appRecordProperty.Delete();
        //    UtilityManager.FreeCOMObject(appRecordProperty);
        //}
    }
}
