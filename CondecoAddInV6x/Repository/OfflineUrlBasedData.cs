﻿using System;
using System.Collections.Generic;

namespace CondecoAddinV2.Repository
{
    //Static class to maintain url based offline data.
    public static class OfflineUrlBasedData
    {
        #region Private method and properties

        class OfflineData
        {
            public DateTime RecordTime { get; set; }
            public string ReturnData { get; set; }
            public string Url { get; set; }
        }

        static List<OfflineData> OfflineLocationCache = new List<OfflineData>();

        private static bool OfflineAllowedForCall(string url)
        {
            if (url.IndexOf("func=convertDateToTZ", StringComparison.OrdinalIgnoreCase) >= 1) return true;
            if (url.IndexOf("func=convertDateToLocalTZ", StringComparison.OrdinalIgnoreCase) >= 1) return true;
            return false;
        }

        #endregion

        public static string GetOfflineData(string url)
        {
            if (!OfflineAllowedForCall(url)) return null;

            OfflineLocationCache.RemoveAll(x => (DateTime.Now - x.RecordTime).TotalSeconds > 10);
            var existingCache = OfflineLocationCache.Find(x => string.Compare(x.Url, url, true) == 0);
            if (existingCache != null)
            {
                existingCache.RecordTime = DateTime.Now;
                return existingCache.ReturnData;
            }
            return null;
        }

        public static void SetOfflineData(string url, string data)
        {
            if (!OfflineAllowedForCall(url)) return;
            OfflineLocationCache.Add(new OfflineData { ReturnData = data, Url = url, RecordTime = DateTime.Now });
        }

        public static void ClearOfflineData()
        {
            OfflineLocationCache.Clear();
            OfflineLocationCache = null;
        }

    }
}

