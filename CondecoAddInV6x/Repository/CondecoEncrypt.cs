﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
namespace CondecoAddinV2
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// Provider the EcryptionServices using ecryption policies set down for Condeco
    /// </summary>
    public static class Encryption
    {
        private const string AESKEY = "*demo key for aesCryptoprovider*";
        public static string EncryptText(string plainText, string salt)
        {
            AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
            aesProvider.Key = System.Text.Encoding.ASCII.GetBytes(AESKEY);
            aesProvider.IV = System.Text.Encoding.ASCII.GetBytes(salt);

            byte[] encrypted;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (StreamWriter streamWriter = new StreamWriter(new CryptoStream(memoryStream, aesProvider.CreateEncryptor(), CryptoStreamMode.Write)))
                {
                    streamWriter.Write(plainText);
                }
                encrypted = memoryStream.ToArray();
            }

            return Convert.ToBase64String(encrypted);
        }
        public static string DecryptText(string encryptedText, string salt)
        {
            AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
            aesProvider.Key = System.Text.Encoding.ASCII.GetBytes(AESKEY);
            aesProvider.IV = System.Text.Encoding.ASCII.GetBytes(salt);

            string plainText = string.Empty;

            using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(encryptedText)))
            {
                using (StreamReader streamReader = new StreamReader(new CryptoStream(memoryStream, aesProvider.CreateDecryptor(), CryptoStreamMode.Read)))
                {
                    plainText = streamReader.ReadToEnd();
                }

            }
            return plainText;
        }
    }
}
