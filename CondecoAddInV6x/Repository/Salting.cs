﻿namespace CondecoAddinV2
{
    using System;
    using System.Linq;

    /// <summary>
    /// Exposes methods to access the Azure Key Vault services.
    /// </summary>
    public static class Salting
    {
        #region Constants
        const string SALT = "CONDECO DEMO IV ";
        #endregion

        /// <summary>
        /// Generates a random salt and stores using the identifier
        /// </summary>
        /// <returns></returns>
        public static string GetAESSalt()
        {
            return SALT;
        }
    }
}
