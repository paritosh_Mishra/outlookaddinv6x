﻿using System;
using System.Collections.Generic;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;

/*
Only Condeco appointment will be consider. Appointment time will be added when event register and keep the original dates 
 */

namespace CondecoAddinV2.Repository
{
    public static class AppointmentDataInfo
    {
        class AppointmentInfo
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public bool SkipWriteCheck { get; set; }
            public string EntryId { get; set; }
            public bool HavingPermission { get; set; }
            public int BookingId { get; set; }
            public bool Prop_StartDateChanged { get; set; }
            public bool Prop_EndDateChanged { get; set; }
            public bool Prop_SubjectChanged { get; set; }
            public bool Prop_IsAppItemSkipSync { get; set; }
            public bool Prop_IsAppItemSaveAndCloseClick { get; set; }
            public bool Prop_IsAppItemCondecoBookingOpen { get; set; }
            public DateTime? MeetingMoveOrgStartDate { get; set; }
            public DateTime? MeetingMoveOrgEndDate { get; set; }
            public bool Prop_IsOnlySeriesLocationUpdated { get; set; }
            public bool Prop_IsAppointmentNonMeeting { get; set; }
            public bool Prop_SetIsItemNonMeeting { get; set; }
            public bool Prop_IsAppRecuDeleteToolbarClick { get; set; }

            public bool Prop_IsMeetingOpenThenClose { get; set; }

        }

        class AppointmentInfoAfterSave
        {
            public string EntryId { get; set; }
            public bool SubjectChangedAfterSave { get; set; }
            public bool WebBookedclick { get; set; }
            public bool OlkAttandeeOrTitleChanged { get; set; }
        }

        private static List<AppointmentInfo> AppointmentDataCollection = new List<AppointmentInfo>();
        //private static List<AppointmentInfoAfterSave> AppointmentDataAfterSaveCollection = new List<AppointmentInfoAfterSave>();

        //////public static void AddAppDataAfterSaveInCollection(Outlook.AppointmentItem appItem)
        //////{
        //////    if (!string.IsNullOrEmpty(appItem.EntryID))
        //////    {
        //////        string entryId = appItem.EntryID;
        //////        AppointmentDataAfterSaveCollection.RemoveAll(x => x.EntryId == entryId);
        //////        AppointmentDataAfterSaveCollection.Add(new AppointmentInfoAfterSave
        //////        {
        //////            EntryId = entryId,
        //////            SubjectChangedAfterSave = false,
        //////            WebBookedclick=false,
        //////            OlkAttandeeOrTitleChanged =false
        //////        });
        //////    }
        //////}

        public static void AddAppointmentInCollection(Outlook.AppointmentItem appItem, bool havingPermission, int bookingId)
        {
            AppointmentInfo appinfo = null;
            try
            {

                if (!string.IsNullOrEmpty(appItem.EntryID))
                {
                    string entryId = appItem.EntryID;
                    bool isSyncSkip = false;
                    bool isSaveAndCloseClick = false;
                    bool isCondecoBookingOpen = false;
                    DateTime? privateMeetingStartdate = null;
                    DateTime? privateMeetingEndDate = null;
                    appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
                    if (appinfo != null)
                    {
                        isSyncSkip = appinfo.Prop_IsAppItemSkipSync;
                        isSaveAndCloseClick = appinfo.Prop_IsAppItemSaveAndCloseClick;
                        isCondecoBookingOpen = appinfo.Prop_IsAppItemCondecoBookingOpen;
                        privateMeetingStartdate = appinfo.MeetingMoveOrgStartDate;
                        privateMeetingEndDate = appinfo.MeetingMoveOrgEndDate;
                    }
                    AppointmentDataCollection.RemoveAll(x => x.EntryId == entryId);
                    AppointmentDataCollection.Add(new AppointmentInfo
                    {
                        EntryId = entryId,
                        StartDate = appItem.Start,
                        EndDate = appItem.End,
                        HavingPermission = havingPermission,
                        BookingId = bookingId,
                        Prop_IsAppItemSkipSync = isSyncSkip,
                        Prop_IsAppItemSaveAndCloseClick = isSaveAndCloseClick,
                        Prop_IsAppItemCondecoBookingOpen = isCondecoBookingOpen,
                        MeetingMoveOrgStartDate = privateMeetingStartdate,
                        MeetingMoveOrgEndDate = privateMeetingEndDate
                    });
                }
            }
            catch
            {
            }
            finally
            {
                if (appinfo != null)
                {
                    appinfo = null;




                }













            }
        }

        public static void AddAppointmentInCollection(Outlook.AppointmentItem appItem, int bookingId)
        {
            AppointmentInfo appinfo = null;
            try
            {
                if (!string.IsNullOrEmpty(appItem.EntryID))
                {
                    string entryId = appItem.EntryID;
                    DateTime? privateMeetingStartdate = null;
                    appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
                    if (appinfo != null)
                    {
                        privateMeetingStartdate = appinfo.MeetingMoveOrgStartDate;
                    }

                    AppointmentDataCollection.RemoveAll(x => x.EntryId == entryId);
                    AppointmentDataCollection.Add(new AppointmentInfo
                    {
                        EntryId = entryId,
                        StartDate = appItem.Start,
                        EndDate = appItem.End,
                        BookingId = bookingId,
                        MeetingMoveOrgStartDate = privateMeetingStartdate

                    });
                }
            }
            finally
            {
                if (appinfo != null)
                {
                    appinfo = null;
                }











            }

        }

        public static bool GetHavingPermission(Outlook.AppointmentItem appItem)
        {
            var originalData = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalData != null)
                return originalData.HavingPermission;
            return true;
        }


        public static int GetBookingId(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalData = null;
            try
            {
                originalData = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
                if (originalData != null)
                    return originalData.BookingId;
                return 0;
            }
            finally
            {
                if (originalData != null)
                {
                    originalData = null;
                }

            }
        }

        public static void SetBookingId(Outlook.AppointmentItem appItem, int value)
        {
            AppointmentInfo appInfo = null;
            try
            {
                appInfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
                if (appInfo != null)
                    appInfo.BookingId = value;
            }
            finally
            {
                if (appInfo != null)
                {
                    appInfo = null;
                }
            }
        }

        public static DateTime? GetOriginalStartDate(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.StartDate;
            return null;
        }

        public static DateTime? GetOriginalEndDate(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.EndDate;
            return null;
        }

        public static void SetSkipWriteCheck(Outlook.AppointmentItem appItem, bool value)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.SkipWriteCheck = value;
        }

        public static void SetHavingPermission(Outlook.AppointmentItem appItem, bool value)
        {
            var originalData = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalData != null)
                originalData.HavingPermission = value;

        }

        public static bool GetSkipWriteCheck(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.SkipWriteCheck;
            return false;
        }

        public static void RemoveAppointmentFromCollection(Outlook.AppointmentItem appItem)
        {
            if (!string.IsNullOrEmpty(appItem.EntryID))
            {
                string entryId = appItem.EntryID;
                AppointmentDataCollection.RemoveAll(x => x.EntryId == entryId);
            }
        }

        public static void SetStartDateChanged(Outlook.AppointmentItem appItem, bool value)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_StartDateChanged = value;
        }
        public static void SetEndDateChanged(Outlook.AppointmentItem appItem, bool value)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_EndDateChanged = value;
        }
        public static void SetSubjectChanged(Outlook.AppointmentItem appItem, bool value)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_SubjectChanged = value;
        }
        public static bool GetStartDateChanged(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_StartDateChanged;
            return false;
        }
        public static bool GetEndDateChanged(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_EndDateChanged;
            return false;
        }
        public static bool GetSubjectChanged(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_SubjectChanged;
            return false;
        }
        //Added by Ravi Goyal to Capture if only Series Location is updated, incase if true, the edited appointment email will be sent by outlook only
        public static void SetIsOnlySeriesLocationUpdated(Outlook.AppointmentItem appItem, bool value)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_IsOnlySeriesLocationUpdated = value;
        }
        public static bool GetIsOnlySeriesLocationUpdated(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_IsOnlySeriesLocationUpdated;
            return false;
        }
        public static void SetIsItemNonMeeting(Outlook.AppointmentItem appItem, bool value)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_SetIsItemNonMeeting = value;
        }
        public static bool GetIsItemNonMeeting(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_SetIsItemNonMeeting;
            return false;
        }
        //End by Ravi Goyal
        public static void ResetProperties(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
            {
                originalDate.Prop_StartDateChanged = false;
                originalDate.Prop_EndDateChanged = false;
                originalDate.Prop_SubjectChanged = false;
                originalDate.Prop_IsAppItemSkipSync = false;
                originalDate.Prop_IsAppointmentNonMeeting = false;
            }
        }
        public static bool GetAppItemSkipSync(Outlook.AppointmentItem appItem)
        {
            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                return appinfo.Prop_IsAppItemSkipSync;
            return false;
        }

        public static void SetAppItemSkipSync(Outlook.AppointmentItem appItem, bool value)
        {
            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                appinfo.Prop_IsAppItemSkipSync = value;
        }

        public static bool GetAppItemIsAppItemSaveAndCloseClick(Outlook.AppointmentItem appItem)
        {
            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                return appinfo.Prop_IsAppItemSaveAndCloseClick;
            return false;
        }

        public static void SetAppItemIsAppItemSaveAndCloseClick(Outlook.AppointmentItem appItem, bool value)
        {
            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                appinfo.Prop_IsAppItemSaveAndCloseClick = value;
        }

        public static bool GetAppItemCondecoBookingOpen(Outlook.AppointmentItem appItem)
        {
            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                return appinfo.Prop_IsAppItemCondecoBookingOpen;
            return false;
        }

        public static void SetAppItemCondecoBookingOpen(Outlook.AppointmentItem appItem, bool value)
        {
            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                appinfo.Prop_IsAppItemCondecoBookingOpen = value;
        }



        public static DateTime? GetOriginalMeetingMoveStartDate(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.MeetingMoveOrgStartDate;
            return null;
        }

        public static DateTime? GetOriginalMeetingMoveEndDate(Outlook.AppointmentItem appItem)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.MeetingMoveOrgEndDate;
            return null;
        }

        public static void SetOriginalMeetingMoveStartDate(Outlook.AppointmentItem appItem, DateTime? value)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.MeetingMoveOrgStartDate = value;
        }
        public static void SetOriginalMeetingMoveEndDate(Outlook.AppointmentItem appItem, DateTime? value)
        {
            var originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.MeetingMoveOrgEndDate = value;
        }
        //Added by Paritosh for reducing private sets memory 
        public static void ClearAppointmentInCollection()
        {
            AppointmentDataCollection.Clear();
            AppointmentDataCollection = null;
        }

        public static void SetAppRecuDeleteToolbarClick(Outlook.AppointmentItem appItem, bool value)
        {

            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                appinfo.Prop_IsAppRecuDeleteToolbarClick = value;
        }


        public static bool GetAppRecuDeleteToolbarClick(Outlook.AppointmentItem appItem)
        {

            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                return appinfo.Prop_IsAppRecuDeleteToolbarClick;
            return false;
        }

        ////////////////public static bool GetWebBookedclick(Outlook.AppointmentItem appItem)
        ////////////////{
        ////////////////    AppointmentInfoAfterSave appinfo = AppointmentDataAfterSaveCollection.Find(x => x.EntryId == appItem.EntryID);
        ////////////////    if (appinfo != null)
        ////////////////        return appinfo.WebBookedclick;
        ////////////////    return false;
        ////////////////}

        ////////////////public static void SetWebBookedclick(Outlook.AppointmentItem appItem, bool value)
        ////////////////{
        ////////////////    AppointmentInfoAfterSave appinfo = AppointmentDataAfterSaveCollection.Find(x => x.EntryId == appItem.EntryID);
        ////////////////    if (appinfo != null)
        ////////////////        appinfo.WebBookedclick = value;
        ////////////////}


        ////////////////public static bool GetOlkAttandeeOrTitleChanged(Outlook.AppointmentItem appItem)
        ////////////////{
        ////////////////    AppointmentInfoAfterSave appinfo = AppointmentDataAfterSaveCollection.Find(x => x.EntryId == appItem.EntryID);
        ////////////////    if (appinfo != null)
        ////////////////        return appinfo.OlkAttandeeOrTitleChanged;
        ////////////////    return false;
        ////////////////}

        ////////////////public static void SetOlkAttandeeOrTitleChanged(Outlook.AppointmentItem appItem, bool value)
        ////////////////{
        ////////////////    AppointmentInfoAfterSave appinfo = AppointmentDataAfterSaveCollection.Find(x => x.EntryId == appItem.EntryID);
        ////////////////    if (appinfo != null)
        ////////////////        appinfo.OlkAttandeeOrTitleChanged = value;
        ////////////////}

        ////////////////public static bool GetAfterSaveSubjectchanges(Outlook.AppointmentItem appItem)
        ////////////////{
        ////////////////    AppointmentInfoAfterSave appinfo = AppointmentDataAfterSaveCollection.Find(x => x.EntryId == appItem.EntryID);
        ////////////////    if (appinfo != null)
        ////////////////        return appinfo.SubjectChangedAfterSave;
        ////////////////    return false;
        ////////////////}

        ////////////////public static void SetAfterSaveSubjectchanges(Outlook.AppointmentItem appItem, bool value)
        ////////////////{
        ////////////////    AppointmentInfoAfterSave appinfo = AppointmentDataAfterSaveCollection.Find(x => x.EntryId == appItem.EntryID);
        ////////////////    if (appinfo != null)
        ////////////////        appinfo.SubjectChangedAfterSave = value;
        ////////////////}


    }
}